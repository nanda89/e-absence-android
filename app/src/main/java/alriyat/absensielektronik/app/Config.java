package alriyat.absensielektronik.app;

/**
 * Created by Riyadh on 03/03/2017.
 */
public class Config {

    public static final String BASE_URL = "http://192.168.8.100/Android-Absensi/";
    //http://demo.virasolution.com/web_absence/

    public static final String DAFTAR_SISWA = BASE_URL+"daftar_siswa.php";
    public static final String LOGIN = "/web_absence/assets/file/login.php";
    public static final String DAFTAR_ABSENSI = "/web_absence/assets/file/daftar_siswa_absensi.php";
    public static final String SIMPAN_ABSEN = "/web_absence/assets/file/simpan_absen.php";
    public static final String SIMPAN_ABSEN_PULANG = "/web_absence/assets/file/simpan_absensi2.php";
    public static final String VALID_ABSEN_MASUK = "/web_absence/assets/file/validasi_masuk.php";
    public static final String VALID_ABSEN_PULANG = "/web_absence/assets/file/validasi_pulang.php";
    public static final String PROFILE_SISWA = "/web_absence/api/siswa_api/profil";
    public static final String EDIT_SISWA = "/web_absence/api/siswa_api/save";
    public static final String PROFILE_GURU = "/web_absence/api/guru_api/profil";
    public static final String PROFILE_NON_GURU = "/web_absence/api/non_guru_api/profil";
    public static final String EDIT_GURU = "/web_absence/api/guru_api/save";
}
