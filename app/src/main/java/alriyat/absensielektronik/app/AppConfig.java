package alriyat.absensielektronik.app;

public class AppConfig {
    public static final String PARAM_ID_SISWA = "id_siswa";
    public static final String PARAM_ID_JENJANG = "id_jenjang";
    public static final String PARAM_NAMA_LENGKAP = "nama_lengkap";
    public static final String PARAM_NISN = "nisn";
    public static final String PARAM_JENIS_KELAMIN = "jenis_kelamin";
    public static final String PARAM_TEMPAT_LAHIR = "tempat_lahir";
    public static final String PARAM_TGL_LAHIR = "tgl_lahir";
    public static final String PARAM_AGAMA = "agama";
    public static final String PARAM_ALAMAT = "alamat";
    public static final String PARAM_NO_HP = "no_hp";
    public static final String PARAM_EMAIL = "email";
    public static final String PARAM_ASAL_SEKOLAH = "asal_sekolah";
    public static final String PARAM_KET_ASAL = "ket_asal";
    public static final String PARAM_NAMA_AYAH = "nama_ayah";
    public static final String PARAM_NAMA_IBU = "nama_ibu";
    public static final String PARAM_GAMBAR = "gambar";
    public static final String PARAM_TGL_DAFTAR = "tgl_daftar";
    public static final String PARAM_NO_INDUK_KELAS = "no_induk_kelas";
    public static final String PARAM_TELP_AYAH = "telp_ayah";
    public static final String PARAM_TELP_IBU = "telp_ibu";
    public static final String PARAM_URL_PHOTO = "url_photo";
    public static final String PARAM_ID_GURU = "id_guru";
    public static final String PARAM_NIK = "nik";
    public static final String PARAM_NAMA = "nama";
    public static final String PARAM_NO_KTP = "no_ktp";
    public static final String PARAM_ALIAS = "alias";
    public static final String PARAM_IJAZAH = "ijazah";
    public static final String PARAM_PEND_TERAKHIR = "pend_terakhir";
    public static final String PARAM_UNIT_KERJA = "unit_kerja";
    public static final String PARAM_TMT = "tmt";
}
