package alriyat.absensielektronik.helper;

/**
 * Created by folcotandiono on 8/14/2018.
 */

import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.widget.Toast;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.activity.DaftarPelajaranActivity;
import es.dmoral.toasty.Toasty;

import static android.content.Context.POWER_SERVICE;

public class AlarmReceiver extends BroadcastReceiver{

    @Override
    public void onReceive(Context context, Intent intent) {
        @SuppressLint("InvalidWakeLockTag") PowerManager.WakeLock screenLock = ((PowerManager)context.getSystemService(POWER_SERVICE)).newWakeLock(
                PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "TAG");
        screenLock.acquire();

        //later
        screenLock.release();

        Toasty.Config.reset();
        Toasty.Config.getInstance()
                .setTextSize(50) // optional
                .apply(); // required
        Toasty.error(context, "5 menit lagi jam akan berakhir", Toast.LENGTH_LONG, false).show();
        final MediaPlayer mp = MediaPlayer.create(context, R.raw.alarm);
        mp.start();

        // Create an explicit intent for an Activity in your app
        Intent intent1 = new Intent(context, DaftarPelajaranActivity.class);
        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent1, 0);

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context, "1")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle("Absence")
                .setContentText("5 menit lagi jam akan berakhir")
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(1, mBuilder.build());
    }
}
