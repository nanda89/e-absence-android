package alriyat.absensielektronik.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Riyadh on 06/04/2017.
 */
public class SessionConfig {
    public SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context ctx;

    public SessionConfig(Context ctx) {
        this.ctx = ctx;
        preferences = ctx.getSharedPreferences("absensiIpConfig",Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void setLogin(boolean status,String ipconfig){
        editor.putBoolean("statusLogin", status);
        editor.putString("ipconfig",ipconfig);
        editor.commit();
    }

    public boolean setLogout(){
        return preferences.getBoolean("statusLogin",false);
    }
}
