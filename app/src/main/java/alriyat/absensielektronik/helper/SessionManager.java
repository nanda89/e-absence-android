package alriyat.absensielektronik.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Riyadh on 04/04/2017.
 */
public class SessionManager {
    public SharedPreferences preferences;
    SharedPreferences.Editor editor;
    Context ctx;

    public SessionManager(Context ctx) {
        this.ctx = ctx;
        preferences = ctx.getSharedPreferences("absensiElek",Context.MODE_PRIVATE);
        editor = preferences.edit();
    }

    public void isConnected(boolean connect){
        editor.putBoolean("isConnect" , connect);
        editor.commit();
    }

    public void setLogin(boolean status,String ipconfig,String id,String username, String password, String statusGuruBK,String isGuru,String idUser,boolean isKepsek){

        editor.putBoolean("statusLogin", status);
        editor.putString("ipconfig",ipconfig);
        editor.putString("username",username);
        editor.putString("password",password);
        editor.putString("id_user",idUser);
        editor.putBoolean("isKepsek",isKepsek);
        // Y/N
        editor.putString("isGuru", isGuru);
        if("Y".equals(isGuru)){
            editor.putString("idguru",id);
            editor.putString("statusGuruBK", statusGuruBK);
        }else if("N".equals(isGuru)){
            editor.putString("idnonguru",id);
        }else{
            // logout
            editor.putString("idguru",id);
            editor.putString("statusGuruBK", statusGuruBK);
            editor.putString("idnonguru",id);
        }
        editor.commit();
    }

    public boolean setLogout(){
        return preferences.getBoolean("statusLogin",false);
    }
}
