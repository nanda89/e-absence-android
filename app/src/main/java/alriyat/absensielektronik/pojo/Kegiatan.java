package alriyat.absensielektronik.pojo;

import com.google.gson.annotations.SerializedName;

public class Kegiatan {
    @SerializedName("id_non_guru")
    private int idNonGuru;

    @SerializedName("tanggal")
    private String tanggal;

    @SerializedName("jam")
    private String jam;

    @SerializedName("type")
    private String type;

    @SerializedName("uraian")
    private String uraian;

    @SerializedName("img")
    private String img;

    @SerializedName("nama")
    private String nama;

    @SerializedName("jabatan")
    private String jabatan;

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }

    public int getIdNonGuru() {
        return idNonGuru;
    }

    public void setIdNonGuru(int idNonGuru) {
        this.idNonGuru = idNonGuru;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUraian() {
        return uraian;
    }

    public void setUraian(String uraian) {
        this.uraian = uraian;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }
}
