package alriyat.absensielektronik.pojo;

import com.google.gson.annotations.SerializedName;

public class AbsenHadirPulang {

    @SerializedName("id_absen_kehadiran_guru")
    private String id_absen_kehadiran_guru;

    @SerializedName("id_guru")
    private String id_guru;

    @SerializedName("tgl")
    private String tgl;

    @SerializedName("jam")
    private String jam;

    @SerializedName("keterangan")
    private String keterangan;

    public String getId_absen_kehadiran_guru() {
        return id_absen_kehadiran_guru;
    }

    public void setId_absen_kehadiran_guru(String id_absen_kehadiran_guru) {
        this.id_absen_kehadiran_guru = id_absen_kehadiran_guru;
    }

    public String getId_guru() {
        return id_guru;
    }

    public void setId_guru(String id_guru) {
        this.id_guru = id_guru;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
