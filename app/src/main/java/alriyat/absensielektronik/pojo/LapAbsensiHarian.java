package alriyat.absensielektronik.pojo;

import com.google.gson.annotations.SerializedName;

public class LapAbsensiHarian {
    @SerializedName("mapel")
    private String mapel;

    @SerializedName("hadir")
    private String hadir;

    @SerializedName("sakit")
    private String sakit;

    @SerializedName("izin")
    private String izin;

    @SerializedName("nihil")
    private String nihil;

    public String getMapel() {
        return mapel;
    }

    public void setMapel(String mapel) {
        this.mapel = mapel;
    }

    public String getHadir() {
        return hadir;
    }

    public void setHadir(String hadir) {
        this.hadir = hadir;
    }

    public String getSakit() {
        return sakit;
    }

    public void setSakit(String sakit) {
        this.sakit = sakit;
    }

    public String getIzin() {
        return izin;
    }

    public void setIzin(String izin) {
        this.izin = izin;
    }

    public String getNihil() {
        return nihil;
    }

    public void setNihil(String nihil) {
        this.nihil = nihil;
    }
}
