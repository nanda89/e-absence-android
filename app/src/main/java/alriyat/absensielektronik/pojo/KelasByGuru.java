package alriyat.absensielektronik.pojo;

import com.google.gson.annotations.SerializedName;

public class KelasByGuru {

    @SerializedName("id_kelas")
    private String id_kelas;

    @SerializedName("kelas")
    private String kelas;

    @SerializedName("jam_awal")
    private String jamAwal;

    @SerializedName("jam_akhir")
    private String jamAkhir;

    @SerializedName("hari")
    private String hari;

    @SerializedName("nama")
    private String nama;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getId_kelas() {
        return id_kelas;
    }

    public void setId_kelas(String id_kelas) {
        this.id_kelas = id_kelas;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getJamAwal() {
        return jamAwal;
    }

    public void setJamAwal(String jamAwal) {
        this.jamAwal = jamAwal;
    }

    public String getJamAkhir() {
        return jamAkhir;
    }

    public void setJamAkhir(String jamAkhir) {
        this.jamAkhir = jamAkhir;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }
}
