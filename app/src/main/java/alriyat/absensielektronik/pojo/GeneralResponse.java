package alriyat.absensielektronik.pojo;

import com.google.gson.annotations.SerializedName;

public class GeneralResponse {

    @SerializedName("code")
    private int code;

    @SerializedName("success")
    boolean success;

    @SerializedName("message")
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
