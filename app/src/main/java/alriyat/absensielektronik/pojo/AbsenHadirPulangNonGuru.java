package alriyat.absensielektronik.pojo;

import com.google.gson.annotations.SerializedName;

public class AbsenHadirPulangNonGuru {

    @SerializedName("id_absen_non_guru")
    private String id_absen_non_guru;

    @SerializedName("id_non_guru")
    private String id_non_guru;

    @SerializedName("tgl")
    private String tgl;

    @SerializedName("jam")
    private String jam;

    @SerializedName("keterangan")
    private String keterangan;

    @SerializedName("jenis")
    private String jenis;

    public String getId_absen_non_guru() {
        return id_absen_non_guru;
    }

    public void setId_absen_non_guru(String id_absen_non_guru) {
        this.id_absen_non_guru = id_absen_non_guru;
    }

    public String getId_non_guru() {
        return id_non_guru;
    }

    public void setId_non_guru(String id_non_guru) {
        this.id_non_guru = id_non_guru;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }
}
