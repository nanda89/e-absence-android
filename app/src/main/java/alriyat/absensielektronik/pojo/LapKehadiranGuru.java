package alriyat.absensielektronik.pojo;

import com.google.gson.annotations.SerializedName;

public class LapKehadiranGuru {

    @SerializedName("id_guru")
    private int id_guru;
    @SerializedName("nama")
    private String nama;
    @SerializedName("absensi")
    private Absensi absensi;

    public class Absensi{
        @SerializedName("datang")
        private int datanag;
        @SerializedName("nihil_dtg")
        private int nihil_dtg;
        @SerializedName("pulang")
        private int pulang;
        @SerializedName("nihil_plg")
        private int nihil_plg;
        @SerializedName("sakit")
        private int sakit;
        @SerializedName("izin")
        private int izin;

        public int getDatanag() {
            return datanag;
        }

        public void setDatanag(int datanag) {
            this.datanag = datanag;
        }

        public int getNihil_dtg() {
            return nihil_dtg;
        }

        public void setNihil_dtg(int nihil_dtg) {
            this.nihil_dtg = nihil_dtg;
        }

        public int getPulang() {
            return pulang;
        }

        public void setPulang(int pulang) {
            this.pulang = pulang;
        }

        public int getNihil_plg() {
            return nihil_plg;
        }

        public void setNihil_plg(int nihil_plg) {
            this.nihil_plg = nihil_plg;
        }

        public int getSakit() {
            return sakit;
        }

        public void setSakit(int sakit) {
            this.sakit = sakit;
        }

        public int getIzin() {
            return izin;
        }

        public void setIzin(int izin) {
            this.izin = izin;
        }
    }

    public int getId_guru() {
        return id_guru;
    }

    public void setId_guru(int id_guru) {
        this.id_guru = id_guru;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Absensi getAbsensi() {
        return absensi;
    }

    public void setAbsensi(Absensi absensi) {
        this.absensi = absensi;
    }
}
