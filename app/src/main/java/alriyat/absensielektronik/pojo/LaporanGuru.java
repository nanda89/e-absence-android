package alriyat.absensielektronik.pojo;

import com.google.gson.annotations.SerializedName;

public class LaporanGuru {

    @SerializedName("id_guru")
    private String id_guru;

    @SerializedName("nama")
    private String nama;

    @SerializedName("tgl")
    private String tgl;

    @SerializedName("jam_masuk_kerja")
    private String jam_masuk_kerja;

    @SerializedName("jam_pulang_kerja")
    private String jam_pulang_kerja;

    @SerializedName("validasi_jam_masuk")
    private String validasi_jam_masuk;

    @SerializedName("validasi_jam_pulang")
    private String validasi_jam_pulang;

    @SerializedName("keterangan_jam_masuk")
    private String keterangan_jam_masuk;

    @SerializedName("keterangan_jam_pulang")
    private String keterangan_jam_pulang;

    public String getId_guru() {
        return id_guru;
    }

    public void setId_guru(String id_guru) {
        this.id_guru = id_guru;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getJam_masuk_kerja() {
        return jam_masuk_kerja;
    }

    public void setJam_masuk_kerja(String jam_masuk_kerja) {
        this.jam_masuk_kerja = jam_masuk_kerja;
    }

    public String getJam_pulang_kerja() {
        return jam_pulang_kerja;
    }

    public void setJam_pulang_kerja(String jam_pulang_kerja) {
        this.jam_pulang_kerja = jam_pulang_kerja;
    }

    public String getValidasi_jam_masuk() {
        return validasi_jam_masuk;
    }

    public void setValidasi_jam_masuk(String validasi_jam_masuk) {
        this.validasi_jam_masuk = validasi_jam_masuk;
    }

    public String getValidasi_jam_pulang() {
        return validasi_jam_pulang;
    }

    public void setValidasi_jam_pulang(String validasi_jam_pulang) {
        this.validasi_jam_pulang = validasi_jam_pulang;
    }

    public String getKeterangan_jam_masuk() {
        return keterangan_jam_masuk;
    }

    public void setKeterangan_jam_masuk(String keterangan_jam_masuk) {
        this.keterangan_jam_masuk = keterangan_jam_masuk;
    }

    public String getKeterangan_jam_pulang() {
        return keterangan_jam_pulang;
    }

    public void setKeterangan_jam_pulang(String keterangan_jam_pulang) {
        this.keterangan_jam_pulang = keterangan_jam_pulang;
    }
}
