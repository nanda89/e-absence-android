package alriyat.absensielektronik.pojo;

import com.google.gson.annotations.SerializedName;

public class LaporanNonGuruHarian {
    @SerializedName("id_non_guru")
    private int idNonGuru;

    @SerializedName("nama")
    private String nama;

    @SerializedName("tgl")
    private String tanggal;

    @SerializedName("jam_masuk_kerja")
    private String jam_masuk;

    @SerializedName("jam_pulang_kerja")
    private String jam_pulang;

    @SerializedName("validasi_jam_masuk")
    private String validasi_jam_masuk;

    @SerializedName("validasi_jam_pulang")
    private String validasi_jam_pulang;

    @SerializedName("keterangan_jam_masuk")
    private String ket_jam_masuk;

    @SerializedName("keterangan_jam_pulang")
    private String ket_jam_pulang;

    public int getIdNonGuru() {
        return idNonGuru;
    }

    public void setIdNonGuru(int idNonGuru) {
        this.idNonGuru = idNonGuru;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getJam_masuk() {
        return jam_masuk;
    }

    public void setJam_masuk(String jam_masuk) {
        this.jam_masuk = jam_masuk;
    }

    public String getJam_pulang() {
        return jam_pulang;
    }

    public void setJam_pulang(String jam_pulang) {
        this.jam_pulang = jam_pulang;
    }

    public String getValidasi_jam_masuk() {
        return validasi_jam_masuk;
    }

    public void setValidasi_jam_masuk(String validasi_jam_masuk) {
        this.validasi_jam_masuk = validasi_jam_masuk;
    }

    public String getValidasi_jam_pulang() {
        return validasi_jam_pulang;
    }

    public void setValidasi_jam_pulang(String validasi_jam_pulang) {
        this.validasi_jam_pulang = validasi_jam_pulang;
    }

    public String getKet_jam_masuk() {
        return ket_jam_masuk;
    }

    public void setKet_jam_masuk(String ket_jam_masuk) {
        this.ket_jam_masuk = ket_jam_masuk;
    }

    public String getKet_jam_pulang() {
        return ket_jam_pulang;
    }

    public void setKet_jam_pulang(String ket_jam_pulang) {
        this.ket_jam_pulang = ket_jam_pulang;
    }
}
