package alriyat.absensielektronik.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DaftarHadir {

    @SerializedName("total_siswa")
    private int totSiswa;

    @SerializedName("total_tatap_muka")
    private int tatapMuka;

    @SerializedName("total_hadir")
    private int hadir;

    @SerializedName("total_sakit")
    private int sakit;

    @SerializedName("total_izin")
    private int izin;

    @SerializedName("total_alpha")
    private int alpha;

    @SerializedName("total_nihil")
    private int nihil;

    @SerializedName("siswa")
    private List<Siswa>siswas;

    @SerializedName("tanggal")
    private List<Tanggal>tanggal;

    @SerializedName("attachment")
    private List<Attachment>attachments;


    public List<Attachment> getAttachments() {
        return attachments;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachments = attachments;
    }

    public int getTotSiswa() {
        return totSiswa;
    }

    public void setTotSiswa(int totSiswa) {
        this.totSiswa = totSiswa;
    }

    public int getTatapMuka() {
        return tatapMuka;
    }

    public void setTatapMuka(int tatapMuka) {
        this.tatapMuka = tatapMuka;
    }

    public int getHadir() {
        return hadir;
    }

    public void setHadir(int hadir) {
        this.hadir = hadir;
    }

    public int getSakit() {
        return sakit;
    }

    public void setSakit(int sakit) {
        this.sakit = sakit;
    }

    public int getIzin() {
        return izin;
    }

    public void setIzin(int izin) {
        this.izin = izin;
    }

    public int getAlpha() {
        return alpha;
    }

    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    public int getNihil() {
        return nihil;
    }

    public void setNihil(int nihil) {
        this.nihil = nihil;
    }

    public List<Siswa> getSiswas() {
        return siswas;
    }

    public void setSiswas(List<Siswa> siswas) {
        this.siswas = siswas;
    }

    public List<Tanggal> getTanggal() {
        return tanggal;
    }

    public void setTanggal(List<Tanggal> tanggal) {
        this.tanggal = tanggal;
    }

    public class Tanggal{
        @SerializedName("tgl")
        private String tgl;

        public String getTgl() {
            return tgl;
        }

        public void setTgl(String tgl) {
            this.tgl = tgl;
        }
    }
    public class Siswa{

        @SerializedName("no")
        private int no;

        @SerializedName("nama")
        private String nama;

        @SerializedName("stb")
        private String stb;

        @SerializedName("sex")
        private String sex;

        @SerializedName("absen")
        private List<String>absen;

        @SerializedName("hadir")
        private int hadir;

        @SerializedName("alpha")
        private int alpha;

        public int getHadir() {
            return hadir;
        }

        public void setHadir(int hadir) {
            this.hadir = hadir;
        }

        public int getAlpha() {
            return alpha;
        }

        public void setAlpha(int alpha) {
            this.alpha = alpha;
        }

        public List<String> getAbsen() {
            return absen;
        }

        public void setAbsen(List<String> absen) {
            this.absen = absen;
        }

        public int getNo() {
            return no;
        }

        public void setNo(int no) {
            this.no = no;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getStb() {
            return stb;
        }

        public void setStb(String stb) {
            this.stb = stb;
        }

        public String getSex() {
            return sex;
        }

        public void setSex(String sex) {
            this.sex = sex;
        }
    }

    public class Attachment{

        @SerializedName("no")
        private int no;

        @SerializedName("id")
        private int id;

        @SerializedName("tgl_tatap_muka")
        private String tanggal;

        @SerializedName("kompetensi")
        private String kopetensi;

        @SerializedName("keterangan")
        private String keterangan;

        public int getNo() {
            return no;
        }

        public void setNo(int no) {
            this.no = no;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTanggal() {
            return tanggal;
        }

        public void setTanggal(String tanggal) {
            this.tanggal = tanggal;
        }

        public String getKopetensi() {
            return kopetensi;
        }

        public void setKopetensi(String kopetensi) {
            this.kopetensi = kopetensi;
        }

        public String getKeterangan() {
            return keterangan;
        }

        public void setKeterangan(String keterangan) {
            this.keterangan = keterangan;
        }
    }

}
