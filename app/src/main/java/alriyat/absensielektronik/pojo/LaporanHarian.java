package alriyat.absensielektronik.pojo;

import com.google.gson.annotations.SerializedName;

public class LaporanHarian {

    @SerializedName("nama")
    private String nama;

    @SerializedName("jenjang")
    private String jenjang;

    @SerializedName("kelas")
    private String kelas;

    @SerializedName("mapel")
    private String mapel;

    @SerializedName("tgl")
    private String tgl;

    @SerializedName("jadwal_absen")
    private String jadwal_absen;

    @SerializedName("jam_absen")
    private String jam_absen;

    @SerializedName("validasi")
    private String validasi;

    @SerializedName("ket")
    private String ket;

    @SerializedName("id_jenjang")
    private int id_jenjang;

    @SerializedName("id_guru")
    private int id_guru;

    @SerializedName("id_jadwal")
    private int id_jadwal;

    @SerializedName("id_kelas")
    private int id_kelas;

    @SerializedName("id_mapel")
    private int id_mapel;

    @SerializedName("tahun_ajaran")
    private String tahunAjaran;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJenjang() {
        return jenjang;
    }

    public void setJenjang(String jenjang) {
        this.jenjang = jenjang;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getMapel() {
        return mapel;
    }

    public void setMapel(String mapel) {
        this.mapel = mapel;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getJadwal_absen() {
        return jadwal_absen;
    }

    public void setJadwal_absen(String jadwal_absen) {
        this.jadwal_absen = jadwal_absen;
    }

    public String getJam_absen() {
        return jam_absen;
    }

    public void setJam_absen(String jam_absen) {
        this.jam_absen = jam_absen;
    }

    public String getValidasi() {
        return validasi;
    }

    public void setValidasi(String validasi) {
        this.validasi = validasi;
    }

    public String getKet() {
        return ket;
    }

    public void setKet(String ket) {
        this.ket = ket;
    }

    public int getId_jenjang() {
        return id_jenjang;
    }

    public void setId_jenjang(int id_jenjang) {
        this.id_jenjang = id_jenjang;
    }

    public int getId_guru() {
        return id_guru;
    }

    public void setId_guru(int id_guru) {
        this.id_guru = id_guru;
    }

    public int getId_jadwal() {
        return id_jadwal;
    }

    public void setId_jadwal(int id_jadwal) {
        this.id_jadwal = id_jadwal;
    }

    public int getId_kelas() {
        return id_kelas;
    }

    public void setId_kelas(int id_kelas) {
        this.id_kelas = id_kelas;
    }

    public int getId_mapel() {
        return id_mapel;
    }

    public void setId_mapel(int id_mapel) {
        this.id_mapel = id_mapel;
    }

    public String getTahunAjaran() {
        return tahunAjaran;
    }

    public void setTahunAjaran(String tahunAjaran) {
        this.tahunAjaran = tahunAjaran;
    }
}
