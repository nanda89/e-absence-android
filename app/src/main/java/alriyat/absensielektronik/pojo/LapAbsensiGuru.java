package alriyat.absensielektronik.pojo;

import com.google.gson.annotations.SerializedName;

public class LapAbsensiGuru {

    @SerializedName("nama")
    private String nama;

    @SerializedName("jam_masuk")
    private String jamMasuk;

    @SerializedName("tgl_masuk")
    private String tglMasuk;

    @SerializedName("keterangan_masuk")
    private String ketMasuk;

    @SerializedName("jam_pulang")
    private String jamPulang;

    @SerializedName("tgl_pulang")
    private String tglPulang;

    @SerializedName("keterangan_pulang")
    private String ketPulang;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJamMasuk() {
        return jamMasuk;
    }

    public void setJamMasuk(String jamMasuk) {
        this.jamMasuk = jamMasuk;
    }

    public String getTglMasuk() {
        return tglMasuk;
    }

    public void setTglMasuk(String tglMasuk) {
        this.tglMasuk = tglMasuk;
    }

    public String getKetMasuk() {
        return ketMasuk;
    }

    public void setKetMasuk(String ketMasuk) {
        this.ketMasuk = ketMasuk;
    }

    public String getJamPulang() {
        return jamPulang;
    }

    public void setJamPulang(String jamPulang) {
        this.jamPulang = jamPulang;
    }

    public String getTglPulang() {
        return tglPulang;
    }

    public void setTglPulang(String tglPulang) {
        this.tglPulang = tglPulang;
    }

    public String getKetPulang() {
        return ketPulang;
    }

    public void setKetPulang(String ketPulang) {
        this.ketPulang = ketPulang;
    }
}
