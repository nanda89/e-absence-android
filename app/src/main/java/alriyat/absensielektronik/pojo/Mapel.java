package alriyat.absensielektronik.pojo;

import com.google.gson.annotations.SerializedName;

public class Mapel {

    @SerializedName("id_mapel")
    private int idMapel;

    @SerializedName("id_jenjang")
    private int idJenjang;

    @SerializedName("id_kat_mapel")
    private int idKatMapel;

    @SerializedName("mapel")
    private String mapel;

    @SerializedName("kkm")
    private String kkm;

    @SerializedName("jenis")
    private String jenis;

    public int getIdMapel() {
        return idMapel;
    }

    public void setIdMapel(int idMapel) {
        this.idMapel = idMapel;
    }

    public int getIdJenjang() {
        return idJenjang;
    }

    public void setIdJenjang(int idJenjang) {
        this.idJenjang = idJenjang;
    }

    public int getIdKatMapel() {
        return idKatMapel;
    }

    public void setIdKatMapel(int idKatMapel) {
        this.idKatMapel = idKatMapel;
    }

    public String getMapel() {
        return mapel;
    }

    public void setMapel(String mapel) {
        this.mapel = mapel;
    }

    public String getKkm() {
        return kkm;
    }

    public void setKkm(String kkm) {
        this.kkm = kkm;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis;
    }
}
