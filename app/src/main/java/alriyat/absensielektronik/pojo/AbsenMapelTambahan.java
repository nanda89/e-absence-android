package alriyat.absensielektronik.pojo;

public class AbsenMapelTambahan {

    private String id_kelas_tambahan;
    private String id_jadwal_tambahan;
    private String tgl;
    private String jam;
    private String valid;
    private String absen;
    private String rekap;
    private String keterangan;

    public String getId_kelas_tambahan() {
        return id_kelas_tambahan;
    }

    public void setId_kelas_tambahan(String id_kelas_tambahan) {
        this.id_kelas_tambahan = id_kelas_tambahan;
    }

    public String getId_jadwal_tambahan() {
        return id_jadwal_tambahan;
    }

    public void setId_jadwal_tambahan(String id_jadwal_tambahan) {
        this.id_jadwal_tambahan = id_jadwal_tambahan;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }

    public String getAbsen() {
        return absen;
    }

    public void setAbsen(String absen) {
        this.absen = absen;
    }

    public String getRekap() {
        return rekap;
    }

    public void setRekap(String rekap) {
        this.rekap = rekap;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
