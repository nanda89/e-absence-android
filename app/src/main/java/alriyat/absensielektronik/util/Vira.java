package alriyat.absensielektronik.util;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Point;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.text.format.Formatter;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import alriyat.absensielektronik.BuildConfig;
import alriyat.absensielektronik.helper.SessionManager;

//import alriyat.absensielektronik.BuildConfig;

/**
 * Created by Rafi on 8/28/16.
 */
public class Vira {
    private static final String TAG = Vira.class.getName();
    private final static String PROPERTY_APP_VERSION = "appVersion";
    private final static String CLASS_NAME = "Vira";
    private final static String REGISTER_ID = "register_id";
    private final static String FIRST_RUN = "first_run";

    public final static int MIN_PASSWORD_LENGTH = 8;

    public static String urlPrefix() {
        return "https";
    }

    public static String DATE_FORMAT_NICE = "d MMM yyyy";
    private static String DATE_FORMAT_DB = "yyyy-MM-dd";

    private static final String JAN = "Januari";
    private static final String FEB = "Februari";
    private static final String MAR = "Maret";
    private static final String APR = "April";
    private static final String MEI = "Mei";
    private static final String JUN = "Juni";
    private static final String JUL = "Juli";
    private static final String AGS = "Agustus";
    private static final String SEP = "September";
    private static final String OCT = "Oktober";
    private static final String NOV = "November";
    private static final String DES = "Desember";

    private static final String[] CAPITALIZE_EXCEPTION = {"PT.", "ATM", "PT", "CV.", "UD."};

    public static String toCapitalize(String string) {
        String[] words = string.split(" ");
        String result = "";
        for (String word : words) {
            if (isAbbreviationWord(word)) {
                result += word + " ";
            } else {
                if (word.length() > 1) {
                    result += word.substring(0, 1).toUpperCase(Locale.getDefault())
                            + word.substring(1).toLowerCase(Locale.getDefault()) + " ";
                } else {
                    result += word;
                }
            }
        }
        return result;
    }

    private static boolean isAbbreviationWord(String word) {
        boolean isAbbreviation = false;
        for (int i = 0; i < CAPITALIZE_EXCEPTION.length; i++) {
            if (word.toUpperCase().equals(CAPITALIZE_EXCEPTION[i])) {
                isAbbreviation = true;
            }
        }
        return isAbbreviation;
    }

    public static String dateFormatIndonesia(String date) {
        String[] split = date.split("-");
        String tahun = split[0];
        String tanggal = split[2];
        String bulanIndex = split[1];
        String bulan = null;
        switch (Integer.parseInt(bulanIndex)) {
            case 1:
                bulan = JAN;
                break;
            case 2:
                bulan = FEB;
                break;
            case 3:
                bulan = MAR;
                break;
            case 4:
                bulan = APR;
                break;
            case 5:
                bulan = MEI;
                break;
            case 6:
                bulan = JUN;
                break;
            case 7:
                bulan = JUL;
                break;
            case 8:
                bulan = AGS;
                break;
            case 9:
                bulan = SEP;
                break;
            case 10:
                bulan = OCT;
                break;
            case 11:
                bulan = NOV;
                break;
            case 12:
                bulan = DES;
                break;

            default:
                bulan = "Unknown";
                break;
        }

        return Integer.parseInt(tanggal) + " " + bulan + " " + tahun;
    }

    public static void showLogError(String text) {
        if (BuildConfig.DEBUG) {
            Log.d("Vira", "Error log " + text);
        }
    }

    public static void showLogContent(String text) {
        if (BuildConfig.DEBUG) {
            Log.d("Vira", "Content log " + text);
        }
    }

    public static void showToast(Context context, String text) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show();
    }

    /**
     * @param money as String
     * @return result as String with currency format
     */
    public static String myCurrencyFormat(String money) {
        String result = money;
        NumberFormat nf = NumberFormat.getCurrencyInstance();

        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setCurrencySymbol("Rp.");
        dfs.setGroupingSeparator('.');
        dfs.setMonetaryDecimalSeparator(',');

        DecimalFormat df = new DecimalFormat();
        df.setDecimalFormatSymbols(dfs);
        result = df.format(Double.valueOf(money));
        return result;
    }

    public static String myCurrencyFormat(double money) {
        String result = String.valueOf(money);
        NumberFormat nf = NumberFormat.getCurrencyInstance();

        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setCurrencySymbol("Rp.");
        dfs.setGroupingSeparator('.');
        dfs.setMonetaryDecimalSeparator(',');

        DecimalFormat df = new DecimalFormat();
        df.setDecimalFormatSymbols(dfs);
        result = df.format(Double.valueOf(money));
        return result;
    }

    public static Double currencyStringToDouble(String currencyString) {
        try {
            DecimalFormat format = (DecimalFormat) NumberFormat.getInstance(Locale.GERMANY);
            //format.setParseBigDecimal(true);
            Number number = format.parse(currencyString);
            return number.doubleValue();
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return null;
    }

    public static String myDecimalFormat(Double number) {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(2);
        String s = nf.format(number);
        return s;
    }

    public static String niceNumberFormat(Double number) {
        DecimalFormatSymbols dfs = new DecimalFormatSymbols();
        dfs.setGroupingSeparator('.');
        dfs.setMonetaryDecimalSeparator(',');

        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(0);
        df.setDecimalFormatSymbols(dfs);
        String result = df.format(Double.valueOf(number));
        return result;
    }

    /**
     * @param date as String, mysql format date
     * @return result as String
     */
    public static String myDateFormatNice(String date) {
        String result = date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date inputdate = sdf.parse(date);
            sdf = new SimpleDateFormat("yyyy-MM-dd");
            result = sdf.format(inputdate);
            return result;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static String myDateFormatGood(String date) {
        String result = date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date inputdate = sdf.parse(date);
            sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
            result = sdf.format(inputdate);
            return result;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static String myDateFormat(String date) {
        String result = date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date inputdate = sdf.parse(date);
            sdf = new SimpleDateFormat("dd-MM-yyyy");
            result = sdf.format(inputdate);
            return result;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }


    /**
     * @param date as String, mysql format date
     * @return Date
     */
    public static Date myStringToDate(String date) {
        String dtStart = date;
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date rldate = format.parse(dtStart);
            return rldate;
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return null;
    }

    public static Date myStringToDate(String date, String dateFormat) {
        String dtStart = date;
        SimpleDateFormat format = new SimpleDateFormat(dateFormat);
        try {
            Date rldate = format.parse(dtStart);
            Vira.log("myStringToDate", rldate.toString());
            return rldate;
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return null;
    }

    public static String getFormatedDate(String date, String formatDate) {
        String result = date;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date inputdate = sdf.parse(date);
            sdf = new SimpleDateFormat(formatDate);
            result = sdf.format(inputdate);
            return result;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static String currentDate() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date currentDate = new Date();
        return format.format(currentDate);
    }

    public static String getDate(int interval) {
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();
        if (interval == 0) {
            return Vira.formatDate(today);
        } else {
            calendar.add(Calendar.DAY_OF_YEAR, interval);
            return Vira.formatDate(calendar.getTime());
        }
    }

    public static String getDate(Date date, int interval) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        Date today = date;
        if (interval == 0) {
            return Vira.formatDate(today);
        } else {
            calendar.add(Calendar.DAY_OF_YEAR, interval);
            return Vira.formatDate(calendar.getTime());
        }
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static String getDate(String date, String formatDate, String formatBefore) {
        String result = date;
        SimpleDateFormat sdf = new SimpleDateFormat(formatBefore);
        try {
            Date inputdate = sdf.parse(date);
            sdf = new SimpleDateFormat(formatDate);
            result = sdf.format(inputdate);
            return result;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return result;
    }

    public static String getDate(String date, int hour) {
        try {
            final SimpleDateFormat sdf = new SimpleDateFormat(Vira.DATE_FORMAT_DB + " HH:mm:ss");
            final String currentDateandTime = date;
            final Date tampDate = sdf.parse(currentDateandTime);
            final Calendar calendar = Calendar.getInstance();

            calendar.setTime(tampDate);
            calendar.add(Calendar.HOUR, hour);

            return sdf.format(calendar.getTime());
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return "";
    }

    public static Date getNextWeekDate(String startDate, int interval) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date currentDate = format.parse(startDate);
            Date newDate = new Date(currentDate.getTime());

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(newDate);

            calendar.add(Calendar.DAY_OF_YEAR, 7);

            calendar.setTime(calendar.getTime());

            calendar.set(Calendar.DAY_OF_WEEK, 2 + interval);

            return calendar.getTime();
        } catch (ParseException pe) {
            Log.e("DateParse", pe.getMessage());
        }

        return null;
    }


    public static String getNextWeekDate() {
        Calendar mCalendar = Calendar.getInstance();
        int i = mCalendar.get(Calendar.WEEK_OF_MONTH);
        mCalendar.set(Calendar.DAY_OF_WEEK, i + 7);
        return Vira.formatDate(mCalendar.getTime());
    }

    public static String formatDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return format.format(date);
    }

    public static double getPeriod(Date start, Date end) {
        long period = end.getTime() - start.getTime();
        return period;
    }

    public static BigDecimal doubleToDecimal(double number) {
        return new BigDecimal(number);
    }

    public static String get12Hour(String hour24) {
        String hour = hour24 + ":00";
        try {
            String s = hour;
            DateFormat f1 = new SimpleDateFormat("HH:mm:ss"); //HH for hour of the day (0 - 23)
            Date d = f1.parse(s);
            /*DateFormat f2 = new SimpleDateFormat("h:mma");
            hour = f2.format(d).toLowerCase(); // "12:18am"*/
            DateFormat f2 = new SimpleDateFormat("HH:mm");
            hour = f2.format(d).toLowerCase(); // "23:14"
        } catch (ParseException pe) {
            pe.printStackTrace();
        }
        return hour;
    }

    /**
     * @param maxTime int in hour
     * @return
     */
    public static boolean isDataExpired(Date start, Date end, double maxTime) {
        Log.d("Period", "" + (getPeriod(start, end) / (60 * 60 * 1000)) + " | " + maxTime);
        if ((getPeriod(start, end) / (60 * 60 * 1000)) > maxTime) {
            return true;
        }
        return false;
    }

    ;

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }

        return isValid;
    }

    public static boolean isUsernameValid(String username) {
        boolean isValid = false;

        String expression = "^[a-z0-9_-]{3,15}$";
        CharSequence inputStr = username;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    @SuppressLint("NewApi")
    public static String getRealPathFromUriAPI19(Context context, Uri uri) {
        Log.d("Result Post", uri.toString());
        String filepath = "";
        String wholeID = DocumentsContract.getDocumentId(uri);
        String id = wholeID.split(":")[1];
        String[] column = {MediaStore.Images.Media.DATA};
        String sel = MediaStore.Images.Media._ID + "=?";
        Cursor cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column, sel, new String[]{id}, null);
        int columnIndex = cursor.getColumnIndex(column[0]);
        if (cursor.moveToFirst()) {
            filepath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filepath;
    }

    @SuppressLint("NewApi")
    public static String getRealPathFromUriAPI11to18(Context context, Uri uri) {
        Log.d("Result Post", uri.toString());
        String[] proj = {MediaStore.Images.Media.DATA};
        String result = null;
        CursorLoader cursorLoader = new CursorLoader(context, uri, proj, null, null, null);
        Cursor cursor = cursorLoader.loadInBackground();

        if (cursor != null) {
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            result = cursor.getString(columnIndex);
        }
        return result;
    }

    public static String getRealPathFromUriAPIBelow11(Context context, Uri uri) {
        Log.d("Result Post", uri.toString());
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(columnIndex);
    }

    public static Matrix getRotateMatrix(String path) {
        int rotate = 0;
        try {
            File imageFile = new File(path);
            ExifInterface ei = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
            Log.d("Result Post", "Exif Orientation : " + orientation);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        Matrix matrix = new Matrix();
        matrix.postRotate(rotate);
        return matrix;
    }

    public static boolean isDebugable(ApplicationInfo appInfo) {
        return 0 != (appInfo.flags & ApplicationInfo.FLAG_DEBUGGABLE);
    }

    public static boolean isConnectingToInternet(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null)
                for (int i = 0; i < info.length; i++)
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }

        }
        return false;
    }

    public static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException nnfe) {
            throw new RuntimeException("Could not package name " + nnfe);
        }
    }

    public static void deleteSharedPreference(SharedPreferences preferences) {
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }

    public static boolean isNumeric(String number) {
        try {
            double d = Double.parseDouble(number);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    static public SharedPreferences getSharedLocalData(Context context) {
        return context.getSharedPreferences(CLASS_NAME, Context.MODE_PRIVATE);
    }

    static public void storeRegisterIdToLocal(Context context, String registerId) {
        final SharedPreferences prefs = getSharedLocalData(context);
        int appVersion = Vira.getAppVersion(context);
        Log.d("Result Post", "Saving notif on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(REGISTER_ID, registerId);
        editor.putInt(Vira.PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    static public String getRegisterIdFromPref(Context context) {
        SharedPreferences preferences = getSharedLocalData(context);
        String registerId = preferences.getString(REGISTER_ID, "");
        if (registerId.isEmpty()) {
            return "";
        }

        int registrationVersion = preferences.getInt(Vira.PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = Vira.getAppVersion(context);
        if (registrationVersion != currentVersion) {
            return "";
        }
        return registerId;
    }

    static public String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    static public void storeFirstRunToLocal(Context context, boolean isFirstRun) {
        final SharedPreferences prefs = getSharedLocalData(context);
        int appVersion = Vira.getAppVersion(context);
        Log.d("Result Post", "Saving notif on app version " + appVersion);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(FIRST_RUN, isFirstRun);
        editor.putInt(Vira.PROPERTY_APP_VERSION, appVersion);
        editor.commit();
    }

    static public boolean isFirstRunFromPref(Context context) {
        SharedPreferences preferences = getSharedLocalData(context);
        int registrationVersion = preferences.getInt(Vira.PROPERTY_APP_VERSION, Integer.MIN_VALUE);
        int currentVersion = Vira.getAppVersion(context);
        if (registrationVersion != currentVersion) {
            return false;
        }

        boolean isFirstRun = preferences.getBoolean(FIRST_RUN, true);
        return isFirstRun;
    }

    public static String getStringLocaleSetting(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        if (!prefs.getString("language", "").isEmpty()) {
            return prefs.getString("language", "");
        }
        return "id";
    }

    public static String getStringCurrencySetting(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        if (!prefs.getString("currency", "").isEmpty()) {
            return prefs.getString("currency", "");
        }
        return "IDR";
    }

    public static void changePreferenceLanguage(Context context, String locale) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("language", locale);
        editor.commit();
    }

    public static void changePreferenceCurrency(Context context, String currency) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("currency", currency);
        editor.commit();
    }

    public static String getOtherStringLocale(String locale) {
        if (locale.equals("id")) {
            return "in";
        }
        return locale;
    }

    public static boolean isLanguageChanged(Context context, String oldLanguage) {
        if (!oldLanguage.equals(Vira.getStringLocaleSetting(context))) {
            return true;
        }
        return false;
    }

    public static boolean isCurrencyChanged(Context context, String oldCurrency) {
        if (!oldCurrency.equals(Vira.getStringCurrencySetting(context))) {
            return true;
        }
        return false;
    }

    public static int getActionBarHeight(Context context) {
        TypedValue tv = new TypedValue();
        int actionBarHeight;
        if (context.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
            actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, context.getResources().getDisplayMetrics());
            return actionBarHeight;
        }
        return 0;
    }

    public static boolean isWebServiceValid(JSONObject json, String param) {
        try {
            if (!json.isNull(param) && !json.getString(param).isEmpty()
                    && json.getString(param).trim().length() > 0
                    && !json.getString(param).trim().equals("null")) {
                return true;
            }
        } catch (JSONException je) {
            je.printStackTrace();
        }
        return false;
    }

    public static Bitmap loadBitmapFromView(View v) {
        if (v.getMeasuredHeight() <= 0) {
            v.measure(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
            Bitmap b = Bitmap.createBitmap(v.getMeasuredWidth(), v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            Canvas c = new Canvas(b);
            v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
            v.draw(c);
            return b;
        }
        return null;
    }

    public static void log(String tag, String content) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, content);
        }
    }

    public static Intent getEmailIntent(String emailTo, String subject, String content) {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("message/rfc822");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{emailTo});
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, content);
        return intent;
    }

    public static void openViewIntent(Context context, String url) {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static int getStatusBarHeight(Activity activity) {
        int result = 0;
        int resourceId = activity.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = activity.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @SuppressLint("NewApi")
    public static Bitmap blur(Context context, Bitmap smallBitmap, int radius) {
        try {
            smallBitmap = RGB565toARGB888(smallBitmap);
        } catch (Exception e) {
            e.printStackTrace();
        }


        Bitmap bitmap = Bitmap.createBitmap(
                smallBitmap.getWidth(), smallBitmap.getHeight(),
                Bitmap.Config.ARGB_8888);

        RenderScript renderScript = RenderScript.create(context);

        Allocation blurInput = Allocation.createFromBitmap(renderScript, smallBitmap);
        Allocation blurOutput = Allocation.createFromBitmap(renderScript, bitmap);

        ScriptIntrinsicBlur blur = ScriptIntrinsicBlur.create(renderScript,
                Element.U8_4(renderScript));
        blur.setInput(blurInput);
        blur.setRadius(radius); // radius must be 0 < r <= 25
        blur.forEach(blurOutput);

        blurOutput.copyTo(bitmap);
        renderScript.destroy();

        return bitmap;

    }


    public static Bitmap RGB565toARGB888(Bitmap img) throws Exception {
        int numPixels = img.getWidth() * img.getHeight();
        int[] pixels = new int[numPixels];

        //Get JPEG pixels.  Each int is the color values for one pixel.
        img.getPixels(pixels, 0, img.getWidth(), 0, 0, img.getWidth(), img.getHeight());

        //Create a Bitmap of the appropriate format.
        Bitmap result = Bitmap.createBitmap(img.getWidth(), img.getHeight(), Bitmap.Config.ARGB_8888);

        //Set RGB pixels.
        result.setPixels(pixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
        return result;
    }

    private static int screenWidth = 0;

    public static int getScreenWidth(Context c) {
        if (screenWidth == 0) {
            WindowManager wm = (WindowManager) c.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            screenWidth = size.x;
        }

        return screenWidth;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static void setTranslucentStatusBarKiKat(Window window) {
        window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
    }

    public static void onWifi(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        wifiManager.setWifiEnabled(true);
    }

    public static boolean isConnectedToServer(Context context) {
        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        int linkSpeed = wifiManager.getConnectionInfo().getRssi();
        int level = WifiManager.calculateSignalLevel(linkSpeed, 5);

        String ip = Formatter.formatIpAddress(wifiManager.getConnectionInfo().getIpAddress());

        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        return mWifi.isConnected();
    }

    public static boolean pingToApp(String url ,GetDataService service , final SessionManager sessionManager){
//        Call<List<Mapel>> call = service.getAllMapel();
//        call.enqueue(new Callback<List<Mapel>>() {
//            @Override
//            public void onResponse(Call<List<alriyat.absensielektronik.pojo.Mapel>> call, retrofit2.Response<List<alriyat.absensielektronik.pojo.Mapel>> response) {
//                Log.d(TAG, "onResponse: successsssssssss");
//                sessionManager.isConnected(true);
//            }
//            @Override
//            public void onFailure(Call<List<alriyat.absensielektronik.pojo.Mapel>> call, Throwable t) {
//                t.printStackTrace();
//                Log.d(TAG, "onResponse: errorrrrrr "+t.getMessage());
//                sessionManager.isConnected(false);
//            }
//        });
//        boolean result = sessionManager.preferences.getBoolean("isConnect",false);
//        Log.d(TAG, "pingToServer: result "+result);
//        return result;
        boolean result = false;
        try {
            Runtime runtime = Runtime.getRuntime();
            Process process = runtime.exec("ping -s 1024 -c 4 -w 4 "+url);
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String s;
            StringBuilder res = new StringBuilder();
            StringBuilder avgRes = new StringBuilder();

            while ((s = stdInput.readLine()) != null) {
                Log.d("PING", "runNVT:  ping into  : " + url +" result : "+s);

                if (s.contains("packets transmitted")) {
                    Log.d("PING", "ping result : "+s);
                    String[] r = s.split(",");
                    String value = r[1].replaceAll("[^-?0-9]+", " ").trim();
                    if(!value.equals("")){
                        if(Integer.parseInt(value) > 0){
                            result = true;
                        }
                    }
                }
            }
            stdInput.close();
            process.destroy();

        }catch (IOException e){
            e.printStackTrace();
        }
        if(result){
            sessionManager.isConnected(true);
        }else{
            sessionManager.isConnected(false);
        }

        return result;
    }
}
