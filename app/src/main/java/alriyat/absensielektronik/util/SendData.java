package alriyat.absensielektronik.util;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import alriyat.absensielektronik.pojo.GeneralResponse;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;

public class SendData {

    private static final String TAG = SendData.class.getName();
    public static GeneralResponse generalResponse;

    @SuppressLint("StaticFieldLeak")
    public static class sendUraian extends AsyncTask<Void, String, Boolean> {

        String idNonGuru;
        String tanggal;
        String uraian;
        String type;
        String nameFile;
        File file;
        String urlServer;
        Context context;
        ProgressDialog dialogo;


        public sendUraian(String idNonGuru, String tanggal, String uraian, String type, String nameFile, File file, String urlServer, Context contex) {
            super();
            this.idNonGuru = idNonGuru;
            this.tanggal = tanggal;
            this.uraian = uraian;
            this.type = type;
            this.nameFile = nameFile;
            this.file = file;
            this.urlServer = urlServer;
            this.context = contex;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialogo = new ProgressDialog(context);
            dialogo.setMessage("Pengiriman data...");
            dialogo.setCancelable(false);
            dialogo.show();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {

            String path;
            if(type.equals("p")){
                path = Environment.getExternalStorageDirectory().getAbsolutePath()+"/eAbsence/image";
            }else{
                path = Environment.getExternalStorageDirectory().getAbsolutePath()+"/eAbsence/video";
            }

            File fileUraian = new File(path , nameFile);
            if (fileUraian.exists()) {
                try {
                    Log.d(TAG, "API :" + urlServer + "assets/file/uraian_non_guru.php\n" +
                            "############## parameter ##############\n" +
                            "idNonGuru : " + idNonGuru + "\n" +
                            "tanggal : " + tanggal + "\n" +
                            "uraian : " + uraian + "\n" +
                            "type : " + type + "\n" +
                            "nameFile : " + nameFile + "\n"
                    );
                    SimpleDateFormat f = new SimpleDateFormat("HH:ss");
                    String jam = f.format(new Date());
                    OkHttpClient client = new OkHttpClient();

                    RequestBody formBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                            .addFormDataPart("fileToUpload", fileUraian.getName(),
                                    RequestBody.create(MediaType.parse("image/*"), fileUraian))
                            .addFormDataPart("idNonGuru", idNonGuru)
                            .addFormDataPart("tanggal", tanggal)
                            .addFormDataPart("jam", jam)
                            .addFormDataPart("uraian", uraian)
                            .addFormDataPart("type", type)
                            .build();

                    Request request = new Request.Builder()
//                            .addHeader("token", token)
//                            .addHeader("Content-Type", "application/x-www-form-urlencoded")
                            .url(urlServer + "assets/file/uraian_non_guru.php")
                            .post(formBody)
                            .build();

                    okhttp3.Response response = client.newCall(request)
                            .execute();
                    Gson gson = new Gson();
                    assert response.body() != null;
                    String jsonData = response.body().string();
                    Log.d(TAG, "doInBackground: response message journey : " + jsonData);
                    final GeneralResponse res = gson.fromJson(jsonData , GeneralResponse.class);
                    try {
                        if (res.isSuccess()){
                            dialogo.setMessage("Data berhasil dikirim");
                        }else{
                            dialogo.setMessage(res.getMessage());
                        }
                    }catch (RuntimeException e){
                        e.printStackTrace();
                    }

                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Boolean response) {
            super.onPostExecute(response);
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            dialogo.dismiss();
        }
    }

    public static boolean sendingUraian(String idNonGuru,String tanggal,String uraian,String type,String nameFile,File file,String urlServer,Context context) {
        ProgressDialog dialogo;
        dialogo = new ProgressDialog(context);
        dialogo.setMessage("Pengiriman data...");
        dialogo.setCancelable(false);
        dialogo.show();

        boolean result = true;

        String path;
        if(type.equals("p")){
            path = Environment.getExternalStorageDirectory().getAbsolutePath()+"/eAbsence/image";
        }else{
            path = Environment.getExternalStorageDirectory().getAbsolutePath()+"/eAbsence/video";
        }

        File fileUraian = new File(path , nameFile);
        if (fileUraian.exists()) {
            try {
                SimpleDateFormat f = new SimpleDateFormat("HH:ss");
                String jam = f.format(new Date());
                Log.d(TAG, "API :" + urlServer + "assets/file/uraian_non_guru.php\n" +
                        "############## parameter ##############\n" +
                        "idNonGuru : " + idNonGuru + "\n" +
                        "tanggal : " + tanggal + "\n" +
                        "jam : " + jam + "\n" +
                        "uraian : " + uraian + "\n" +
                        "type : " + type + "\n" +
                        "nameFile : " + nameFile + "\n"
                );

                OkHttpClient client = new OkHttpClient();

                RequestBody formBody = new MultipartBody.Builder().setType(MultipartBody.FORM)
                        .addFormDataPart("fileToUpload", fileUraian.getName(),
                                RequestBody.create(MediaType.parse("image/*"), fileUraian))
                        .addFormDataPart("idNonGuru", idNonGuru)
                        .addFormDataPart("tanggal", tanggal)
                        .addFormDataPart("jam", jam)
                        .addFormDataPart("uraian", uraian)
                        .addFormDataPart("type", type)
                        .build();

                Request request = new Request.Builder()
                        .url(urlServer + "assets/file/uraian_non_guru.php")
                        .post(formBody)
                        .build();
                Log.d(TAG, "sendingUraian: URL : "+urlServer + "assets/file/uraian_non_guru.php");
                okhttp3.Response response = client.newCall(request)
                        .execute();
                Gson gson = new Gson();
                assert response.body() != null;
                String jsonData = response.body().string();
                Log.d(TAG, "doInBackground: response message journey : " + jsonData);
                final GeneralResponse res = gson.fromJson(jsonData , GeneralResponse.class);
                if (res.isSuccess()){
                    dialogo.setMessage("Data berhasil dikirim");
                }else{
                    dialogo.setMessage(res.getMessage());
                    result = false;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                dialogo.dismiss();
            }
        }
        return result;
    }

}
