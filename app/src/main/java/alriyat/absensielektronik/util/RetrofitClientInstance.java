package alriyat.absensielektronik.util;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import alriyat.absensielektronik.helper.SessionConfig;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static alriyat.absensielektronik.app.AppController.TAG;

/**
 * Created by folcotandiono on 8/25/2018.
 */

public class RetrofitClientInstance {

    private static Retrofit retrofit;
    private static String BASE_URL = "http://192.168.42.42/web_absence/";

    public static Retrofit getRetrofitInstance(SessionConfig sessionConfig) {
        String ip = sessionConfig.preferences.getString("ipconfig","");
        Log.d(TAG, "getRetrofitInstance: ip address : "+ip);
        if (ip.isEmpty()) ip = "192.168.42.42";
        BASE_URL = "http://"+ip+ "/web_absence/";
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

            OkHttpClient client = new OkHttpClient.Builder()
                    .connectTimeout(500, TimeUnit.SECONDS)
                    .readTimeout(500,TimeUnit.SECONDS)
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL).client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();
//        }
        return retrofit;
    }
}
