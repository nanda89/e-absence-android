package alriyat.absensielektronik.util;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.os.Build;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alriyat.absensielektronik.activity.AbsensiActivity;
import alriyat.absensielektronik.helper.AlarmReceiver;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.AbsenMapelOffline;
import alriyat.absensielektronik.model.AbsenOffline;
import alriyat.absensielektronik.model.CabangOffline;
import alriyat.absensielektronik.model.GuruOffline;
import alriyat.absensielektronik.model.Jadwal;
import alriyat.absensielektronik.model.JadwalOffline;
import alriyat.absensielektronik.model.JadwalTambahanOffline;
import alriyat.absensielektronik.model.JamMasukOffline;
import alriyat.absensielektronik.model.JenjangOffline;
import alriyat.absensielektronik.model.KalenderOffline;
import alriyat.absensielektronik.model.KatMapelOffline;
import alriyat.absensielektronik.model.KategoriOffline;
import alriyat.absensielektronik.model.KehadiranResponse;
import alriyat.absensielektronik.model.KelasOffline;
import alriyat.absensielektronik.model.KelasTambahanOffline;
import alriyat.absensielektronik.model.Mapel;
import alriyat.absensielektronik.model.MapelOffline;
import alriyat.absensielektronik.model.MapelTambahan;
import alriyat.absensielektronik.model.NonGuruOffline;
import alriyat.absensielektronik.model.PengampuOffline;
import alriyat.absensielektronik.model.ScheduleOffline;
import alriyat.absensielektronik.model.SettingOffline;
import alriyat.absensielektronik.model.Siswa;
import alriyat.absensielektronik.model.SiswaOffline;
import alriyat.absensielektronik.model.SiswakelasOffline;
import alriyat.absensielektronik.model.TahunajaranOffline;
import alriyat.absensielektronik.model.UserOffline;
import alriyat.absensielektronik.pojo.AbsenHadirPulang;
import alriyat.absensielektronik.pojo.AbsenMapelTambahan;
import retrofit2.Call;
import retrofit2.Callback;

import static android.content.Context.ALARM_SERVICE;

/**
 * Created by folcotandiono on 8/16/2018.
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    private static final String TAG = DatabaseHandler.class.getName();
    // static variable
    private static final int DATABASE_VERSION = 8;

    // Database name
    private static final String DATABASE_NAME = "dbs_absence";

    // table name
    private static final String TABLE_TALL = "talls";

    // column tables
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "user";
    private static final String KEY_TALL = "tall";

    private ProgressDialog dialog;

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //Create table
    @Override
    public void onCreate(SQLiteDatabase db) {
        String guru = "CREATE TABLE " + "tabel_guru" + "("
                + "id_guru" + " TEXT,"
                + "id_jenjang" + " TEXT,"
                + "nik" + " TEXT,"
                + "nama" + " TEXT,"
                + "no_ktp" + " TEXT,"
                + "alias" + " TEXT,"
                + "jenis_kelamin" + " TEXT,"
                + "tempat_lahir" + " TEXT,"
                + "tgl_lahir" + " TEXT,"
                + "agama" + " TEXT,"
                + "no_hp" + " TEXT,"
                + "email" + " TEXT,"
                + "ijazah" + " TEXT,"
                + "pend_terakhir" + " TEXT,"
                + "gambar" + " TEXT,"
                + "alamat" + " TEXT,"
                + "unit_kerja" + " TEXT,"
                + "tmt" + " TEXT,"
                + "status_guru_bk" + " TEXT,"
                + "status_kepsek" + " TEXT"
                + ")";
        db.execSQL(guru);

        String jadwal = "CREATE TABLE " + "tabel_jadwal" + "("
                + "id_jadwal" + " TEXT,"
                + "id_ta" + " TEXT,"
                + "id_jenjang" + " TEXT,"
                + "id_kelas" + " TEXT,"
                + "id_mapel" + " TEXT,"
                + "id_guru" + " TEXT,"
                + "hari" + " TEXT,"
                + "jam_awal" + " TEXT,"
                + "jam_akhir" + " TEXT,"
                + "status" + " TEXT"
                + ")";
        db.execSQL(jadwal);

        String jenjang = "CREATE TABLE " + "tabel_jenjang" + "("
                + "id_jenjang" + " TEXT,"
                + "jenjang" + " TEXT,"
                + "jam_masuk" + " TEXT,"
                + "jam_pulang" + " TEXT"
                + ")";
        db.execSQL(jenjang);

        String kelas = "CREATE TABLE " + "tabel_kelas" + "("
                + "id_kelas" + " TEXT,"
                + "kelas" + " TEXT,"
                + "id_guru" + " TEXT,"
                + "id_jenjang" + " TEXT"
                + ")";
        db.execSQL(kelas);

        String mapel = "CREATE TABLE " + "tabel_mapel" + "("
                + "id_mapel" + " TEXT,"
                + "id_jenjang" + " TEXT,"
                + "id_kat_mapel" + " TEXT,"
                + "mapel" + " TEXT,"
                + "kkm" + " TEXT"
                + ")";
        db.execSQL(mapel);

        String setting = "CREATE TABLE " + "tabel_setting" + "("
                + "id_setting" + " TEXT,"
                + "logo_mini" + " TEXT,"
                + "logo_gambar" + " TEXT,"
                + "logo_favicon" + " TEXT,"
                + "nik" + " TEXT,"
                + "kartu_gsm" + " TEXT,"
                + "cek_pulsa" + " TEXT,"
                + "metode" + " TEXT,"
                + "kode_aktivasi" + " TEXT,"
                + "link_tujuan" + " TEXT,"
                + "logo_besar" + " TEXT,"
                + "logo_sekolah" + " TEXT,"
                + "pimpinan" + " TEXT,"
                + "akun_gmail" + " TEXT,"
                + "alamat" + " TEXT,"
                + "website" + " TEXT,"
                + "telat" + " TEXT,"
                + "toleransi_jam_akhir" + " TEXT,"
                + "lat" + " TEXT,"
                + "lng" + " TEXT,"
                + "radius" + " TEXT,"
                + "jam_pulang" + " TEXT,"
                + "jam_masuk" + " TEXT,"
                + "kapasitas_kelas" + " TEXT"
                + ")";
        db.execSQL(setting);

        String absenMapel = "CREATE TABLE " + "tabel_absen_mapel" + "("
                + "id_absen_mapel" + " TEXT,"
                + "id_penempatan" + " TEXT,"
                + "id_jadwal" + " TEXT,"
                + "tgl" + " TEXT,"
                + "jam" + " TEXT,"
                + "absen" + " TEXT,"
                + "valid" + " TEXT,"
                + "rekap" + " TEXT,"
                + "status" + " TEXT,"
                + "keterangan" + " TEXT"
                + ")";
        db.execSQL(absenMapel);

        String siswaKelas = "CREATE TABLE " + "tabel_siswakelas" + "("
                + "id_penempatan" + " TEXT,"
                + "id_ta" + " TEXT,"
                + "id_kelas" + " TEXT,"
                + "id_siswa" + " TEXT"
                + ")";
        db.execSQL(siswaKelas);

        String siswa = "CREATE TABLE " + "tabel_siswa" + "("
                + "id_siswa" + " TEXT,"
                + "id_jenjang" + " TEXT,"
                + "nama_lengkap" + " TEXT,"
                + "nisn" + " TEXT,"
                + "jenis_kelamin" + " TEXT,"
                + "tempat_lahir" + " TEXT,"
                + "tgl_lahir" + " TEXT,"
                + "agama" + " TEXT,"
                + "alamat" + " TEXT,"
                + "no_hp" + " TEXT,"
                + "email" + " TEXT,"
                + "asal_sekolah" + " TEXT,"
                + "ket_asal" + " TEXT,"
                + "nama_ayah" + " TEXT,"
                + "nama_ibu" + " TEXT,"
                + "gambar" + " TEXT,"
                + "tgl_daftar" + " TEXT"
                + ")";
        db.execSQL(siswa);

        String tahunAjaran = "CREATE TABLE " + "tabel_tahunajaran" + "("
                + "id_ta" + " TEXT,"
                + "tahun_ajaran" + " TEXT,"
                + "tgl_awal" + " TEXT,"
                + "tgl_akhir" + " TEXT,"
                + "status" + " TEXT"
                + ")";
        db.execSQL(tahunAjaran);

        String katMapel = "CREATE TABLE " + "tabel_kat_mapel" + "("
                + "id_kat_mapel" + " TEXT,"
                + "kategori_mapel" + " TEXT"
                + ")";
        db.execSQL(katMapel);

        String absen = "CREATE TABLE " + "tabel_absen" + "("
                + "id_absen" + " TEXT,"
                + "id_penempatan" + " TEXT,"
                + "tgl" + " TEXT,"
                + "jam_masuk" + " TEXT,"
                + "absen_masuk" + " TEXT,"
                + "valid_masuk" + " TEXT,"
                + "sms_masuk" + " TEXT,"
                + "jam_pulang" + " TEXT,"
                + "absen_pulang" + " TEXT,"
                + "valid_pulang" + " TEXT,"
                + "sms_pulang" + " TEXT"
                + ")";
        db.execSQL(absen);

        String cabang = "CREATE TABLE " + "tabel_cabang" + "("
                + "id_cabang" + " TEXT,"
                + "cabang" + " TEXT"
                + ")";
        db.execSQL(cabang);

        String kalender = "CREATE TABLE " + "tabel_kalender" + "("
                + "id_kalender" + " TEXT,"
                + "tanggal" + " TEXT,"
                + "agenda" + " TEXT,"
                + "keterangan" + " TEXT"
                + ")";
        db.execSQL(kalender);

        String kategori = "CREATE TABLE " + "tabel_kategori" + "("
                + "id_kategori" + " TEXT,"
                + "kategori" + " TEXT,"
                + "link" + " TEXT"
                + ")";
        db.execSQL(kategori);

        String kelulusan = "CREATE TABLE " + "tabel_kelulusan" + "("
                + "id_kelulusan" + " TEXT,"
                + "id_siswa" + " TEXT,"
                + "id_ta" + " TEXT,"
                + "id_kelas" + " TEXT,"
                + "angkatan" + " TEXT,"
                + "tgl_lulus" + " TEXT"
                + ")";
        db.execSQL(kelulusan);

        String pengampu = "CREATE TABLE " + "tabel_pengampu" + "("
                + "id_pengampu" + " TEXT,"
                + "nik" + " TEXT,"
                + "id_mapel" + " TEXT"
                + ")";
        db.execSQL(pengampu);

        String schedule = "CREATE TABLE " + "tabel_schedule" + "("
                + "id_schedule" + " TEXT,"
                + "tipe_sms" + " TEXT,"
                + "status" + " TEXT,"
                + "aturan" + " TEXT"
                + ")";
        db.execSQL(schedule);

        String user = "CREATE TABLE " + "tabel_user" + "("
                + "id_user" + " TEXT,"
                + "username" + " TEXT,"
                + "password" + " TEXT,"
                + "nama" + " TEXT,"
                + "level" + " TEXT,"
                + "id_cabang" + " TEXT,"
                + "avatar" + " TEXT,"
                + "tgl_daftar" + " TEXT"
                + ")";
        db.execSQL(user);

        String jamMasuk = "CREATE TABLE " + "tabel_jam_masuk" + "("
                + "id_jam_masuk" + " TEXT,"
                + "hari" + " TEXT,"
                + "jam" + " TEXT"
                + ")";
        db.execSQL(jamMasuk);

        String absenKehadiranGuru = "CREATE TABLE " + "tabel_absen_kehadiran_guru" + "("
                + "id_absen_kehadiran_guru" + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + "id_guru" + " TEXT,"
                + "tgl" + " TEXT,"
                + "jam" + " TEXT,"
                + "keterangan" + " TEXT"
                + ")";
        db.execSQL(absenKehadiranGuru);
        String daftarMapelTambahan = "CREATE TABLE " + "tabel_jadwal_tambahan" + "("
                + "id_jadwal_tambahan" + " INT,"
                + "nama_kelas" + " TEXT,"
                + "id_jenjang" + " TEXT,"
                + "id_guru" + " TEXT,"
                + "id_mapel" + " INT,"
                + "id_pembimbing_1" + " INT,"
                + "id_pembimbing_2" + " INT,"
                + "id_pembimbing_3" + " INT,"
                + "start_date" + " TEXT,"
                + "end_date" + " TEXT,"
                + "hari" + " TEXT,"
                + "jam_awal" + " TEXT,"
                + "jam_akhir" + " TEXT,"
                + "minggu_ke" + " INT"
                + ")";
        db.execSQL(daftarMapelTambahan);
        String kelasTambahan = "CREATE TABLE " + "tabel_kelas_tambahan" + "("
                + "id_kelas_tambahan" + " INT,"
                + "id_jadwal_tambahan" + " INT,"
                + "id_siswa" + " INT"
                + ")";
        db.execSQL(kelasTambahan);
        String daftarAbsenTambahan = "CREATE TABLE " + "tabel_absen_mapel_tambahan" + "("
                + "id_absen_mapel_tambahan" + " INT,"
                + "id_kelas_tambahan" + " INT,"
                + "id_jadwal_tambahan" + " INT,"
                + "tgl" + " TEXT,"
                + "jam" + " TEXT,"
                + "absen" + " TEXT,"
                + "valid" + " TEXT,"
                + "rekap" + " TEXT,"
                + "keterangan" + " TEXT"
                + ")";
        db.execSQL(daftarAbsenTambahan);

        String nonguru = "CREATE TABLE " + "tabel_non_guru" + "("
                + "id_non_guru" + " TEXT,"
                + "nip" + " TEXT,"
                + "nama" + " TEXT,"
                + "no_ktp" + " TEXT,"
                + "alias" + " TEXT,"
                + "jenis_kelamin" + " TEXT,"
                + "tempat_lahir" + " TEXT,"
                + "tgl_lahir" + " TEXT,"
                + "agama" + " TEXT,"
                + "no_hp" + " TEXT,"
                + "email" + " TEXT,"
                + "ijazah" + " TEXT,"
                + "pend_terakhir" + " TEXT,"
                + "gambar" + " TEXT,"
                + "alamat" + " TEXT,"
                + "unit_kerja" + " TEXT,"
                + "tmt" + " TEXT,"
                + "jabatan" + " TEXT"
                + ")";
        db.execSQL(nonguru);

        String absenNonGuru = "CREATE TABLE " + "tabel_absen_non_guru" + "("
                + "id_absen_non_guru" + " INTEGER primary key autoincrement,"
                + "id_non_guru" + " TEXT,"
                + "tgl" + " TEXT,"
                + "jam" + " TEXT,"
                + "keterangan" + " TEXT,"
                + "jenis" + " TEXT"
                + ")";
        db.execSQL(absenNonGuru);
    }

    // on Upgrade database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d("oldversion", String.valueOf(oldVersion));
        Log.d("newversion", String.valueOf(newVersion));
        if (oldVersion < 2) {
            db.execSQL("ALTER TABLE tabel_absen_mapel add column keterangan text");
        }
        if (oldVersion < 3) {
            db.execSQL("ALTER TABLE tabel_guru add column status_guru_bk text");
        }
        if (oldVersion < 4) {
            db.execSQL("ALTER TABLE tabel_setting add column lat text");
            db.execSQL("ALTER TABLE tabel_setting add column lng text");
            db.execSQL("ALTER TABLE tabel_setting add column radius text");
        }
        if (oldVersion < 5) {
            db.execSQL("ALTER TABLE tabel_guru add column status_kepsek text");
        }
        if (oldVersion < 6) {
            db.execSQL("ALTER TABLE tabel_setting add column jam_masuk text");
            db.execSQL("ALTER TABLE tabel_setting add column jam_pulang text");
        }
        if (oldVersion < 7) {
            db.execSQL("CREATE TABLE " + "tabel_jam_masuk" + "("
                    + "id_jam_masuk" + " TEXT,"
                    + "hari" + " TEXT,"
                    + "jam" + " TEXT"
                    + ")");
            db.execSQL("CREATE TABLE " + "tabel_absen_kehadiran_guru" + "("
                    + "id_absen_kehadiran_guru" + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + "id_guru" + " TEXT,"
                    + "tgl" + " TEXT,"
                    + "jam" + " TEXT,"
                    + "keterangan" + " TEXT"
                    + ")");
        }
        if (oldVersion < 8) {
            db.execSQL("drop table tabel_setting");
            String setting = "CREATE TABLE " + "tabel_setting" + "("
                    + "id_setting" + " TEXT,"
                    + "logo_mini" + " TEXT,"
                    + "logo_gambar" + " TEXT,"
                    + "logo_favicon" + " TEXT,"
                    + "nik" + " TEXT,"
                    + "kartu_gsm" + " TEXT,"
                    + "cek_pulsa" + " TEXT,"
                    + "metode" + " TEXT,"
                    + "kode_aktivasi" + " TEXT,"
                    + "link_tujuan" + " TEXT,"
                    + "logo_besar" + " TEXT,"
                    + "logo_sekolah" + " TEXT,"
                    + "pimpinan" + " TEXT,"
                    + "akun_gmail" + " TEXT,"
                    + "alamat" + " TEXT,"
                    + "website" + " TEXT,"
                    + "telat" + " TEXT,"
                    + "toleransi_jam_akhir" + " TEXT,"
                    + "lat" + " TEXT,"
                    + "lng" + " TEXT,"
                    + "radius" + " TEXT,"
                    + "jam_pulang" + " TEXT,"
                    + "jam_masuk" + " TEXT,"
                    + "kapasitas_kelas" + " TEXT"
                    + ")";
            db.execSQL(setting);

        }
    }

    public void insertGuru(final Context context) {
        final SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("INSERT INTO tabel_guru (nik) values ('haha')");

    }

    public ArrayList<Jadwal> debugTabelJadwal() {
        final SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from tabel_jadwal", null);
        ArrayList<Jadwal> haha = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                Jadwal jadwal = new Jadwal();
                jadwal.setIdguru(cursor.getString(cursor.getColumnIndex("id_guru")));
                jadwal.setHari(cursor.getString(cursor.getColumnIndex("hari")));
                haha.add(jadwal);
            } while (cursor.moveToNext());
        }
        return haha;
    }

    public SettingOffline getSetting() {

        final SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery("select * from tabel_setting limit 1", null);
        cursor.moveToFirst();

        Log.d(TAG, "getSetting: " + cursor.getColumnIndex("logo_gambar") + " , " + cursor.getCount());
        SettingOffline offline = new SettingOffline();
        if (cursor.getCount() > 0) {
            offline.setLogoMini(cursor.getString(cursor.getColumnIndex("logo_mini")));
            offline.setLogoGambar(cursor.getString(cursor.getColumnIndex("logo_gambar")));
            offline.setLogoFavicon(cursor.getString(cursor.getColumnIndex("logo_favicon")));
            offline.setNik(cursor.getString(cursor.getColumnIndex("nik")));
            offline.setKartuGsm(cursor.getString(cursor.getColumnIndex("kartu_gsm")));
            offline.setCekPulsa(cursor.getString(cursor.getColumnIndex("cek_pulsa")));
            offline.setMetode(cursor.getString(cursor.getColumnIndex("metode")));
            offline.setKodeAktivasi(cursor.getString(cursor.getColumnIndex("kode_aktivasi")));
            offline.setLinkTujuan(cursor.getString(cursor.getColumnIndex("link_tujuan")));
            offline.setLogoBesar(cursor.getString(cursor.getColumnIndex("logo_besar")));
            offline.setLogoSekolah(cursor.getString(cursor.getColumnIndex("logo_sekolah")));
            offline.setPimpinan(cursor.getString(cursor.getColumnIndex("pimpinan")));
            offline.setAkunGmail(cursor.getString(cursor.getColumnIndex("akun_gmail")));
            offline.setAlamat(cursor.getString(cursor.getColumnIndex("alamat")));
            offline.setTelat(cursor.getString(cursor.getColumnIndex("telat")));
            offline.setToleransiJamAkhir(cursor.getString(cursor.getColumnIndex("toleransi_jam_akhir")));
            offline.setLat(cursor.getString(cursor.getColumnIndex("lat")));
            offline.setLng(cursor.getString(cursor.getColumnIndex("lng")));
            offline.setRadius(cursor.getString(cursor.getColumnIndex("radius")));
            offline.setJamPulang(cursor.getString(cursor.getColumnIndex("jam_pulang")));
            offline.setJamMasuk(cursor.getString(cursor.getColumnIndex("jam_masuk")));

        }

        return offline;
    }

    public void syncUpdateTabelGuru(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog = new ProgressDialog(context);
            dialog.setMessage("Sync data...1/22");
            dialog.setCancelable(false);
            dialog.show();
            String query = "select * from tabel_guru where id_guru is null or id_guru = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Log.d("haha", String.valueOf(db.getVersion()));

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            db.execSQL("delete from tabel_guru");
            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<GuruOffline>> call = service.syncUpdateTabelGuru(param);
            call.enqueue(new Callback<List<GuruOffline>>() {
                @Override
                public void onResponse(Call<List<GuruOffline>> call, retrofit2.Response<List<GuruOffline>> response) {
                    List<GuruOffline> listGuruOffline = response.body();
                    String query = "";
                    for (GuruOffline guruOffline : listGuruOffline) {
                        query = "";
                        query += "INSERT INTO tabel_guru (id_guru, id_jenjang, nik, nama, no_ktp, alias, jenis_kelamin, tempat_lahir, tgl_lahir, agama, no_hp, email, ijazah, pend_terakhir, gambar, alamat, unit_kerja, tmt, status_guru_bk, status_kepsek) VALUES (";
                        query += "\"" + guruOffline.getIdGuru() + "\",";
                        query += "\"" + guruOffline.getIdJenjang() + "\",";
                        query += "\"" + guruOffline.getNik() + "\",";
                        query += "\"" + guruOffline.getNama() + "\",";
                        query += "\"" + guruOffline.getNoKtp() + "\",";
                        query += "\"" + guruOffline.getAlias() + "\",";
                        query += "\"" + guruOffline.getJenisKelamin() + "\",";
                        query += "\"" + guruOffline.getTempatLahir() + "\",";
                        query += "\"" + guruOffline.getTglLahir() + "\",";
                        query += "\"" + guruOffline.getAgama() + "\",";
                        query += "\"" + guruOffline.getNoHp() + "\",";
                        query += "\"" + guruOffline.getEmail() + "\",";
                        query += "\"" + guruOffline.getIjazah() + "\",";
                        query += "\"" + guruOffline.getPendTerakhir() + "\",";
                        query += "\"" + guruOffline.getGambar() + "\",";
                        query += "\"" + guruOffline.getAlamat() + "\",";
                        query += "\"" + guruOffline.getUnitKerja() + "\",";
                        query += "\"" + guruOffline.getTmt() + "\",";
                        query += "\"" + guruOffline.getStatusGuruBk() + "\",";
                        query += "\"" + guruOffline.getStatusKepsek() + "\")";
                        db.execSQL(query);
                    }

                    syncUpdateTabelJadwal(context, idGuru);

                }

                @Override
                public void onFailure(Call<List<GuruOffline>> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateTabelNonGuru(final Context context, final String idNonGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog = new ProgressDialog(context);
            dialog.setMessage("Sync data...(tabel_non_guru)");
            dialog.setCancelable(false);
            dialog.show();
            String query = "select * from tabel_non_guru where id_non_guru is null or id_non_guru = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            db.execSQL("delete from tabel_non_guru");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            Map<String, String> param = new HashMap<>();
            param.put("id_non_guru", idNonGuru);

            Call<List<NonGuruOffline>> call = service.updateNonGuru(param);
            call.enqueue(new Callback<List<NonGuruOffline>>() {

                @Override
                public void onResponse(Call<List<NonGuruOffline>> call, retrofit2.Response<List<NonGuruOffline>> response) {
                    List<NonGuruOffline> listNonGuruOffline = response.body();

                    for (NonGuruOffline nonGuruOffline : listNonGuruOffline) {
                        String query = "";
                        query += "INSERT INTO tabel_non_guru (id_non_guru, nip, nama, no_ktp, alias, jenis_kelamin, " +
                                "tempat_lahir, tgl_lahir, agama, no_hp, email, ijazah, pend_terakhir, gambar, alamat, " +
                                "unit_kerja, tmt, jabatan) VALUES (";
                        query += "\"" + nonGuruOffline.getIdNonGuru() + "\",";
                        query += "\"" + nonGuruOffline.getNip() + "\",";
                        query += "\"" + nonGuruOffline.getNama() + "\",";
                        query += "\"" + nonGuruOffline.getNoKtp() + "\",";
                        query += "\"" + nonGuruOffline.getAlias() + "\",";
                        query += "\"" + nonGuruOffline.getJenisKelamin() + "\",";
                        query += "\"" + nonGuruOffline.getTempatLahir() + "\",";
                        query += "\"" + nonGuruOffline.getTglLahir() + "\",";
                        query += "\"" + nonGuruOffline.getAgama() + "\",";
                        query += "\"" + nonGuruOffline.getNoHp() + "\",";
                        query += "\"" + nonGuruOffline.getEmail() + "\",";
                        query += "\"" + nonGuruOffline.getIjazah() + "\",";
                        query += "\"" + nonGuruOffline.getPendTerakhir() + "\",";
                        query += "\"" + nonGuruOffline.getGambar() + "\",";
                        query += "\"" + nonGuruOffline.getAlamat() + "\",";
                        query += "\"" + nonGuruOffline.getUnitKerja() + "\",";
                        query += "\"" + nonGuruOffline.getTmt() + "\",";
                        query += "\"" + nonGuruOffline.getJabatan() + "\")";
                        db.execSQL(query);
                    }
                    syncUpdateTabelJadwal(context, idNonGuru);
                }

                @Override
                public void onFailure(Call<List<NonGuruOffline>> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateTabelJadwal(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog.setMessage("Sync data...2/22");
            String query = "select * from tabel_jadwal where id_jadwal is null or id_jadwal = ''";

            final SQLiteDatabase db = this.getWritableDatabase();


            db.execSQL("delete from tabel_jadwal");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<JadwalOffline>> call = service.syncUpdateTabelJadwal(param);
            call.enqueue(new Callback<List<JadwalOffline>>() {
                @Override
                public void onResponse(Call<List<JadwalOffline>> call, retrofit2.Response<List<JadwalOffline>> response) {
                    List<JadwalOffline> listJadwalOffline = response.body();
                    String query = "";

                    for (JadwalOffline jadwalOffline : listJadwalOffline) {
                        query = "";
                        query += "INSERT INTO tabel_jadwal (id_jadwal, id_ta, id_jenjang, id_kelas, id_mapel, id_guru, hari, jam_awal, jam_akhir, status) VALUES (";
                        query += "\"" + jadwalOffline.getIdJadwal() + "\",";
                        query += "\"" + jadwalOffline.getIdTa() + "\",";
                        query += "\"" + jadwalOffline.getIdJenjang() + "\",";
                        query += "\"" + jadwalOffline.getIdKelas() + "\",";
                        query += "\"" + jadwalOffline.getIdMapel() + "\",";
                        query += "\"" + jadwalOffline.getIdGuru() + "\",";
                        query += "\"" + jadwalOffline.getHari() + "\",";
                        query += "\"" + jadwalOffline.getJamAwal() + "\",";
                        query += "\"" + jadwalOffline.getJamAkhir() + "\",";
                        query += "\"" + jadwalOffline.getStatus() + "\")";
                        db.execSQL(query);
                    }

                    syncUpdateTabelJenjang(context, idGuru);

                }

                @Override
                public void onFailure(Call<List<JadwalOffline>> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });

        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateTabelJenjang(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog.setMessage("Sync data...3/22");
            String query = "select * from tabel_jenjang where id_jenjang is null or id_jenjang = ''";

            final SQLiteDatabase db = this.getWritableDatabase();


            db.execSQL("delete from tabel_jenjang");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<JenjangOffline>> call = service.syncUpdateTabelJenjang(param);
            call.enqueue(new Callback<List<JenjangOffline>>() {
                @Override
                public void onResponse(Call<List<JenjangOffline>> call, retrofit2.Response<List<JenjangOffline>> response) {
                    List<JenjangOffline> listJenjangOffline = response.body();

                    String query = "";

                    for (JenjangOffline jenjangOffline : listJenjangOffline) {
                        query = "";
                        query += "INSERT INTO tabel_jenjang (id_jenjang, jenjang, jam_masuk, jam_pulang) VALUES (";
                        query += "\"" + jenjangOffline.getIdJenjang() + "\",";
                        query += "\"" + jenjangOffline.getJenjang() + "\",";
                        query += "\"" + jenjangOffline.getJamMasuk() + "\",";
                        query += "\"" + jenjangOffline.getJamPulang() + "\")";
                        db.execSQL(query);
                    }

                    syncUpdateTabelKelas(context, idGuru);

                }

                @Override
                public void onFailure(Call<List<JenjangOffline>> call, Throwable t) {
                    // Log error here since request failed
//                    loading.dismiss();
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateTabelKelas(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog.setMessage("Sync data...4/22");
            String query = "select * from tabel_kelas where id_kelas is null or id_kelas = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            db.execSQL("delete from tabel_kelas");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<KelasOffline>> call = service.syncUpdateTabelKelas(param);
            call.enqueue(new Callback<List<KelasOffline>>() {
                @Override
                public void onResponse(Call<List<KelasOffline>> call, retrofit2.Response<List<KelasOffline>> response) {
                    List<KelasOffline> listKelasOffline = response.body();

                    String query = "";

                    for (KelasOffline kelasOffline : listKelasOffline) {
                        query = "";
                        query += "INSERT INTO tabel_kelas (id_kelas, kelas, id_guru, id_jenjang) VALUES (";
                        query += "\"" + kelasOffline.getIdKelas() + "\",";
                        query += "\"" + kelasOffline.getKelas() + "\",";
                        query += "\"" + kelasOffline.getIdGuru() + "\",";
                        query += "\"" + kelasOffline.getIdJenjang() + "\")";
                        db.execSQL(query);
                    }

                    syncUpdateTabelMapel(context, idGuru);

                }

                @Override
                public void onFailure(Call<List<KelasOffline>> call, Throwable t) {
                    // Log error here since request failed
//                    loading.dismiss();
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });

        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateTabelMapel(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog.setMessage("Sync data...5/22");
            String query = "select * from tabel_mapel where id_mapel is null or id_mapel = ''";

            final SQLiteDatabase db = this.getWritableDatabase();


            db.execSQL("delete from tabel_mapel");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<MapelOffline>> call = service.syncUpdateTabelMapel(param);
            call.enqueue(new Callback<List<MapelOffline>>() {
                @Override
                public void onResponse(Call<List<MapelOffline>> call, retrofit2.Response<List<MapelOffline>> response) {
//                    loading.dismiss();
                    List<MapelOffline> listMapelOffline = response.body();

                    String query = "";

                    for (MapelOffline mapelOffline : listMapelOffline) {
                        query = "";
                        query += "INSERT INTO tabel_mapel (id_mapel, id_jenjang, id_kat_mapel, mapel, kkm) VALUES (";
                        query += "\"" + mapelOffline.getIdMapel() + "\",";
                        query += "\"" + mapelOffline.getIdJenjang() + "\",";
                        query += "\"" + mapelOffline.getIdKatMapel() + "\",";
                        query += "\"" + mapelOffline.getMapel() + "\",";
                        query += "\"" + mapelOffline.getKkm() + "\")";
                        db.execSQL(query);
                    }

                    syncUpdateTabelSetting(context, idGuru);
                }

                @Override
                public void onFailure(Call<List<MapelOffline>> call, Throwable t) {
                    // Log error here since request failed
//                    loading.dismiss();
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });

        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateTabelSetting(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog.setMessage("Sync data...6/22");
            String query = "select * from tabel_setting where id_setting is null or id_setting = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            db.execSQL("delete from tabel_setting");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<SettingOffline>> call = service.syncUpdateTabelSetting(param);
            call.enqueue(new Callback<List<SettingOffline>>() {
                @Override
                public void onResponse(Call<List<SettingOffline>> call, retrofit2.Response<List<SettingOffline>> response) {
//                    loading.dismiss();
                    List<SettingOffline> listSettingOffline = response.body();

                    String query = "";

                    for (SettingOffline settingOffline : listSettingOffline) {
                        query = "";
                        query += "INSERT INTO tabel_setting (id_setting, logo_mini, logo_gambar, logo_favicon, nik, " +
                                "kartu_gsm, cek_pulsa, metode, kode_aktivasi, link_tujuan, logo_besar, logo_sekolah, " +
                                "pimpinan, akun_gmail, alamat, website, telat, toleransi_jam_akhir, lat, lng, radius, jam_pulang,jam_masuk,kapasitas_kelas) VALUES (";
                        query += "\"" + settingOffline.getIdSetting() + "\",";
                        query += "\"" + settingOffline.getLogoMini() + "\",";
                        query += "\"" + settingOffline.getLogoGambar() + "\",";
                        query += "\"" + settingOffline.getLogoFavicon() + "\",";
                        query += "\"" + settingOffline.getNik() + "\",";
                        query += "\"" + settingOffline.getKartuGsm() + "\",";
                        query += "\"" + settingOffline.getCekPulsa() + "\",";
                        query += "\"" + settingOffline.getMetode() + "\",";
                        query += "\"" + settingOffline.getKodeAktivasi() + "\",";
                        query += "\"" + settingOffline.getLinkTujuan() + "\",";
                        query += "\"" + settingOffline.getLogoBesar() + "\",";
                        query += "\"" + settingOffline.getLogoSekolah() + "\",";
                        query += "\"" + settingOffline.getPimpinan() + "\",";
                        query += "\"" + settingOffline.getAkunGmail() + "\",";
                        query += "\"" + settingOffline.getAlamat() + "\",";
                        query += "\"" + settingOffline.getWebsite() + "\",";
                        query += "\"" + settingOffline.getTelat() + "\",";
                        query += "\"" + settingOffline.getToleransiJamAkhir() + "\",";
                        query += "\"" + settingOffline.getLat() + "\",";
                        query += "\"" + settingOffline.getLng() + "\",";
                        query += "\"" + settingOffline.getRadius() + "\",";
                        query += "\"" + settingOffline.getJamPulang() + "\",";
                        query += "\"" + settingOffline.getJamMasuk() + "\",";
                        query += "\"" + settingOffline.getKapasitasKelas() + "\")";

                        db.execSQL(query);
                    }

                    syncUpdateTabelAbsenMapel(context, idGuru);

                }

                @Override
                public void onFailure(Call<List<SettingOffline>> call, Throwable t) {
                    Log.d(TAG, "onFailure: gagal di tabel_setting");
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });

//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Log.d("setting", (error.getMessage()));
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateTabelAbsenMapel(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog.setMessage("Sync data...7/22");
            String query = "select * from tabel_absen_mapel where id_absen_mapel is null or id_absen_mapel = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            db.execSQL("delete from tabel_absen_mapel");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<AbsenMapelOffline>> call = service.syncUpdateTabelAbsenMapel(param);
            call.enqueue(new Callback<List<AbsenMapelOffline>>() {
                @Override
                public void onResponse(Call<List<AbsenMapelOffline>> call, retrofit2.Response<List<AbsenMapelOffline>> response) {
//                    loading.dismiss();
                    List<AbsenMapelOffline> listAbsenMapelOffline = response.body();

                    String query = "";

                    for (AbsenMapelOffline absenMapelOffline : listAbsenMapelOffline) {
                        query = "";
                        query += "INSERT INTO tabel_absen_mapel (id_absen_mapel, id_penempatan, id_jadwal, tgl, jam, absen, valid, rekap, status, keterangan) VALUES (";
                        query += "\"" + absenMapelOffline.getIdAbsenMapel() + "\",";
                        query += "\"" + absenMapelOffline.getIdPenempatan() + "\",";
                        query += "\"" + absenMapelOffline.getIdJadwal() + "\",";
                        query += "\"" + absenMapelOffline.getTgl() + "\",";
                        query += "\"" + absenMapelOffline.getJam() + "\",";
                        query += "\"" + absenMapelOffline.getAbsen() + "\",";
                        query += "\"" + absenMapelOffline.getValid() + "\",";
                        query += "\"" + absenMapelOffline.getRekap() + "\",";
                        query += "\"" + absenMapelOffline.getStatus() + "\",";
                        query += "\"" + absenMapelOffline.getKeterangan() + "\")";

                        db.execSQL(query);
                    }

                    syncUpdateTabelSiswakelas(context, idGuru);

                }

                @Override
                public void onFailure(Call<List<AbsenMapelOffline>> call, Throwable t) {
                    // Log error here since request failed
//                    loading.dismiss();
                    Log.d(TAG, "onFailure: gagal di tabel_absen_mapel");
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });

//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Log.d("absen_mapel", (error.getMessage()));
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateTabelSiswakelas(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog.setMessage("Sync data...8/22");
            String query = "select * from tabel_siswakelas where id_penempatan is null or id_penempatan = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

//            Cursor cursor = db.rawQuery(query, null);
//
//            // looping through all rows and adding to list
//            query = "";
//            if (cursor.moveToFirst()) {
//                Boolean first = true;
//                do {
//                    if (first) {
//                        query += "INSERT INTO tabel_siswakelas (id_ta, id_kelas, id_siswa) VALUES (";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_ta")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_kelas")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_siswa")) + "\")";
//                    }
//                    else {
//                        query += ",(";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_ta")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_kelas")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_siswa")) + "\")";
//                    }
//
//                    first = false;
//                } while (cursor.moveToNext());
//            }
//
//            SessionConfig sessionConfig = new SessionConfig(context);
//            final String ip = sessionConfig.preferences.getString("ipconfig",null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {

            db.execSQL("delete from tabel_siswakelas");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//                            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/siswakelas_offline.php"+"?id_guru="+idGuru,null,
//                                    new Response.Listener<JSONArray>() {
//                                        @Override
//                                        public void onResponse(JSONArray response) {
//                                            int count = 0;
//                                            String query = "";
//                                            while (count < response.length()){
//                                                try{
//                                                    JSONObject object =  response.getJSONObject(count);
//                                                    query = "";
//                                                    query += "INSERT INTO tabel_siswakelas (id_penempatan, id_ta, id_kelas, id_siswa) VALUES (";
//                                                    query += "\"" + object.getString("id_penempatan") + "\",";
//                                                    query += "\"" + object.getString("id_ta") + "\",";
//                                                    query += "\"" + object.getString("id_kelas") + "\",";
//                                                    query += "\"" + object.getString("id_siswa") + "\")";
//
//                                                    db.execSQL(query);
//                                                    count++;
//                                                } catch (JSONException e) {
//                                                    e.printStackTrace();
//                                                }
//                                            }
////                                            db.execSQL(query);
//                                            syncUpdateTabelSiswa(context, idGuru);
//
//                                        }
//                                    },
//                                    new Response.ErrorListener() {
//                                        @Override
//                                        public void onErrorResponse(VolleyError error) {
//                                            dialog.dismiss();
//                                            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
//
//                                        }
//                                    });
//
//                            //Creating a request queue
//                            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//                            //Adding request to the queue
//                            requestQueue.add(jsonArrayRequest);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<SiswakelasOffline>> call = service.syncUpdateTabelSiswakelas(param);
            call.enqueue(new Callback<List<SiswakelasOffline>>() {
                @Override
                public void onResponse(Call<List<SiswakelasOffline>> call, retrofit2.Response<List<SiswakelasOffline>> response) {
//                    loading.dismiss();
                    List<SiswakelasOffline> listSiswakelasOffline = response.body();

                    String query = "";

                    for (SiswakelasOffline siswakelasOffline : listSiswakelasOffline) {
                        query = "";
                        query += "INSERT INTO tabel_siswakelas (id_penempatan, id_ta, id_kelas, id_siswa) VALUES (";
                        query += "\"" + siswakelasOffline.getIdPenempatan() + "\",";
                        query += "\"" + siswakelasOffline.getIdTa() + "\",";
                        query += "\"" + siswakelasOffline.getIdKelas() + "\",";
                        query += "\"" + siswakelasOffline.getIdSiswa() + "\")";

                        db.execSQL(query);
                    }

                    syncUpdateTabelSiswa(context, idGuru);

                }

                @Override
                public void onFailure(Call<List<SiswakelasOffline>> call, Throwable t) {
                    // Log error here since request failed
//                    loading.dismiss();
                    Log.d(TAG, "onFailure: gagal di tabel_siswa_kelas");
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Log.d("siswakelas", (error.getMessage()));
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateTabelSiswa(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog.setMessage("Sync data...9/22");
            String query = "select * from tabel_siswa where id_siswa is null or id_siswa = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

//            Cursor cursor = db.rawQuery(query, null);
//
//            // looping through all rows and adding to list
//            query = "";
//            if (cursor.moveToFirst()) {
//                Boolean first = true;
//                do {
//                    if (first) {
//                        query += "INSERT INTO tabel_siswa (id_jenjang, nama_lengkap, nisn, jenis_kelamin, tempat_lahir, tgl_lahir, agama, alamat, no_hp, email, asal_sekolah, ket_asal, nama_ayah, nama_ibu, gambar, tgl_daftar) VALUES (";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_jenjang")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("nama_lengkap")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("nisn")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("jenis_kelamin")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("tempat_lahir")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_lahir")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("agama")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("alamat")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("no_hp")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("email")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("asal_sekolah")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("ket_asal")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("nama_ayah")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("nama_ibu")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("gambar")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_daftar")) + "\")";
//                    }
//                    else {
//                        query += ",(";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_jenjang")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("nama_lengkap")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("nisn")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("jenis_kelamin")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("tempat_lahir")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_lahir")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("agama")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("alamat")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("no_hp")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("email")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("asal_sekolah")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("ket_asal")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("nama_ayah")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("nama_ibu")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("gambar")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_daftar")) + "\")";
//                    }
//
//                    first = false;
//                } while (cursor.moveToNext());
//            }
//
//            SessionConfig sessionConfig = new SessionConfig(context);
//            final String ip = sessionConfig.preferences.getString("ipconfig",null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {

            db.execSQL("delete from tabel_siswa");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//                            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/siswa_offline.php"+"?id_guru="+idGuru,null,
//                                    new Response.Listener<JSONArray>() {
//                                        @Override
//                                        public void onResponse(JSONArray response) {
//                                            int count = 0;
//                                            String query = "";
//                                            while (count < response.length()){
//                                                try{
//                                                    JSONObject object =  response.getJSONObject(count);
//                                                    query = "";
//                                                    query += "INSERT INTO tabel_siswa (id_siswa, id_jenjang, nama_lengkap, nisn, jenis_kelamin, tempat_lahir, tgl_lahir, agama, alamat, no_hp, email, asal_sekolah, ket_asal, nama_ayah, nama_ibu, gambar, tgl_daftar) VALUES (";
//                                                    query += "\"" + object.getString("id_siswa") + "\",";
//                                                    query += "\"" + object.getString("id_jenjang") + "\",";
//                                                    query += "\"" + object.getString("nama_lengkap") + "\",";
//                                                    query += "\"" + object.getString("nisn") + "\",";
//                                                    query += "\"" + object.getString("jenis_kelamin") + "\",";
//                                                    query += "\"" + object.getString("tempat_lahir") + "\",";
//                                                    query += "\"" + object.getString("tgl_lahir") + "\",";
//                                                    query += "\"" + object.getString("agama") + "\",";
//                                                    query += "\"" + object.getString("alamat") + "\",";
//                                                    query += "\"" + object.getString("no_hp") + "\",";
//                                                    query += "\"" + object.getString("email") + "\",";
//                                                    query += "\"" + object.getString("asal_sekolah") + "\",";
//                                                    query += "\"" + object.getString("ket_asal") + "\",";
//                                                    query += "\"" + object.getString("nama_ayah") + "\",";
//                                                    query += "\"" + object.getString("nama_ibu") + "\",";
//                                                    query += "\"" + object.getString("gambar") + "\",";
//                                                    query += "\"" + object.getString("tgl_daftar") + "\")";
//
//                                                    db.execSQL(query);
//                                                    count++;
//                                                } catch (JSONException e) {
//                                                    e.printStackTrace();
//                                                }
//                                            }
////                                            db.execSQL(query);
//                                            syncUpdateTabelTahunajaran(context, idGuru);
//                                        }
//                                    },
//                                    new Response.ErrorListener() {
//                                        @Override
//                                        public void onErrorResponse(VolleyError error) {
//                                            dialog.dismiss();
//                                            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
//
//                                        }
//                                    });
//
//                            //Creating a request queue
//                            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//                            //Adding request to the queue
//                            requestQueue.add(jsonArrayRequest);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<SiswaOffline>> call = service.syncUpdateTabelSiswa(param);
            call.enqueue(new Callback<List<SiswaOffline>>() {
                @Override
                public void onResponse(Call<List<SiswaOffline>> call, retrofit2.Response<List<SiswaOffline>> response) {
//                    loading.dismiss();
                    List<SiswaOffline> listSiswaOffline = response.body();

                    String query = "";

                    for (SiswaOffline siswaOffline : listSiswaOffline) {
                        query = "";
                        query += "INSERT INTO tabel_siswa (id_siswa, id_jenjang, nama_lengkap, nisn, jenis_kelamin, tempat_lahir, tgl_lahir, agama, alamat, no_hp, email, asal_sekolah, ket_asal, nama_ayah, nama_ibu, gambar, tgl_daftar) VALUES (";
                        query += "\"" + siswaOffline.getIdSiswa() + "\",";
                        query += "\"" + siswaOffline.getIdJenjang() + "\",";
                        query += "\"" + siswaOffline.getNamaLengkap() + "\",";
                        query += "\"" + siswaOffline.getNisn() + "\",";
                        query += "\"" + siswaOffline.getJenisKelamin() + "\",";
                        query += "\"" + siswaOffline.getTempatLahir() + "\",";
                        query += "\"" + siswaOffline.getTglLahir() + "\",";
                        query += "\"" + siswaOffline.getAgama() + "\",";
                        query += "\"" + siswaOffline.getAlamat() + "\",";
                        query += "\"" + siswaOffline.getNoHp() + "\",";
                        query += "\"" + siswaOffline.getEmail() + "\",";
                        query += "\"" + siswaOffline.getAsalSekolah() + "\",";
                        query += "\"" + siswaOffline.getKetAsal() + "\",";
                        query += "\"" + siswaOffline.getNamaAyah() + "\",";
                        query += "\"" + siswaOffline.getNamaIbu() + "\",";
                        query += "\"" + siswaOffline.getGambar() + "\",";
                        query += "\"" + siswaOffline.getTglDaftar() + "\")";

                        db.execSQL(query);
                    }

                    syncUpdateTabelTahunajaran(context, idGuru);

                }

                @Override
                public void onFailure(Call<List<SiswaOffline>> call, Throwable t) {
                    // Log error here since request failed
//                    loading.dismiss();
                    Log.d(TAG, "onFailure: gagal di tabel_siswa");
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });

        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateTabelTahunajaran(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog.setMessage("Sync data...10/22");
            String query = "select * from tabel_tahunajaran where id_ta is null or id_ta = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            db.execSQL("delete from tabel_tahunajaran");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<TahunajaranOffline>> call = service.syncUpdateTabelTahunajaran(param);
            call.enqueue(new Callback<List<TahunajaranOffline>>() {
                @Override
                public void onResponse(Call<List<TahunajaranOffline>> call, retrofit2.Response<List<TahunajaranOffline>> response) {
//                    loading.dismiss();
                    List<TahunajaranOffline> listTahunajaranOffline = response.body();

                    String query = "";

                    for (TahunajaranOffline tahunajaranOffline : listTahunajaranOffline) {
                        query = "";
                        query += "INSERT INTO tabel_tahunajaran (id_ta, tahun_ajaran, tgl_awal, tgl_akhir,status) VALUES (";
                        query += "\"" + tahunajaranOffline.getIdTa() + "\",";
                        query += "\"" + tahunajaranOffline.getTahunAjaran() + "\",";
                        query += "\"" + tahunajaranOffline.getTglAwal() + "\",";
                        query += "\"" + tahunajaranOffline.getTglAkhir() + "\",";
                        query += "\"" + tahunajaranOffline.getStatus() + "\")";

                        db.execSQL(query);
                    }

                    syncUpdateTabelKatMapel(context, idGuru);

                }

                @Override
                public void onFailure(Call<List<TahunajaranOffline>> call, Throwable t) {
                    // Log error here since request failed
//                    loading.dismiss();
                    Log.d(TAG, "onFailure: gagal di tabel_tahun_ajaran");
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Log.d("tahunajaran", (error.getMessage()));
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateTabelKatMapel(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog.setMessage("Sync data...11/22");
            String query = "select * from tabel_kat_mapel where id_kat_mapel is null or id_kat_mapel = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

//            Cursor cursor = db.rawQuery(query, null);
//
//            // looping through all rows and adding to list
//            query = "";
//            if (cursor.moveToFirst()) {
//                Boolean first = true;
//                do {
//                    if (first) {
//                        query += "INSERT INTO tabel_kat_mapel (kategori_mapel) VALUES (";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("kategori_mapel")) + "\")";
//                    }
//                    else {
//                        query += ",(";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("kategori_mapel")) + "\")";
//                    }
//
//                    first = false;
//                } while (cursor.moveToNext());
//            }
//
//            SessionConfig sessionConfig = new SessionConfig(context);
//            final String ip = sessionConfig.preferences.getString("ipconfig",null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {

            db.execSQL("delete from tabel_kat_mapel");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//                            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/kat_mapel_offline.php"+"?id_guru="+idGuru,null,
//                                    new Response.Listener<JSONArray>() {
//                                        @Override
//                                        public void onResponse(JSONArray response) {
//                                            int count = 0;
//                                            String query = "";
//                                            while (count < response.length()){
//                                                try{
//                                                    JSONObject object =  response.getJSONObject(count);
//                                                    query = "";
//                                                    query += "INSERT INTO tabel_kat_mapel (id_kat_mapel, kategori_mapel) VALUES (";
//                                                    query += "\"" + object.getString("id_kat_mapel") + "\",";
//                                                    query += "\"" + object.getString("kategori_mapel") + "\")";
//
//                                                    db.execSQL(query);
//                                                    count++;
//                                                } catch (JSONException e) {
//                                                    e.printStackTrace();
//                                                }
//                                            }
////                                            db.execSQL(query);
//                                            syncUpdateTabelAbsen(context, idGuru);
//                                        }
//                                    },
//                                    new Response.ErrorListener() {
//                                        @Override
//                                        public void onErrorResponse(VolleyError error) {
//                                            dialog.dismiss();
//                                            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
//
//                                        }
//                                    });
//
//                            //Creating a request queue
//                            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//                            //Adding request to the queue
//                            requestQueue.add(jsonArrayRequest);
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Log.d("kat_mapel", (error.getMessage()));
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<KatMapelOffline>> call = service.syncUpdateTabelKatMapel(param);
            call.enqueue(new Callback<List<KatMapelOffline>>() {
                @Override
                public void onResponse(Call<List<KatMapelOffline>> call, retrofit2.Response<List<KatMapelOffline>> response) {
//                    loading.dismiss();
                    List<KatMapelOffline> listKatMapelOffline = response.body();

                    String query = "";

                    for (KatMapelOffline katMapelOffline : listKatMapelOffline) {
                        query = "";
                        query += "INSERT INTO tabel_kat_mapel (id_kat_mapel, kategori_mapel) VALUES (";
                        query += "\"" + katMapelOffline.getIdKatMapel() + "\",";
                        query += "\"" + katMapelOffline.getKategoriMapel() + "\")";

                        db.execSQL(query);
                    }

                    syncUpdateTabelAbsen(context, idGuru);

                }

                @Override
                public void onFailure(Call<List<KatMapelOffline>> call, Throwable t) {
                    // Log error here since request failed
//                    loading.dismiss();
                    Log.d(TAG, "onFailure: gagal di tabel_kat_mapel");
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateTabelAbsen(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog.setMessage("Sync data...12/22");
            String query = "select * from tabel_absen where id_absen is null or id_absen = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

//            Cursor cursor = db.rawQuery(query, null);
//
//            // looping through all rows and adding to list
//            query = "";
//            if (cursor.moveToFirst()) {
//                Boolean first = true;
//                do {
//                    if (first) {
//                        query += "INSERT INTO tabel_absen (id_penempatan, tgl, jam_masuk, absen_masuk, valid_masuk, sms_masuk, jam_pulang, absen_pulang, valid_pulang, sms_pulang) VALUES (";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_penempatan")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam_masuk")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("absen_masuk")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("valid_masuk")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("sms_masuk")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam_pulang")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("absen_pulang")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("valid_pulang")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("sms_pulang")) + "\")";
//
//
//                    }
//                    else {
//                        query += ",(";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_penempatan")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam_masuk")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("absen_masuk")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("valid_masuk")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("sms_masuk")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam_pulang")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("absen_pulang")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("valid_pulang")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("sms_pulang")) + "\")";
//                    }
//
//                    first = false;
//                } while (cursor.moveToNext());
//            }
//
//            SessionConfig sessionConfig = new SessionConfig(context);
//            final String ip = sessionConfig.preferences.getString("ipconfig",null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {

            db.execSQL("delete from tabel_absen");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//                            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/absen_offline.php"+"?id_guru="+idGuru,null,
//                                    new Response.Listener<JSONArray>() {
//                                        @Override
//                                        public void onResponse(JSONArray response) {
//                                            int count = 0;
//                                            String query = "";
//                                            while (count < response.length()){
//                                                try{
//                                                    JSONObject object =  response.getJSONObject(count);
//                                                    query = "";
//                                                    query += "INSERT INTO tabel_absen (id_absen, id_penempatan, tgl, jam_masuk, absen_masuk, valid_masuk, sms_masuk, jam_pulang, absen_pulang, valid_pulang, sms_pulang) VALUES (";
//                                                    query += "\"" + object.getString("id_absen") + "\",";
//                                                    query += "\"" + object.getString("id_penempatan") + "\",";
//                                                    query += "\"" + object.getString("tgl") + "\",";
//                                                    query += "\"" + object.getString("jam_masuk") + "\",";
//                                                    query += "\"" + object.getString("absen_masuk") + "\",";
//                                                    query += "\"" + object.getString("valid_masuk") + "\",";
//                                                    query += "\"" + object.getString("sms_masuk") + "\",";
//                                                    query += "\"" + object.getString("jam_pulang") + "\",";
//                                                    query += "\"" + object.getString("absen_pulang") + "\",";
//                                                    query += "\"" + object.getString("valid_pulang") + "\",";
//                                                    query += "\"" + object.getString("sms_pulang") + "\")";
//
//                                                    db.execSQL(query);
//                                                    count++;
//                                                } catch (JSONException e) {
//                                                    e.printStackTrace();
//                                                }
//                                            }
////                                            db.execSQL(query);
//                                            syncUpdateTabelCabang(context, idGuru);
//                                        }
//                                    },
//                                    new Response.ErrorListener() {
//                                        @Override
//                                        public void onErrorResponse(VolleyError error) {
//                                            dialog.dismiss();
//                                            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
//
//                                        }
//                                    });
//
//                            //Creating a request queue
//                            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//                            //Adding request to the queue
//                            requestQueue.add(jsonArrayRequest);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<AbsenOffline>> call = service.syncUpdateTabelAbsen(param);
            call.enqueue(new Callback<List<AbsenOffline>>() {
                @Override
                public void onResponse(Call<List<AbsenOffline>> call, retrofit2.Response<List<AbsenOffline>> response) {
//                    loading.dismiss();
                    List<AbsenOffline> listAbsenOffline = response.body();

                    String query = "";

                    for (AbsenOffline absenOffline : listAbsenOffline) {
                        query = "";
                        query += "INSERT INTO tabel_absen (id_absen, id_penempatan, tgl, jam_masuk, absen_masuk, valid_masuk, sms_masuk, jam_pulang, absen_pulang, valid_pulang, sms_pulang) VALUES (";
                        query += "\"" + absenOffline.getIdAbsen() + "\",";
                        query += "\"" + absenOffline.getIdPenempatan() + "\",";
                        query += "\"" + absenOffline.getTgl() + "\",";
                        query += "\"" + absenOffline.getJamMasuk() + "\",";
                        query += "\"" + absenOffline.getAbsenMasuk() + "\",";
                        query += "\"" + absenOffline.getValidMasuk() + "\",";
                        query += "\"" + absenOffline.getSmsMasuk() + "\",";
                        query += "\"" + absenOffline.getJamPulang() + "\",";
                        query += "\"" + absenOffline.getAbsenPulang() + "\",";
                        query += "\"" + absenOffline.getValidPulang() + "\",";
                        query += "\"" + absenOffline.getSmsPulang() + "\")";

                        db.execSQL(query);
                    }

                    syncUpdateTabelCabang(context, idGuru);

                }

                @Override
                public void onFailure(Call<List<AbsenOffline>> call, Throwable t) {
                    // Log error here since request failed
//                    loading.dismiss();
                    Log.d(TAG, "onFailure: gagal di tabel_absen");
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Log.d("absen", (error.getMessage()));
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateTabelCabang(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog.setMessage("Sync data...13/22");
            String query = "select * from tabel_cabang where id_cabang is null or id_cabang = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

//            Cursor cursor = db.rawQuery(query, null);
//
//            // looping through all rows and adding to list
//            query = "";
//            if (cursor.moveToFirst()) {
//                Boolean first = true;
//                do {
//                    if (first) {
//                        query += "INSERT INTO tabel_cabang (cabang) VALUES (";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("cabang")) + "\")";
//
//
//                    }
//                    else {
//                        query += ",(";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("cabang")) + "\")";
//                    }
//
//                    first = false;
//                } while (cursor.moveToNext());
//            }
//
//            SessionConfig sessionConfig = new SessionConfig(context);
//            final String ip = sessionConfig.preferences.getString("ipconfig",null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {

            db.execSQL("delete from tabel_cabang");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//                            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/cabang_offline.php"+"?id_guru="+idGuru,null,
//                                    new Response.Listener<JSONArray>() {
//                                        @Override
//                                        public void onResponse(JSONArray response) {
//                                            int count = 0;
//                                            String query = "";
//                                            while (count < response.length()){
//                                                try{
//                                                    JSONObject object =  response.getJSONObject(count);
//                                                    query = "";
//                                                    query += "INSERT INTO tabel_cabang (id_cabang, cabang) VALUES (";
//                                                    query += "\"" + object.getString("id_cabang") + "\",";
//                                                    query += "\"" + object.getString("cabang") + "\")";
//
//                                                    db.execSQL(query);
//                                                    count++;
//                                                } catch (JSONException e) {
//                                                    e.printStackTrace();
//                                                }
//                                            }
////                                            db.execSQL(query);
//                                            syncUpdateTabelKalender(context, idGuru);
//                                        }
//                                    },
//                                    new Response.ErrorListener() {
//                                        @Override
//                                        public void onErrorResponse(VolleyError error) {
//                                            dialog.dismiss();
//                                            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
//
//                                        }
//                                    });
//
//                            //Creating a request queue
//                            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//                            //Adding request to the queue
//                            requestQueue.add(jsonArrayRequest);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<CabangOffline>> call = service.syncUpdateTabelCabang(param);
            call.enqueue(new Callback<List<CabangOffline>>() {
                @Override
                public void onResponse(Call<List<CabangOffline>> call, retrofit2.Response<List<CabangOffline>> response) {
//                    loading.dismiss();
                    List<CabangOffline> listCabangOffline = response.body();

                    String query = "";

                    for (CabangOffline cabangOffline : listCabangOffline) {
                        query = "";
                        query += "INSERT INTO tabel_cabang (id_cabang, cabang) VALUES (";
                        query += "\"" + cabangOffline.getIdCabang() + "\",";
                        query += "\"" + cabangOffline.getCabang() + "\")";

                        db.execSQL(query);
                    }

                    syncUpdateTabelKalender(context, idGuru);

                }

                @Override
                public void onFailure(Call<List<CabangOffline>> call, Throwable t) {
                    // Log error here since request failed
//                    loading.dismiss();
                    Log.d(TAG, "onFailure: gagal di tabel_cabang");
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Log.d("cabang", (error.getMessage()));
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateTabelKalender(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog.setMessage("Sync data...14/22");
            String query = "select * from tabel_kalender where id_kalender is null or id_kalender = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

//            Cursor cursor = db.rawQuery(query, null);
//
//            // looping through all rows and adding to list
//            query = "";
//            if (cursor.moveToFirst()) {
//                Boolean first = true;
//                do {
//                    if (first) {
//                        query += "INSERT INTO tabel_kalender (tanggal, agenda, keterangan) VALUES (";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("tanggal")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("agenda")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("keterangan")) + "\")";
//
//                    }
//                    else {
//                        query += ",(";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("tanggal")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("agenda")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("keterangan")) + "\")";
//                    }
//
//                    first = false;
//                } while (cursor.moveToNext());
//            }
//
//            SessionConfig sessionConfig = new SessionConfig(context);
//            final String ip = sessionConfig.preferences.getString("ipconfig",null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {

            db.execSQL("delete from tabel_kalender");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//                            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/kalender_offline.php"+"?id_guru="+idGuru,null,
//                                    new Response.Listener<JSONArray>() {
//                                        @Override
//                                        public void onResponse(JSONArray response) {
//                                            int count = 0;
//                                            String query = "";
//                                            while (count < response.length()){
//                                                try{
//                                                    JSONObject object =  response.getJSONObject(count);
//                                                    query = "";
//                                                    query += "INSERT INTO tabel_kalender (id_kalender, tanggal, agenda, keterangan) VALUES (";
//                                                    query += "\"" + object.getString("id_kalender") + "\",";
//                                                    query += "\"" + object.getString("tanggal") + "\",";
//                                                    query += "\"" + object.getString("agenda") + "\",";
//                                                    query += "\"" + object.getString("keterangan") + "\")";
//
//                                                    db.execSQL(query);
//                                                    count++;
//                                                } catch (JSONException e) {
//                                                    e.printStackTrace();
//                                                }
//                                            }
////                                            db.execSQL(query);
//                                            syncUpdateTabelKategori(context, idGuru);
//                                        }
//                                    },
//                                    new Response.ErrorListener() {
//                                        @Override
//                                        public void onErrorResponse(VolleyError error) {
//                                            dialog.dismiss();
//                                            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
//
//                                        }
//                                    });
//
//                            //Creating a request queue
//                            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//                            //Adding request to the queue
//                            requestQueue.add(jsonArrayRequest);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<KalenderOffline>> call = service.syncUpdateTabelKalender(param);
            call.enqueue(new Callback<List<KalenderOffline>>() {
                @Override
                public void onResponse(Call<List<KalenderOffline>> call, retrofit2.Response<List<KalenderOffline>> response) {
//                    loading.dismiss();
                    List<KalenderOffline> listKalenderOffline = response.body();

                    String query = "";

                    for (KalenderOffline kalenderOffline : listKalenderOffline) {
                        query = "";
                        query += "INSERT INTO tabel_kalender (id_kalender, tanggal, agenda, keterangan) VALUES (";
                        query += "\"" + kalenderOffline.getIdKalender() + "\",";
                        query += "\"" + kalenderOffline.getTanggal() + "\",";
                        query += "\"" + kalenderOffline.getAgenda() + "\",";
                        query += "\"" + kalenderOffline.getKeterangan() + "\")";

                        db.execSQL(query);
                    }

                    syncUpdateTabelKategori(context, idGuru);

                }

                @Override
                public void onFailure(Call<List<KalenderOffline>> call, Throwable t) {
                    // Log error here since request failed
//                    loading.dismiss();
                    Log.d(TAG, "onFailure: gagal di tabel_kalender");
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Log.d("kalender", (error.getMessage()));
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateTabelKategori(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog.setMessage("Sync data...15/22");
            String query = "select * from tabel_kategori where id_kategori is null or id_kategori = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

//            Cursor cursor = db.rawQuery(query, null);
//
//            // looping through all rows and adding to list
//            query = "";
//            if (cursor.moveToFirst()) {
//                Boolean first = true;
//                do {
//                    if (first) {
//                        query += "INSERT INTO tabel_kategori (kategori, link) VALUES (";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("kategori")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("link")) + "\")";
//
//                    }
//                    else {
//                        query += ",(";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("kategori")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("link")) + "\")";
//                    }
//
//                    first = false;
//                } while (cursor.moveToNext());
//            }
//
//            SessionConfig sessionConfig = new SessionConfig(context);
//            final String ip = sessionConfig.preferences.getString("ipconfig",null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {

            db.execSQL("delete from tabel_kategori");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//                            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/kategori_offline.php"+"?id_guru="+idGuru,null,
//                                    new Response.Listener<JSONArray>() {
//                                        @Override
//                                        public void onResponse(JSONArray response) {
//                                            int count = 0;
//                                            String query = "";
//                                            while (count < response.length()){
//                                                try{
//                                                    JSONObject object =  response.getJSONObject(count);
//                                                    query = "";
//                                                    query += "INSERT INTO tabel_kategori (id_kategori, kategori, link) VALUES (";
//                                                    query += "\"" + object.getString("id_kategori") + "\",";
//                                                    query += "\"" + object.getString("kategori") + "\",";
//                                                    query += "\"" + object.getString("link") + "\")";
//
//                                                    db.execSQL(query);
//                                                    count++;
//                                                } catch (JSONException e) {
//                                                    e.printStackTrace();
//                                                }
//                                            }
////                                            db.execSQL(query);
//                                            syncUpdateTabelPengampu(context, idGuru);
//                                        }
//                                    },
//                                    new Response.ErrorListener() {
//                                        @Override
//                                        public void onErrorResponse(VolleyError error) {
//                                            dialog.dismiss();
//                                            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
//
//                                        }
//                                    });
//
//                            //Creating a request queue
//                            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//                            //Adding request to the queue
//                            requestQueue.add(jsonArrayRequest);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<KategoriOffline>> call = service.syncUpdateTabelKategori(param);
            call.enqueue(new Callback<List<KategoriOffline>>() {
                @Override
                public void onResponse(Call<List<KategoriOffline>> call, retrofit2.Response<List<KategoriOffline>> response) {
//                    loading.dismiss();
                    List<KategoriOffline> listKategoriOffline = response.body();

                    String query = "";

                    for (KategoriOffline kategoriOffline : listKategoriOffline) {
                        query = "";
                        query += "INSERT INTO tabel_kategori (id_kategori, kategori, link) VALUES (";
                        query += "\"" + kategoriOffline.getIdKategori() + "\",";
                        query += "\"" + kategoriOffline.getKategori() + "\",";
                        query += "\"" + kategoriOffline.getLink() + "\")";

                        db.execSQL(query);
                    }

                    syncUpdateTabelPengampu(context, idGuru);

                }

                @Override
                public void onFailure(Call<List<KategoriOffline>> call, Throwable t) {
                    // Log error here since request failed
//                    loading.dismiss();
                    Log.d(TAG, "onFailure: gagal di tabel_kategori");
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Log.d("kategori", (error.getMessage()));
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateTabelPengampu(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog.setMessage("Sync data...16/22");
            String query = "select * from tabel_pengampu where id_pengampu is null or id_pengampu = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

//            Cursor cursor = db.rawQuery(query, null);
//
//            // looping through all rows and adding to list
//            query = "";
//            if (cursor.moveToFirst()) {
//                Boolean first = true;
//                do {
//                    if (first) {
//                        query += "INSERT INTO tabel_pengampu (nik, id_mapel) VALUES (";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("nik")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_mapel")) + "\")";
//
//                    }
//                    else {
//                        query += ",(";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("nik")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_mapel")) + "\")";
//                    }
//
//                    first = false;
//                } while (cursor.moveToNext());
//            }
//
//            SessionConfig sessionConfig = new SessionConfig(context);
//            final String ip = sessionConfig.preferences.getString("ipconfig",null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {

            db.execSQL("delete from tabel_pengampu");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//                            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/pengampu_offline.php"+"?id_guru="+idGuru,null,
//                                    new Response.Listener<JSONArray>() {
//                                        @Override
//                                        public void onResponse(JSONArray response) {
//                                            int count = 0;
//                                            String query = "";
//                                            while (count < response.length()){
//                                                try{
//                                                    JSONObject object =  response.getJSONObject(count);
//                                                    query = "";
//                                                    query += "INSERT INTO tabel_pengampu (id_pengampu, nik, id_mapel) VALUES (";
//                                                    query += "\"" + object.getString("id_pengampu") + "\",";
//                                                    query += "\"" + object.getString("nik") + "\",";
//                                                    query += "\"" + object.getString("id_mapel") + "\")";
//
//                                                    db.execSQL(query);
//                                                    count++;
//                                                } catch (JSONException e) {
//                                                    e.printStackTrace();
//                                                }
//                                            }
////                                            db.execSQL(query);
//                                            syncUpdateTabelSchedule(context, idGuru);
//                                        }
//                                    },
//                                    new Response.ErrorListener() {
//                                        @Override
//                                        public void onErrorResponse(VolleyError error) {
//                                            dialog.dismiss();
//                                            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
//
//                                        }
//                                    });
//
//                            //Creating a request queue
//                            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//                            //Adding request to the queue
//                            requestQueue.add(jsonArrayRequest);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<PengampuOffline>> call = service.syncUpdateTabelPengampu(param);
            call.enqueue(new Callback<List<PengampuOffline>>() {
                @Override
                public void onResponse(Call<List<PengampuOffline>> call, retrofit2.Response<List<PengampuOffline>> response) {
//                    loading.dismiss();
                    List<PengampuOffline> listPengampuOffline = response.body();

                    String query = "";

                    for (PengampuOffline pengampuOffline : listPengampuOffline) {
                        query = "";
                        query += "INSERT INTO tabel_pengampu (id_pengampu, nik, id_mapel) VALUES (";
                        query += "\"" + pengampuOffline.getIdPengampu() + "\",";
                        query += "\"" + pengampuOffline.getNik() + "\",";
                        query += "\"" + pengampuOffline.getIdMapel() + "\")";

                        db.execSQL(query);
                    }

                    syncUpdateTabelSchedule(context, idGuru);

                }

                @Override
                public void onFailure(Call<List<PengampuOffline>> call, Throwable t) {
                    // Log error here since request failed
//                    loading.dismiss();
                    Log.d(TAG, "onFailure: gagal di tabel_pengampu");
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateTabelSchedule(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog.setMessage("Sync data...17/22");
            String query = "select * from tabel_schedule where id_schedule is null or id_schedule = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

//            Cursor cursor = db.rawQuery(query, null);
//
//            // looping through all rows and adding to list
//            query = "";
//            if (cursor.moveToFirst()) {
//                Boolean first = true;
//                do {
//                    if (first) {
//                        query += "INSERT INTO tabel_schedule (tipe_sms, status, aturan) VALUES (";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("tipe_sms")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("status")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("aturan")) + "\")";
//
//                    }
//                    else {
//                        query += ",(";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("tipe_sms")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("status")) + "\",";
//                        query += "\"" + cursor.getString(cursor.getColumnIndex("aturan")) + "\")";
//                    }
//
//                    first = false;
//                } while (cursor.moveToNext());
//            }
//
//            SessionConfig sessionConfig = new SessionConfig(context);
//            final String ip = sessionConfig.preferences.getString("ipconfig",null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {

            db.execSQL("delete from tabel_schedule");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//                            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/schedule_offline.php"+"?id_guru="+idGuru,null,
//                                    new Response.Listener<JSONArray>() {
//                                        @Override
//                                        public void onResponse(JSONArray response) {
//                                            int count = 0;
//                                            String query = "";
//                                            while (count < response.length()){
//                                                try{
//                                                    JSONObject object =  response.getJSONObject(count);
//                                                    query = "";
//                                                    query += "INSERT INTO tabel_schedule (id_schedule, tipe_sms, status, aturan) VALUES (";
//                                                    query += "\"" + object.getString("id_schedule") + "\",";
//                                                    query += "\"" + object.getString("tipe_sms") + "\",";
//                                                    query += "\"" + object.getString("status") + "\",";
//                                                    query += "\"" + object.getString("aturan") + "\")";
//
//                                                    db.execSQL(query);
//                                                    count++;
//                                                } catch (JSONException e) {
//                                                    e.printStackTrace();
//                                                }
//                                            }
////                                            db.execSQL(query);
//                                            syncUpdateTabelUser(context, idGuru);
//                                        }
//                                    },
//                                    new Response.ErrorListener() {
//                                        @Override
//                                        public void onErrorResponse(VolleyError error) {
//                                            dialog.dismiss();
//                                            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
//
//                                        }
//                                    });
//
//                            //Creating a request queue
//                            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//                            //Adding request to the queue
//                            requestQueue.add(jsonArrayRequest);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<ScheduleOffline>> call = service.syncUpdateTabelSchedule(param);
            call.enqueue(new Callback<List<ScheduleOffline>>() {
                @Override
                public void onResponse(Call<List<ScheduleOffline>> call, retrofit2.Response<List<ScheduleOffline>> response) {
//                    loading.dismiss();
                    List<ScheduleOffline> listScheduleOffline = response.body();

                    String query = "";

                    for (ScheduleOffline scheduleOffline : listScheduleOffline) {
                        query = "";
                        query += "INSERT INTO tabel_schedule (id_schedule, tipe_sms, status, aturan) VALUES (";
                        query += "\"" + scheduleOffline.getIdSchedule() + "\",";
                        query += "\"" + scheduleOffline.getTipeSms() + "\",";
                        query += "\"" + scheduleOffline.getStatus() + "\",";
                        query += "\"" + scheduleOffline.getAturan() + "\")";

                        db.execSQL(query);
                    }

                    syncUpdateTabelJamMasuk(context, idGuru);

                }

                @Override
                public void onFailure(Call<List<ScheduleOffline>> call, Throwable t) {
                    // Log error here since request failed
//                    loading.dismiss();
                    Log.d(TAG, "onFailure: gagal di tabel_scheduled");
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Log.d("schedule", (error.getMessage()));
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateTabelJamMasuk(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog.setMessage("Sync data...18/22");
            String query = "select * from tabel_jam_masuk where id_jam_masuk is null or id_jam_masuk = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            db.execSQL("delete from tabel_jam_masuk");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<JamMasukOffline>> call = service.syncUpdateTabelJamMasuk(param);
            call.enqueue(new Callback<List<JamMasukOffline>>() {
                @Override
                public void onResponse(Call<List<JamMasukOffline>> call, retrofit2.Response<List<JamMasukOffline>> response) {
                    List<JamMasukOffline> listJamMasukOffline = response.body();

                    String query = "";

                    for (JamMasukOffline jamMasukOffline : listJamMasukOffline) {
                        query = "";
                        query += "INSERT INTO tabel_jam_masuk (id_jam_masuk, hari, jam) VALUES (";
                        query += "\"" + jamMasukOffline.getIdJamMasuk() + "\",";
                        query += "\"" + jamMasukOffline.getHari() + "\",";
                        query += "\"" + jamMasukOffline.getJam() + "\")";

                        db.execSQL(query);
                    }

                    syncUpdateTabelUser(context, idGuru);

                }

                @Override
                public void onFailure(Call<List<JamMasukOffline>> call, Throwable t) {
                    Log.d(TAG, "onFailure: gagal di tabel_jam_masuk");
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Log.d("kategori", (error.getMessage()));
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateTabelUser(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog.setMessage("Sync data...19/22");
            String query = "select * from tabel_user where id_user is null or id_user = ''";
            final SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("delete from tabel_user");

            final SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);


            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<UserOffline>> call = service.syncUpdateTabelUser(param);
            call.enqueue(new Callback<List<UserOffline>>() {
                @Override
                public void onResponse(Call<List<UserOffline>> call, retrofit2.Response<List<UserOffline>> response) {
//                    loading.dismiss();
                    List<UserOffline> listUserOffline = response.body();

                    String query = "";

                    for (UserOffline userOffline : listUserOffline) {

                        query = "";
                        query += "INSERT INTO tabel_user (id_user, username, password, nama, level, id_cabang, avatar, tgl_daftar) VALUES (";
                        query += "\"" + userOffline.getIdUser() + "\",";
                        query += "\"" + userOffline.getUsername() + "\",";
                        query += "\"" + userOffline.getPassword() + "\",";
                        query += "\"" + userOffline.getNama() + "\",";
                        query += "\"" + userOffline.getLevel() + "\",";
                        query += "\"" + userOffline.getIdCabang() + "\",";
                        query += "\"" + userOffline.getAvatar() + "\",";
                        query += "\"" + userOffline.getTglDaftar() + "\")";
                        Log.d(TAG, "onResponse: "+query);
                        db.execSQL(query);
                    }
                    syncUpdateJadwalTambahan(context, idGuru);
                }

                @Override
                public void onFailure(Call<List<UserOffline>> call, Throwable t) {
                    // Log error here since request failed
//                    loading.dismiss();
                    Log.d(TAG, "onFailure: gagal di tabel_user");
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });

        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateJadwalTambahan(final Context context, final String idGuru) {

        if (Vira.isConnectedToServer(context)) {
            dialog.setMessage("Sync data...20/22");
            final SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("delete from tabel_jadwal_tambahan");
            final SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            Log.d(TAG, "syncUpdateJadwalTambahan: still running");
            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            Call<List<JadwalTambahanOffline>> call = service.syncUpdateTabelJadwalTambahan();

            call.enqueue(new Callback<List<JadwalTambahanOffline>>() {
                @Override
                public void onResponse(Call<List<JadwalTambahanOffline>> call, retrofit2.Response<List<JadwalTambahanOffline>> response) {

                    List<JadwalTambahanOffline> listJadwaltambahan = response.body();
                    String query = "";

                    if(listJadwaltambahan.size() > 0 ){
                        for (JadwalTambahanOffline data : listJadwaltambahan) {
                            Log.d(TAG, "onResponse: total jadwal tambahan : " + listJadwaltambahan.size() + "\n" +
                                    "minggu ke : " + data.getMingguKe());
                            query = "";
                            query += "INSERT INTO tabel_jadwal_tambahan (id_jadwal_tambahan,nama_kelas,id_jenjang,id_guru,id_mapel,id_pembimbing_1,id_pembimbing_2,id_pembimbing_3,start_date,end_date,hari,jam_awal,jam_akhir,minggu_ke) VALUES (";
                            query += "\"" + data.getIdJadwalTambahan() + "\",";
                            query += "\"" + data.getKelas() + "\",";
                            query += "\"" + data.getIdJenjang() + "\",";
                            query += "\"" + data.getIdGuru() + "\",";
                            query += "\"" + data.getIdMapel() + "\",";
                            query += "\"" + data.getIdPembimbing1() + "\",";
                            query += "\"" + data.getIdPembimbing2() + "\",";
                            query += "\"" + data.getIdPembimbing3() + "\",";
                            query += "\"" + data.getStartDate() + "\",";
                            query += "\"" + data.getEndDate() + "\",";
                            query += "\"" + data.getHari() + "\",";
                            query += "\"" + data.getJamAwal() + "\",";
                            query += "\"" + data.getJamAkhir() + "\",";
                            query += "\"" + data.getMingguKe() + "\")";

                            db.execSQL(query);
                        }
                    }
                    syncUpdateKelasTambahan(context, idGuru);
                }

                @Override
                public void onFailure(Call<List<JadwalTambahanOffline>> call, Throwable t) {
                    Log.d(TAG, "onFailure: gagal di tabel_jadwal_tambahan");
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });

        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateKelasTambahan(final Context context, final String idGuru) {

        if (Vira.isConnectedToServer(context)) {
            Log.d(TAG, "syncUpdateKelasTambahan: 21/22");
            dialog.setMessage("Sync data...21/22");
            final SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("delete from tabel_kelas_tambahan");
            final SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            Cursor getJadwal = db.rawQuery("SELECT * FROM tabel_jadwal_tambahan", null);
            String idJadwal = "";
            Log.d(TAG, "syncUpdateKelasTambahan: "+getJadwal.getCount());
            if (getJadwal.getCount() > 0) {
                if (getJadwal.moveToFirst()) {
                    do {
                        int id = getJadwal.getInt(getJadwal.getColumnIndex("id_jadwal_tambahan"));
                        if (getJadwal.isLast()) {
                            idJadwal += id;
                        } else {
                            idJadwal += id + ",";
                        }
                        Log.d(TAG, "syncUpdateKelasTambahan: id "+id);
                    } while (getJadwal.moveToNext());
                }
                Log.d(TAG, "syncUpdateKelasTambahan: id jadwal tambaha : " + idJadwal);
                GetDataService service =
                        RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

                HashMap<String, String> param = new HashMap<>();
                param.put("id_jadwal", idJadwal);
                Call<List<KelasTambahanOffline>> call = service.syncUpdateKelasTambahan(param);

                call.enqueue(new Callback<List<KelasTambahanOffline>>() {
                    @Override
                    public void onResponse(Call<List<KelasTambahanOffline>> call, retrofit2.Response<List<KelasTambahanOffline>> response) {

                        List<KelasTambahanOffline> listKelastambahan = response.body();

                        String query = "";

                        for (KelasTambahanOffline data : listKelastambahan) {
                            query = "";
                            query += "INSERT INTO tabel_kelas_tambahan (id_kelas_tambahan,id_jadwal_tambahan,id_siswa) VALUES (";
                            query += "\"" + data.getIdKelasTambahan() + "\",";
                            query += "\"" + data.getIdJadwalTambahan() + "\",";
                            query += "\"" + data.getIdSiswa() + "\")";

                            db.execSQL(query);
                        }

                        dialog.dismiss();
                        Toast.makeText(context, "Sync berhasil", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<List<KelasTambahanOffline>> call, Throwable t) {
                        Log.d(TAG, "onFailure: gagal di tabel_kelas_tambahan");
                        dialog.dismiss();
                        t.printStackTrace();
                        Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                    }
                });
            }else{
                dialog.dismiss();
                Toast.makeText(context, "Sync berhasil", Toast.LENGTH_LONG).show();
            }
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAbsenGuru(final Context context, final String idGuru){
        dialog = new ProgressDialog(context);
        dialog.setMessage("Sync data absen guru...");
        dialog.setCancelable(false);
        dialog.show();
        String query = "select * from tabel_absen_kehadiran_guru where keterangan not like '%Done%' and keterangan like '%offline%'";
        final SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        query = "";
        if (cursor.moveToFirst()) {
            Boolean first = true;
            do {
                if (first) {
                    query += "INSERT INTO tabel_absen_kehadiran_guru (id_guru, tgl, jam, keterangan) VALUES (";
                    query += "\"" + cursor.getString(cursor.getColumnIndex("id_guru")) + "\",";
                    query += "\"" + cursor.getString(cursor.getColumnIndex("tgl")) + "\",";
                    query += "\"" + cursor.getString(cursor.getColumnIndex("jam")) + "\",";
                    query += "\"" + cursor.getString(cursor.getColumnIndex("keterangan")) + "(Done)\")";

                } else {
                    query += ",(";
                    query += "\"" + cursor.getString(cursor.getColumnIndex("id_guru")) + "\",";
                    query += "\"" + cursor.getString(cursor.getColumnIndex("tgl")) + "\",";
                    query += "\"" + cursor.getString(cursor.getColumnIndex("jam")) + "\",";
                    query += "\"" + cursor.getString(cursor.getColumnIndex("keterangan")) + "(Done)\")";
                }

                first = false;
            } while (cursor.moveToNext());
        }
        SessionConfig sessionConfig = new SessionConfig(context);
        GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
        Log.d(TAG, "syncAbsenGuru: query : "+query);
        Call<String> call = service.insertOffline(query);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                dialog.dismiss();
                Toast.makeText(context, "Sync Data Kehadiran berhasil" , Toast.LENGTH_LONG).show();
                doUpdateAbsenceGuru();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dialog.dismiss();
                t.printStackTrace();
                Toast.makeText(context, "Sync error " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void syncAbsenNonGuru(final Context context, final String idGuru){
        dialog = new ProgressDialog(context);
        dialog.setMessage("Sync data absen Non guru...");
        dialog.setCancelable(false);
        dialog.show();
        String query = "select * from tabel_absen_non_guru where keterangan not like '%Done%' and keterangan like '%offline%'";
        final SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(query, null);

        query = "";
        if (cursor.moveToFirst()) {
            Boolean first = true;
            do {
                if (first) {
                    query += "INSERT INTO tabel_absen_non_guru (id_non_guru, tgl, jam, jenis,keterangan) VALUES (";
                    query += "\"" + cursor.getString(cursor.getColumnIndex("id_non_guru")) + "\",";
                    query += "\"" + cursor.getString(cursor.getColumnIndex("tgl")) + "\",";
                    query += "\"" + cursor.getString(cursor.getColumnIndex("jam")) + "\",";
                    query += "\"" + cursor.getString(cursor.getColumnIndex("jenis")) + "\",";
                    query += "\"" + cursor.getString(cursor.getColumnIndex("keterangan")) + "(Done)\")";

                } else {
                    query += ",(";
                    query += "\"" + cursor.getString(cursor.getColumnIndex("id_non_guru")) + "\",";
                    query += "\"" + cursor.getString(cursor.getColumnIndex("tgl")) + "\",";
                    query += "\"" + cursor.getString(cursor.getColumnIndex("jam")) + "\",";
                    query += "\"" + cursor.getString(cursor.getColumnIndex("jenis")) + "\",";
                    query += "\"" + cursor.getString(cursor.getColumnIndex("keterangan")) + "(Done)\")";
                }

                first = false;
            } while (cursor.moveToNext());
        }
        SessionConfig sessionConfig = new SessionConfig(context);
        GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
        Log.d(TAG, "syncAbsenGuru: query : "+query);
        Call<String> call = service.insertOffline(query);
        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                dialog.dismiss();
                Toast.makeText(context, "Sync Data Kehadiran berhasil" , Toast.LENGTH_LONG).show();
                doUpdateAbsenceGuru();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                dialog.dismiss();
                t.printStackTrace();
                Toast.makeText(context, "Sync error, " + t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void doUpdateAbsenceGuru() {
        String query = "select * from tabel_absen_kehadiran_guru where keterangan not like '%Done%' and keterangan like '%offline%'";
        final SQLiteDatabase db = this.getWritableDatabase();
        Cursor updateCursor = db.rawQuery(query, null);
        if(updateCursor.moveToFirst()){
            do{
                int id = updateCursor.getInt(updateCursor.getColumnIndex("id_absen_kehadiran_guru"));
                String keterangan = updateCursor.getString(updateCursor.getColumnIndex("keterangan"));

                String qUpdate = "UPDATE tabel_absen_kehadiran_guru set keterangan = '"+keterangan+ "(Done)' WHERE id_absen_kehadiran_guru = "+id;
                db.execSQL(qUpdate);

            }while (updateCursor.moveToNext());
        }
    }

    public void syncAmbilTabelGuru(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog = new ProgressDialog(context);
            dialog.setMessage("Sync data...");
            dialog.setCancelable(false);
            dialog.show();
            String query = "select * from tabel_guru where id_guru is null or id_guru = ''";
            final SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_guru (id_jenjang, nik, nama, no_ktp, alias, jenis_kelamin, tempat_lahir, tgl_lahir, agama, no_hp, email, ijazah, pend_terakhir, gambar, alamat, unit_kerja, tmt, status_guru_bk, status_kepsek) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_jenjang")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nik")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nama")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("no_ktp")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("alias")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jenis_kelamin")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tempat_lahir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_lahir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("agama")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("no_hp")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("email")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("ijazah")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("pend_terakhir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("gambar")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("alamat")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("unit_kerja")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tmt")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("status_guru_bk")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("status_kepsek")) + "\")";

                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_jenjang")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nik")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nama")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("no_ktp")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("alias")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jenis_kelamin")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tempat_lahir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_lahir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("agama")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("no_hp")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("email")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("ijazah")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("pend_terakhir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("gambar")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("alamat")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("unit_kerja")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tmt")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("status_guru_bk")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("status_kepsek")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            Log.d(TAG, "syncAmbilTabelGuru:  query guru : " + query);
            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {


                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    syncAmbilTabelJadwal(context, idGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "absen mapel " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelNonGuru(final Context context, final String idNonGuru) {
        if (Vira.isConnectedToServer(context)) {
            dialog = new ProgressDialog(context);
            dialog.setMessage("Sync data...");
            dialog.setCancelable(false);
            dialog.show();
            String query = "select * from tabel_non_guru where id_non_guru is null or id_non_guru = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_non_guru (nip, nama, no_ktp, alias, jenis_kelamin, tempat_lahir, tgl_lahir, agama, no_hp, email, ijazah, pend_terakhir, gambar, alamat, unit_kerja, tmt, jabatan) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nip")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nama")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("no_ktp")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("alias")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jenis_kelamin")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tempat_lahir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_lahir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("agama")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("no_hp")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("email")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("ijazah")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("pend_terakhir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("gambar")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("alamat")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("unit_kerja")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tmt")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jabatan")) + "\")";

                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nip")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nama")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("no_ktp")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("alias")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jenis_kelamin")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tempat_lahir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_lahir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("agama")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("no_hp")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("email")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("ijazah")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("pend_terakhir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("gambar")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("alamat")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("unit_kerja")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tmt")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jabatan")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {

                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    syncAmbilTabelJadwal(context, idNonGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "absen mapel " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelJadwal(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_jadwal where id_jadwal is null or id_jadwal = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_jadwal (id_ta, id_jenjang, id_kelas, id_mapel, id_guru, hari, jam_awal, jam_akhir, status) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_ta")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_jenjang")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_kelas")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_mapel")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_guru")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("hari")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam_awal")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam_akhir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("status")) + "\")";

                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_ta")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_jenjang")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_kelas")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_mapel")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_guru")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("hari")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam_awal")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam_akhir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("status")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {


                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    syncAmbilTabelJenjang(context, idGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "absen mapel " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelJenjang(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_jenjang where id_jenjang is null or id_jenjang = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_jenjang (jenjang, jam_masuk, jam_pulang) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jenjang")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam_masuk")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam_pulang")) + "\")";

                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jenjang")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam_masuk")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam_pulang")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            Log.d(TAG, "syncAmbilTabelJenjang: tabel jenjang : "+query);
            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {

                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    syncAmbilTabelKelas(context, idGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "absen mapel " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelKelas(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_kelas where id_kelas is null or id_kelas = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_kelas (kelas, id_guru, id_jenjang) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("kelas")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_guru")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_jenjang")) + "\")";

                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("kelas")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_guru")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_jenjang")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {


                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    syncAmbilTabelMapel(context, idGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "absen mapel " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelMapel(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_mapel where id_mapel is null or id_mapel = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_mapel (id_jenjang, id_kat_mapel, mapel, kkm) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_jenjang")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_kat_mapel")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("mapel")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("kkm")) + "\")";

                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_jenjang")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_kat_mapel")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("mapel")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("kkm")) + "\")";

                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {


                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    syncAmbilTabelSetting(context, idGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "absen mapel " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelSetting(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_setting where id_setting is null or id_setting = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_setting (logo_mini, logo_gambar, logo_favicon, nik," +
                                " kartu_gsm, cek_pulsa, metode, kode_aktivasi, link_tujuan, logo_besar," +
                                " logo_sekolah, pimpinan, akun_gmail, alamat, website, telat, toleransi_jam_akhir, lat, lng, radius) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("logo_mini")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("logo_gambar")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("logo_favicon")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nik")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("kartu_gsm")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("cek_pulsa")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("metode")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("kode_aktivasi")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("link_tujuan")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("logo_besar")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("logo_sekolah")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("pimpinan")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("akun_gmail")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("alamat")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("website")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("telat")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("toleransi_jam_akhir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("lat")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("lng")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("radius")) + "\")";
                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("logo_mini")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("logo_gambar")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("logo_favicon")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nik")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("kartu_gsm")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("cek_pulsa")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("metode")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("kode_aktivasi")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("link_tujuan")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("logo_besar")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("logo_sekolah")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("pimpinan")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("akun_gmail")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("alamat")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("website")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("telat")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("toleransi_jam_akhir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("lat")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("lng")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("radius")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {

                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    syncAmbilTabelAbsenMapel(context, idGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "absen mapel " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelAbsenMapel(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_absen_mapel where id_absen_mapel is null or id_absen_mapel = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_absen_mapel (id_penempatan, id_jadwal, tgl, jam, absen, valid, rekap, status, keterangan) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_penempatan")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_jadwal")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("absen")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("valid")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("rekap")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("status")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("keterangan")) + "\")";
                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_penempatan")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_jadwal")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("absen")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("valid")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("rekap")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("status")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("keterangan")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);

            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Log.d(TAG, "syncAmbilTabelAbsenMapel: tabel absen mapel : " + query);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {


                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    db.execSQL("delete from tabel_absen_mapel");
                    syncAmbilTabelSiswakelas(context, idGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "absen mapel " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelSiswakelas(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_siswakelas where id_penempatan is null or id_penempatan = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_siswakelas (id_ta, id_kelas, id_siswa) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_ta")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_kelas")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_siswa")) + "\")";
                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_ta")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_kelas")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_siswa")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {
//
//                            syncAmbilTabelSiswa(context, idGuru);
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Toast.makeText(context, "siswakelas " + error.getLocalizedMessage() + "" , Toast.LENGTH_LONG).show();
//
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {
//                @Override
//                public void onResponse(Call<JSONArray> call, Response<JSONArray> response) {
//
//                }

//                @Override
//                public void onResponse(Call<Query> call, Response<Query> response) {
//                    syncAmbilTabelSiswakelas(context, idGuru);
//                }

                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    syncAmbilTabelSiswa(context, idGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "absen mapel " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelSiswa(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_siswa where id_siswa is null or id_siswa = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_siswa (id_jenjang, nama_lengkap, nisn, jenis_kelamin, tempat_lahir, tgl_lahir, agama, alamat, no_hp, email, asal_sekolah, ket_asal, nama_ayah, nama_ibu, gambar, tgl_daftar) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_jenjang")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nama_lengkap")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nisn")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jenis_kelamin")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tempat_lahir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_lahir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("agama")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("alamat")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("no_hp")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("email")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("asal_sekolah")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("ket_asal")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nama_ayah")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nama_ibu")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("gambar")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_daftar")) + "\")";
                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_jenjang")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nama_lengkap")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nisn")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jenis_kelamin")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tempat_lahir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_lahir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("agama")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("alamat")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("no_hp")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("email")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("asal_sekolah")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("ket_asal")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nama_ayah")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nama_ibu")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("gambar")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_daftar")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {
//
//                            syncAmbilTabelTahunajaran(context, idGuru);
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Toast.makeText(context, "siswa " + error.getLocalizedMessage() + "" , Toast.LENGTH_LONG).show();
//
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {
//                @Override
//                public void onResponse(Call<JSONArray> call, Response<JSONArray> response) {
//
//                }

//                @Override
//                public void onResponse(Call<Query> call, Response<Query> response) {
//                    syncAmbilTabelSiswakelas(context, idGuru);
//                }

                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    syncAmbilTabelTahunajaran(context, idGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "absen mapel " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelTahunajaran(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_tahunajaran where id_ta is null or id_ta = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_tahunajaran (tahun_ajaran, tgl_awal, tgl_akhir,status) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tahun_ajaran")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_awal")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_akhir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("status")) + "\")";
                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tahun_ajaran")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_awal")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_akhir")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("status")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {
//
//                            syncAmbilTabelKatMapel(context, idGuru);
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Toast.makeText(context, "tahunajaran " + error.getLocalizedMessage() + "" , Toast.LENGTH_LONG).show();
//
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {
//                @Override
//                public void onResponse(Call<JSONArray> call, Response<JSONArray> response) {
//
//                }

//                @Override
//                public void onResponse(Call<Query> call, Response<Query> response) {
//                    syncAmbilTabelSiswakelas(context, idGuru);
//                }

                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    syncAmbilTabelKatMapel(context, idGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "tahunajaran " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelKatMapel(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_kat_mapel where id_kat_mapel is null or id_kat_mapel = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_kat_mapel (kategori_mapel) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("kategori_mapel")) + "\")";
                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("kategori_mapel")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {
//
//                            syncAmbilTabelAbsen(context, idGuru);
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Toast.makeText(context, "katmapel " + error.getLocalizedMessage() + "" , Toast.LENGTH_LONG).show();
//
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {
//                @Override
//                public void onResponse(Call<JSONArray> call, Response<JSONArray> response) {
//
//                }

//                @Override
//                public void onResponse(Call<Query> call, Response<Query> response) {
//                    syncAmbilTabelSiswakelas(context, idGuru);
//                }

                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    syncAmbilTabelAbsen(context, idGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "kat mapel " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelAbsen(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_absen where id_absen is null or id_absen = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_absen (id_penempatan, tgl, jam_masuk, absen_masuk, valid_masuk, sms_masuk, jam_pulang, absen_pulang, valid_pulang, sms_pulang) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_penempatan")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam_masuk")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("absen_masuk")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("valid_masuk")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("sms_masuk")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam_pulang")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("absen_pulang")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("valid_pulang")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("sms_pulang")) + "\")";


                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_penempatan")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam_masuk")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("absen_masuk")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("valid_masuk")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("sms_masuk")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam_pulang")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("absen_pulang")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("valid_pulang")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("sms_pulang")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {
//
//                            syncAmbilTabelCabang(context, idGuru);
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Toast.makeText(context, "absen " + error.getLocalizedMessage() + "" , Toast.LENGTH_LONG).show();
//
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Log.d(TAG, "syncAmbilTabelAbsen: query absen : " + query);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {
//                @Override
//                public void onResponse(Call<JSONArray> call, Response<JSONArray> response) {
//
//                }

//                @Override
//                public void onResponse(Call<Query> call, Response<Query> response) {
//                    syncAmbilTabelSiswakelas(context, idGuru);
//                }

                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    syncAmbilTabelCabang(context, idGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "absen " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelCabang(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_cabang where id_cabang is null or id_cabang = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_cabang (cabang) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("cabang")) + "\")";


                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("cabang")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {
//
//                            syncAmbilTabelKalender(context, idGuru);
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Toast.makeText(context, "cabang " + error.getLocalizedMessage() + "" , Toast.LENGTH_LONG).show();
//
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {
//                @Override
//                public void onResponse(Call<JSONArray> call, Response<JSONArray> response) {
//
//                }

//                @Override
//                public void onResponse(Call<Query> call, Response<Query> response) {
//                    syncAmbilTabelSiswakelas(context, idGuru);
//                }

                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    syncAmbilTabelKalender(context, idGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "cabang " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelKalender(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_kalender where id_kalender is null or id_kalender = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_kalender (tanggal, agenda, keterangan) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tanggal")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("agenda")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("keterangan")) + "\")";

                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tanggal")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("agenda")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("keterangan")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {
//
//                            syncAmbilTabelKategori(context, idGuru);
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Toast.makeText(context, "kalender " + error.getLocalizedMessage() + "" , Toast.LENGTH_LONG).show();
//
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {
//                @Override
//                public void onResponse(Call<JSONArray> call, Response<JSONArray> response) {
//
//                }

//                @Override
//                public void onResponse(Call<Query> call, Response<Query> response) {
//                    syncAmbilTabelSiswakelas(context, idGuru);
//                }

                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    syncAmbilTabelKategori(context, idGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "kalender " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelKategori(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_kategori where id_kategori is null or id_kategori = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_kategori (kategori, link) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("kategori")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("link")) + "\")";

                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("kategori")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("link")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {
//
//                            syncAmbilTabelKelulusan(context, idGuru);
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Toast.makeText(context, "kategori " + error.getLocalizedMessage() + "" , Toast.LENGTH_LONG).show();
//
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {
//                @Override
//                public void onResponse(Call<JSONArray> call, Response<JSONArray> response) {
//
//                }

//                @Override
//                public void onResponse(Call<Query> call, Response<Query> response) {
//                    syncAmbilTabelSiswakelas(context, idGuru);
//                }

                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    syncAmbilTabelKelulusan(context, idGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "kategori " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelKelulusan(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_kelulusan where id_kelulusan is null or id_kelulusan = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_kelulusan (id_siswa, id_ta, id_kelas, angkatan, tgl_lulus) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_siswa")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_ta")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_kelas")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("angkatan")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_lulus")) + "\")";

                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_siswa")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_ta")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_kelas")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("angkatan")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_lulus")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {
//
//                            syncAmbilTabelPengampu(context, idGuru);
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Toast.makeText(context, "kelulusan " + error.getLocalizedMessage() + "" , Toast.LENGTH_LONG).show();
//
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {
//                @Override
//                public void onResponse(Call<JSONArray> call, Response<JSONArray> response) {
//
//                }

//                @Override
//                public void onResponse(Call<Query> call, Response<Query> response) {
//                    syncAmbilTabelSiswakelas(context, idGuru);
//                }

                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    syncAmbilTabelPengampu(context, idGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "kelulusan " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelPengampu(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_pengampu where id_pengampu is null or id_pengampu = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_pengampu (nik, id_mapel) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nik")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_mapel")) + "\")";

                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nik")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_mapel")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {
//
//                            syncAmbilTabelSchedule(context, idGuru);
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Toast.makeText(context, "pengampu " + error.getLocalizedMessage() + "" , Toast.LENGTH_LONG).show();
//
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {
//                @Override
//                public void onResponse(Call<JSONArray> call, Response<JSONArray> response) {
//
//                }

//                @Override
//                public void onResponse(Call<Query> call, Response<Query> response) {
//                    syncAmbilTabelSiswakelas(context, idGuru);
//                }

                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    syncAmbilTabelSchedule(context, idGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "pengampu " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelSchedule(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_schedule where id_schedule is null or id_schedule = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_schedule (tipe_sms, status, aturan) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tipe_sms")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("status")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("aturan")) + "\")";

                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tipe_sms")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("status")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("aturan")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {
//
//                            syncAmbilTabelUser(context, idGuru);
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Toast.makeText(context, "schedule " + error.getLocalizedMessage() + "" , Toast.LENGTH_LONG).show();
//
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {
//                @Override
//                public void onResponse(Call<JSONArray> call, Response<JSONArray> response) {
//
//                }

//                @Override
//                public void onResponse(Call<Query> call, Response<Query> response) {
//                    syncAmbilTabelSiswakelas(context, idGuru);
//                }

                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    syncAmbilTabelAbsenKehadiranGuru(context, idGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "schedule " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelAbsenKehadiranGuru(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            Calendar calendar = Calendar.getInstance();
            String tgl = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));
            String query = "select * from tabel_absen_kehadiran_guru where (id_absen_kehadiran_guru is null or id_absen_kehadiran_guru = '') and tgl = '" + tgl + "'";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_absen_kehadiran_guru (id_absen_kehadiran_guru, id_guru, tgl, jam, keterangan) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_absen_kehadiran_guru")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_guru")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("keterangan")) + "\")";

                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_absen_kehadiran_guru")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_guru")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("jam")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("keterangan")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ "/web_absence/assets/file/insert_offline.php"+"?query="+query,null,
//                    new Response.Listener<JSONArray>() {
//                        @Override
//                        public void onResponse(JSONArray response) {
//
//                            syncAmbilTabelUser(context, idGuru);
//
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            dialog.dismiss();
//                            Toast.makeText(context, "schedule " + error.getLocalizedMessage() + "" , Toast.LENGTH_LONG).show();
//
//                        }
//                    });
//
//            //Creating a request queue
//            RequestQueue requestQueue = Volley.newRequestQueue(context);
//
//            //Adding request to the queue
//            requestQueue.add(jsonArrayRequest);
            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {
//                @Override
//                public void onResponse(Call<JSONArray> call, Response<JSONArray> response) {
//
//                }

//                @Override
//                public void onResponse(Call<Query> call, Response<Query> response) {
//                    syncAmbilTabelSiswakelas(context, idGuru);
//                }

                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    syncAmbilTabelUser(context, idGuru);
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "absen kehadiran guru " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbilTabelUser(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_user where id_user is null or id_user = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            Cursor cursor = db.rawQuery(query, null);

            // looping through all rows and adding to list
            query = "";
            if (cursor.moveToFirst()) {
                Boolean first = true;
                do {
                    if (first) {
                        query += "INSERT INTO tabel_user (username, password, nama, level, id_cabang, avatar, tgl_daftar) VALUES (";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("username")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("password")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nama")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("level")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_cabang")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("avatar")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_daftar")) + "\")";

                    } else {
                        query += ",(";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("username")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("password")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("nama")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("level")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("id_cabang")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("avatar")) + "\",";
                        query += "\"" + cursor.getString(cursor.getColumnIndex("tgl_daftar")) + "\")";
                    }

                    first = false;
                } while (cursor.moveToNext());
            }

            final SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertOffline(query);
            call.enqueue(new Callback<String>() {


                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {

                    GetDataService service =
                            RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

                    HashMap<String, String> param = new HashMap<>();
                    param.put("id_guru", idGuru);

                    Call<String> call1 = service.changePassSync(param);
                    call1.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, retrofit2.Response<String> response) {
//                    loading.dismiss();
                            syncAmbiltabelAbsenTambahan(context, idGuru);
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            // Log error here since request failed
//                    loading.dismiss();
                            t.printStackTrace();
                        }
                    });


                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "user " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncAmbiltabelAbsenTambahan(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            final SQLiteDatabase db = this.getWritableDatabase();
            String query = "select * from tabel_absen_mapel_tambahan";
            Cursor cursor = db.rawQuery(query, null);

            List<AbsenMapelTambahan> dataAbsen = new ArrayList<>();

            if (cursor.getCount() > 0) {
                cursor.moveToFirst();

                do {
                    String idKelasTambahan = cursor.getString(cursor.getColumnIndex("id_kelas_tambahan"));
                    String idJadwalTambahan = cursor.getString(cursor.getColumnIndex("id_jadwal_tambahan"));
                    String tgl = cursor.getString(cursor.getColumnIndex("tgl"));
                    String jam = cursor.getString(cursor.getColumnIndex("jam"));
                    String valid = cursor.getString(cursor.getColumnIndex("valid"));
                    String absen = cursor.getString(cursor.getColumnIndex("absen"));
                    String rekap = cursor.getString(cursor.getColumnIndex("rekap"));
                    String keterangan = cursor.getString(cursor.getColumnIndex("keterangan"));

                    AbsenMapelTambahan dataAbs = new AbsenMapelTambahan();
                    dataAbs.setId_kelas_tambahan(idKelasTambahan);
                    dataAbs.setId_jadwal_tambahan(idJadwalTambahan);
                    dataAbs.setTgl(tgl);
                    dataAbs.setJam(jam);
                    dataAbs.setValid(valid);
                    dataAbs.setAbsen(absen);
                    dataAbs.setRekap(rekap);
                    dataAbs.setKeterangan(keterangan);

                    dataAbsen.add(dataAbs);

                } while (cursor.moveToNext());
            }

            String dataSend = new Gson().toJson(dataAbsen);
            Log.d(TAG, "syncAmbiltabelAbsentambahan: data will send : " + dataSend);

            final SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<String> call = service.insertAbsentambahan(dataSend);
            call.enqueue(new Callback<String>() {


                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    dialog.dismiss();
                    Toast.makeText(context, "Sync berhasil", Toast.LENGTH_LONG).show();
                    Log.d(TAG, "onResponse: " + response.body());
                    if (response.body().equals("ok")) {
                        Toast.makeText(context, "data berhasil dikirim", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, "data gagal terkirim", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "user " + t.getMessage(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public String hariBerapa(Calendar calendar) {
        int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
        String weekDay = "";
        if (Calendar.MONDAY == dayOfWeek) {
            weekDay = "Senin";
        } else if (Calendar.TUESDAY == dayOfWeek) {
            weekDay = "Selasa";
        } else if (Calendar.WEDNESDAY == dayOfWeek) {
            weekDay = "Rabu";
        } else if (Calendar.THURSDAY == dayOfWeek) {
            weekDay = "Kamis";
        } else if (Calendar.FRIDAY == dayOfWeek) {
            weekDay = "Jumat";
        } else if (Calendar.SATURDAY == dayOfWeek) {
            weekDay = "Sabtu";
        } else if (Calendar.SUNDAY == dayOfWeek) {
            weekDay = "Minggu";
        }
        return weekDay;
    }

    public String jamBerapa(Calendar calendar) {
        String jam = String.valueOf(calendar.get(Calendar.HOUR_OF_DAY));
        String menit = String.valueOf(calendar.get(Calendar.MINUTE));
        if (jam.length() < 2) jam = "0" + jam;
        if (menit.length() < 2) menit = "0" + menit;
        return jam + ":" + menit;
    }

    public ArrayList<Mapel> daftarMapelAll(Context context, String guru) {
        ArrayList<Mapel> arrayList = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        String jam = jamBerapa(calendar);
        String hari = hariBerapa(calendar);

        SQLiteDatabase db = this.getWritableDatabase();

        String queryGuru = "select * from tabel_guru where nik = '" + guru + "'";
        Cursor cursorGuru = db.rawQuery(queryGuru, null);
        if (cursorGuru.getCount() == 0) return arrayList;
        cursorGuru.moveToFirst();
        String statusGuruBK = cursorGuru.getString(cursorGuru.getColumnIndex("status_guru_bk"));

        String query;
        Cursor cursorKelas;
        if (statusGuruBK.equals("1")) {
            query = "SELECT DISTINCT (tabel_kelas.id_kelas),tabel_jenjang.jenjang,tabel_kelas.kelas, tabel_mapel.mapel, " +
                    "tabel_jadwal.jam_awal, tabel_jadwal.jam_akhir, tabel_jadwal.status,tabel_jadwal.id_jadwal, tabel_jadwal.status, " +
                    "tabel_guru.nama " +
                    "FROM tabel_jadwal INNER JOIN tabel_jenjang " +
                    "ON tabel_jadwal.id_jenjang = tabel_jenjang.id_jenjang INNER JOIN tabel_kelas " +
                    "ON tabel_jadwal.id_kelas = tabel_kelas.id_kelas INNER JOIN tabel_guru " +
                    "ON tabel_jadwal.id_guru = tabel_guru.id_guru INNER JOIN tabel_mapel " +
                    "ON tabel_jadwal.id_mapel = tabel_mapel.id_mapel " +
                    "WHERE SUBSTR(tabel_jadwal.hari,3) = ?" +
                    "ORDER BY tabel_kelas.kelas,tabel_jadwal.jam_awal ASC";

            cursorKelas = db.rawQuery(query, new String[]{hari});
        } else {
            query = "SELECT DISTINCT (tabel_kelas.id_kelas),tabel_jenjang.jenjang,tabel_kelas.kelas, tabel_mapel.mapel, " +
                    "tabel_jadwal.jam_awal, tabel_jadwal.jam_akhir, tabel_jadwal.status,tabel_jadwal.id_jadwal, tabel_jadwal.status, " +
                    "tabel_guru.nama " +
                    "FROM tabel_jadwal INNER JOIN tabel_jenjang " +
                    "ON tabel_jadwal.id_jenjang = tabel_jenjang.id_jenjang INNER JOIN tabel_kelas " +
                    "ON tabel_jadwal.id_kelas = tabel_kelas.id_kelas INNER JOIN tabel_guru " +
                    "ON tabel_jadwal.id_guru = tabel_guru.id_guru INNER JOIN tabel_mapel " +
                    "ON tabel_jadwal.id_mapel = tabel_mapel.id_mapel " +
                    "WHERE tabel_guru.nik = ? AND SUBSTR(tabel_jadwal.hari,3) = ?" +
                    "ORDER BY tabel_kelas.kelas,tabel_jadwal.jam_awal ASC";

            cursorKelas = db.rawQuery(query, new String[]{guru, hari});
        }

        String setting = "select * from tabel_setting where id_setting = '1'";
        Cursor cursorSetting = db.rawQuery(setting, null);
        if (cursorSetting.getCount() == 0) return arrayList;
        cursorSetting.moveToFirst();
        int detik = Integer.parseInt(cursorSetting.getString(cursorSetting.getColumnIndex("toleransi_jam_akhir"))) * 60;
        List<Mapel> listMapel = new ArrayList<>();
        if (cursorKelas.moveToFirst()) {
            do {
                String status = cursorKelas.getString(cursorKelas.getColumnIndex("status"));
                String jadwal = cursorKelas.getString(cursorKelas.getColumnIndex("id_jadwal"));
                String waktu = "";
                String jamAwal = cursorKelas.getString(cursorKelas.getColumnIndex("jam_awal"));
                String jamAkhir = cursorKelas.getString(cursorKelas.getColumnIndex("jam_akhir"));
                Calendar calendar1 = (Calendar) calendar.clone();
                calendar1.set(Calendar.HOUR_OF_DAY, Integer.valueOf(jamAkhir.substring(0, 2)));
                calendar1.set(Calendar.MINUTE, Integer.valueOf(jamAkhir.substring(3, 5)));
                Calendar calendar2 = (Calendar) calendar1.clone();
                calendar2.add(Calendar.SECOND, -detik);
                String jamAkhir2 = String.format("%02d", calendar2.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d", calendar2.get(Calendar.MINUTE));
                calendar1.add(Calendar.SECOND, detik);
                String jamAkhir1 = String.format("%02d", calendar1.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d", calendar1.get(Calendar.MINUTE));
                Cursor cek;
                if (status.equals("pulang"))
                    cek = db.rawQuery("SELECT * FROM tabel_jadwal WHERE id_jadwal = '" + jadwal + "' AND " +
                            "'" + jam + "'" + " BETWEEN '" + jamAkhir2 + "' AND '" + jamAkhir1 + "'", null);
                else {
                    cek = db.rawQuery("SELECT * FROM tabel_jadwal WHERE id_jadwal = '" + jadwal + "' AND " +
                            "'" + jam + "'" + " BETWEEN '" + jamAwal + "' AND '" + jamAkhir + "'", null);
                }
                int ac = cek.getCount();
                if (ac > 0) {
                    waktu = "ada";
                } else {
                    waktu = "";
                }
                Mapel news = new Mapel();
                news.setIdapel(cursorKelas.getString(cursorKelas.getColumnIndex("id_kelas")));
                news.setMapel(cursorKelas.getString(cursorKelas.getColumnIndex("mapel")));
                news.setWaktu(cursorKelas.getString(cursorKelas.getColumnIndex("jam_awal")));
                news.setJam(cursorKelas.getString(cursorKelas.getColumnIndex("jam_akhir")));
                news.setKelas(cursorKelas.getString(cursorKelas.getColumnIndex("kelas")));
                news.setIdjadwal(cursorKelas.getString(cursorKelas.getColumnIndex("id_jadwal")));
                news.setJenjang(cursorKelas.getString(cursorKelas.getColumnIndex("jenjang")));
                news.setStatus(waktu);
                news.setStatus1(status);
                news.setNama(cursorKelas.getString(cursorKelas.getColumnIndex("nama")));
                news.setStatusGuruBK(statusGuruBK);


                String jam1 = "";
                jam1 += news.getJam().substring(0, 2);
                String menit = "";
                menit += news.getJam().substring(3, 5);

                AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
                calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(jam1));
                calendar.set(Calendar.MINUTE, Integer.valueOf(menit));
                calendar.add(Calendar.MINUTE, -5);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);

                Intent notificationIntent = new Intent(context, AlarmReceiver.class);
                PendingIntent broadcast = PendingIntent.getBroadcast(context, 1, notificationIntent, 0);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && calendar.getTimeInMillis() >= Calendar.getInstance().getTimeInMillis() && news.getStatus1().equals("pulang")) {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), broadcast);
                }

                arrayList.add(news);
            } while (cursorKelas.moveToNext());
        }
        return arrayList;
    }

    public ArrayList<MapelTambahan> daftarMapelTambahanAll(Context context, String guru) {
        ArrayList<MapelTambahan> arrayList = new ArrayList<>();

        SQLiteDatabase db = this.getWritableDatabase();
        String queryGuru = "select * from tabel_guru where nik = '" + guru + "'";
        Cursor cursorGuru = db.rawQuery(queryGuru, null);
        if (cursorGuru.getCount() == 0) return arrayList;
        cursorGuru.moveToFirst();
        String statusGuruBK = cursorGuru.getString(cursorGuru.getColumnIndex("status_guru_bk"));

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        String tgl = simpleDateFormat.format(new Date());
        String currentTime = timeFormat.format(new Date());

        String hariIni = Util.getDay(tgl);
        int mingguKe = Util.getWeekOfMonth();

        Log.d(TAG, "daftarMapelTambahanAll -----: \n" +
                "apakah guru BK : " + statusGuruBK + "\n" +
                "hari ini : " + hariIni + "\n" +
                "minggu ke : " + mingguKe + "\n" +
                "tanggal : " + tgl + "\n" +
                "jam : " + currentTime + "\n" +
                "nik : " + guru + "\n" +

                "");

        String query;
        Cursor cursorKelas;
        if (statusGuruBK.equals("1")) {
            query = "SELECT" +
                    "  tabel_jenjang.jenjang,tabel_mapel.mapel, tabel_jadwal_tambahan.nama_kelas," +
                    "  tabel_jadwal_tambahan.jam_awal, tabel_jadwal_tambahan.jam_akhir,tabel_jadwal_tambahan.id_jadwal_tambahan , tabel_guru.nama" +
                    "  FROM tabel_jadwal_tambahan " +
                    "  INNER JOIN tabel_jenjang ON tabel_jadwal_tambahan.id_jenjang = tabel_jenjang.id_jenjang" +
                    "  INNER JOIN tabel_guru ON tabel_jadwal_tambahan.id_guru = tabel_guru.id_guru " +
                    "  INNER JOIN tabel_mapel ON tabel_jadwal_tambahan.id_mapel = tabel_mapel.id_mapel" +
                    "  WHERE tabel_jadwal_tambahan.hari = ?" +
                    "  ORDER BY tabel_jadwal_tambahan.nama_kelas,tabel_jadwal_tambahan.jam_awal ASC";

            cursorKelas = db.rawQuery(query, new String[]{hariIni});
        } else {

            String check = "SELECT * FROM tabel_jadwal_tambahan";
            Cursor cursor = db.rawQuery(check, new String[]{});
            Log.d(TAG, "daftarMapelTambahanAll: total jadwal tambahan : " + cursor.getCount());
            cursor.moveToFirst();
            do {
                Log.d(TAG, "daftarMapelTambahanAll: -------- data jadwal tambahan ----------\n" +
                        "id : " + cursor.getInt(cursor.getColumnIndex("id_jadwal_tambahan")) + "\n" +
                        "id guru : " + cursor.getInt(cursor.getColumnIndex("id_guru")) + "\n" +
                        "hari : " + cursor.getString(cursor.getColumnIndex("hari")) + "\n" +
                        "minggu ke : " + cursor.getInt(cursor.getColumnIndex("minggu_ke")) + "\n" +
                        "");
            } while (cursor.moveToNext());


            query = "SELECT " +
                    "  tabel_jenjang.jenjang, tabel_mapel.mapel," +
                    "  tabel_jadwal_tambahan.nama_kelas,tabel_jadwal_tambahan.jam_awal, tabel_jadwal_tambahan.jam_akhir," +
                    "  tabel_jadwal_tambahan.id_jadwal_tambahan,tabel_guru.nama" +
                    "  FROM tabel_jadwal_tambahan" +
                    "  INNER JOIN tabel_jenjang ON tabel_jadwal_tambahan.id_jenjang = tabel_jenjang.id_jenjang" +
                    "  INNER JOIN tabel_guru ON (tabel_jadwal_tambahan.id_guru = tabel_guru.id_guru OR tabel_jadwal_tambahan.id_pembimbing_1 = tabel_guru.id_guru\n" +
                    "  OR tabel_jadwal_tambahan.id_pembimbing_2 = tabel_guru.id_guru OR tabel_jadwal_tambahan.id_pembimbing_3 = tabel_guru.id_guru)" +
                    "  INNER JOIN tabel_mapel ON tabel_jadwal_tambahan.id_mapel = tabel_mapel.id_mapel" +
                    "  WHERE tabel_guru.nik = ? " +
                    "AND tabel_jadwal_tambahan.hari = ? " +
                    "AND tabel_jadwal_tambahan.minggu_ke = ?" +
                    "  ORDER BY tabel_jadwal_tambahan.nama_kelas,tabel_jadwal_tambahan.jam_awal ASC";

            cursorKelas = db.rawQuery(query, new String[]{guru, hariIni, String.valueOf(mingguKe)});
//            cursorKelas = db.rawQuery(query, new String[]{guru, hariIni, String.valueOf(mingguKe)});
        }

        String setting = "select * from tabel_setting where id_setting = '1'";
        Cursor cursorSetting = db.rawQuery(setting, null);
        if (cursorSetting.getCount() == 0) return arrayList;
        cursorSetting.moveToFirst();
        int detik = Integer.parseInt(cursorSetting.getString(cursorSetting.getColumnIndex("toleransi_jam_akhir"))) * 60;
        Log.d(TAG, "daftarMapelTambahanAll: total : " + cursorKelas.getCount());
        if (cursorKelas.moveToFirst()) {
            do {
                int jadwal = cursorKelas.getInt(cursorKelas.getColumnIndex("id_jadwal_tambahan"));
                String waktu = "";
                String jamAwal = cursorKelas.getString(cursorKelas.getColumnIndex("jam_awal"));
                String jamAkhir = cursorKelas.getString(cursorKelas.getColumnIndex("jam_akhir"));

                Cursor cek = db.rawQuery("SELECT * FROM tabel_jadwal_tambahan WHERE id_jadwal_tambahan = '" + jadwal + "' AND " +
                        "'" + currentTime + "'" + " BETWEEN '" + jamAwal + "' AND '" + jamAkhir + "'", null);

                int ac = cek.getCount();
                if (ac > 0) {
                    waktu = "ada";
                } else {
                    waktu = "";
                }

                Log.d(TAG, "daftarMapelTambahanAll: \n" +
                        "waktu : " + waktu + "\n" +
                        "jam awal : " + jamAwal + "\n" +
                        "jam akhir : " + jamAkhir + "\n" +
                        "waktu : " + waktu + "\n" +
                        "");

                MapelTambahan news = new MapelTambahan();
                news.setIdkelas("1");
                news.setJenjang(cursorKelas.getString(cursorKelas.getColumnIndex("jenjang")));
                news.setKelas(cursorKelas.getString(cursorKelas.getColumnIndex("nama_kelas")));
                news.setMapel(cursorKelas.getString(cursorKelas.getColumnIndex("mapel")));
                news.setJamAwal(cursorKelas.getString(cursorKelas.getColumnIndex("jam_awal")));
                news.setJamAkhir(cursorKelas.getString(cursorKelas.getColumnIndex("jam_akhir")));
                news.setIdjadwal(cursorKelas.getString(cursorKelas.getColumnIndex("id_jadwal_tambahan")));
                news.setWaktu(waktu);
                news.setJam(currentTime);
                news.setNama(cursorKelas.getString(cursorKelas.getColumnIndex("nama")));
                news.setStatusGuruBK(statusGuruBK);

                String jam1 = "";
                jam1 += news.getJam().substring(0, 2);
                String menit = "";
                menit += news.getJam().substring(3, 5);

                AlarmManager alarmManager = (AlarmManager) context.getSystemService(ALARM_SERVICE);
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(jam1));
                calendar.set(Calendar.MINUTE, Integer.valueOf(menit));
                calendar.add(Calendar.MINUTE, -5);
                calendar.set(Calendar.SECOND, 0);
                calendar.set(Calendar.MILLISECOND, 0);

                Intent notificationIntent = new Intent(context, AlarmReceiver.class);
                PendingIntent broadcast = PendingIntent.getBroadcast(context, 1, notificationIntent, 0);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && calendar.getTimeInMillis() >= Calendar.getInstance().getTimeInMillis()
                    //&& news.getStatus1().equals("pulang")
                ) {
                    alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), broadcast);
                }

                arrayList.add(news);
            } while (cursorKelas.moveToNext());
        }
        return arrayList;
    }

    public ArrayList<Siswa> getAllSiswa(String kelas, String jadwal) {
        SQLiteDatabase db = this.getWritableDatabase();

        Calendar calendar = Calendar.getInstance();
        String jam = String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d", calendar.get(Calendar.MINUTE));
        String hari = hariBerapa(calendar);
        String tanggal = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));

        Cursor cek = db.rawQuery("SELECT * FROM tabel_absen_mapel WHERE tgl=? ANd id_jadwal=?", new String[]{tanggal, jadwal});

        Cursor cek2 = db.rawQuery("SELECT * FROM tabel_jadwal WHERE id_jadwal=?", new String[]{jadwal});

        cek2.moveToFirst();
        if (cek2.getCount() == 0) return new ArrayList<Siswa>();
        String idTa = cek2.getString(cek2.getColumnIndex("id_ta"));
        cek2.moveToPosition(-1);
        idTa = "";

        Cursor cek3 = db.rawQuery("SELECT COUNT(id_penempatan) AS jum FROM tabel_siswakelas WHERE id_kelas=? " +
                "AND id_ta = ?", new String[]{kelas, idTa});
        cek3.moveToFirst();
        String valid2 = cek3.getString(cek3.getColumnIndex("jum"));

        if (cek.getCount() == 0) {
            String queryKelas = "SELECT DISTINCT(tabel_siswakelas.id_penempatan),tabel_siswa.nama_lengkap,tabel_siswa.gambar," +
                    "tabel_siswa.id_siswa,tabel_jenjang.jenjang,tabel_kelas.kelas,tabel_tahunajaran.tahun_ajaran,tabel_guru.nama," +
                    "tabel_siswakelas.id_kelas " +
                    "FROM tabel_siswakelas LEFT JOIN tabel_siswa " +
                    "ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang " +
                    "ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas " +
                    "ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru " +
                    "ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran " +
                    "ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) " +
                    "WHERE tabel_kelas.id_kelas =? " +
                    "ORDER BY tabel_siswa.nama_lengkap ASC";

            Cursor jad = db.rawQuery("SELECT * FROM tabel_jadwal WHERE id_jadwal = ?", new String[]{jadwal});
            if (jad.getCount() == 0) return new ArrayList<Siswa>();
            jad.moveToFirst();
            String status = jad.getString(jad.getColumnIndex("status"));
            jad.moveToPosition(-1);
            String valid = "";
            String absen = "";
            Cursor result = db.rawQuery(queryKelas, new String[]{kelas});
            ArrayList<Siswa> siswaList = new ArrayList<>();
            int no = 1;
            if (result.moveToFirst()) {
                do {
                    Siswa siswa = new Siswa();
                    siswa.setNo(String.valueOf(no++));
                    siswa.setNis(result.getString(result.getColumnIndex("id_siswa")));
                    siswa.setNama(result.getString(result.getColumnIndex("nama_lengkap")));
                    siswa.setGmbr(result.getString(result.getColumnIndex("gambar")));
                    siswa.setIdpenmpatan(result.getString(result.getColumnIndex("id_penempatan")));
                    siswa.setValid(valid);
                    siswa.setStatus(absen);
                    siswa.setStatusdua(status);
                    siswaList.add(siswa);
                } while (result.moveToNext());
            }
            return siswaList;
        } else if (cek.getCount() != Integer.parseInt(valid2)) {
            String queryKelas = "SELECT DISTINCT(tabel_siswakelas.id_penempatan),tabel_siswa.nama_lengkap,tabel_siswa.gambar," +
                    "tabel_siswa.id_siswa,tabel_jenjang.jenjang,tabel_kelas.kelas,tabel_tahunajaran.tahun_ajaran,tabel_guru.nama," +
                    "tabel_siswakelas.id_kelas " +
                    "FROM tabel_siswakelas LEFT JOIN tabel_siswa " +
                    "ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang " +
                    "ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas " +
                    "ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru " +
                    "ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran " +
                    "ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) " +
                    "WHERE tabel_kelas.id_kelas = ? " +
                    "ORDER BY tabel_siswa.nama_lengkap ASC";
            Cursor result = db.rawQuery(queryKelas, new String[]{kelas});
            ArrayList<Siswa> siswaList = new ArrayList<>();
            int no = 1;
            if (result.moveToFirst()) {
                do {
                    String id_penempatan = result.getString(result.getColumnIndex("id_penempatan"));
                    Cursor jad = db.rawQuery("SELECT * FROM tabel_jadwal WHERE id_jadwal = ?", new String[]{jadwal});

                    Cursor v = db.rawQuery("SELECT * FROM tabel_absen_mapel WHERE tgl = ? AND id_jadwal = ? " +
                            "AND id_penempatan = ?", new String[]{tanggal, jadwal, id_penempatan});
                    String status = "";
                    String valid = "";
                    String absen = "";
                    if (v.getCount() == 0) {
                        if (jad.getCount() == 0) return new ArrayList<Siswa>();
                        jad.moveToFirst();
                        status = jad.getString(jad.getColumnIndex("status"));
                        jad.moveToPosition(-1);
                        valid = "";
                        absen = "";
                    } else {
                        if (jad.getCount() == 0) return new ArrayList<Siswa>();
                        if (v.getCount() == 0) return new ArrayList<Siswa>();
                        v.moveToFirst();
                        jad.moveToFirst();
                        valid = v.getString(v.getColumnIndex("valid"));
                        status = jad.getString(jad.getColumnIndex("status"));
                        absen = v.getString(v.getColumnIndex("absen"));
                        v.moveToPosition(-1);
                        jad.moveToPosition(-1);
                    }
                    Siswa siswa = new Siswa();
                    siswa.setNo(String.valueOf(no++));
                    siswa.setNis(result.getString(result.getColumnIndex("id_siswa")));
                    siswa.setNama(result.getString(result.getColumnIndex("nama_lengkap")));
                    siswa.setGmbr(result.getString(result.getColumnIndex("gambar")));
                    siswa.setIdpenmpatan(result.getString(result.getColumnIndex("id_penempatan")));
                    siswa.setValid(valid);
                    siswa.setStatus(absen);
                    siswa.setStatusdua(status);
                    siswaList.add(siswa);
                } while (result.moveToNext());
            }
            return siswaList;
        } else {
            String queryKelas = "SELECT DISTINCT(tabel_siswakelas.id_penempatan),tabel_siswa.nama_lengkap,tabel_siswa.gambar, " +
                    "tabel_siswa.id_siswa,tabel_jenjang.jenjang,tabel_kelas.kelas,tabel_tahunajaran.tahun_ajaran,tabel_guru.nama, " +
                    "tabel_siswakelas.id_kelas,tabel_absen_mapel.valid,tabel_jadwal.status,tabel_absen_mapel.absen " +
                    "FROM tabel_siswakelas LEFT JOIN tabel_siswa " +
                    "ON (tabel_siswakelas.id_siswa = tabel_siswa.id_siswa) INNER JOIN tabel_jenjang " +
                    "ON (tabel_siswa.id_jenjang = tabel_jenjang.id_jenjang) LEFT JOIN tabel_kelas " +
                    "ON (tabel_siswakelas.id_kelas = tabel_kelas.id_kelas) INNER JOIN tabel_guru " +
                    "ON (tabel_kelas.id_guru = tabel_guru.id_guru) LEFT JOIN tabel_tahunajaran " +
                    "ON (tabel_siswakelas.id_ta = tabel_tahunajaran.id_ta) LEFT JOIN tabel_absen_mapel " +
                    "ON (tabel_siswakelas.id_penempatan = tabel_absen_mapel.id_penempatan) LEFT JOIN tabel_jadwal " +
                    "ON (tabel_absen_mapel.id_jadwal = tabel_jadwal.id_jadwal) " +
                    "WHERE tabel_kelas.id_kelas = ? AND tabel_absen_mapel.tgl = ? " +
                    "ORDER BY tabel_siswa.nama_lengkap ASC";
            Cursor result = db.rawQuery(queryKelas, new String[]{kelas, tanggal});
            ArrayList<Siswa> siswaList = new ArrayList<>();
            int no = 1;
            if (result.moveToFirst()) {
                do {
                    String id_penempatan = result.getString(result.getColumnIndex("id_penempatan"));
                    Cursor jad = db.rawQuery("SELECT * FROM tabel_jadwal WHERE id_jadwal = ?", new String[]{jadwal});

                    Cursor v = db.rawQuery("SELECT * FROM tabel_absen_mapel WHERE tgl = ? AND id_jadwal = ? " +
                            "AND id_penempatan = ?", new String[]{tanggal, jadwal, id_penempatan});
                    String status = "";
                    String valid = "";
                    String absen = "";
                    if (v.getCount() == 0) {
                        if (jad.getCount() == 0) return new ArrayList<Siswa>();
                        jad.moveToFirst();
                        status = jad.getString(jad.getColumnIndex("status"));
                        jad.moveToPosition(-1);
                        valid = "";
                        absen = "";
                    } else {
                        if (jad.getCount() == 0) return new ArrayList<Siswa>();
                        if (v.getCount() == 0) return new ArrayList<Siswa>();
                        v.moveToFirst();
                        jad.moveToFirst();
                        valid = v.getString(v.getColumnIndex("valid"));
                        status = jad.getString(jad.getColumnIndex("status"));
                        absen = v.getString(v.getColumnIndex("absen"));
                        v.moveToPosition(-1);
                        jad.moveToPosition(-1);
                    }
                    Siswa siswa = new Siswa();
                    siswa.setNo(String.valueOf(no++));
                    siswa.setNis(result.getString(result.getColumnIndex("id_siswa")));
                    siswa.setNama(result.getString(result.getColumnIndex("nama_lengkap")));
                    siswa.setGmbr(result.getString(result.getColumnIndex("gambar")));
                    siswa.setIdpenmpatan(result.getString(result.getColumnIndex("id_penempatan")));
                    siswa.setValid(result.getString(result.getColumnIndex("valid")));
                    siswa.setStatus(result.getString(result.getColumnIndex("absen")));
                    siswa.setStatusdua(result.getString(result.getColumnIndex("status")));
                    siswaList.add(siswa);
                } while (result.moveToNext());
            }
            return siswaList;
        }
    }

    public ArrayList<Siswa> getAllSiswaTambahan(String jadwalKelas) {
        SQLiteDatabase db = this.getWritableDatabase();
        Calendar calendar = Calendar.getInstance();
//        String jam = String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d", calendar.get(Calendar.MINUTE));
//        String hari = hariBerapa(calendar);
        String tanggal = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));

        String query = "SELECT kt.id_siswa,s.nama_lengkap,s.gambar,kt.id_kelas_tambahan,sk.id_penempatan,jt.nama_kelas " +
                "FROM tabel_jadwal_tambahan jt " +
                "INNER JOIN tabel_kelas_tambahan kt ON jt.id_jadwal_tambahan = kt.id_jadwal_tambahan " +
                "INNER JOIN tabel_siswa s ON kt.id_siswa = s.id_siswa " +
                "INNER JOIN tabel_siswakelas sk ON s.id_siswa = sk.id_siswa " +
                "WHERE jt.id_jadwal_tambahan = ?";
        Cursor result = db.rawQuery(query, new String[]{jadwalKelas});
        ArrayList<Siswa> siswaList = new ArrayList<>();
        Log.d(TAG, "getAllSiswaTambahan: total siswa kelas tambahan : " + result.getCount());
        if (result.getCount() > 0) {
            result.moveToFirst();
            int no = 1;
            do {
                String idKelasTambahan = result.getString(result.getColumnIndex("id_kelas_tambahan"));
                Cursor isAbsence = db.rawQuery("SELECT * FROM tabel_absen_mapel_tambahan " +
                                "WHERE id_jadwal_tambahan = ? AND id_kelas_tambahan = ? AND tgl = ?"
                        , new String[]{jadwalKelas, idKelasTambahan, tanggal});

                Siswa siswa = new Siswa();


                siswa.setNo(String.valueOf(no++));
                siswa.setNis(result.getString(result.getColumnIndex("id_siswa")));
                siswa.setNama(result.getString(result.getColumnIndex("nama_lengkap")));
                siswa.setGmbr(result.getString(result.getColumnIndex("gambar")));
                siswa.setIdpenmpatan(result.getString(result.getColumnIndex("id_penempatan")));
                if (isAbsence.getCount() > 0) {
                    isAbsence.moveToFirst();
                    siswa.setValid(isAbsence.getString(isAbsence.getColumnIndex("valid")));
                    siswa.setStatus(isAbsence.getString(isAbsence.getColumnIndex("absen")));
//                    siswa.setStatusdua(isAbsence.getString(isAbsence.getColumnIndex("status")));
                }
                siswa.setKelas(result.getString(result.getColumnIndex("nama_kelas")));
                siswa.setIdkelas(result.getString(result.getColumnIndex("id_kelas_tambahan")));

                siswaList.add(siswa);

            } while (result.moveToNext());
        }
        return siswaList;
    }

    public void fastAbsen(Context context, String kelas, String jadwal, Intent intent) {
        SQLiteDatabase db = this.getWritableDatabase();

        Calendar calendar = Calendar.getInstance();
        String jam = String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d", calendar.get(Calendar.MINUTE));
        String tanggal = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));

        Cursor cek = db.rawQuery("SELECT tabel_siswa.id_siswa,tabel_siswa.nama_lengkap,tabel_kelas.kelas, " +
                "tabel_siswa.email,tabel_siswakelas.id_penempatan FROM tabel_siswakelas INNER JOIN tabel_siswa " +
                "ON tabel_siswakelas.id_siswa=tabel_siswa.id_siswa INNER JOIN tabel_kelas " +
                "ON tabel_siswakelas.id_kelas = tabel_kelas.id_kelas " +
                "WHERE tabel_siswakelas.id_kelas=?", new String[]{kelas});

        if (cek.getCount() > 0) {
            if (cek.moveToFirst()) {
                do {
                    String id_s = cek.getString(cek.getColumnIndex("id_penempatan"));
                    Cursor cek_absen = db.rawQuery("SELECT tabel_absen_mapel.*,tabel_jadwal.status " +
                            "FROM tabel_absen_mapel INNER JOIN tabel_jadwal ON tabel_absen_mapel.id_jadwal=tabel_jadwal.id_jadwal " +
                            "WHERE tabel_absen_mapel.id_penempatan = ? AND tabel_absen_mapel.tgl = ? " +
                            "AND tabel_absen_mapel.id_jadwal = ?", new String[]{id_s, tanggal, jadwal});

                    if (cek_absen.getCount() == 0) {
                        Cursor amb = db.rawQuery("SELECT * FROM tabel_jadwal WHERE id_jadwal = ?", new String[]{jadwal});
                        if (amb.getCount() == 0) return;
                        amb.moveToFirst();
                        String status = amb.getString(amb.getColumnIndex("status"));
                        amb.moveToPosition(-1);
                        db.execSQL("INSERT INTO tabel_absen_mapel (id_penempatan, id_jadwal, tgl, jam, absen, valid, rekap, status) VALUES ('" + id_s + "','" + jadwal + "','" + tanggal + "','" + jam + "','hadir','','','" + status + "')");

                    }
                } while (cek.moveToNext());
            }
        }
    }

    public void fastAbsenTambahan(String jadwalTambahan) {
        SQLiteDatabase db = this.getWritableDatabase();

        Calendar calendar = Calendar.getInstance();
        String jam = String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d", calendar.get(Calendar.MINUTE));
        String tanggal = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));

        Cursor listOfStudent = db.rawQuery("SELECT * FROM tabel_kelas_tambahan WHERE id_jadwal_tambahan = ?",
                new String[]{jadwalTambahan});
        if (listOfStudent.getCount() > 0) {
            listOfStudent.moveToFirst();

            do {
                String idKelasTambahan = listOfStudent.getString(listOfStudent.getColumnIndex("id_kelas_tambahan"));

                // check each student is already absence today
                Cursor isAbsence = db.rawQuery("SELECT * FROM tabel_absen_mapel_tambahan " +
                                "WHERE id_kelas_tambahan = ? AND id_jadwal_tambahan = ? AND tgl = ?",
                        new String[]{idKelasTambahan, jadwalTambahan, tanggal});

                if (isAbsence.getCount() == 0) {
                    db.execSQL("INSERT INTO tabel_absen_mapel_tambahan(id_kelas_tambahan,id_jadwal_tambahan,tgl,jam,absen)" +
                            "VALUES('" + idKelasTambahan + "','" + jadwalTambahan + "','" + tanggal + "','" + jam + "','absen')");
                }
            } while (listOfStudent.moveToNext());
        }
    }

    public void saveAbsen(String idpenempatan, String jadwal, String ketAbsen, String status) {
        SQLiteDatabase db = this.getWritableDatabase();

        Calendar calendar = Calendar.getInstance();
        String jam = String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d", calendar.get(Calendar.MINUTE));
        String tanggal = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));

        Cursor am = db.rawQuery("SELECT * FROM tabel_jadwal WHERE id_jadwal = ?", new String[]{jadwal});
        if (am.getCount() == 0) return;
        am.moveToFirst();
        status = am.getString(am.getColumnIndex("status"));
        am.moveToPosition(-1);

        String valid = "";
        String rekap = "";

        Cursor cek = db.rawQuery("SELECT COUNT(*) as jum from tabel_absen_mapel where id_penempatan = ? AND " +
                " tgl = ? AND id_jadwal = ?", new String[]{idpenempatan, tanggal, jadwal});
        cek.moveToFirst();
        Integer hasil = Integer.valueOf(cek.getString(cek.getColumnIndex("jum")));
        cek.moveToPosition(-1);
        if (hasil == 0) {
            String c = "input";
            db.execSQL("INSERT INTO tabel_absen_mapel (id_penempatan, id_jadwal, tgl, jam, absen, valid, rekap, status) values('" + idpenempatan + "','" + jadwal + "','" + tanggal + "','" + jam + "','" + ketAbsen + "','" + valid + "','" + rekap + "','" + status + "')");
        } else {
            String c = "update";
            db.execSQL("UPDATE tabel_absen_mapel set absen = '" + ketAbsen + "' where id_penempatan='" + idpenempatan + "' and tgl='" + tanggal + "' AND id_jadwal = '" + jadwal + "'");
        }
    }

    public void saveAbsenTambahan(String idpenempatan, String jadwalTambahan, String absen) {

        SQLiteDatabase db = this.getWritableDatabase();
        Calendar calendar = Calendar.getInstance();
        String jam = String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d", calendar.get(Calendar.MINUTE));
        String tanggal = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));

        Cursor getDataSiswa = db.rawQuery("SELECT * FROM tabel_siswakelas WHERE id_penempatan = ? LIMIT 1",
                new String[]{idpenempatan});
        if (getDataSiswa.getCount() > 0) {
            getDataSiswa.moveToFirst();
            String idSiswa = getDataSiswa.getString(getDataSiswa.getColumnIndex("id_siswa"));

            Cursor getIdKelastambahan = db.rawQuery("SELECT * " +
                            "FROM tabel_kelas_tambahan WHERE id_jadwal_tambahan = ? AND id_siswa = ? LIMIT 1",
                    new String[]{jadwalTambahan, idSiswa});
            if (getIdKelastambahan.getCount() > 0) {
                getIdKelastambahan.moveToFirst();
                String kelasTambahan = getIdKelastambahan.getString(getIdKelastambahan.getColumnIndex("id_kelas_tambahan"));

                Cursor isAbsent = db.rawQuery("SELECT * FROM tabel_absen_mapel_tambahan " +
                                "WHERE id_kelas_tambahan = ? AND id_jadwal_tambahan = ? AND tgl = ? LIMIT 1",
                        new String[]{kelasTambahan, jadwalTambahan, tanggal});

                if (isAbsent.getCount() > 0) {
                    isAbsent.moveToFirst();
                    String idKelastambahan = isAbsent.getString(isAbsent.getColumnIndex("id_kelas_tambahan"));
                    db.execSQL("UPDATE tabel_absen_mapel_tambahan SET absen = '" + absen + "' " +
                            "WHERE id_kelas_tambahan = '" + idKelastambahan + "' AND id_jadwal_tambahan ='" + jadwalTambahan + "' " +
                            "AND tgl = '" + tanggal + "'");
                } else {

                    db.execSQL("INSERT INTO tabel_absen_mapel_tambahan(id_kelas_tambahan,id_jadwal_tambahan,tgl,jam,absen,keterangan)" +
                            "VALUES('" + kelasTambahan + "','" + jadwalTambahan + "','" + tanggal + "','" + jam + "','absen','offline')");
                }
            }
        }
    }

    public String saveValidasiMasuk(String kelas, String jadwal, String status) {
        SQLiteDatabase db = this.getWritableDatabase();

        Calendar calendar = Calendar.getInstance();
        String jam = String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY)) + ":" + String.format("%02d", calendar.get(Calendar.MINUTE));
        String tanggal = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));

        Cursor result = db.rawQuery("SELECT tabel_siswa.id_siswa,tabel_siswa.nama_lengkap,tabel_kelas.kelas,tabel_siswakelas.id_ta, " +
                        "tabel_siswa.email,tabel_siswakelas.id_penempatan FROM tabel_siswakelas INNER JOIN tabel_siswa " +
                        "ON tabel_siswakelas.id_siswa=tabel_siswa.id_siswa INNER JOIN tabel_kelas " +
                        "ON tabel_siswakelas.id_kelas = tabel_kelas.id_kelas " +
                        "WHERE tabel_siswakelas.id_kelas=? AND tabel_siswakelas.id_penempatan IN (SELECT id_penempatan FROM tabel_absen_mapel " +
                        "WHERE tabel_absen_mapel.tgl=? AND tabel_absen_mapel.id_jadwal = ?) ORDER BY tabel_siswakelas.id_penempatan",
                new String[]{kelas, tanggal, jadwal});

        Cursor am = db.rawQuery("SELECT * FROM tabel_jadwal WHERE id_jadwal = ?", new String[]{jadwal});
        if (am.getCount() == 0) return "error";
        am.moveToFirst();
        String jamAkhir = am.getString(am.getColumnIndex("jam_akhir"));
        String idTa = am.getString(am.getColumnIndex("id_ta"));
        am.moveToPosition(-1);

        Calendar akhir = calendar;
        akhir.set(Calendar.HOUR_OF_DAY, Integer.valueOf(jamAkhir.substring(0, 2)));
        akhir.set(Calendar.MINUTE, Integer.valueOf(jamAkhir.substring(3, 5)));

        Cursor setting = db.rawQuery("select * from tabel_setting where id_setting = ?", new String[]{"1"});
        if (setting.getCount() == 0) return "error";
        setting.moveToFirst();
        Long detik = Long.valueOf(setting.getString(setting.getColumnIndex("toleransi_jam_akhir"))) * 60;
        setting.moveToPosition(-1);

        Cursor am2 = db.rawQuery("SELECT COUNT(id_penempatan) AS jum FROM tabel_siswakelas WHERE id_kelas = ? " +
                " AND id_ta = ?", new String[]{kelas, idTa});
//        if (am2.getCount() == 0) return "error";
        am2.moveToFirst();
        Integer valid2 = Integer.valueOf(am2.getString(am2.getColumnIndex("jum")));
        am2.moveToPosition(-1);
        if (result.getCount() > 0) {
            if (result.getCount() == valid2) {
                Long menit = detik / 60;
                if (result.moveToFirst()) {
                    do {
                        String id_s = result.getString(result.getColumnIndex("id_penempatan"));
                        if (status.equals(""))
                            db.execSQL("UPDATE tabel_absen_mapel SET rekap = '-',valid = 'valid',keterangan = 'offline' " +
                                    "WHERE id_penempatan='" + id_s + "' AND tgl='" + tanggal + "' AND tabel_absen_mapel.id_jadwal = '" + jadwal + "'");
                        else
                            db.execSQL("UPDATE tabel_absen_mapel SET rekap = '-',valid = 'valid',keterangan = '" + status + "' " +
                                    "WHERE id_penempatan='" + id_s + "' AND tgl='" + tanggal + "' AND tabel_absen_mapel.id_jadwal = '" + jadwal + "'");
                    } while (result.moveToNext());
                }
                return "sip";

            } else {
                return "error";
            }
        } else {
            return "error";
        }
    }

    public String saveValidasiMasukTambahan(String jadwaltambahan) {
        SQLiteDatabase db = this.getWritableDatabase();
        Calendar calendar = Calendar.getInstance();
        String tanggal = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));

        Cursor isNotyet = db.rawQuery("SELECT * FROM tabel_absen_mapel_tambahan " +
                        "WHERE id_kelas_tambahan IN (SELECT id_kelas_tambahan \n" +
                        "FROM tabel_kelas_tambahan WHERE id_jadwal_tambahan = ?) " +
                        "AND tgl = ? AND (absen is null or absen ='')",
                new String[]{jadwaltambahan, tanggal});
        if (isNotyet.getCount() > 0) {
            return "error";
        } else {
            // get all student
            Cursor getAllStudent = db.rawQuery("SELECT * FROM tabel_absen_mapel_tambahan " +
                    "WHERE id_kelas_tambahan IN (SELECT id_kelas_tambahan \n" +
                    "FROM tabel_kelas_tambahan WHERE id_jadwal_tambahan = ?) " +
                    "AND tgl = ?", new String[]{jadwaltambahan, tanggal});
            if (getAllStudent.getCount() > 0) {
                getAllStudent.moveToFirst();
                do {
                    String idKelastambahan = getAllStudent.getString(getAllStudent.getColumnIndex("id_kelas_tambahan"));
                    db.execSQL("UPDATE tabel_absen_mapel_tambahan SET valid = 'valid',rekap = '-',keterangan = 'offline' " +
                            "WHERE id_jadwal_tambahan = '" + jadwaltambahan + "' " +
                            "AND id_kelas_tambahan = '" + idKelastambahan + "' AND tgl = '" + tanggal + "'");
                } while (getAllStudent.moveToNext());

            }
            return "sip";
        }
    }

    public String cekLogin(Context context, String username, String password) {
        SessionManager sessionManager = new SessionManager(context);
        SQLiteDatabase db = this.getWritableDatabase();

        String queryUser = "SELECT * FROM tabel_user JOIN tabel_guru ON tabel_guru.nik = tabel_user.username WHERE username='" + username + "' AND password=md5('" + password + "') AND level='guru'";
        queryUser = "SELECT * FROM tabel_user JOIN tabel_guru ON tabel_guru.nik = tabel_user.username WHERE username='" + username + "' AND level='guru'";

        Cursor result = db.rawQuery(queryUser, null);
        if (result.getCount() > 0) {
            boolean isKepsek = result.getString(result.getColumnIndex("status_kepsek")).equals("1");
            result.moveToFirst();
            sessionManager.setLogin(true, "", result.getString(result.getColumnIndex("id_guru")), result.getString(result.getColumnIndex("username")), result.getString(result.getColumnIndex("password")), result.getString(result.getColumnIndex("status_guru_bk")), "N",result.getString(result.getColumnIndex("id_user")),isKepsek);
            return "ok";
        } else {
            return "gagal";
        }
    }

    public void ambilUser(Context context, String username, TextView nmHead, TextView nmWali) {
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "select * from tabel_user where username='" + username + "'";
        Cursor result = db.rawQuery(query, null);

        if (result.getCount() > 0) {
            result.moveToFirst();
            nmHead.setText(result.getString(result.getColumnIndex("nama")));
            nmWali.setText(result.getString(result.getColumnIndex("nama")));
        }
    }

    public void ambilDepan(Context context, String username, TextView nmSekolah, TextView
            alamat, boolean isGuru) {
        SQLiteDatabase db = this.getWritableDatabase();
        if (isGuru) {
            Cursor isi = db.rawQuery("SELECT * FROM tabel_guru LEFT JOIN tabel_kelas ON tabel_guru.id_guru=tabel_kelas.id_guru " +
                    " WHERE tabel_guru.nik = '" + username + "'", null);
            if (isi.getCount() == 0) return;
            isi.moveToFirst();
            String guru = isi.getString(isi.getColumnIndex("nama"));
            String kelas = isi.getString(isi.getColumnIndex("kelas"));
            isi.moveToPosition(-1);

            Cursor isi2 = db.rawQuery("SELECT * FROM tabel_setting WHERE id_setting = '1'", null);
            if (isi2.getCount() == 0) return;
            isi2.moveToFirst();
            String namasekolah = isi2.getString(isi2.getColumnIndex("logo_besar"));
            String namaalamat = isi2.getString(isi2.getColumnIndex("alamat"));

            nmSekolah.setText(namasekolah);
            alamat.setText(namaalamat);
        } else {
            Cursor isi2 = db.rawQuery("SELECT * FROM tabel_setting WHERE id_setting = '1'", null);
            if (isi2.getCount() == 0) return;
            isi2.moveToFirst();
            String namasekolah = isi2.getString(isi2.getColumnIndex("logo_besar"));
            String namaalamat = isi2.getString(isi2.getColumnIndex("alamat"));

            nmSekolah.setText(namasekolah);
            alamat.setText(namaalamat);
        }


    }

    public Boolean cekStatusWaliKelas(Context context, String idguru, String idkelas) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cek = db.rawQuery("SELECT * FROM tabel_kelas inner join tabel_guru on tabel_kelas.id_guru = tabel_guru.id_guru " +
                "where tabel_guru.nik = '" + idguru + "' and tabel_kelas.id_kelas = '" + idkelas + "'", null);
        int ac = cek.getCount();
        if (ac > 0) {
            return true;
        } else return false;
    }

    public Boolean cekStatusWaliKelasTambahan(Context context, String idguru, String
            idJadwalTambahan) {
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cek = db.rawQuery("SELECT * FROM tabel_jadwal_tambahan inner join tabel_guru on tabel_jadwal_tambahan.id_guru = tabel_guru.id_guru " +
                "where tabel_guru.nik = '" + idguru + "' and tabel_jadwal_tambahan.id_jadwal_tambahan = '" + idJadwalTambahan + "'", null);
        int ac = cek.getCount();
        if (ac > 0) {
            return true;
        } else return false;
    }

    public ArrayList<Siswa> getDataSiswa(Context context, String guru) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor result = db.rawQuery("SELECT *, tabel_siswa.gambar as gambar_siswa FROM  tabel_siswa " +
                "inner join tabel_siswakelas on tabel_siswa.id_siswa = tabel_siswakelas.id_siswa " +
                "inner join tabel_kelas on tabel_siswakelas.id_kelas = tabel_kelas.id_kelas " +
                "inner join tabel_guru on tabel_kelas.id_guru = tabel_guru.id_guru " +
                "WHERE tabel_guru.nik='" + guru + "' order by tabel_siswa.nama_lengkap", null);

        ArrayList<Siswa> temp = new ArrayList<>();
        int no = 1;
        if (result.moveToFirst()) {
            do {
                Siswa siswa = new Siswa();
                siswa.setNo(String.valueOf(no++));
                siswa.setNis(result.getString(result.getColumnIndex("id_siswa")));
                siswa.setNama(result.getString(result.getColumnIndex("nama_lengkap")));
                siswa.setIdpenmpatan(result.getString(result.getColumnIndex("id_penempatan")));
                siswa.setIdkelas(result.getString(result.getColumnIndex("id_kelas")));
                siswa.setKelas(result.getString(result.getColumnIndex("kelas")));
                temp.add(siswa);
            } while (result.moveToNext());
        }

        return temp;
    }

    public Location getLocation(Context context) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor result = db.rawQuery("SELECT * from tabel_setting", null);
        if (result.getCount() == 0) return new Location("");
        result.moveToFirst();

        double lat = Double.valueOf(result.getString(result.getColumnIndex("lat")));
        double lng = Double.valueOf(result.getString(result.getColumnIndex("lng")));
        Location location = new Location("");
        location.setLatitude(lat);
        location.setLongitude(lng);

        return location;
    }

    public double getRadius(Context context) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor result = db.rawQuery("SELECT * from tabel_setting", null);
        if (result.getCount() == 0) return 0.0;
        result.moveToFirst();

        double radius = Double.valueOf(result.getString(result.getColumnIndex("radius")));

        return radius;
    }

    public String cekStatusKepsek(Context context, String idguru) {
        SQLiteDatabase db = this.getWritableDatabase();

        Cursor result = db.rawQuery("SELECT status_kepsek from tabel_guru where nik = '" + idguru + "'", null);
        if (result.getCount() == 0) return "0";
        result.moveToFirst();

        return result.getString(result.getColumnIndex("status_kepsek"));
    }

    /**
     * function to insert into local database in tabel_absen_kehadiran_guru when clicking
     * button coming at home page
     * @param context
     * @param idguru
     * @param keterangan
     */
    public void absenKehadiranGuruOnline(Context context, String idguru,String keterangan) {
        SQLiteDatabase db = this.getWritableDatabase();
        Calendar calendar = Calendar.getInstance();
        String tgl = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));
        String jam = jamBerapa(calendar);
        db.execSQL("INSERT INTO tabel_absen_kehadiran_guru(id_guru, tgl, jam, keterangan) VALUES ('" + idguru + "','" + tgl + "','" + jam + "','"+keterangan+"')");
    }
    /**
     * function to insert into local database in tabel_absen_non_guru when clicking
     * button coming at home page
     * @param context
     * @param idnonguru
     * @param keterangan
     */
    public void absenKehadiranNonGuruOnline(Context context, String idnonguru,String keterangan,String jenis) {
        SQLiteDatabase db = this.getWritableDatabase();
        Calendar calendar = Calendar.getInstance();
        String tgl = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));
        String jam = jamBerapa(calendar);
        db.execSQL("INSERT INTO tabel_absen_non_guru(id_non_guru, tgl, jam, keterangan,jenis) VALUES ('" + idnonguru + "','" + tgl + "','" + jam + "','"+keterangan+"','"+jenis+"')");
    }

    public KehadiranResponse absenHadirGuruOffline(Context context, String idguru, LatLng ll){

        SQLiteDatabase db = this.getWritableDatabase();
        Calendar calendar = Calendar.getInstance();
        String tgl = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));
        String jam = jamBerapa(calendar);
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        String hari;
        switch (day) {
            case 1:
                hari = "0 Minggu";
                break;
            case 2:
                hari = "1 Senin";
                break;
            case 3:
                hari = "2 Selasa";
                break;
            case 4:
                hari = "3 Rabu";
                break;
            case 5:
                hari = "4 Kamis";
                break;
            case 6:
                hari = "5 Jumat";
                break;
            case 7:
                hari = "6 Sabtu";
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + day);
        }
        /**
         *  1 = Minggu
         *  2 = Senin
         *  3 = Selasa
         *  4 = Rabu
         *  5 = Kamis
         *  6 = Jumat
         *  7 = Sabtu
         */

        Cursor cekHariLibur = db.rawQuery("SELECT * from tabel_kalender where tanggal = '" + tgl + "'", null);
        int totalHariLibur = cekHariLibur.getCount();

        Cursor cekAbsensi = db.rawQuery("SELECT * from tabel_absen_kehadiran_guru " +
                "where id_guru = '" + idguru + "' and tgl = '" + tgl + "' and keterangan not like '%pulang%'", null);
        int totalAbsensi = cekAbsensi.getCount();

        Cursor setting = db.rawQuery("SELECT * from tabel_setting", null);

        KehadiranResponse kehadiranResponse = new KehadiranResponse();
        kehadiranResponse.setStatus("error");
        kehadiranResponse.setKeterangan("error");

        if (setting.getCount() == 0) return kehadiranResponse;
        setting.moveToFirst();
        int radius = setting.getInt(setting.getColumnIndex("radius"));
        Cursor jamMasuk = db.rawQuery("select * from tabel_jam_masuk where hari = '" + hari + "'", null);

        kehadiranResponse = new KehadiranResponse();
        kehadiranResponse.setStatus("error");
        kehadiranResponse.setKeterangan("error");

        if (jamMasuk.getCount() == 0) return kehadiranResponse;
        jamMasuk.moveToFirst();
        String jammasukkerja = jamMasuk.getString(jamMasuk.getColumnIndex("jam"));

        Calendar calendarJamMasukKerja = Calendar.getInstance();
        calendarJamMasukKerja.set(Calendar.HOUR_OF_DAY, Integer.valueOf(jammasukkerja.substring(0, 2)));
        calendarJamMasukKerja.set(Calendar.MINUTE, Integer.valueOf(jammasukkerja.substring(3, 5)));

        /* cek jarak antara posisi user dengan sekolah */
        Location locUser = new Location("point a");
        locUser.setLatitude(ll.latitude);
        locUser.setLongitude(ll.longitude);

        Location locScholl = new Location("point b");
        double setLat = setting.getDouble(setting.getColumnIndex("lat"));
        double setLng = setting.getDouble(setting.getColumnIndex("lng"));
        locScholl.setLatitude(setLat);
        locScholl.setLongitude(setLng);

        Log.d(TAG, "absenKehadiranGuru: jarak : "+ll.latitude+","+ll.longitude+ " to "+setting.getColumnIndex("lat")+","+setting.getColumnIndex("lng"));
        double jarak = locUser.distanceTo(locScholl);

        KehadiranResponse response = null;

        //apakah hari ini bukan hari minggu ?
        if(day != 0){
            // apakah hari ini bukan hari libur ?
            if(totalHariLibur == 0){
                //apakah belum absen masuk sebelumnya ?
                if(totalAbsensi == 0){
                    //apakah jarak absen diijinkan ?
                    if(jarak < radius){
                        long seconds = (calendar.getTimeInMillis() - calendarJamMasukKerja.getTimeInMillis()) / 1000;
                        int menit = (int) (seconds / 60);

                        String keteranganJamMasuk = "";
                        if (menit > 0) {
                            keteranganJamMasuk += "terlambat" + " " + menit + " menit (offline)";
                        } else {
                            keteranganJamMasuk += "absen sesuai (offline)";
                        }
                        db.execSQL("INSERT INTO tabel_absen_kehadiran_guru(id_guru, tgl, jam, keterangan) VALUES ('" + idguru + "','" + tgl + "','" + jam + "','"+keteranganJamMasuk+"')");
                        response = setResponse("ok" , "Absensi berhasil,"+keteranganJamMasuk);
                    }else {
                        response = setResponse("error" , "Absensi gagal, jarak anda dengan sekolah lebih dari "+radius+" meter, yaitu "+jarak+" meter");
                    }
                }else{
                    response = setResponse("error" , "Absensi gagal, Anda sudah absen sebelumnya");
                }
            }else{
                response = setResponse("error" , "Absensi gagal, hari ini hari libur");
            }
        }else{
           response = setResponse("error" , "Absensi gagal, hari ini hari libur");
        }

        return response;
    }

    public KehadiranResponse absenHadirNonGuruOffline(Context context, String idNonguru, LatLng ll){

        SQLiteDatabase db = this.getWritableDatabase();
        Calendar calendar = Calendar.getInstance();
        String tgl = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));
        String jam = jamBerapa(calendar);
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        String hari;
        switch (day) {
            case 1:
                hari = "0 Minggu";
                break;
            case 2:
                hari = "1 Senin";
                break;
            case 3:
                hari = "2 Selasa";
                break;
            case 4:
                hari = "3 Rabu";
                break;
            case 5:
                hari = "4 Kamis";
                break;
            case 6:
                hari = "5 Jumat";
                break;
            case 7:
                hari = "6 Sabtu";
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + day);
        }
        /**
         *  1 = Minggu
         *  2 = Senin
         *  3 = Selasa
         *  4 = Rabu
         *  5 = Kamis
         *  6 = Jumat
         *  7 = Sabtu
         */

        Cursor cekHariLibur = db.rawQuery("SELECT * from tabel_kalender where tanggal = '" + tgl + "'", null);
        int totalHariLibur = cekHariLibur.getCount();

        Cursor cekAbsensi = db.rawQuery("SELECT * from tabel_absen_non_guru where id_non_guru = '" + idNonguru + "' and tgl = '" + tgl + "' and jenis = 'hadir'", null);
        int totalAbsensi = cekAbsensi.getCount();

        Cursor setting = db.rawQuery("SELECT * from tabel_setting", null);

        KehadiranResponse kehadiranResponse = new KehadiranResponse();
        kehadiranResponse.setStatus("error");
        kehadiranResponse.setKeterangan("error");

        if (setting.getCount() == 0) return kehadiranResponse;
        setting.moveToFirst();
        int radius = setting.getInt(setting.getColumnIndex("radius"));
        Cursor jamMasuk = db.rawQuery("select * from tabel_jam_masuk where hari = '" + hari + "'", null);

        kehadiranResponse = new KehadiranResponse();
        kehadiranResponse.setStatus("error");
        kehadiranResponse.setKeterangan("error");

        if (jamMasuk.getCount() == 0) return kehadiranResponse;
        jamMasuk.moveToFirst();
        String jammasukkerja = jamMasuk.getString(jamMasuk.getColumnIndex("jam"));

        Calendar calendarJamMasukKerja = Calendar.getInstance();
        calendarJamMasukKerja.set(Calendar.HOUR_OF_DAY, Integer.valueOf(jammasukkerja.substring(0, 2)));
        calendarJamMasukKerja.set(Calendar.MINUTE, Integer.valueOf(jammasukkerja.substring(3, 5)));

        /* cek jarak antara posisi user dengan sekolah */
        Location locUser = new Location("point a");
        locUser.setLatitude(ll.latitude);
        locUser.setLongitude(ll.longitude);

        Location locScholl = new Location("point b");
        double setLat = setting.getDouble(setting.getColumnIndex("lat"));
        double setLng = setting.getDouble(setting.getColumnIndex("lng"));
        locScholl.setLatitude(setLat);
        locScholl.setLongitude(setLng);

        Log.d(TAG, "absenKehadiranGuru: jarak : "+ll.latitude+","+ll.longitude+ " to "+setting.getColumnIndex("lat")+","+setting.getColumnIndex("lng"));
        double jarak = locUser.distanceTo(locScholl);

        KehadiranResponse response;

        if(day != 0){
            if(totalHariLibur == 0){
                if(totalAbsensi == 0){
                    if(jarak < radius){
                        long seconds = (calendar.getTimeInMillis() - calendarJamMasukKerja.getTimeInMillis()) / 1000;
                        int menit = (int) (seconds / 60);

                        String keteranganJamMasuk = "";
                        if (menit > 0) {
                            keteranganJamMasuk += "terlambat" + " " + menit + " menit (offline)";
                        } else {
                            keteranganJamMasuk += "absen sesuai (offline)";
                        }
                        db.execSQL("INSERT INTO tabel_absen_non_guru(id_non_guru, tgl, jam, keterangan,jenis) VALUES ('" + idNonguru + "','" + tgl + "','" + jam + "','"+keteranganJamMasuk+"','hadir')");
                        response = setResponse("ok" , "Absensi berhasil,"+keteranganJamMasuk);
                    }else {
                        response = setResponse("error" , "Absensi gagal, jarak anda dengan sekolah lebih dari "+radius+" meter, yaitu "+jarak+" meter");
                    }
                }else{
                    response = setResponse("error" , "Absensi gagal, Anda sudah absen sebelumnya");
                }
            }else{
                response = setResponse("error" , "Absensi gagal, hari ini hari libur");
            }
        }else{
            response = setResponse("error" , "Absensi gagal, hari ini hari libur");
        }

        return response;
    }

    public KehadiranResponse absenPulangGuruOffline(Context context, String idguru, LatLng ll) throws ParseException {

        SQLiteDatabase db = this.getWritableDatabase();
        Calendar calendar = Calendar.getInstance();
        String tgl = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));
        String jam = jamBerapa(calendar);
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        String hari;
        switch (day) {
            case 1:
                hari = "0 Minggu";
                break;
            case 2:
                hari = "1 Senin";
                break;
            case 3:
                hari = "2 Selasa";
                break;
            case 4:
                hari = "3 Rabu";
                break;
            case 5:
                hari = "4 Kamis";
                break;
            case 6:
                hari = "5 Jumat";
                break;
            case 7:
                hari = "6 Sabtu";
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + day);
        }
        /**
         *  1 = Minggu
         *  2 = Senin
         *  3 = Selasa
         *  4 = Rabu
         *  5 = Kamis
         *  6 = Jumat
         *  7 = Sabtu
         */

        Cursor cekHariLibur = db.rawQuery("SELECT * from tabel_kalender where tanggal = '" + tgl + "'", null);
        int totalHariLibur = cekHariLibur.getCount();

        Cursor cekAbsensi = db.rawQuery("SELECT * from tabel_absen_kehadiran_guru " +
                "where id_guru = '" + idguru + "' and tgl = '" + tgl + "' and keterangan like '%pulang%'", null);
        int totalAbsensi = cekAbsensi.getCount();

        Cursor setting = db.rawQuery("SELECT * from tabel_setting", null);

        KehadiranResponse kehadiranResponse = new KehadiranResponse();
        kehadiranResponse.setStatus("error");
        kehadiranResponse.setKeterangan("error");

        if (setting.getCount() == 0) return kehadiranResponse;
        setting.moveToFirst();
        int radius = setting.getInt(setting.getColumnIndex("radius"));
        String jamPulang = setting.getString(setting.getColumnIndex("jam_pulang"));

        /* cek jarak antara posisi user dengan sekolah */
        Location locUser = new Location("point a");
        locUser.setLatitude(ll.latitude);
        locUser.setLongitude(ll.longitude);

        Location locScholl = new Location("point b");
        double setLat = setting.getDouble(setting.getColumnIndex("lat"));
        double setLng = setting.getDouble(setting.getColumnIndex("lng"));
        locScholl.setLatitude(setLat);
        locScholl.setLongitude(setLng);

        Log.d(TAG, "absenKehadiranGuru: jarak : "+ll.latitude+","+ll.longitude+ " to "+setting.getColumnIndex("lat")+","+setting.getColumnIndex("lng"));
        double jarak = locUser.distanceTo(locScholl);

        KehadiranResponse response = null;

        //apakah hari ini bukan hari minggu ?
        if(day != 0){
            //apakah hari ini bukan hari libur ?
            if(totalHariLibur == 0){
                //apakah anda belum absen pulang ?
                if(totalAbsensi == 0){
                    //apakah jarak diperbolehkan ?
                    if(jarak < radius){
                        SimpleDateFormat f = new SimpleDateFormat("HH:mm");
                        String timeNow = f.format(new Date());
                        Date waktuPulang = f.parse(jamPulang);
                        Date waktuSekarang = f.parse(timeNow);

                        //apakah sudah waktunnya pulang ?
                        if(waktuPulang.before(waktuSekarang)){
                            Calendar wPulang = Calendar.getInstance();
                            wPulang.set(Calendar.HOUR_OF_DAY, Integer.valueOf(jamPulang.substring(0,2)));
                            wPulang.set(Calendar.MINUTE, Integer.valueOf(jamPulang.substring(3,5)));

                            long seconds = (calendar.getTimeInMillis() - wPulang.getTimeInMillis()) / 1000;
                            int menit = (int) (seconds / 60);

                            String keteranganJamPulang = "";
                            if (menit > 0) {
                                keteranganJamPulang += "jam pulang lebih" + " " + menit + " menit (offline)";
                            } else {
                                keteranganJamPulang += "absen pulang sesuai (offline)";
                            }
                            db.execSQL("INSERT INTO tabel_absen_kehadiran_guru(id_guru, tgl, jam, keterangan) VALUES ('" + idguru + "','" + tgl + "','" + jam + "','"+keteranganJamPulang+"')");
                            response = setResponse("ok" , "Absensi berhasil,"+keteranganJamPulang);
                        }else{
                            response = setResponse("error" , "Absensi gagal, belum waktunya pulang");
                        }
                    }else {
                        response = setResponse("error" , "Absensi gagal, jarak anda dengan sekolah lebih dari "+radius+" meter, yaitu "+jarak+" meter");
                    }
                }else{
                    response = setResponse("error" , "Absensi gagal, Anda sudah absen sebelumnya");
                }
            }else{
                response = setResponse("error" , "Absensi gagal, hari ini hari libur");
            }
        }else{
            response = setResponse("error" , "Absensi gagal, hari ini hari libur");
        }

        return response;
    }

    public KehadiranResponse absenPulangNonGuruOffline(Context context, String idnonguru, LatLng ll) throws ParseException {

        SQLiteDatabase db = this.getWritableDatabase();
        Calendar calendar = Calendar.getInstance();
        String tgl = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));
        String jam = jamBerapa(calendar);
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        String hari;
        switch (day) {
            case 1:
                hari = "0 Minggu";
                break;
            case 2:
                hari = "1 Senin";
                break;
            case 3:
                hari = "2 Selasa";
                break;
            case 4:
                hari = "3 Rabu";
                break;
            case 5:
                hari = "4 Kamis";
                break;
            case 6:
                hari = "5 Jumat";
                break;
            case 7:
                hari = "6 Sabtu";
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + day);
        }
        /**
         *  1 = Minggu
         *  2 = Senin
         *  3 = Selasa
         *  4 = Rabu
         *  5 = Kamis
         *  6 = Jumat
         *  7 = Sabtu
         */

        Cursor cekHariLibur = db.rawQuery("SELECT * from tabel_kalender where tanggal = '" + tgl + "'", null);
        int totalHariLibur = cekHariLibur.getCount();

        Cursor cekAbsensi = db.rawQuery("SELECT * from tabel_absen_non_guru " +
                "where id_non_guru = '" + idnonguru + "' and tgl = '" + tgl + "' and jenis = 'pulang'", null);
        int totalAbsensi = cekAbsensi.getCount();

        Cursor setting = db.rawQuery("SELECT * from tabel_setting", null);

        KehadiranResponse kehadiranResponse = new KehadiranResponse();
        kehadiranResponse.setStatus("error");
        kehadiranResponse.setKeterangan("error");

        if (setting.getCount() == 0) return kehadiranResponse;
        setting.moveToFirst();
        int radius = setting.getInt(setting.getColumnIndex("radius"));
        String jamPulang = setting.getString(setting.getColumnIndex("jam_pulang"));

        /* cek jarak antara posisi user dengan sekolah */
        Location locUser = new Location("point a");
        locUser.setLatitude(ll.latitude);
        locUser.setLongitude(ll.longitude);

        Location locScholl = new Location("point b");
        double setLat = setting.getDouble(setting.getColumnIndex("lat"));
        double setLng = setting.getDouble(setting.getColumnIndex("lng"));
        locScholl.setLatitude(setLat);
        locScholl.setLongitude(setLng);

        Log.d(TAG, "absenKehadiranGuru: jarak : "+ll.latitude+","+ll.longitude+ " to "+setting.getColumnIndex("lat")+","+setting.getColumnIndex("lng"));
        double jarak = locUser.distanceTo(locScholl);

        KehadiranResponse response;
        //apakah hari ini bukan hari minggu ?
        if(day != 0){
            // apakah hari ini bukan hari libur ?
            if(totalHariLibur == 0){
                //apakah anda belum absen pulang sebelumnya ?
                if(totalAbsensi == 0){
                    //apakah jarak absen diperbolehkan ?
                    if(jarak < radius){
                        SimpleDateFormat f = new SimpleDateFormat("HH:mm");
                        String timeNow = f.format(new Date());
                        Date waktuPulang = f.parse(jamPulang);
                        Date waktuSekarang = f.parse(timeNow);
                        //apakah sudah waktunya pulang ?
                        if(waktuPulang.before(waktuSekarang)){

                            Calendar wPulang = Calendar.getInstance();
                            wPulang.set(Calendar.HOUR_OF_DAY, Integer.valueOf(jamPulang.substring(0,2)));
                            wPulang.set(Calendar.MINUTE, Integer.valueOf(jamPulang.substring(3,5)));

                            long seconds = (calendar.getTimeInMillis() - wPulang.getTimeInMillis()) / 1000;
                            int menit = (int) (seconds / 60);

                            String keteranganJamPulang = "";
                            if (menit > 0) {
                                keteranganJamPulang += "jam pulang lebih" + " " + menit + " menit (offline)";
                            } else {
                                keteranganJamPulang += "absen pulang sesuai (offline)";
                            }
                            db.execSQL("INSERT INTO tabel_absen_non_guru(id_non_guru, tgl, jam, keterangan,jenis) VALUES ('" + idnonguru + "','" + tgl + "','" + jam + "','"+keteranganJamPulang+"','pulang')");
                            response = setResponse("ok" , "Absensi berhasil,"+keteranganJamPulang);
                        }else{
                            response = setResponse("error" , "Absensi gagal, belum waktunya pulang");
                        }
                    }else {
                        response = setResponse("error" , "Absensi gagal, jarak anda dengan sekolah lebih dari "+radius+" meter, yaitu "+jarak+" meter");
                    }
                }else{
                    response = setResponse("error" , "Absensi gagal, Anda sudah absen sebelumnya");
                }
            }else{
                response = setResponse("error" , "Absensi gagal, hari ini hari libur");
            }
        }else{
            response = setResponse("error" , "Absensi gagal, hari ini hari libur");
        }

        return response;
    }

    public List<AbsenHadirPulang> getInfoAbsenOffline(String id_guru,String filter){
        SQLiteDatabase db = this.getWritableDatabase();
        Calendar calendar = Calendar.getInstance();
        String tgl = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));

        String q = "SELECT * from tabel_absen_kehadiran_guru where id_guru = '" + id_guru + "' and tgl = '"+tgl+"' ";
        if(filter != null){
            q = q + filter;
        }
        q = q+ " order by id_absen_kehadiran_guru asc";
        Cursor getDataAbsen = db.rawQuery(q, null);

        List<AbsenHadirPulang>result = new ArrayList<>();
        if(getDataAbsen.getCount() > 0 ){
            getDataAbsen.moveToFirst();
            do{
                AbsenHadirPulang ap = new AbsenHadirPulang();
                ap.setId_absen_kehadiran_guru(String.valueOf(getDataAbsen.getInt(getDataAbsen.getColumnIndex("id_absen_kehadiran_guru"))));
                ap.setId_guru(getDataAbsen.getString(getDataAbsen.getColumnIndex("id_guru")));
                ap.setJam(getDataAbsen.getString(getDataAbsen.getColumnIndex("jam")));
                ap.setKeterangan(getDataAbsen.getString(getDataAbsen.getColumnIndex("keterangan")));
                result.add(ap);
            }while (getDataAbsen.moveToNext());
        }
        return result;
    }

    public List<AbsenHadirPulang> getInfoAbsenNonGuruOffline(String id_non_guru){
        SQLiteDatabase db = this.getWritableDatabase();
        Calendar calendar = Calendar.getInstance();
        String tgl = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));
        Cursor getDataAbsen = db.rawQuery("SELECT * from tabel_absen_non_guru where id_non_guru = '" + id_non_guru + "' and tgl = '"+tgl+"' order by id_absen_non_guru asc", null);

        List<AbsenHadirPulang>result = new ArrayList<>();
        if(getDataAbsen.getCount() > 0 ){
            getDataAbsen.moveToFirst();
            do{
                AbsenHadirPulang ap = new AbsenHadirPulang();
                ap.setId_absen_kehadiran_guru(String.valueOf(getDataAbsen.getInt(getDataAbsen.getColumnIndex("id_absen_non_guru"))));
                ap.setId_guru(getDataAbsen.getString(getDataAbsen.getColumnIndex("id_non_guru")));
                ap.setJam(getDataAbsen.getString(getDataAbsen.getColumnIndex("jam")));
                ap.setKeterangan(getDataAbsen.getString(getDataAbsen.getColumnIndex("keterangan")));
                result.add(ap);
            }while (getDataAbsen.moveToNext());
        }
        return result;
    }

    public List<AbsenHadirPulang> getInfoAbsenOfflineNonGuru(String id_non_guru , String jenis){
        SQLiteDatabase db = this.getWritableDatabase();
        Calendar calendar = Calendar.getInstance();
        String tgl = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));
        Cursor getDataAbsen = db.rawQuery("SELECT * from tabel_absen_non_guru where id_non_guru = '" + id_non_guru + "' and tgl = '"+tgl+"' and jenis = '"+jenis+"' order by id_absen_non_guru asc", null);

        List<AbsenHadirPulang>result = new ArrayList<>();
        if(getDataAbsen.getCount() > 0 ){
            getDataAbsen.moveToFirst();
            do{
                AbsenHadirPulang ap = new AbsenHadirPulang();
                ap.setId_absen_kehadiran_guru(String.valueOf(getDataAbsen.getInt(getDataAbsen.getColumnIndex("id_absen_non_guru"))));
                ap.setId_guru(getDataAbsen.getString(getDataAbsen.getColumnIndex("id_non_guru")));
                ap.setJam(getDataAbsen.getString(getDataAbsen.getColumnIndex("jam")));
                ap.setKeterangan(getDataAbsen.getString(getDataAbsen.getColumnIndex("keterangan")));
                result.add(ap);
            }while (getDataAbsen.moveToNext());
        }
        return result;
    }

    private KehadiranResponse setResponse (String status , String message){
        KehadiranResponse kehadiranResponse = new KehadiranResponse();
        kehadiranResponse.setStatus(status);
        kehadiranResponse.setKeterangan(message);
        return kehadiranResponse;
    }

    public KehadiranResponse absenKehadiranGuru(Context context, String idguru, LatLng ll) {
        SQLiteDatabase db = this.getWritableDatabase();

        Calendar calendar = Calendar.getInstance();
        String tgl = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));
        String jam = jamBerapa(calendar);
        String hari = "";
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        switch (day) {
            case 1:
                hari = "0 Minggu";
                break;
            case 2:
                hari = "1 Senin";
                break;
            case 3:
                hari = "2 Selasa";
                break;
            case 4:
                hari = "3 Rabu";
                break;
            case 5:
                hari = "4 Kamis";
                break;
            case 6:
                hari = "5 Jumat";
                break;
            case 7:
                hari = "6 Sabtu";
        }

        Cursor cek = db.rawQuery("SELECT * from tabel_absen_kehadiran_guru where id_guru = '" + idguru + "' and tgl = '" + tgl + "'", null);
        int c = cek.getCount();
        Log.d(TAG, "absenKehadiranGuru: total cek : "+c);
        Cursor setting = db.rawQuery("SELECT * from tabel_setting", null);

        KehadiranResponse kehadiranResponse = new KehadiranResponse();
        kehadiranResponse.setStatus("error");
        kehadiranResponse.setKeterangan("error");

        if (setting.getCount() == 0) return kehadiranResponse;
        setting.moveToFirst();
        String jampulangkerja = setting.getString(setting.getColumnIndex("jam_pulang"));
        int radius = setting.getInt(setting.getColumnIndex("radius"));
        Log.d(TAG, "absenKehadiranGuru: jam pulang kerja "+jampulangkerja);
        Cursor jamMasuk = db.rawQuery("select * from tabel_jam_masuk where hari = '" + hari + "'", null);

        kehadiranResponse = new KehadiranResponse();
        kehadiranResponse.setStatus("error");
        kehadiranResponse.setKeterangan("error");

        if (jamMasuk.getCount() == 0) return kehadiranResponse;
        jamMasuk.moveToFirst();
        String jammasukkerja = jamMasuk.getString(jamMasuk.getColumnIndex("jam"));

        Calendar calendarJamMasukKerja = Calendar.getInstance();
        calendarJamMasukKerja.set(Calendar.HOUR_OF_DAY, Integer.valueOf(jammasukkerja.substring(0, 2)));
        calendarJamMasukKerja.set(Calendar.MINUTE, Integer.valueOf(jammasukkerja.substring(3, 5)));

        Calendar calendarJamPulangKerja = Calendar.getInstance();
        calendarJamPulangKerja.set(Calendar.HOUR_OF_DAY, Integer.valueOf(jampulangkerja.substring(0, 2)));
        calendarJamPulangKerja.set(Calendar.MINUTE, Integer.valueOf(jampulangkerja.substring(3, 5)));

        /* cek jarak antara posisi user dengan sekolah */
        Location locUser = new Location("point a");
        locUser.setLatitude(ll.latitude);
        locUser.setLongitude(ll.longitude);

        Location locScholl = new Location("point b");
        double setLat = setting.getDouble(setting.getColumnIndex("lat"));
        double setLng = setting.getDouble(setting.getColumnIndex("lng"));
        locScholl.setLatitude(setLat);
        locScholl.setLongitude(setLng);

        Log.d(TAG, "absenKehadiranGuru: jarak : "+ll.latitude+","+ll.longitude+ " to "+setting.getColumnIndex("lat")+","+setting.getColumnIndex("lng"));
        double jarak = locUser.distanceTo(locScholl);


        if (jarak > radius) {
            kehadiranResponse = new KehadiranResponse();
            kehadiranResponse.setStatus("error");
            kehadiranResponse.setKeterangan("Gagal absen : Jarak posisi anda dengan sekolah terlalu jauh ("+new DecimalFormat("##").format(jarak) + " m)");

            return  kehadiranResponse;
        } else {
            if (c == 0) {
                Calendar sampai = (Calendar) calendarJamPulangKerja.clone();
                sampai.add(Calendar.MINUTE, 30);
                if (calendar.getTimeInMillis() <= sampai.getTimeInMillis()) {
                    db.execSQL("INSERT INTO tabel_absen_kehadiran_guru VALUES (" + idguru + "','" + tgl + "','" + jam + "','')");
                    db.execSQL("INSERT INTO tabel_absen_kehadiran_guru VALUES (" + idguru + "','" + tgl + "','" + jam + "','offline')");

                    long seconds = (calendar.getTimeInMillis() - calendarJamMasukKerja.getTimeInMillis()) / 1000;
                    int menit = (int) (seconds / 60);

                    String keteranganJamMasuk = "";
                    if (menit > 0) {
                        keteranganJamMasuk += "terlambat" + " " + menit + " menit";
                    } else {
                        keteranganJamMasuk += "absen sesuai";
                    }

                    kehadiranResponse = new KehadiranResponse();
                    kehadiranResponse.setStatus("ok");
                    kehadiranResponse.setKeterangan(keteranganJamMasuk);

                    return kehadiranResponse;
                } else {
                    kehadiranResponse = new KehadiranResponse();
                    kehadiranResponse.setStatus("error");
                    kehadiranResponse.setKeterangan("tidak bisa absen masuk lagi");

                    return kehadiranResponse;
                }
            } else {
                Calendar sampai = (Calendar) calendarJamPulangKerja.clone();
                sampai.set(Calendar.HOUR_OF_DAY, 17);
                sampai.set(Calendar.MINUTE, 0);
                if (calendar.getTimeInMillis() >= calendarJamPulangKerja.getTimeInMillis() && calendar.getTimeInMillis() <= sampai.getTimeInMillis()) {
                    db.execSQL("INSERT INTO tabel_absen_kehadiran_guru VALUES (" + idguru + "','" + tgl + "','" + jam + "','')");
                    db.execSQL("INSERT INTO tabel_absen_kehadiran_guru VALUES (" + idguru + "','" + tgl + "','" + jam + "','offline')");
                    kehadiranResponse = new KehadiranResponse();
                    kehadiranResponse.setStatus("ok");
                    kehadiranResponse.setKeterangan("absen sesuai");

                    return kehadiranResponse;
                } else if (calendar.getTimeInMillis() < calendarJamPulangKerja.getTimeInMillis()) {
                    long tunggu = (calendarJamPulangKerja.getTimeInMillis() - calendar.getTimeInMillis()) / 1000;
                    long jamjam = (tunggu / 3600);
                    long menitmenit = ((tunggu % 3600) / 60);
                    kehadiranResponse = new KehadiranResponse();
                    kehadiranResponse.setStatus("error");
                    kehadiranResponse.setKeterangan("belum waktunya absen pulang. " +
                            "Tunggu " + jamjam + " jam " + menitmenit + " menit ");

                    return kehadiranResponse;
                } else {
                    kehadiranResponse = new KehadiranResponse();
                    kehadiranResponse.setStatus("error");
                    kehadiranResponse.setKeterangan("absen sudah off");

                    return kehadiranResponse;
                }
            }
        }
    }

    public void absenNonGuruOnline(Context context, String idnonguru, String jenis) {
        SQLiteDatabase db = this.getWritableDatabase();

        Calendar calendar = Calendar.getInstance();
        String tgl = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));
        String jam = jamBerapa(calendar);

        db.execSQL("INSERT INTO tabel_absen_non_guru(id_non_guru,tgl,jam,keterangan,jenis) VALUES ('" + idnonguru + "','" + tgl + "','" + jam + "','','" + jenis + "')");
    }


    public void synUpdateSetting(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_setting where id_setting is null or id_setting = ''";
            final SQLiteDatabase db = this.getWritableDatabase();
            db.execSQL("delete from tabel_setting");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<SettingOffline>> call = service.syncUpdateTabelSetting(param);
            call.enqueue(new Callback<List<SettingOffline>>() {
                @Override
                public void onResponse(Call<List<SettingOffline>> call, retrofit2.Response<List<SettingOffline>> response) {
                    List<SettingOffline> listSettingOffline = response.body();
                    String query = "";

                    for (SettingOffline settingOffline : listSettingOffline) {
                        query = "";
                        query += "INSERT INTO tabel_setting (id_setting, logo_mini, logo_gambar, logo_favicon, nik, " +
                                "kartu_gsm, cek_pulsa, metode, kode_aktivasi, link_tujuan, logo_besar, logo_sekolah, " +
                                "pimpinan, akun_gmail, alamat, website, telat, toleransi_jam_akhir, lat, lng, radius, jam_pulang,jam_masuk,kapasitas_kelas) VALUES (";
                        query += "\"" + settingOffline.getIdSetting() + "\",";
                        query += "\"" + settingOffline.getLogoMini() + "\",";
                        query += "\"" + settingOffline.getLogoGambar() + "\",";
                        query += "\"" + settingOffline.getLogoFavicon() + "\",";
                        query += "\"" + settingOffline.getNik() + "\",";
                        query += "\"" + settingOffline.getKartuGsm() + "\",";
                        query += "\"" + settingOffline.getCekPulsa() + "\",";
                        query += "\"" + settingOffline.getMetode() + "\",";
                        query += "\"" + settingOffline.getKodeAktivasi() + "\",";
                        query += "\"" + settingOffline.getLinkTujuan() + "\",";
                        query += "\"" + settingOffline.getLogoBesar() + "\",";
                        query += "\"" + settingOffline.getLogoSekolah() + "\",";
                        query += "\"" + settingOffline.getPimpinan() + "\",";
                        query += "\"" + settingOffline.getAkunGmail() + "\",";
                        query += "\"" + settingOffline.getAlamat() + "\",";
                        query += "\"" + settingOffline.getWebsite() + "\",";
                        query += "\"" + settingOffline.getTelat() + "\",";
                        query += "\"" + settingOffline.getToleransiJamAkhir() + "\",";
                        query += "\"" + settingOffline.getLat() + "\",";
                        query += "\"" + settingOffline.getLng() + "\",";
                        query += "\"" + settingOffline.getRadius() + "\",";
                        query += "\"" + settingOffline.getJamPulang() + "\",";
                        query += "\"" + settingOffline.getJamMasuk() + "\",";
                        query += "\"" + settingOffline.getKapasitasKelas() + "\")";

                        db.execSQL(query);
                    }

                    syncUpdateJamMasuk(context, idGuru);
                }

                @Override
                public void onFailure(Call<List<SettingOffline>> call, Throwable t) {
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public void syncUpdateJamMasuk(final Context context, final String idGuru) {
        if (Vira.isConnectedToServer(context)) {
            String query = "select * from tabel_jam_masuk where id_jam_masuk is null or id_jam_masuk = ''";

            final SQLiteDatabase db = this.getWritableDatabase();

            db.execSQL("delete from tabel_jam_masuk");

            SessionConfig sessionConfig = new SessionConfig(context);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("id_guru", idGuru);

            Call<List<JamMasukOffline>> call = service.syncUpdateTabelJamMasuk(param);
            call.enqueue(new Callback<List<JamMasukOffline>>() {
                @Override
                public void onResponse(Call<List<JamMasukOffline>> call, retrofit2.Response<List<JamMasukOffline>> response) {
                    List<JamMasukOffline> listJamMasukOffline = response.body();

                    String query = "";

                    for (JamMasukOffline jamMasukOffline : listJamMasukOffline) {
                        query = "";
                        query += "INSERT INTO tabel_jam_masuk (id_jam_masuk, hari, jam) VALUES (";
                        query += "\"" + jamMasukOffline.getIdJamMasuk() + "\",";
                        query += "\"" + jamMasukOffline.getHari() + "\",";
                        query += "\"" + jamMasukOffline.getJam() + "\")";

                        db.execSQL(query);
                    }

                    Intent n = new Intent(context, AbsensiActivity.class);
                    context.startActivity(n);

                }

                @Override
                public void onFailure(Call<List<JamMasukOffline>> call, Throwable t) {
                    dialog.dismiss();
                    t.printStackTrace();
                    Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
                }
            });
        } else {
            dialog.dismiss();
            Toast.makeText(context, "Sync gagal", Toast.LENGTH_LONG).show();
        }
    }

    public int getTotalAbsenMapelOffline(){
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor total = db.rawQuery("SELECT COUNT(*) AS total FROM tabel_absen_mapel",null);
        total.moveToFirst();
        String total_ = total.getString(total.getColumnIndex("total"));
        return Integer.parseInt(total_);
    }

    public KehadiranResponse absenNonGuru(Context context, String idnonguru, String jenis) {
        SQLiteDatabase db = this.getWritableDatabase();

        Calendar calendar = Calendar.getInstance();
        String tgl = String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));
        String jam = jamBerapa(calendar);
        String hari = "";
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        switch (day) {
            case 1:
                hari = "0 Minggu";
                break;
            case 2:
                hari = "1 Senin";
                break;
            case 3:
                hari = "2 Selasa";
                break;
            case 4:
                hari = "3 Rabu";
                break;
            case 5:
                hari = "4 Kamis";
                break;
            case 6:
                hari = "5 Jumat";
                break;
            case 7:
                hari = "6 Sabtu";
        }

        Cursor cek = db.rawQuery("SELECT * from tabel_absen_non_guru where id_non_guru = '" + idnonguru + "' and tgl = '" + tgl + "' and jenis = '" + jenis + "'", null);
        int c = cek.getCount();

        Cursor setting = db.rawQuery("SELECT * from tabel_setting", null);

        KehadiranResponse kehadiranResponse = new KehadiranResponse();
        kehadiranResponse.setStatus("error");
        kehadiranResponse.setKeterangan("error");

        if (setting.getCount() == 0) return kehadiranResponse;
        setting.moveToFirst();
        String jampulangkerja = setting.getString(setting.getColumnIndex("jam_pulang"));

        Cursor jamMasuk = db.rawQuery("select * from tabel_jam_masuk where hari = '" + hari + "'", null);
        if (jamMasuk.getCount() == 0) return kehadiranResponse;
        jamMasuk.moveToFirst();
        String jammasukkerja = jamMasuk.getString(jamMasuk.getColumnIndex("jam"));

        Calendar calendarJamMasukKerja = Calendar.getInstance();
        calendarJamMasukKerja.set(Calendar.HOUR_OF_DAY, Integer.valueOf(jammasukkerja.substring(0, 2)));
        calendarJamMasukKerja.set(Calendar.MINUTE, Integer.valueOf(jammasukkerja.substring(3, 5)));

        Calendar calendarJamPulangKerja = Calendar.getInstance();
        calendarJamPulangKerja.set(Calendar.HOUR_OF_DAY, Integer.valueOf(jampulangkerja.substring(0, 2)));
        calendarJamPulangKerja.set(Calendar.MINUTE, Integer.valueOf(jampulangkerja.substring(3, 5)));

        if (c == 0) {
            if (jenis.equals("hadir")) {
                Calendar sampai = (Calendar) calendarJamPulangKerja.clone();
                sampai.add(Calendar.MINUTE, 30);
                if (calendar.getTimeInMillis() <= sampai.getTimeInMillis()) {

                    long seconds = (calendar.getTimeInMillis() - calendarJamMasukKerja.getTimeInMillis()) / 1000;
                    int menit = (int) (seconds / 60);

                    String keteranganJamMasuk = "";
                    if (menit > 0) {
                        keteranganJamMasuk += "terlambat" + " " + menit + " menit";
                    } else {
                        keteranganJamMasuk += "absen sesuai";
                    }

                    kehadiranResponse = new KehadiranResponse();
                    kehadiranResponse.setStatus("ok");
                    kehadiranResponse.setKeterangan(keteranganJamMasuk);

                    db.execSQL("INSERT INTO tabel_absen_non_guru(id_non_guru,tgl,jam,keterangan,jenis) VALUES ('" + idnonguru + "','" + tgl + "','" + jam + "','offline','" + jenis + "')");

                    return kehadiranResponse;
                } else {
                    kehadiranResponse = new KehadiranResponse();
                    kehadiranResponse.setStatus("error");
                    kehadiranResponse.setKeterangan("tidak bisa absen masuk lagi");

                    return kehadiranResponse;
                }
            } else {
                Calendar sampai = (Calendar) calendarJamPulangKerja.clone();
                sampai.set(Calendar.HOUR_OF_DAY, 17);
                sampai.set(Calendar.MINUTE, 0);

                if (calendar.getTimeInMillis() >= calendarJamPulangKerja.getTimeInMillis() && calendar.getTimeInMillis() <= sampai.getTimeInMillis()) {
                    db.execSQL("INSERT INTO tabel_absen_non_guru(id_non_guru,tgl,jam,keterangan,jenis) VALUES ('" + idnonguru + "','" + tgl + "','" + jam + "','offline','" + jenis + "')");
                    kehadiranResponse = new KehadiranResponse();
                    kehadiranResponse.setStatus("ok");
                    kehadiranResponse.setKeterangan("absen sesuai");

                    return kehadiranResponse;
                } else if (calendar.getTimeInMillis() < calendarJamPulangKerja.getTimeInMillis()) {
                    long tunggu = (calendarJamPulangKerja.getTimeInMillis() - calendar.getTimeInMillis()) / 1000;
                    long jamjam = (tunggu / 3600);
                    long menitmenit = ((tunggu % 3600) / 60);
                    kehadiranResponse = new KehadiranResponse();
                    kehadiranResponse.setStatus("error");
                    kehadiranResponse.setKeterangan("belum waktunya absen pulang. " +
                            "Tunggu " + jamjam + " jam " + menitmenit + " menit ");

                    return kehadiranResponse;
                } else {
                    kehadiranResponse = new KehadiranResponse();
                    kehadiranResponse.setStatus("error");
                    kehadiranResponse.setKeterangan("absen sudah off");

                    return kehadiranResponse;
                }

            }

        } else {
            kehadiranResponse = new KehadiranResponse();
            kehadiranResponse.setStatus("error");
            kehadiranResponse.setKeterangan("anda sudah absen sebelumnya");

            return kehadiranResponse;

        }
    }
    public double distincTwoPosition(LatLng ll){
        double jarak = 0;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor setting = db.rawQuery("SELECT * from tabel_setting", null);
        if(setting.getCount() > 0){
            setting.moveToFirst();

            Log.d(TAG, "distincTwoPosition: latitude : "+ll.latitude+" , "+ll.longitude);
            /* cek jarak antara posisi user dengan sekolah */
            Location locUser = new Location("point a");
            locUser.setLatitude(ll.latitude);
            locUser.setLongitude(ll.longitude);

            Location locScholl = new Location("point b");
            double setLat = setting.getDouble(setting.getColumnIndex("lat"));
            double setLng = setting.getDouble(setting.getColumnIndex("lng"));
            locScholl.setLatitude(setLat);
            locScholl.setLongitude(setLng);
            jarak = locUser.distanceTo(locScholl);
        }
        return jarak;
    }
}