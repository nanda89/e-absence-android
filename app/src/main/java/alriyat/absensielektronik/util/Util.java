package alriyat.absensielektronik.util;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Util {

    private static final String TAG = Util.class.getName();
    static String getDay(String date) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        String hari = null;
        try {
            cal.setTime(sdf.parse(date));
            int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

            switch (dayOfWeek) {
                case 1:
                    hari = "minggu";
                    break;
                case 2:
                    hari = "senin";
                    break;
                case 3:
                    hari = "selasa";
                    break;
                case 4:
                    hari = "rabu";
                    break;
                case 5:
                    hari = "kamis";
                    break;
                case 6:
                    hari = "jumat";
                    break;
                case 7:
                    hari = "sabtu";
                    break;
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return hari;
    }

    static int getWeekOfMonth(){
        Calendar ca1 = Calendar.getInstance();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String tgl = simpleDateFormat.format(new Date());

        String tglParse[] = tgl.split("-");
        int day = Integer.valueOf(tglParse[0]);
        int month = Integer.valueOf(tglParse[1]) -1;
        int year = Integer.valueOf(tglParse[2]);

        ca1.set(year, month, day);
        ca1.setMinimalDaysInFirstWeek(1);
        int wk = ca1.get(Calendar.WEEK_OF_MONTH);
        Log.d(TAG, "getWeekOfMonth: "+wk);
        return wk + 1;
    }

}
