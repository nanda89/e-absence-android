package alriyat.absensielektronik.util;

import java.io.File;
import java.util.List;
import java.util.Map;

import alriyat.absensielektronik.model.AbsenMapelOffline;
import alriyat.absensielektronik.model.AbsenOffline;
import alriyat.absensielektronik.model.AmbilSiswa;
import alriyat.absensielektronik.model.CabangOffline;
import alriyat.absensielektronik.model.Depan;
import alriyat.absensielektronik.model.GuruOffline;
import alriyat.absensielektronik.model.HapusSiswa;
import alriyat.absensielektronik.model.Jadwal;
import alriyat.absensielektronik.model.JadwalOffline;
import alriyat.absensielektronik.model.JadwalTambahanOffline;
import alriyat.absensielektronik.model.JamMasukOffline;
import alriyat.absensielektronik.model.JenjangOffline;
import alriyat.absensielektronik.model.KalenderOffline;
import alriyat.absensielektronik.model.KatMapelOffline;
import alriyat.absensielektronik.model.KategoriOffline;
import alriyat.absensielektronik.model.Kehadiran;
import alriyat.absensielektronik.model.KehadiranGuru;
import alriyat.absensielektronik.model.KehadiranNonGuru;
import alriyat.absensielektronik.model.KehadiranResponse;
import alriyat.absensielektronik.model.KelasOffline;
import alriyat.absensielektronik.model.KelasTambahanOffline;
import alriyat.absensielektronik.model.Login;
import alriyat.absensielektronik.model.Mapel;
import alriyat.absensielektronik.model.MapelOffline;
import alriyat.absensielektronik.model.MapelTambahan;
import alriyat.absensielektronik.model.NonGuruOffline;
import alriyat.absensielektronik.model.PassSync;
import alriyat.absensielektronik.model.PengampuOffline;
import alriyat.absensielektronik.model.RekapMengajarGuru;
import alriyat.absensielektronik.model.ScheduleOffline;
import alriyat.absensielektronik.model.SettingOffline;
import alriyat.absensielektronik.model.Siswa;
import alriyat.absensielektronik.model.SiswaOffline;
import alriyat.absensielektronik.model.SiswakelasOffline;
import alriyat.absensielektronik.model.TahunajaranOffline;
import alriyat.absensielektronik.model.TambahSiswa;
import alriyat.absensielektronik.model.User;
import alriyat.absensielektronik.model.UserOffline;
import alriyat.absensielektronik.pojo.AbsenHadirPulang;
import alriyat.absensielektronik.pojo.AbsenHadirPulangNonGuru;
import alriyat.absensielektronik.pojo.DaftarHadir;
import alriyat.absensielektronik.pojo.GeneralResponse;
import alriyat.absensielektronik.pojo.Kegiatan;
import alriyat.absensielektronik.pojo.KelasByGuru;
import alriyat.absensielektronik.pojo.LapAbsensiGuru;
import alriyat.absensielektronik.pojo.LapAbsensiHarian;
import alriyat.absensielektronik.pojo.LapKehadiranGuru;
import alriyat.absensielektronik.pojo.LaporanGuru;
import alriyat.absensielektronik.pojo.LaporanHarian;
import alriyat.absensielektronik.pojo.LaporanNonGuruHarian;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.QueryMap;

/**
 * Created by folcotandiono on 8/25/2018.
 */

public interface GetDataService {

    @FormUrlEncoded
    @POST("assets/file/insert_offline1.php")
    Call<String> insertOffline(@Field("query") String query);

    @FormUrlEncoded
    @POST("assets/file/insert_absen_tambahan.php")
    Call<String> insertAbsentambahan(@Field("data") String data);

    @GET("assets/file/rekap_kehadiran_guru.php")
    Call<List<KehadiranGuru>> rekapKehadiranGuru(@QueryMap Map<String, String> params);

    @GET("assets/file/absen_harian_guru.php")
    Call<List<LaporanGuru>> laporanGuru(@QueryMap Map<String, String> params);

    @GET("assets/file/laporan_harian.php")
    Call<List<LaporanHarian>> laporanHarian(@QueryMap Map<String, String> params);

    @GET("assets/file/daftar_hadir.php")
    Call<DaftarHadir> daftarHadir(@QueryMap Map<String, String> params);

    @GET("assets/file/rekap_kehadiran.php")
    Call<List<LapKehadiranGuru>> rekapHadirGuru(@QueryMap Map<String, String> params);

    @GET("assets/file/non_guru_offline.php")
    Call<List<NonGuruOffline>> updateNonGuru(@QueryMap Map<String, String> params);

    @GET("assets/file/rekap_mengajar_guru.php")
    Call<List<RekapMengajarGuru>>rekapMengajarGuru(@QueryMap Map<String, String> params);

    @GET("assets/file/laporan_absensi_guru.php")
    Call<List<LapAbsensiGuru>>lapAbsensiGuru(@QueryMap Map<String, String> params);


    @GET("assets/file/absen_non_guru.php")
    Call<KehadiranResponse> absenNonGuru(@QueryMap Map<String, String> params);

    @GET("assets/file/sync_non_guru_offline.php")
    Call<String> syncNonGuruOffline(@QueryMap Map<String, String> params);

    @GET("assets/file/pass_sync.php")
    Call<List<PassSync>> passSync();

    @GET("assets/file/daftar_kegiatan.php")
    Call<List<Kegiatan>> daftarKegiatan(@QueryMap Map<String, String> params);

    @GET("assets/file/kehadiran_non_guru.php")
    Call<List<LaporanNonGuruHarian>> laporan_harian(@QueryMap Map<String, String> params);

    @GET("assets/file/daftar_mapel.php")
    Call<List<Jadwal>> daftarMapel(@QueryMap Map<String, String> params);

    @GET("assets/file/get_all_mapel.php")
    Call<List<alriyat.absensielektronik.pojo.Mapel>> getAllMapel();

    @GET("assets/file/mapelBy_guru.php")
    Call<List<alriyat.absensielektronik.pojo.Mapel>> getMapelByGuru(@QueryMap Map<String, String> params);

    @GET("assets/file/getMapelByJenis.php")
    Call<List<alriyat.absensielektronik.pojo.Mapel>> getMapelByJenis(@QueryMap Map<String, String> params);

    @GET("assets/file/ambil_user.php")
    Call<User> ambilUser(@QueryMap Map<String, String> params);

    @GET("assets/file/ambil_user_non_guru.php")
    Call<User> ambilUserNonGuru(@QueryMap Map<String, String> params);

    @GET("assets/file/depan_guru.php")
    Call<Depan> ambilDepan(@QueryMap Map<String, String> params);

    @GET("assets/file/depan_non_guru.php")
    Call<Depan> depanNonGuru(@QueryMap Map<String, String> params);

    @GET("assets/file/sync_offline.php")
    Call<String> syncOffline(@QueryMap Map<String, String> params);

    @GET("assets/file/update_password.php")
    Call<String> updatePassword(@QueryMap Map<String, String> params);

    @GET("assets/file/cek_status_kepsek.php")
    Call<String> cekStatusKepsek(@QueryMap Map<String, String> params);

    @GET("assets/file/login.php")
    Call<Login> login(@QueryMap Map<String, String> params);

    @GET("assets/file/daftar_siswa_absensi.php")
    Call<List<Siswa>> daftarSiswaAbsensi(@QueryMap Map<String, String> params);

    @GET("assets/file/daftar_mapel_absensi.php")
    Call<List<Siswa>> daftarMapelAbsensi(@QueryMap Map<String, String> params);

    @GET("assets/file/cek_status_wali_kelas.php")
    Call<String> cekStatusWaliKelas(@QueryMap Map<String, String> params);

    @POST("assets/file/hadir.php")
    Call<String> hadir(@QueryMap Map<String, String> params);

    @POST("assets/file/simpan_absen_mapel.php")
    Call<String> simpanAbsenMapel(@QueryMap Map<String, String> params);

    @POST("assets/file/validasi.php")
    Call<String> validasi(@QueryMap Map<String, String> params);

    @GET("assets/file/attachment.php")
    Call<GeneralResponse> attachment(@QueryMap Map<String, String> map);

    @GET("assets/file/daftar_mapel_all.php")
    Call<List<Mapel>> daftarMapelAll(@QueryMap Map<String, String> params);

    @GET("assets/file/ambil_data_siswa.php")
    Call<List<Siswa>> ambilDataSiswa(@QueryMap Map<String, String> params);

    @GET("assets/file/ambil_siswa.php")
    Call<AmbilSiswa> ambilSiswa(@QueryMap Map<String, String> params);

    @POST("assets/file/update_user_guru.php")
    Call<String> updateUserGuru(@QueryMap Map<String, String> params);

    @GET("assets/file/rekap_kehadiran_non_guru.php")
    Call<List<KehadiranNonGuru>> rekapKehadiranNonGuru(@QueryMap Map<String, String> params);

    @GET("assets/file/rekap_kehadiran_siswa.php")
    Call<List<Kehadiran>> rekapKehadiranSiswa(@QueryMap Map<String, String> params);

    @GET("assets/file/guru_offline.php")
    Call<List<GuruOffline>> syncUpdateTabelGuru(@QueryMap Map<String, String> params);

    @GET("assets/file/jadwal_offline.php")
    Call<List<JadwalOffline>> syncUpdateTabelJadwal(@QueryMap Map<String, String> params);

    @GET("assets/file/jenjang_offline.php")
    Call<List<JenjangOffline>> syncUpdateTabelJenjang(@QueryMap Map<String, String> params);

    @GET("assets/file/kelas_offline.php")
    Call<List<KelasOffline>> syncUpdateTabelKelas(@QueryMap Map<String, String> params);

    @GET("assets/file/mapel_offline.php")
    Call<List<MapelOffline>> syncUpdateTabelMapel(@QueryMap Map<String, String> params);

    @GET("assets/file/setting_offline.php")
    Call<List<SettingOffline>> syncUpdateTabelSetting(@QueryMap Map<String, String> params);

    @GET("assets/file/absen_mapel_offline.php")
    Call<List<AbsenMapelOffline>> syncUpdateTabelAbsenMapel(@QueryMap Map<String, String> params);

    @GET("assets/file/siswakelas_offline.php")
    Call<List<SiswakelasOffline>> syncUpdateTabelSiswakelas(@QueryMap Map<String, String> params);

    @GET("assets/file/siswa_offline.php")
    Call<List<SiswaOffline>> syncUpdateTabelSiswa(@QueryMap Map<String, String> params);

    @GET("assets/file/tahunajaran_offline.php")
    Call<List<TahunajaranOffline>> syncUpdateTabelTahunajaran(@QueryMap Map<String, String> params);

    @GET("assets/file/kat_mapel_offline.php")
    Call<List<KatMapelOffline>> syncUpdateTabelKatMapel(@QueryMap Map<String, String> params);

    @GET("assets/file/absen_offline.php")
    Call<List<AbsenOffline>> syncUpdateTabelAbsen(@QueryMap Map<String, String> params);

    @GET("assets/file/cabang_offline.php")
    Call<List<CabangOffline>> syncUpdateTabelCabang(@QueryMap Map<String, String> params);

    @GET("assets/file/kalender_offline.php")
    Call<List<KalenderOffline>> syncUpdateTabelKalender(@QueryMap Map<String, String> params);

    @GET("assets/file/kategori_offline.php")
    Call<List<KategoriOffline>> syncUpdateTabelKategori(@QueryMap Map<String, String> params);

    @GET("assets/file/pengampu_offline.php")
    Call<List<PengampuOffline>> syncUpdateTabelPengampu(@QueryMap Map<String, String> params);

    @GET("assets/file/schedule_offline.php")
    Call<List<ScheduleOffline>> syncUpdateTabelSchedule(@QueryMap Map<String, String> params);

    @GET("assets/file/jam_masuk_offline.php")
    Call<List<JamMasukOffline>> syncUpdateTabelJamMasuk(@QueryMap Map<String, String> params);

    @GET("assets/file/user_offline.php")
    Call<List<UserOffline>> syncUpdateTabelUser(@QueryMap Map<String, String> params);

    @GET("assets/file/change_pass_sync.php")
    Call<String> changePassSync(@QueryMap Map<String, String> params);

    @GET("assets/file/absen_kehadiran_guru.php")
    Call<KehadiranResponse> absenKehadiranGuru(@QueryMap Map<String, String> params);

    @GET("assets/file/absen_hadir_guru.php")
    Call<KehadiranResponse> absenHadirGuru(@QueryMap Map<String, String> params);

    @GET("assets/file/absen_pulang_guru.php")
    Call<KehadiranResponse> absenPulangGuru(@QueryMap Map<String, String> params);

    @GET("assets/file/get_info_absen_hadir_pulang.php")
    Call<List<AbsenHadirPulang>> getInfoAbsensi(@QueryMap Map<String, String> params);

    @GET("assets/file/get_info_absen_hadir_pulang_non_guru.php")
    Call<List<AbsenHadirPulangNonGuru>> getInfoAbsensiNonGuru(@QueryMap Map<String, String> params);

    @GET("assets/file/non_guru_offline.php")
    Call<List<NonGuruOffline>> nonGuru(@QueryMap Map<String, String> params);

    @GET("assets/file/pilih_kelas.php")
    Call<List<KelasOffline>> pilihKelas(@QueryMap Map<String, String> params);

    @GET("assets/file/pilih_kelas_wali_kelas.php")
    Call<List<KelasOffline>> pilihKelasWaliKelas(@QueryMap Map<String, String> params);

    @GET("assets/file/mapel_by_guru.php")
    Call<List<KelasByGuru>> pilihKelasByGuru(@QueryMap Map<String, String> params);

    @FormUrlEncoded
    @POST("assets/file/tambah_siswa.php")
    Call<TambahSiswa> tambahSiswa(@FieldMap Map<String, String> params);

    @POST("assets/file/hapus_siswa.php")
    Call<HapusSiswa> hapusSiswa(@QueryMap Map<String, String> params);

    @GET("assets/file/jadwal_offline_tambahan.php")
    Call<List<JadwalTambahanOffline>> syncUpdateTabelJadwalTambahan();

    @GET("assets/file/jadwal_offline_kelas_tambahan.php")
    Call<List<KelasTambahanOffline>> syncUpdateKelasTambahan(@QueryMap Map<String, String> params);

    @GET("assets/file/absen_kelas_harian.php")
    Call<List<LapAbsensiHarian>> getLapAbsensiHarian(@QueryMap Map<String, String> params);

    @GET("assets/file/daftar_mapel_all_tambahan.php")
    Call<List<MapelTambahan>> daftarMapelTambahanAll(@QueryMap Map<String, String> params);

    @GET("assets/file/daftar_mapel_absensi_tambahan.php")
    Call<List<Siswa>> daftarMapelAbsensiTambahan(@QueryMap Map<String, String> params);

    @POST("assets/file/simpan_absen_mapel_tambahan.php")
    Call<String> simpanAbsenMapelTambahan(@QueryMap Map<String, String> params);

    @POST("assets/file/validasi_tambahan.php")
    Call<String> validasiTambahan(@QueryMap Map<String, String> params);

    @POST("assets/file/hadir_tambahan.php")
    Call<String> hadirTambahan(@QueryMap Map<String, String> params);

    @FormUrlEncoded
    @POST("assets/file/uraian_non_guru.php")
    Call<GeneralResponse>uploadUraian(
            @Field("idNonGuru") String idNonGuru,
            @Field("tanggal") String tanggal,
            @Field("uraian") String uraian,
            @Field("type") String type,
            @Field("fileToUpload") File fileToUpload
    );
}
