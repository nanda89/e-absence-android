package alriyat.absensielektronik.model;

import android.support.v4.view.ViewCompat;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 8/16/2018.
 */

public class AbsenOffline {
    @SerializedName("id_absen")
    private String idAbsen;
    @SerializedName("id_penempatan")
    private String idPenempatan;
    @SerializedName("tgl")
    private String tgl;
    @SerializedName("jam_masuk")
    private String jamMasuk;
    @SerializedName("absen_masuk")
    private String absenMasuk;
    @SerializedName("valid_masuk")
    private String validMasuk;
    @SerializedName("sms_masuk")
    private String smsMasuk;
    @SerializedName("jam_pulang")
    private String jamPulang;
    @SerializedName("absen_pulang")
    private String absenPulang;
    @SerializedName("valid_pulang")
    private String validPulang;
    @SerializedName("sms_pulang")
    private String smsPulang;

    public String getIdAbsen() {
        return idAbsen;
    }

    public void setIdAbsen(String idAbsen) {
        this.idAbsen = idAbsen;
    }

    public String getIdPenempatan() {
        return idPenempatan;
    }

    public void setIdPenempatan(String idPenempatan) {
        this.idPenempatan = idPenempatan;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getJamMasuk() {
        return jamMasuk;
    }

    public void setJamMasuk(String jamMasuk) {
        this.jamMasuk = jamMasuk;
    }

    public String getAbsenMasuk() {
        return absenMasuk;
    }

    public void setAbsenMasuk(String absenMasuk) {
        this.absenMasuk = absenMasuk;
    }

    public String getValidMasuk() {
        return validMasuk;
    }

    public void setValidMasuk(String validMasuk) {
        this.validMasuk = validMasuk;
    }

    public String getSmsMasuk() {
        return smsMasuk;
    }

    public void setSmsMasuk(String smsMasuk) {
        this.smsMasuk = smsMasuk;
    }

    public String getJamPulang() {
        return jamPulang;
    }

    public void setJamPulang(String jamPulang) {
        this.jamPulang = jamPulang;
    }

    public String getAbsenPulang() {
        return absenPulang;
    }

    public void setAbsenPulang(String absenPulang) {
        this.absenPulang = absenPulang;
    }

    public String getValidPulang() {
        return validPulang;
    }

    public void setValidPulang(String validPulang) {
        this.validPulang = validPulang;
    }

    public String getSmsPulang() {
        return smsPulang;
    }

    public void setSmsPulang(String smsPulang) {
        this.smsPulang = smsPulang;
    }
}
