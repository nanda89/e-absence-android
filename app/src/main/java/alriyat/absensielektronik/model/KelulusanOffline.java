package alriyat.absensielektronik.model;

/**
 * Created by folcotandiono on 8/16/2018.
 */

public class KelulusanOffline {
    private String idKelulusan;
    private String idSiswa;
    private String idTa;
    private String idKelas;
    private String angkatan;
    private String tglLulus;

    public String getIdKelulusan() {
        return idKelulusan;
    }

    public void setIdKelulusan(String idKelulusan) {
        this.idKelulusan = idKelulusan;
    }

    public String getIdSiswa() {
        return idSiswa;
    }

    public void setIdSiswa(String idSiswa) {
        this.idSiswa = idSiswa;
    }

    public String getIdTa() {
        return idTa;
    }

    public void setIdTa(String idTa) {
        this.idTa = idTa;
    }

    public String getIdKelas() {
        return idKelas;
    }

    public void setIdKelas(String idKelas) {
        this.idKelas = idKelas;
    }

    public String getAngkatan() {
        return angkatan;
    }

    public void setAngkatan(String angkatan) {
        this.angkatan = angkatan;
    }

    public String getTglLulus() {
        return tglLulus;
    }

    public void setTglLulus(String tglLulus) {
        this.tglLulus = tglLulus;
    }
}
