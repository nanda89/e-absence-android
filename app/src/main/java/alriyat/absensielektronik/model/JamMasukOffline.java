package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 8/16/2018.
 */

public class JamMasukOffline {
    @SerializedName("id_jam_masuk")
    private String idJamMasuk;
    @SerializedName("hari")
    private String hari;
    @SerializedName("jam")
    private String jam;

    public String getIdJamMasuk() {
        return idJamMasuk;
    }

    public void setIdJamMasuk(String idJamMasuk) {
        this.idJamMasuk = idJamMasuk;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }
}
