package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * created by Nanda on 14/11/2019
 */
public class MapelTambahan {
    @SerializedName("id_kelas")
    private String idkelas;

    @SerializedName("mapel")
    private String mapel;

    @SerializedName("jam_awal")
    private String jamAwal;

    @SerializedName("jam_akhir")
    private String jamAkhir;

    @SerializedName("kelas")
    private String kelas;

    @SerializedName("id_jadwal")
    private String idjadwal;

    @SerializedName("jenjang")
    private String jenjang;

    @SerializedName("waktu")
    private String waktu;

    @SerializedName("jam")
    private String jam;

    @SerializedName("nama")
    private String nama;

    @SerializedName("status_guru_bk")
    private String statusGuruBK;

    public String getIdkelas() {
        return idkelas;
    }

    public void setIdkelas(String idkelas) {
        this.idkelas = idkelas;
    }

    public String getMapel() {
        return mapel;
    }

    public void setMapel(String mapel) {
        this.mapel = mapel;
    }

    public String getJamAwal() {
        return jamAwal;
    }

    public void setJamAwal(String jamAwal) {
        this.jamAwal = jamAwal;
    }

    public String getJamAkhir() {
        return jamAkhir;
    }

    public void setJamAkhir(String jamAkhir) {
        this.jamAkhir = jamAkhir;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getIdjadwal() {
        return idjadwal;
    }

    public void setIdjadwal(String idjadwal) {
        this.idjadwal = idjadwal;
    }

    public String getJenjang() {
        return jenjang;
    }

    public void setJenjang(String jenjang) {
        this.jenjang = jenjang;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getStatusGuruBK() {
        return statusGuruBK;
    }

    public void setStatusGuruBK(String statusGuruBK) {
        this.statusGuruBK = statusGuruBK;
    }
}
