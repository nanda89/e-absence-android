package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 12/25/2018.
 */

public class User {
    @SerializedName("gambar")
    private String gambar;
    @SerializedName("nama")
    private String nama;
    @SerializedName("username")
    private String username;

    private boolean isWalikelas;

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isWalikelas() {
        return isWalikelas;
    }

    public void setWalikelas(boolean walikelas) {
        isWalikelas = walikelas;
    }
}
