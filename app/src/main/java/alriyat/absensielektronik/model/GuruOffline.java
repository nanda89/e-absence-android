package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 8/16/2018.
 */

public class GuruOffline {
    @SerializedName("id_guru")
    private String idGuru;
    @SerializedName("id_jenjang")
    private String idJenjang;
    @SerializedName("nik")
    private String nik;
    @SerializedName("nama")
    private String nama;
    @SerializedName("no_ktp")
    private String noKtp;
    @SerializedName("alias")
    private String alias;
    @SerializedName("jenis_kelamin")
    private String jenisKelamin;
    @SerializedName("tempat_lahir")
    private String tempatLahir;
    @SerializedName("tgl_lahir")
    private String tglLahir;
    @SerializedName("agama")
    private String agama;
    @SerializedName("no_hp")
    private String noHp;
    @SerializedName("email")
    private String email;
    @SerializedName("ijazah")
    private String ijazah;
    @SerializedName("pend_terakhir")
    private String pendTerakhir;
    @SerializedName("gambar")
    private String gambar;
    @SerializedName("alamat")
    private String alamat;
    @SerializedName("unit_kerja")
    private String unitKerja;
    @SerializedName("tmt")
    private String tmt;
    @SerializedName("status_guru_bk")
    private String statusGuruBk;
    @SerializedName("status_kepsek")
    private String statusKepsek;

    public String getIdGuru() {
        return idGuru;
    }

    public void setIdGuru(String idGuru) {
        this.idGuru = idGuru;
    }

    public String getIdJenjang() {
        return idJenjang;
    }

    public void setIdJenjang(String idJenjang) {
        this.idJenjang = idJenjang;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNoKtp() {
        return noKtp;
    }

    public void setNoKtp(String noKtp) {
        this.noKtp = noKtp;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getTglLahir() {
        return tglLahir;
    }

    public void setTglLahir(String tglLahir) {
        this.tglLahir = tglLahir;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIjazah() {
        return ijazah;
    }

    public void setIjazah(String ijazah) {
        this.ijazah = ijazah;
    }

    public String getPendTerakhir() {
        return pendTerakhir;
    }

    public void setPendTerakhir(String pendTerakhir) {
        this.pendTerakhir = pendTerakhir;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getUnitKerja() {
        return unitKerja;
    }

    public void setUnitKerja(String unitKerja) {
        this.unitKerja = unitKerja;
    }

    public String getTmt() {
        return tmt;
    }

    public void setTmt(String tmt) {
        this.tmt = tmt;
    }

    public String getStatusGuruBk() {
        return statusGuruBk;
    }

    public void setStatusGuruBk(String statusGuruBk) {
        this.statusGuruBk = statusGuruBk;
    }

    public String getStatusKepsek() {
        return statusKepsek;
    }

    public void setStatusKepsek(String statusKepsek) {
        this.statusKepsek = statusKepsek;
    }
}
