package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 8/16/2018.
 */

public class SiswaOffline {
    @SerializedName("id_siswa")
    private String idSiswa;
    @SerializedName("id_jenjang")
    private String idJenjang;
    @SerializedName("nama_lengkap")
    private String namaLengkap;
    @SerializedName("nisn")
    private String nisn;
    @SerializedName("jenis_kelamin")
    private String jenisKelamin;
    @SerializedName("tempat_lahir")
    private String tempatLahir;
    @SerializedName("tgl_lahir")
    private String tglLahir;
    @SerializedName("agama")
    private String agama;
    @SerializedName("alamat")
    private String alamat;
    @SerializedName("no_hp")
    private String noHp;
    @SerializedName("email")
    private String email;
    @SerializedName("asal_sekolah")
    private String asalSekolah;
    @SerializedName("ket_asal")
    private String ketAsal;
    @SerializedName("nama_ayah")
    private String namaAyah;
    @SerializedName("nama_ibu")
    private String namaIbu;
    @SerializedName("gambar")
    private String gambar;
    @SerializedName("tgl_daftar")
    private String tglDaftar;

    public String getIdSiswa() {
        return idSiswa;
    }

    public void setIdSiswa(String idSiswa) {
        this.idSiswa = idSiswa;
    }

    public String getIdJenjang() {
        return idJenjang;
    }

    public void setIdJenjang(String idJenjang) {
        this.idJenjang = idJenjang;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public String getNisn() {
        return nisn;
    }

    public void setNisn(String nisn) {
        this.nisn = nisn;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getTempatLahir() {
        return tempatLahir;
    }

    public void setTempatLahir(String tempatLahir) {
        this.tempatLahir = tempatLahir;
    }

    public String getTglLahir() {
        return tglLahir;
    }

    public void setTglLahir(String tglLahir) {
        this.tglLahir = tglLahir;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNoHp() {
        return noHp;
    }

    public void setNoHp(String noHp) {
        this.noHp = noHp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAsalSekolah() {
        return asalSekolah;
    }

    public void setAsalSekolah(String asalSekolah) {
        this.asalSekolah = asalSekolah;
    }

    public String getKetAsal() {
        return ketAsal;
    }

    public void setKetAsal(String ketAsal) {
        this.ketAsal = ketAsal;
    }

    public String getNamaAyah() {
        return namaAyah;
    }

    public void setNamaAyah(String namaAyah) {
        this.namaAyah = namaAyah;
    }

    public String getNamaIbu() {
        return namaIbu;
    }

    public void setNamaIbu(String namaIbu) {
        this.namaIbu = namaIbu;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getTglDaftar() {
        return tglDaftar;
    }

    public void setTglDaftar(String tglDaftar) {
        this.tglDaftar = tglDaftar;
    }
}
