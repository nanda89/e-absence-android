package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Riyadh on 01/10/2017.
 */

public class Mapel {
    @SerializedName("id_kelas")
    private String idapel;
    @SerializedName("mapel")
    private String mapel;
    @SerializedName("jam_awal")
    private String waktu;
    @SerializedName("jam_akhir")
    private String jam;
    @SerializedName("kelas")
    private String kelas;
    @SerializedName("id_jadwal")
    private String idjadwal;
    @SerializedName("jenjang")
    private String jenjang;
    @SerializedName("waktu")
    private String status;
    @SerializedName("status")
    private String status1;
    @SerializedName("nama")
    private String nama;
    @SerializedName("status_guru_bk")
    private String statusGuruBK;

    public String getIdapel() {
        return idapel;
    }

    public void setIdapel(String idapel) {
        this.idapel = idapel;
    }

    public String getMapel() {
        return mapel;
    }

    public void setMapel(String mapel) {
        this.mapel = mapel;
    }

    public String getWaktu() {
        return waktu;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getIdjadwal() {
        return idjadwal;
    }

    public void setIdjadwal(String idjadwal) {
        this.idjadwal = idjadwal;
    }

    public String getJenjang() {
        return jenjang;
    }

    public void setJenjang(String jenjang) {
        this.jenjang = jenjang;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus1() {
        return status1;
    }

    public void setStatus1(String status1) {
        this.status1 = status1;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getStatusGuruBK() {
        return statusGuruBK;
    }

    public void setStatusGuruBK(String statusGuruBK) {
        this.statusGuruBK = statusGuruBK;
    }
}
