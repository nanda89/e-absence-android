package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 8/16/2018.
 */

public class PengampuOffline {
    @SerializedName("id_pengampu")
    private String idPengampu;
    @SerializedName("nik")
    private String nik;
    @SerializedName("id_mapel")
    private String idMapel;

    public String getIdPengampu() {
        return idPengampu;
    }

    public void setIdPengampu(String idPengampu) {
        this.idPengampu = idPengampu;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getIdMapel() {
        return idMapel;
    }

    public void setIdMapel(String idMapel) {
        this.idMapel = idMapel;
    }
}
