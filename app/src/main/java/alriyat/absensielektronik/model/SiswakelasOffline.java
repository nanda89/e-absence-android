package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 8/16/2018.
 */

public class SiswakelasOffline {
    @SerializedName("id_penempatan")
    private String idPenempatan;
    @SerializedName("id_ta")
    private String idTa;
    @SerializedName("id_kelas")
    private String idKelas;
    @SerializedName("id_siswa")
    private String idSiswa;

    public String getIdPenempatan() {
        return idPenempatan;
    }

    public void setIdPenempatan(String idPenempatan) {
        this.idPenempatan = idPenempatan;
    }

    public String getIdTa() {
        return idTa;
    }

    public void setIdTa(String idTa) {
        this.idTa = idTa;
    }

    public String getIdKelas() {
        return idKelas;
    }

    public void setIdKelas(String idKelas) {
        this.idKelas = idKelas;
    }

    public String getIdSiswa() {
        return idSiswa;
    }

    public void setIdSiswa(String idSiswa) {
        this.idSiswa = idSiswa;
    }
}
