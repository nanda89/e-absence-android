package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

public class RekapMengajarGuru {

    @SerializedName("nama")
    private String nama;

    @SerializedName("nik")
    private String nik;

    @SerializedName("jenjang")
    private String jenjang;

    @SerializedName("mapel")
    private String mapel;

    @SerializedName("hadir")
    private int hadir;

    @SerializedName("sakit")
    private int sakit;

    @SerializedName("izin")
    private int izin;

    @SerializedName("nihil")
    private int nihil;

    @SerializedName("tanpa_keterangan")
    private int tk;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getJenjang() {
        return jenjang;
    }

    public void setJenjang(String jenjang) {
        this.jenjang = jenjang;
    }

    public String getMapel() {
        return mapel;
    }

    public void setMapel(String mapel) {
        this.mapel = mapel;
    }

    public int getHadir() {
        return hadir;
    }

    public void setHadir(int hadir) {
        this.hadir = hadir;
    }

    public int getSakit() {
        return sakit;
    }

    public void setSakit(int sakit) {
        this.sakit = sakit;
    }

    public int getIzin() {
        return izin;
    }

    public void setIzin(int izin) {
        this.izin = izin;
    }

    public int getNihil() {
        return nihil;
    }

    public void setNihil(int nihil) {
        this.nihil = nihil;
    }

    public int getTk() {
        return tk;
    }

    public void setTk(int tk) {
        this.tk = tk;
    }
}
