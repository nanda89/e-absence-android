package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 8/16/2018.
 */

public class AbsenMapelOffline {
    @SerializedName("id_absen_mapel")
    private String idAbsenMapel;
    @SerializedName("id_penempatan")
    private String idPenempatan;
    @SerializedName("id_jadwal")
    private String idJadwal;
    @SerializedName("tgl")
    private String tgl;
    @SerializedName("jam")
    private String jam;
    @SerializedName("absen")
    private String absen;
    @SerializedName("valid")
    private String valid;
    @SerializedName("rekap")
    private String rekap;
    @SerializedName("status")
    private String status;
    @SerializedName("keterangan")
    private String keterangan;

    public String getIdAbsenMapel() {
        return idAbsenMapel;
    }

    public void setIdAbsenMapel(String idAbsenMapel) {
        this.idAbsenMapel = idAbsenMapel;
    }

    public String getIdPenempatan() {
        return idPenempatan;
    }

    public void setIdPenempatan(String idPenempatan) {
        this.idPenempatan = idPenempatan;
    }

    public String getIdJadwal() {
        return idJadwal;
    }

    public void setIdJadwal(String idJadwal) {
        this.idJadwal = idJadwal;
    }

    public String getTgl() {
        return tgl;
    }

    public void setTgl(String tgl) {
        this.tgl = tgl;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }

    public String getAbsen() {
        return absen;
    }

    public void setAbsen(String absen) {
        this.absen = absen;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }

    public String getRekap() {
        return rekap;
    }

    public void setRekap(String rekap) {
        this.rekap = rekap;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
