package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 8/16/2018.
 */

public class JadwalOffline {
    @SerializedName("id_jadwal")
    private String idJadwal;
    @SerializedName("id_ta")
    private String idTa;
    @SerializedName("id_jenjang")
    private String idJenjang;
    @SerializedName("id_kelas")
    private String idKelas;
    @SerializedName("id_mapel")
    private String idMapel;
    @SerializedName("id_guru")
    private String idGuru;
    @SerializedName("hari")
    private String hari;
    @SerializedName("jam_awal")
    private String jamAwal;
    @SerializedName("jam_akhir")
    private String jamAkhir;
    @SerializedName("status")
    private String status;

    public String getIdJadwal() {
        return idJadwal;
    }

    public void setIdJadwal(String idJadwal) {
        this.idJadwal = idJadwal;
    }

    public String getIdTa() {
        return idTa;
    }

    public void setIdTa(String idTa) {
        this.idTa = idTa;
    }

    public String getIdJenjang() {
        return idJenjang;
    }

    public void setIdJenjang(String idJenjang) {
        this.idJenjang = idJenjang;
    }

    public String getIdKelas() {
        return idKelas;
    }

    public void setIdKelas(String idKelas) {
        this.idKelas = idKelas;
    }

    public String getIdMapel() {
        return idMapel;
    }

    public void setIdMapel(String idMapel) {
        this.idMapel = idMapel;
    }

    public String getIdGuru() {
        return idGuru;
    }

    public void setIdGuru(String idGuru) {
        this.idGuru = idGuru;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public String getJamAwal() {
        return jamAwal;
    }

    public void setJamAwal(String jamAwal) {
        this.jamAwal = jamAwal;
    }

    public String getJamAkhir() {
        return jamAkhir;
    }

    public void setJamAkhir(String jamAkhir) {
        this.jamAkhir = jamAkhir;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
