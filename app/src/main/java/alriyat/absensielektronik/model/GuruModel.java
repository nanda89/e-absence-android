package alriyat.absensielektronik.model;

import org.json.JSONException;
import org.json.JSONObject;

import alriyat.absensielektronik.app.AppConfig;
import alriyat.absensielektronik.util.Vira;

public class GuruModel {
    private String id_guru, id_jenjang, nik, nama, no_ktp, alias, jenis_kelamin, tempat_lahir,
            tgl_lahir, agama, no_hp, email, ijazah, pend_terakhir, gambar, alamat, unit_kerja, tmt,
            url_photo;

    public GuruModel(JSONObject json) {
        this.id_guru = "";
        this.id_jenjang = "";
        this.nik = "";
        this.nama = "";
        this.no_ktp = "";
        this.alias = "";
        this.jenis_kelamin = "";
        this.tempat_lahir = "";
        this.tgl_lahir = "";
        this.agama = "";
        this.no_hp = "";
        this.email = "";
        this.ijazah = "";
        this.pend_terakhir = "";
        this.gambar = "";
        this.alamat = "";
        this.unit_kerja = "";
        this.tmt = "";
        this.url_photo = "";
        this.setData(json);
    }

    private void setData(JSONObject jsonObject) {
        try {
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_ID_GURU)) {
                this.setId_guru(jsonObject.getString(AppConfig.PARAM_ID_GURU));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_ID_JENJANG)) {
                this.setId_jenjang(jsonObject.getString(AppConfig.PARAM_ID_JENJANG));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_NIK)) {
                this.setNik(jsonObject.getString(AppConfig.PARAM_NIK));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_NAMA)) {
                this.setNama(jsonObject.getString(AppConfig.PARAM_NAMA));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_NO_KTP)) {
                this.setNo_ktp(jsonObject.getString(AppConfig.PARAM_NO_KTP));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_ALIAS)) {
                this.setAlias(jsonObject.getString(AppConfig.PARAM_ALIAS));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_JENIS_KELAMIN)) {
                this.setJenis_kelamin(jsonObject.getString(AppConfig.PARAM_JENIS_KELAMIN));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_TEMPAT_LAHIR)) {
                this.setTempat_lahir(jsonObject.getString(AppConfig.PARAM_TEMPAT_LAHIR));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_TGL_LAHIR)) {
                this.setTgl_lahir(jsonObject.getString(AppConfig.PARAM_TGL_LAHIR));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_AGAMA)) {
                this.setAgama(jsonObject.getString(AppConfig.PARAM_AGAMA));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_NO_HP)) {
                this.setNo_hp(jsonObject.getString(AppConfig.PARAM_NO_HP));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_EMAIL)) {
                this.setEmail(jsonObject.getString(AppConfig.PARAM_EMAIL));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_IJAZAH)) {
                this.setIjazah(jsonObject.getString(AppConfig.PARAM_IJAZAH));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_PEND_TERAKHIR)) {
                this.setPend_terakhir(jsonObject.getString(AppConfig.PARAM_PEND_TERAKHIR));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_GAMBAR)) {
                this.setGambar(jsonObject.getString(AppConfig.PARAM_GAMBAR));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_ALAMAT)) {
                this.setAlamat(jsonObject.getString(AppConfig.PARAM_ALAMAT));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_UNIT_KERJA)) {
                this.setUnit_kerja(jsonObject.getString(AppConfig.PARAM_UNIT_KERJA));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_TMT)) {
                this.setTmt(jsonObject.getString(AppConfig.PARAM_TMT));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_URL_PHOTO)) {
                this.setUrl_photo(jsonObject.getString(AppConfig.PARAM_URL_PHOTO));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getId_guru() {
        return id_guru;
    }

    public void setId_guru(String id_guru) {
        this.id_guru = id_guru;
    }

    public String getId_jenjang() {
        return id_jenjang;
    }

    public void setId_jenjang(String id_jenjang) {
        this.id_jenjang = id_jenjang;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNo_ktp() {
        return no_ktp;
    }

    public void setNo_ktp(String no_ktp) {
        this.no_ktp = no_ktp;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getJenis_kelamin() {
        return jenis_kelamin;
    }

    public void setJenis_kelamin(String jenis_kelamin) {
        this.jenis_kelamin = jenis_kelamin;
    }

    public String getTempat_lahir() {
        return tempat_lahir;
    }

    public void setTempat_lahir(String tempat_lahir) {
        this.tempat_lahir = tempat_lahir;
    }

    public String getTgl_lahir() {
        return tgl_lahir;
    }

    public void setTgl_lahir(String tgl_lahir) {
        this.tgl_lahir = tgl_lahir;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIjazah() {
        return ijazah;
    }

    public void setIjazah(String ijazah) {
        this.ijazah = ijazah;
    }

    public String getPend_terakhir() {
        return pend_terakhir;
    }

    public void setPend_terakhir(String pend_terakhir) {
        this.pend_terakhir = pend_terakhir;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getUnit_kerja() {
        return unit_kerja;
    }

    public void setUnit_kerja(String unit_kerja) {
        this.unit_kerja = unit_kerja;
    }

    public String getTmt() {
        return tmt;
    }

    public void setTmt(String tmt) {
        this.tmt = tmt;
    }

    public String getUrl_photo() {
        return url_photo;
    }

    public void setUrl_photo(String url_photo) {
        this.url_photo = url_photo;
    }
}
