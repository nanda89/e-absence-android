package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 10/3/2018.
 */

public class KehadiranGuru {

    @SerializedName("nik")
    private String nik;
    @SerializedName("nama")
    private String nama;
    @SerializedName("jenjang")
    private String jenjang;
    @SerializedName("mapel")
    private String mapel;
    @SerializedName("hadir")
    private String hadir;
    @SerializedName("tidak_absen")
    private String tidakAbsen;
    @SerializedName("sakit")
    private String sakit;
    @SerializedName("izin")
    private String izin;
    @SerializedName("tanpa_keterangan")
    private String tanpaKeterangan;

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJenjang() {
        return jenjang;
    }

    public void setJenjang(String jenjang) {
        this.jenjang = jenjang;
    }

    public String getMapel() {
        return mapel;
    }

    public void setMapel(String mapel) {
        this.mapel = mapel;
    }

    public String getTidakAbsen() {
        return tidakAbsen;
    }

    public void setTidakAbsen(String tidakAbsen) {
        this.tidakAbsen = tidakAbsen;
    }

    public String getHadir() {
        return hadir;
    }

    public void setHadir(String hadir) {
        this.hadir = hadir;
    }

    public String getSakit() {
        return sakit;
    }

    public void setSakit(String sakit) {
        this.sakit = sakit;
    }

    public String getIzin() {
        return izin;
    }

    public void setIzin(String izin) {
        this.izin = izin;
    }

    public String getTanpaKeterangan() {
        return tanpaKeterangan;
    }

    public void setTanpaKeterangan(String tanpaKeterangan) {
        this.tanpaKeterangan = tanpaKeterangan;
    }
}