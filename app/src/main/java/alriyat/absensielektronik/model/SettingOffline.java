package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 8/16/2018.
 */

public class SettingOffline {
    @SerializedName("id_setting")
    private String idSetting;
    @SerializedName("logo_mini")
    private String logoMini;
    @SerializedName("logo_gambar")
    private String logoGambar;
    @SerializedName("logo_favicon")
    private String logoFavicon;
    @SerializedName("nik")
    private String nik;
    @SerializedName("kartu_gsm")
    private String kartuGsm;
    @SerializedName("cek_pulsa")
    private String cekPulsa;
    @SerializedName("metode")
    private String metode;
    @SerializedName("kode_aktivasi")
    private String kodeAktivasi;
    @SerializedName("link_tujuan")
    private String linkTujuan;
    @SerializedName("logo_besar")
    private String logoBesar;
    @SerializedName("logo_sekolah")
    private String logoSekolah;
    @SerializedName("pimpinan")
    private String pimpinan;
    @SerializedName("akun_gmail")
    private String akunGmail;
    @SerializedName("alamat")
    private String alamat;
    @SerializedName("website")
    private String website;
    @SerializedName("kop_surat")
    private String kopSurat;
    @SerializedName("ttd")
    private String ttd;
    @SerializedName("telat")
    private String telat;
    @SerializedName("toleransi_jam_akhir")
    private String toleransiJamAkhir;
    @SerializedName("lat")
    private String lat;
    @SerializedName("lng")
    private String lng;
    @SerializedName("radius")
    private String radius;
    @SerializedName("jam_masuk")
    private String jamMasuk;
    @SerializedName("jam_pulang")
    private String jamPulang;
    @SerializedName("kota")
    private String kota;
    @SerializedName("kapasitas_kelas")
    private String kapasitasKelas;

    public String getIdSetting() {
        return idSetting;
    }

    public void setIdSetting(String idSetting) {
        this.idSetting = idSetting;
    }

    public String getLogoMini() {
        return logoMini;
    }

    public void setLogoMini(String logoMini) {
        this.logoMini = logoMini;
    }

    public String getLogoGambar() {
        return logoGambar;
    }

    public void setLogoGambar(String logoGambar) {
        this.logoGambar = logoGambar;
    }

    public String getLogoFavicon() {
        return logoFavicon;
    }

    public void setLogoFavicon(String logoFavicon) {
        this.logoFavicon = logoFavicon;
    }

    public String getNik() {
        return nik;
    }

    public void setNik(String nik) {
        this.nik = nik;
    }

    public String getKartuGsm() {
        return kartuGsm;
    }

    public void setKartuGsm(String kartuGsm) {
        this.kartuGsm = kartuGsm;
    }

    public String getCekPulsa() {
        return cekPulsa;
    }

    public void setCekPulsa(String cekPulsa) {
        this.cekPulsa = cekPulsa;
    }

    public String getMetode() {
        return metode;
    }

    public void setMetode(String metode) {
        this.metode = metode;
    }

    public String getKodeAktivasi() {
        return kodeAktivasi;
    }

    public void setKodeAktivasi(String kodeAktivasi) {
        this.kodeAktivasi = kodeAktivasi;
    }

    public String getLinkTujuan() {
        return linkTujuan;
    }

    public void setLinkTujuan(String linkTujuan) {
        this.linkTujuan = linkTujuan;
    }

    public String getLogoBesar() {
        return logoBesar;
    }

    public void setLogoBesar(String logoBesar) {
        this.logoBesar = logoBesar;
    }

    public String getLogoSekolah() {
        return logoSekolah;
    }

    public void setLogoSekolah(String logoSekolah) {
        this.logoSekolah = logoSekolah;
    }

    public String getPimpinan() {
        return pimpinan;
    }

    public void setPimpinan(String pimpinan) {
        this.pimpinan = pimpinan;
    }

    public String getAkunGmail() {
        return akunGmail;
    }

    public void setAkunGmail(String akunGmail) {
        this.akunGmail = akunGmail;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getTelat() {
        return telat;
    }

    public void setTelat(String telat) {
        this.telat = telat;
    }

    public String getToleransiJamAkhir() {
        return toleransiJamAkhir;
    }

    public void setToleransiJamAkhir(String toleransiJamAkhir) {
        this.toleransiJamAkhir = toleransiJamAkhir;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getRadius() {
        return radius;
    }

    public void setRadius(String radius) {
        this.radius = radius;
    }

    public String getJamMasuk() {
        return jamMasuk;
    }

    public void setJamMasuk(String jamMasuk) {
        this.jamMasuk = jamMasuk;
    }

    public String getJamPulang() {
        return jamPulang;
    }

    public void setJamPulang(String jamPulang) {
        this.jamPulang = jamPulang;
    }

    public String getKopSurat() {
        return kopSurat;
    }

    public void setKopSurat(String kopSurat) {
        this.kopSurat = kopSurat;
    }

    public String getTtd() {
        return ttd;
    }

    public void setTtd(String ttd) {
        this.ttd = ttd;
    }

    public String getKota() {
        return kota;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public String getKapasitasKelas() {
        return kapasitasKelas;
    }

    public void setKapasitasKelas(String kapasitasKelas) {
        this.kapasitasKelas = kapasitasKelas;
    }
}
