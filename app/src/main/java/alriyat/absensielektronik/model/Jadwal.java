package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Riyadh on 14/11/2017.
 */

public class Jadwal {
    @SerializedName("kelas")
    private String kelas;
    @SerializedName("jam")
    private String jam;
    @SerializedName("idguru")
    private String idguru;
    @SerializedName("hari")
    private String hari;

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public String getIdguru() {
        return idguru;
    }

    public void setIdguru(String idguru) {
        this.idguru = idguru;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getJam() {
        return jam;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }
}
