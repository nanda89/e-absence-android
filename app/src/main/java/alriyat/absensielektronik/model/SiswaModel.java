package alriyat.absensielektronik.model;

import org.json.JSONException;
import org.json.JSONObject;

import alriyat.absensielektronik.app.AppConfig;
import alriyat.absensielektronik.util.Vira;

public class SiswaModel {
    private String id_siswa, id_jenjang, nama_lengkap, nisn, jenis_kelamin, tempat_lahir, tgl_lahir,
            agama, alamat, no_hp, email, asal_sekolah, ket_asal, nama_ayah, nama_ibu, gambar,
            tgl_daftar, no_induk_kelas, telp_ayah, telp_ibu, url_photo;

    public SiswaModel(JSONObject json) {
        this.id_siswa = "";
        this.id_jenjang = "";
        this.nama_lengkap = "";
        this.nisn = "";
        this.jenis_kelamin = "";
        this.tempat_lahir = "";
        this.tgl_lahir = "";
        this.agama = "";
        this.alamat = "";
        this.no_hp = "";
        this.email = "";
        this.asal_sekolah = "";
        this.ket_asal = "";
        this.nama_ayah = "";
        this.nama_ibu = "";
        this.gambar = "";
        this.tgl_daftar = "";
        this.no_induk_kelas = "";
        this.telp_ayah = "";
        this.telp_ibu = "";
        this.url_photo = "";
        this.setData(json);
    }

    private void setData(JSONObject jsonObject) {
        try {
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_ID_SISWA)) {
                this.setId_siswa(jsonObject.getString(AppConfig.PARAM_ID_SISWA));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_ID_JENJANG)) {
                this.setId_jenjang(jsonObject.getString(AppConfig.PARAM_ID_JENJANG));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_NAMA_LENGKAP)) {
                this.setNama_lengkap(jsonObject.getString(AppConfig.PARAM_NAMA_LENGKAP));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_NISN)) {
                this.setNisn(jsonObject.getString(AppConfig.PARAM_NISN));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_JENIS_KELAMIN)) {
                this.setJenis_kelamin(jsonObject.getString(AppConfig.PARAM_JENIS_KELAMIN));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_TEMPAT_LAHIR)) {
                this.setTempat_lahir(jsonObject.getString(AppConfig.PARAM_TEMPAT_LAHIR));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_TGL_LAHIR)) {
                this.setTgl_lahir(jsonObject.getString(AppConfig.PARAM_TGL_LAHIR));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_AGAMA)) {
                this.setAgama(jsonObject.getString(AppConfig.PARAM_AGAMA));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_ALAMAT)) {
                this.setAlamat(jsonObject.getString(AppConfig.PARAM_ALAMAT));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_NO_HP)) {
                this.setNo_hp(jsonObject.getString(AppConfig.PARAM_NO_HP));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_EMAIL)) {
                this.setEmail(jsonObject.getString(AppConfig.PARAM_EMAIL));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_ASAL_SEKOLAH)) {
                this.setAsal_sekolah(jsonObject.getString(AppConfig.PARAM_ASAL_SEKOLAH));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_KET_ASAL)) {
                this.setKet_asal(jsonObject.getString(AppConfig.PARAM_KET_ASAL));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_NAMA_AYAH)) {
                this.setNama_ayah(jsonObject.getString(AppConfig.PARAM_NAMA_AYAH));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_NAMA_IBU)) {
                this.setNama_ibu(jsonObject.getString(AppConfig.PARAM_NAMA_IBU));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_GAMBAR)) {
                this.setGambar(jsonObject.getString(AppConfig.PARAM_GAMBAR));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_TGL_DAFTAR)) {
                this.setTgl_daftar(jsonObject.getString(AppConfig.PARAM_TGL_DAFTAR));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_NO_INDUK_KELAS)) {
                this.setNo_induk_kelas(jsonObject.getString(AppConfig.PARAM_NO_INDUK_KELAS));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_TELP_AYAH)) {
                this.setTelp_ayah(jsonObject.getString(AppConfig.PARAM_TELP_AYAH));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_TELP_IBU)) {
                this.setTelp_ibu(jsonObject.getString(AppConfig.PARAM_TELP_IBU));
            }
            if(Vira.isWebServiceValid(jsonObject, AppConfig.PARAM_URL_PHOTO)) {
                this.setUrl_photo(jsonObject.getString(AppConfig.PARAM_URL_PHOTO));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getId_siswa() {
        return id_siswa;
    }

    public void setId_siswa(String id_siswa) {
        this.id_siswa = id_siswa;
    }

    public String getId_jenjang() {
        return id_jenjang;
    }

    public void setId_jenjang(String id_jenjang) {
        this.id_jenjang = id_jenjang;
    }

    public String getNama_lengkap() {
        return nama_lengkap;
    }

    public void setNama_lengkap(String nama_lengkap) {
        this.nama_lengkap = nama_lengkap;
    }

    public String getNisn() {
        return nisn;
    }

    public void setNisn(String nisn) {
        this.nisn = nisn;
    }

    public String getJenis_kelamin() {
        return jenis_kelamin;
    }

    public void setJenis_kelamin(String jenis_kelamin) {
        this.jenis_kelamin = jenis_kelamin;
    }

    public String getTempat_lahir() {
        return tempat_lahir;
    }

    public void setTempat_lahir(String tempat_lahir) {
        this.tempat_lahir = tempat_lahir;
    }

    public String getTgl_lahir() {
        return tgl_lahir;
    }

    public void setTgl_lahir(String tgl_lahir) {
        this.tgl_lahir = tgl_lahir;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAsal_sekolah() {
        return asal_sekolah;
    }

    public void setAsal_sekolah(String asal_sekolah) {
        this.asal_sekolah = asal_sekolah;
    }

    public String getKet_asal() {
        return ket_asal;
    }

    public void setKet_asal(String ket_asal) {
        this.ket_asal = ket_asal;
    }

    public String getNama_ayah() {
        return nama_ayah;
    }

    public void setNama_ayah(String nama_ayah) {
        this.nama_ayah = nama_ayah;
    }

    public String getNama_ibu() {
        return nama_ibu;
    }

    public void setNama_ibu(String nama_ibu) {
        this.nama_ibu = nama_ibu;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getTgl_daftar() {
        return tgl_daftar;
    }

    public void setTgl_daftar(String tgl_daftar) {
        this.tgl_daftar = tgl_daftar;
    }

    public String getNo_induk_kelas() {
        return no_induk_kelas;
    }

    public void setNo_induk_kelas(String no_induk_kelas) {
        this.no_induk_kelas = no_induk_kelas;
    }

    public String getTelp_ayah() {
        return telp_ayah;
    }

    public void setTelp_ayah(String telp_ayah) {
        this.telp_ayah = telp_ayah;
    }

    public String getTelp_ibu() {
        return telp_ibu;
    }

    public void setTelp_ibu(String telp_ibu) {
        this.telp_ibu = telp_ibu;
    }

    public String getUrl_photo() {
        return url_photo;
    }

    public void setUrl_photo(String url_photo) {
        this.url_photo = url_photo;
    }
}
