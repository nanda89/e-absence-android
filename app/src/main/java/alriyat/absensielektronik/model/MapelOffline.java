package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 8/16/2018.
 */

public class MapelOffline {
    @SerializedName("id_mapel")
    private String idMapel;
    @SerializedName("id_jenjang")
    private String idJenjang;
    @SerializedName("id_kat_mapel")
    private String idKatMapel;
    @SerializedName("mapel")
    private String mapel;
    @SerializedName("kkm")
    private String kkm;

    public String getIdMapel() {
        return idMapel;
    }

    public void setIdMapel(String idMapel) {
        this.idMapel = idMapel;
    }

    public String getIdJenjang() {
        return idJenjang;
    }

    public void setIdJenjang(String idJenjang) {
        this.idJenjang = idJenjang;
    }

    public String getIdKatMapel() {
        return idKatMapel;
    }

    public void setIdKatMapel(String idKatMapel) {
        this.idKatMapel = idKatMapel;
    }

    public String getMapel() {
        return mapel;
    }

    public void setMapel(String mapel) {
        this.mapel = mapel;
    }

    public String getKkm() {
        return kkm;
    }

    public void setKkm(String kkm) {
        this.kkm = kkm;
    }
}
