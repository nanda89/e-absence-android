package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 12/25/2018.
 */

public class Login {
    @SerializedName("id_user")
    private String id_user;
    @SerializedName("username")
    private String username;
    @SerializedName("password")
    private String password;
    @SerializedName("id_guru")
    private String idGuru;
    @SerializedName("id_non_guru")
    private String idNonGuru;
    @SerializedName("status_guru_bk")
    private String statusGuruBK;
    @SerializedName("message")
    private String message;
    @SerializedName("level")
    private String level;
    @SerializedName("status_kepsek")
    private String statusKepsek;

    public String getStatusKepsek() {
        return statusKepsek;
    }

    public void setStatusKepsek(String statusKepsek) {
        this.statusKepsek = statusKepsek;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getIdNonGuru() {
        return idNonGuru;
    }

    public void setIdNonGuru(String idNonGuru) {
        this.idNonGuru = idNonGuru;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIdGuru() {
        return idGuru;
    }

    public void setIdGuru(String idGuru) {
        this.idGuru = idGuru;
    }

    public String getStatusGuruBK() {
        return statusGuruBK;
    }

    public void setStatusGuruBK(String statusGuruBK) {
        this.statusGuruBK = statusGuruBK;
    }
}
