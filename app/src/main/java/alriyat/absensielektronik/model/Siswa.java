package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Riyadh on 03/03/2017.
 */
public class Siswa {
    @SerializedName("no")
    private String no;
    @SerializedName("id_siswa")
    private String nis;
    @SerializedName("nama_lengkap")
    private String nama;
    @SerializedName("gambar")
    private String gmbr;
    @SerializedName("id_penempatan")
    private String idpenmpatan;
    @SerializedName("ket_masuk")
    private String ketMasuk;
    @SerializedName("valid")
    private String valid;
    @SerializedName("absen")
    private String status;
    @SerializedName("status")
    private String statusdua;
    @SerializedName("absen_masuk")
    private String abmasuk;
    @SerializedName("absen_pulang")
    private String abkluar;
    @SerializedName("absen_pulang1")
    private String abkeluar;
    @SerializedName("kelas")
    private String kelas;
    @SerializedName("id_kelas")
    private String idkelas;

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getNis() {
        return nis;
    }

    public void setNis(String nis) {
        this.nis = nis;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getGmbr() {
        return gmbr;
    }

    public void setGmbr(String gmbr) {
        this.gmbr = gmbr;
    }

    public String getIdpenmpatan() {
        return idpenmpatan;
    }

    public void setIdpenmpatan(String idpenmpatan) {
        this.idpenmpatan = idpenmpatan;
    }

    public String getKetMasuk() {
        return ketMasuk;
    }

    public void setKetMasuk(String ketMasuk) {
        this.ketMasuk = ketMasuk;
    }

    public String getValid() {
        return valid;
    }

    public void setValid(String valid) {
        this.valid = valid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusdua() {
        return statusdua;
    }

    public void setStatusdua(String statusdua) {
        this.statusdua = statusdua;
    }

    public String getAbmasuk() {
        return abmasuk;
    }

    public void setAbmasuk(String abmasuk) {
        this.abmasuk = abmasuk;
    }

    public String getAbkluar() {
        return abkluar;
    }

    public void setAbkluar(String abkluar) {
        this.abkluar = abkluar;
    }

    public String getAbkeluar() {
        return abkeluar;
    }

    public void setAbkeluar(String abkeluar) {
        this.abkeluar = abkeluar;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getIdkelas() {
        return idkelas;
    }

    public void setIdkelas(String idkelas) {
        this.idkelas = idkelas;
    }
}
