package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 8/16/2018.
 */

public class TahunajaranOffline {
    @SerializedName("id_ta")
    private String idTa;
    @SerializedName("tahun_ajaran")
    private String tahunAjaran;
    @SerializedName("tgl_awal")
    private String tglAwal;
    @SerializedName("tgl_akhir")
    private String tglAkhir;
    @SerializedName("status")
    private String status;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIdTa() {
        return idTa;
    }

    public void setIdTa(String idTa) {
        this.idTa = idTa;
    }

    public String getTahunAjaran() {
        return tahunAjaran;
    }

    public void setTahunAjaran(String tahunAjaran) {
        this.tahunAjaran = tahunAjaran;
    }

    public String getTglAwal() {
        return tglAwal;
    }

    public void setTglAwal(String tglAwal) {
        this.tglAwal = tglAwal;
    }

    public String getTglAkhir() {
        return tglAkhir;
    }

    public void setTglAkhir(String tglAkhir) {
        this.tglAkhir = tglAkhir;
    }
}
