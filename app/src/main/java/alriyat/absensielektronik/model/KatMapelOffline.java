package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 8/16/2018.
 */

public class KatMapelOffline {
    @SerializedName("id_kat_mapel")
    private String idKatMapel;
    @SerializedName("kategori_mapel")
    private String kategoriMapel;

    public String getIdKatMapel() {
        return idKatMapel;
    }

    public void setIdKatMapel(String idKatMapel) {
        this.idKatMapel = idKatMapel;
    }

    public String getKategoriMapel() {
        return kategoriMapel;
    }

    public void setKategoriMapel(String kategoriMapel) {
        this.kategoriMapel = kategoriMapel;
    }
}
