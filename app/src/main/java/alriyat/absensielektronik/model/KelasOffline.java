package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 8/16/2018.
 */

public class KelasOffline {
    @SerializedName("id_kelas")
    private String idKelas;
    @SerializedName("kelas")
    private String kelas;
    @SerializedName("id_guru")
    private String idGuru;
    @SerializedName("id_jenjang")
    private String idJenjang;
    @SerializedName("nama_guru")
    private String namaGuru;

    public String getIdKelas() {
        return idKelas;
    }

    public void setIdKelas(String idKelas) {
        this.idKelas = idKelas;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getIdGuru() {
        return idGuru;
    }

    public void setIdGuru(String idGuru) {
        this.idGuru = idGuru;
    }

    public String getIdJenjang() {
        return idJenjang;
    }

    public void setIdJenjang(String idJenjang) {
        this.idJenjang = idJenjang;
    }

    public String getNamaGuru() {
        return namaGuru;
    }

    public void setNamaGuru(String namaGuru) {
        this.namaGuru = namaGuru;
    }
}
