package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 10/3/2018.
 */

public class KehadiranNonGuru {
    @SerializedName("nip")
    private String nip;
    @SerializedName("nama")
    private String nama;
    @SerializedName("masuk_absen_sesuai")
    private String masukAbsenSesuai;
    @SerializedName("masuk_terlambat")
    private String masukTerlambat;
    @SerializedName("masuk_tidak_absen")
    private String masukTidakAbsen;
    @SerializedName("pulang_absen_sesuai")
    private String pulangAbsenSesuai;
    @SerializedName("pulang_terlambat")
    private String pulangTerlambat;
    @SerializedName("pulang_tidak_absen")
    private String pulangTidakAbsen;

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getMasukAbsenSesuai() {
        return masukAbsenSesuai;
    }

    public void setMasukAbsenSesuai(String masukAbsenSesuai) {
        this.masukAbsenSesuai = masukAbsenSesuai;
    }

    public String getMasukTerlambat() {
        return masukTerlambat;
    }

    public void setMasukTerlambat(String masukTerlambat) {
        this.masukTerlambat = masukTerlambat;
    }

    public String getMasukTidakAbsen() {
        return masukTidakAbsen;
    }

    public void setMasukTidakAbsen(String masukTidakAbsen) {
        this.masukTidakAbsen = masukTidakAbsen;
    }

    public String getPulangAbsenSesuai() {
        return pulangAbsenSesuai;
    }

    public void setPulangAbsenSesuai(String pulangAbsenSesuai) {
        this.pulangAbsenSesuai = pulangAbsenSesuai;
    }

    public String getPulangTerlambat() {
        return pulangTerlambat;
    }

    public void setPulangTerlambat(String pulangTerlambat) {
        this.pulangTerlambat = pulangTerlambat;
    }

    public String getPulangTidakAbsen() {
        return pulangTidakAbsen;
    }

    public void setPulangTidakAbsen(String pulangTidakAbsen) {
        this.pulangTidakAbsen = pulangTidakAbsen;
    }
}