package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * created by Nanda on 20/11/2019
 */
public class JadwalTambahanOffline {
    @SerializedName("id_jadwal_tambahan")
    private Integer idJadwalTambahan;

    @SerializedName("nama_kelas")
    private String kelas;

    @SerializedName("id_jenjang")
    private Integer idJenjang;

    @SerializedName("id_guru")
    private Integer idGuru;

    @SerializedName("id_mapel")
    private Integer idMapel;

    @SerializedName("id_pembimbing_1")
    private Integer idPembimbing1;

    @SerializedName("id_pembimbing_2")
    private Integer idPembimbing2;

    @SerializedName("id_pembimbing_3")
    private Integer idPembimbing3;

    @SerializedName("start_date")
    private String startDate;

    @SerializedName("end_date")
    private String endDate;

    @SerializedName("hari")
    private String hari;

    @SerializedName("jam_awal")
    private String jamAwal;

    @SerializedName("jam_akhir")
    private String jamAkhir;

    @SerializedName("minggu_ke")
    private Integer mingguKe;

    public Integer getIdJadwalTambahan() {
        return idJadwalTambahan;
    }

    public void setIdJadwalTambahan(Integer idJadwalTambahan) {
        this.idJadwalTambahan = idJadwalTambahan;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public Integer getIdJenjang() {
        return idJenjang;
    }

    public void setIdJenjang(Integer idJenjang) {
        this.idJenjang = idJenjang;
    }

    public Integer getIdGuru() {
        return idGuru;
    }

    public void setIdGuru(Integer idGuru) {
        this.idGuru = idGuru;
    }

    public Integer getIdMapel() {
        return idMapel;
    }

    public void setIdMapel(Integer idMapel) {
        this.idMapel = idMapel;
    }

    public Integer getIdPembimbing1() {
        return idPembimbing1;
    }

    public void setIdPembimbing1(Integer idPembimbing1) {
        this.idPembimbing1 = idPembimbing1;
    }

    public Integer getIdPembimbing2() {
        return idPembimbing2;
    }

    public void setIdPembimbing2(Integer idPembimbing2) {
        this.idPembimbing2 = idPembimbing2;
    }

    public Integer getIdPembimbing3() {
        return idPembimbing3;
    }

    public void setIdPembimbing3(Integer idPembimbing3) {
        this.idPembimbing3 = idPembimbing3;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public String getJamAwal() {
        return jamAwal;
    }

    public void setJamAwal(String jamAwal) {
        this.jamAwal = jamAwal;
    }

    public String getJamAkhir() {
        return jamAkhir;
    }

    public void setJamAkhir(String jamAkhir) {
        this.jamAkhir = jamAkhir;
    }

    public Integer getMingguKe() {
        return mingguKe;
    }

    public void setMingguKe(Integer mingguKe) {
        this.mingguKe = mingguKe;
    }
}
