package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 11/21/2018.
 */

public class RekapKehadiranGuru {
    @SerializedName("nama")
    private String nama;
    @SerializedName("jenjang")
    private String jenjang;
    @SerializedName("kelas")
    private String kelas;
    @SerializedName("mapel")
    private String mapel;
    @SerializedName("jam_absen")
    private String jamAbsen;
    @SerializedName("status")
    private String status;
    @SerializedName("keterangan")
    private String keterangan;

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJenjang() {
        return jenjang;
    }

    public void setJenjang(String jenjang) {
        this.jenjang = jenjang;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getMapel() {
        return mapel;
    }

    public void setMapel(String mapel) {
        this.mapel = mapel;
    }

    public String getJamAbsen() {
        return jamAbsen;
    }

    public void setJamAbsen(String jamAbsen) {
        this.jamAbsen = jamAbsen;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
