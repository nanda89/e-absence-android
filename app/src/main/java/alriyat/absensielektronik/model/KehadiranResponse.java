package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 1/18/2019.
 */

public class KehadiranResponse {
    @SerializedName("status")
    private String status;
    @SerializedName("keterangan")
    private String keterangan;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
