package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 8/16/2018.
 */

public class KalenderOffline {
    @SerializedName("id_kalender")
    private String idKalender;
    @SerializedName("tanggal")
    private String tanggal;
    @SerializedName("agenda")
    private String agenda;
    @SerializedName("keterangan")
    private String keterangan;

    public String getIdKalender() {
        return idKalender;
    }

    public void setIdKalender(String idKalender) {
        this.idKalender = idKalender;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getAgenda() {
        return agenda;
    }

    public void setAgenda(String agenda) {
        this.agenda = agenda;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }
}
