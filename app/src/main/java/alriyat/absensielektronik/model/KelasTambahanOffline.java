package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * created by Nanda on 20/11/2019
 */
public class KelasTambahanOffline {
    @SerializedName("id_kelas_tambahan")
    private String idKelasTambahan;

    @SerializedName("id_jadwal_tambahan")
    private String idJadwalTambahan;

    @SerializedName("id_siswa")
    private String idSiswa;

    public String getIdKelasTambahan() {
        return idKelasTambahan;
    }

    public void setIdKelasTambahan(String idKelasTambahan) {
        this.idKelasTambahan = idKelasTambahan;
    }

    public String getIdJadwalTambahan() {
        return idJadwalTambahan;
    }

    public void setIdJadwalTambahan(String idJadwalTambahan) {
        this.idJadwalTambahan = idJadwalTambahan;
    }

    public String getIdSiswa() {
        return idSiswa;
    }

    public void setIdSiswa(String idSiswa) {
        this.idSiswa = idSiswa;
    }
}
