package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 10/3/2018.
 */

public class Kehadiran {
    @SerializedName("nisn")
    private String nisn;
    @SerializedName("nama_lengkap")
    private String namaLengkap;
    @SerializedName("pelajaran")
    private String pelajaran;
    @SerializedName("hadir")
    private String hadir;
    @SerializedName("izin")
    private String izin;
    @SerializedName("sakit")
    private String sakit;
    @SerializedName("alpha")
    private String alpha;
    @SerializedName("nihil")
    private String nihil;
    @SerializedName("wali_kelas")
    private String waliKelas;

    public String getNisn() {
        return nisn;
    }

    public void setNisn(String nisn) {
        this.nisn = nisn;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public String getPelajaran() {
        return pelajaran;
    }

    public void setPelajaran(String pelajaran) {
        this.pelajaran = pelajaran;
    }

    public String getHadir() {
        return hadir;
    }

    public void setHadir(String hadir) {
        this.hadir = hadir;
    }

    public String getIzin() {
        return izin;
    }

    public void setIzin(String izin) {
        this.izin = izin;
    }

    public String getSakit() {
        return sakit;
    }

    public void setSakit(String sakit) {
        this.sakit = sakit;
    }

    public String getAlpha() {
        return alpha;
    }

    public void setAlpha(String alpha) {
        this.alpha = alpha;
    }

    public String getNihil() {
        return nihil;
    }

    public void setNihil(String nihil) {
        this.nihil = nihil;
    }

    public String getWaliKelas() {
        return waliKelas;
    }

    public void setWaliKelas(String waliKelas) {
        this.waliKelas = waliKelas;
    }
}