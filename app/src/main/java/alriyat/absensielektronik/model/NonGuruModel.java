package alriyat.absensielektronik.model;

import org.json.JSONException;
import org.json.JSONObject;

import alriyat.absensielektronik.util.Vira;

public class NonGuruModel {
    private String id_non_guru, id_jenjang, nip, nama, no_ktp, alias, jenis_kelamin, tempat_lahir, tgl_lahir, agama, no_hp,
    email, ijazah, pend_terakhir, gambar, alamat, unit_kerja, tmt, url_photo, jabatan;

    public NonGuruModel(JSONObject json) {
        this.id_non_guru = "";
        this.id_jenjang = "";
        this.nip = "";
        this.nama = "";
        this.no_ktp = "";
        this.alias = "";
        this.jenis_kelamin = "";
        this.tempat_lahir = "";
        this.tgl_lahir = "";
        this.agama = "";
        this.no_hp = "";
        this.email = "";
        this.ijazah = "";
        this.pend_terakhir = "";
        this.gambar = "";
        this.alamat = "";
        this.unit_kerja = "";
        this.tmt = "";
        this.url_photo = "";
        this.jabatan = "";
        this.setData(json);
    }

    private void setData(JSONObject jsonObject) {
        try {
            if(Vira.isWebServiceValid(jsonObject, "id_non_guru")) {
                this.setId_non_guru(jsonObject.getString("id_non_guru"));
            }
            if(Vira.isWebServiceValid(jsonObject, "id_jenjang")) {
                this.setId_jenjang(jsonObject.getString("id_jenjang"));
            }
            if(Vira.isWebServiceValid(jsonObject, "nip")) {
                this.setNip(jsonObject.getString("nip"));
            }
            if(Vira.isWebServiceValid(jsonObject, "nama")) {
                this.setNama(jsonObject.getString("nama"));
            }
            if(Vira.isWebServiceValid(jsonObject, "no_ktp")) {
                this.setNo_ktp(jsonObject.getString("no_ktp"));
            }
            if(Vira.isWebServiceValid(jsonObject, "alias")) {
                this.setAlias(jsonObject.getString("alias"));
            }
            if(Vira.isWebServiceValid(jsonObject, "jenis_kelamin")) {
                this.setJenis_kelamin(jsonObject.getString("jenis_kelamin"));
            }
            if(Vira.isWebServiceValid(jsonObject, "tempat_lahir")) {
                this.setTempat_lahir(jsonObject.getString("tempat_lahir"));
            }
            if(Vira.isWebServiceValid(jsonObject, "tgl_lahir")) {
                this.setTgl_lahir(jsonObject.getString("tgl_lahir"));
            }
            if(Vira.isWebServiceValid(jsonObject, "agama")) {
                this.setAgama(jsonObject.getString("agama"));
            }
            if(Vira.isWebServiceValid(jsonObject, "no_hp")) {
                this.setNo_hp(jsonObject.getString("no_hp"));
            }
            if(Vira.isWebServiceValid(jsonObject, "email")) {
                this.setEmail(jsonObject.getString("email"));
            }
            if(Vira.isWebServiceValid(jsonObject, "ijazah")) {
                this.setIjazah(jsonObject.getString("ijazah"));
            }
            if(Vira.isWebServiceValid(jsonObject, "pend_terakhir")) {
                this.setPend_terakhir(jsonObject.getString("pend_terakhir"));
            }
            if(Vira.isWebServiceValid(jsonObject, "gambar")) {
                this.setGambar(jsonObject.getString("gambar"));
            }
            if(Vira.isWebServiceValid(jsonObject, "alamat")) {
                this.setAlamat(jsonObject.getString("alamat"));
            }
            if(Vira.isWebServiceValid(jsonObject, "unit_kerja")) {
                this.setUnit_kerja(jsonObject.getString("unit_kerja"));
            }
            if(Vira.isWebServiceValid(jsonObject, "tmt")) {
                this.setTmt(jsonObject.getString("tmt"));
            }
            if(Vira.isWebServiceValid(jsonObject, "url_photo")) {
                this.setUrl_photo(jsonObject.getString("url_photo"));
            }
            if(Vira.isWebServiceValid(jsonObject, "jabatan")) {
                this.setJabatan(jsonObject.getString("jabatan"));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public String getId_non_guru() {
        return id_non_guru;
    }

    public void setId_non_guru(String id_non_guru) {
        this.id_non_guru = id_non_guru;
    }

    public String getId_jenjang() {
        return id_jenjang;
    }

    public void setId_jenjang(String id_jenjang) {
        this.id_jenjang = id_jenjang;
    }

    public String getNip() {
        return nip;
    }

    public void setNip(String nip) {
        this.nip = nip;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNo_ktp() {
        return no_ktp;
    }

    public void setNo_ktp(String no_ktp) {
        this.no_ktp = no_ktp;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getJenis_kelamin() {
        return jenis_kelamin;
    }

    public void setJenis_kelamin(String jenis_kelamin) {
        this.jenis_kelamin = jenis_kelamin;
    }

    public String getTempat_lahir() {
        return tempat_lahir;
    }

    public void setTempat_lahir(String tempat_lahir) {
        this.tempat_lahir = tempat_lahir;
    }

    public String getTgl_lahir() {
        return tgl_lahir;
    }

    public void setTgl_lahir(String tgl_lahir) {
        this.tgl_lahir = tgl_lahir;
    }

    public String getAgama() {
        return agama;
    }

    public void setAgama(String agama) {
        this.agama = agama;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIjazah() {
        return ijazah;
    }

    public void setIjazah(String ijazah) {
        this.ijazah = ijazah;
    }

    public String getPend_terakhir() {
        return pend_terakhir;
    }

    public void setPend_terakhir(String pend_terakhir) {
        this.pend_terakhir = pend_terakhir;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getUnit_kerja() {
        return unit_kerja;
    }

    public void setUnit_kerja(String unit_kerja) {
        this.unit_kerja = unit_kerja;
    }

    public String getTmt() {
        return tmt;
    }

    public void setTmt(String tmt) {
        this.tmt = tmt;
    }

    public String getUrl_photo() {
        return url_photo;
    }

    public void setUrl_photo(String url_photo) {
        this.url_photo = url_photo;
    }

    public String getJabatan() {
        return jabatan;
    }

    public void setJabatan(String jabatan) {
        this.jabatan = jabatan;
    }
}
