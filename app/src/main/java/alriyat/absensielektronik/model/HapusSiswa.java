package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 2/9/2019.
 */

public class HapusSiswa {
    @SerializedName("response")
    private String response;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
