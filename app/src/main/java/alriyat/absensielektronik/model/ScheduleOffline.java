package alriyat.absensielektronik.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by folcotandiono on 8/16/2018.
 */

public class ScheduleOffline {
    @SerializedName("id_schedule")
    private String idSchedule;
    @SerializedName("tipe_sms")
    private String tipeSms;
    @SerializedName("status")
    private String status;
    @SerializedName("aturan")
    private String aturan;

    public String getIdSchedule() {
        return idSchedule;
    }

    public void setIdSchedule(String idSchedule) {
        this.idSchedule = idSchedule;
    }

    public String getTipeSms() {
        return tipeSms;
    }

    public void setTipeSms(String tipeSms) {
        this.tipeSms = tipeSms;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAturan() {
        return aturan;
    }

    public void setAturan(String aturan) {
        this.aturan = aturan;
    }
}
