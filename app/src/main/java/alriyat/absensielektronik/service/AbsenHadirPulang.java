package alriyat.absensielektronik.service;

import android.content.Context;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import alriyat.absensielektronik.activity.AbsensiActivity;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.KehadiranResponse;
import alriyat.absensielektronik.model.NonGuruOffline;
import alriyat.absensielektronik.pojo.AbsenHadirPulangNonGuru;
import alriyat.absensielektronik.util.DatabaseHandler;
import alriyat.absensielektronik.util.GetDataService;
import retrofit2.Call;
import retrofit2.Callback;

public class AbsenHadirPulang {
    private static String TAG = AbsenHadirPulang.class.getName();

    public static void absenHadir(GetDataService service, final Map<String, String> param, final Context context, final SessionManager sessionManager, final LatLng ll, final boolean isGuru) {
        // cek data pada local database terlebih dahulu,apakah sudah absen masuk sebelumnya
        DatabaseHandler db = new DatabaseHandler(context);
        List<alriyat.absensielektronik.pojo.AbsenHadirPulang> dataLocal;
        if (isGuru) {
            String id_guru = param.get("id_guru");
            String filter = "and keterangan not like '%pulang%'";
            dataLocal = db.getInfoAbsenOffline(id_guru,filter);
        } else {
            String id_non_guru = param.get("id_non_guru");
            dataLocal = db.getInfoAbsenOfflineNonGuru(id_non_guru,"hadir");
        }

        Log.d(TAG, "absenHadir: totaaaaaaaaaaal "+dataLocal.size());
        if (dataLocal.size() == 0) {
            Call<KehadiranResponse> call;
            if (isGuru) {
                call = service.absenHadirGuru(param);
            } else {
                call = service.absenNonGuru(param);
            }
            call.enqueue(new Callback<KehadiranResponse>() {
                @Override
                public void onResponse(Call<KehadiranResponse> call, retrofit2.Response<KehadiranResponse> response) {
                    KehadiranResponse kehadiranResponse = response.body();
                    Log.d(TAG, "onResponse: response absence " + kehadiranResponse.getStatus());
                    if (kehadiranResponse.getStatus().equals("ok")) {
                        /*insert into local database*/
                        DatabaseHandler dh = new DatabaseHandler(context);
                        if (isGuru) {
                            dh.absenKehadiranGuruOnline(context, sessionManager.preferences.getString("idguru", null), kehadiranResponse.getKeterangan());
                        } else {
                            dh.absenKehadiranNonGuruOnline(context, sessionManager.preferences.getString("idnonguru", null), kehadiranResponse.getKeterangan(), param.get("jenis"));
                        }

                        AlertDialog.Builder builder;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
                        } else {
                            builder = new AlertDialog.Builder(context);
                        }
                        builder = new AlertDialog.Builder(context);
                        builder.setTitle("Status")
                                .setMessage("Absen online " + kehadiranResponse.getKeterangan())
                                .show();
                    } else {
                        AlertDialog.Builder builder;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
                        } else {
                            builder = new AlertDialog.Builder(context);
                        }
                        builder = new AlertDialog.Builder(context);
                        builder.setTitle("Status")
                                .setMessage(kehadiranResponse.getKeterangan())
                                .show();
                    }
                }

                @Override
                public void onFailure(Call<KehadiranResponse> call, Throwable t) {
                    t.printStackTrace();
//                    AlertDialog.Builder builder;
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
//                    } else {
//                        builder = new AlertDialog.Builder(context);
//                    }
//                    builder = new AlertDialog.Builder(context);
                    absenHadirOffline(ll,context,sessionManager,isGuru);
//                    builder.setTitle("Status")
//                            .setMessage("Error : absen gagal, terdapat permasalahan komunikasi ( anda absen dengan metode offline)")
//                            .show();
                }
            });
        } else {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(context);
            }
            String ketOffline = "";
            for (alriyat.absensielektronik.pojo.AbsenHadirPulang data : dataLocal) {
                if (data.getKeterangan().contains("pulang")) {
                    if (data.getKeterangan().contains("offline")) {
                        ketOffline = "dengan metode offline";
                    }
                } else {
                    continue;
                }
            }

            builder = new AlertDialog.Builder(context);
            builder.setTitle("Status")
                    .setMessage("Error : absen gagal, anda sudah absen sebelumnya " + ketOffline)
                    .show();
        }
    }

    public static void absenHadirOffline(final LatLng ll, Context context, final SessionManager sessionManager, boolean isGuru) {
        Log.d(TAG, "onNavigationItemSelected: coordinate : " + ll.latitude + " , " + ll.longitude);
        DatabaseHandler dh = new DatabaseHandler(context);
        KehadiranResponse response = null;
        if (isGuru) {
            response = dh.absenHadirGuruOffline(context, sessionManager.preferences.getString("idguru", null), ll);
        } else {
            response = dh.absenHadirNonGuruOffline(context, sessionManager.preferences.getString("idnonguru", null), ll);
        }

        if (response.getStatus().equals("ok")) {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(context);
            }
            builder = new AlertDialog.Builder(context);
            builder.setTitle("Status")
                    .setMessage("Absen offline " + response.getKeterangan())
                    .show();
        } else {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(context);
            }
            builder = new AlertDialog.Builder(context);
            builder.setTitle("Status")
                    .setMessage(response.getKeterangan())
                    .show();
        }
    }

    public static void absenPulangOffline(final LatLng ll, Context context, final SessionManager sessionManager, boolean isGuru) {
        try {
            // cek data pada local database,apakah sudah absen pulang terlebih dahulu ?
            DatabaseHandler dh = new DatabaseHandler(context);
            KehadiranResponse response;
            if (isGuru) {
                response = dh.absenPulangGuruOffline(context, sessionManager.preferences.getString("idguru", null), ll);
            } else {
                response = dh.absenPulangNonGuruOffline(context, sessionManager.preferences.getString("idnonguru", null), ll);
            }

            if (response.getStatus().equals("ok")) {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(context);
                }
                builder = new AlertDialog.Builder(context);
                builder.setTitle("Status")
                        .setMessage("Absen offline " + response.getKeterangan())
                        .show();
            } else {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(context);
                }
                builder = new AlertDialog.Builder(context);
                builder.setTitle("Status")
                        .setMessage(response.getKeterangan())
                        .show();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public static void absenPulang(GetDataService service, final Map<String, String> param, final Context context, final SessionManager sessionManager, final LatLng ll, final boolean isGuru) {
        // cek data pada local database terlebih dahulu,apakah sudah absen pulang terlebih dahulu ?
        DatabaseHandler db = new DatabaseHandler(context);
        List<alriyat.absensielektronik.pojo.AbsenHadirPulang> dataLocal = null;
        if (isGuru) {
            String id_guru = param.get("id_guru");
            String filter = "and keterangan like '%pulang%'";
            dataLocal = db.getInfoAbsenOffline(id_guru,filter);
        } else {
            String id_non_guru = param.get("id_non_guru");
            dataLocal = db.getInfoAbsenOfflineNonGuru(id_non_guru , "pulang");
        }

        //jika belum absen pulang sebelumnya
        if (dataLocal.size() == 0) {
            Call<KehadiranResponse> call;
            if (isGuru) {
                call = service.absenPulangGuru(param);
            } else {
                call = service.absenNonGuru(param);
            }
            call.enqueue(new Callback<KehadiranResponse>() {
                @Override
                public void onResponse(Call<KehadiranResponse> call, retrofit2.Response<KehadiranResponse> response) {
                    KehadiranResponse kehadiranResponse = response.body();
                    if (kehadiranResponse.getStatus().equals("ok")) {
                        //simpan di local database
                        DatabaseHandler dh = new DatabaseHandler(context);
                        if (isGuru) {
                            dh.absenKehadiranGuruOnline(context, sessionManager.preferences.getString("idguru", null), kehadiranResponse.getKeterangan());
                        } else {
                            dh.absenKehadiranNonGuruOnline(context, sessionManager.preferences.getString("idnonguru", null), kehadiranResponse.getKeterangan(), param.get("jenis"));
                        }

                        AlertDialog.Builder builder;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
                        } else {
                            builder = new AlertDialog.Builder(context);
                        }
                        builder = new AlertDialog.Builder(context);
                        builder.setTitle("Status")
                                .setMessage("Absen online " + kehadiranResponse.getKeterangan())
                                .show();
                    } else {
                        AlertDialog.Builder builder;
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
                        } else {
                            builder = new AlertDialog.Builder(context);
                        }
                        builder = new AlertDialog.Builder(context);
                        builder.setTitle("Status")
                                .setMessage(kehadiranResponse.getKeterangan())
                                .show();
                    }
                }

                @Override
                public void onFailure(Call<KehadiranResponse> call, Throwable t) {
                    t.printStackTrace();
//                    AlertDialog.Builder builder;
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                        builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
//                    } else {
//                        builder = new AlertDialog.Builder(context);
//                    }

                    absenPulangOffline(ll,context,sessionManager,isGuru);

//                    builder = new AlertDialog.Builder(context);
//                    builder.setTitle("Status")
//                            .setMessage("Error : absen gagal, terdapat permasalahan komunikasi (anda absen dengan mode offline)")
//                            .show();
                }
            });
        } else {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(context);
            }
            String ketOffline = "";
            if (dataLocal.size() > 0) {
                for (alriyat.absensielektronik.pojo.AbsenHadirPulang data : dataLocal) {
                    if (data.getKeterangan().contains("pulang")) {
                        if (data.getKeterangan().contains("offline")) {
                            ketOffline = "dengan metode offline";
                        }
                    } else {
                        continue;
                    }
                }
            }
            builder = new AlertDialog.Builder(context);
            builder.setTitle("Status")
                    .setMessage("Error : absen gagal, anda sudah absen sebelumnya " + ketOffline)
                    .show();
        }
    }


    public static void getInfoAbsen(final TextView jamDatang, final TextView jamDatangKeterangan, final TextView jamPulang, final TextView jamPulangKeterangan, GetDataService service, final Map<String, String> param, final Context context,
                                    final SessionManager sessionManager, final LatLng ll,boolean isGuru,boolean isConnected) {

        // cek data pada local database terlebih dahulu
        DatabaseHandler db = new DatabaseHandler(context);
        List<alriyat.absensielektronik.pojo.AbsenHadirPulang> dataLocal;
        if(isGuru){
            String id_guru = param.get("id_guru");
            dataLocal = db.getInfoAbsenOffline(id_guru,null);
        }else{
            String id_non_guru = param.get("id_non_guru");
            dataLocal = db.getInfoAbsenNonGuruOffline(id_non_guru);
        }
        Log.d(TAG, "getInfoAbsen: totalllllll "+dataLocal.size());
        if (dataLocal.size() == 0 && isConnected) {
            if(isGuru){
                Call<List<alriyat.absensielektronik.pojo.AbsenHadirPulang>> call = service.getInfoAbsensi(param);
                call.enqueue(new Callback<List<alriyat.absensielektronik.pojo.AbsenHadirPulang>>() {
                    @Override
                    public void onResponse(Call<List<alriyat.absensielektronik.pojo.AbsenHadirPulang>> call, retrofit2.Response<List<alriyat.absensielektronik.pojo.AbsenHadirPulang>> response) {
                        List<alriyat.absensielektronik.pojo.AbsenHadirPulang> data = response.body();
                        setInfoAbsence(data, jamDatang, jamDatangKeterangan, jamPulang, jamPulangKeterangan);
                    }
                    @Override
                    public void onFailure(Call<List<alriyat.absensielektronik.pojo.AbsenHadirPulang>> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }else{
                Call<List<AbsenHadirPulangNonGuru>> call = service.getInfoAbsensiNonGuru(param);
                call.enqueue(new Callback<List<AbsenHadirPulangNonGuru>>() {
                    @Override
                    public void onResponse(Call<List<AbsenHadirPulangNonGuru>> call, retrofit2.Response<List<AbsenHadirPulangNonGuru>> response) {
                        List<AbsenHadirPulangNonGuru> data = response.body();
                        if(data.size() > 0 ){
                            List<alriyat.absensielektronik.pojo.AbsenHadirPulang>newData = new ArrayList<>();

                            for (AbsenHadirPulangNonGuru abNonGuru : data){
                                alriyat.absensielektronik.pojo.AbsenHadirPulang dt = new alriyat.absensielektronik.pojo.AbsenHadirPulang();
                                dt.setId_absen_kehadiran_guru(abNonGuru.getId_absen_non_guru());
                                dt.setKeterangan(abNonGuru.getKeterangan());
                                dt.setJam(abNonGuru.getJam());
                                dt.setId_guru(abNonGuru.getId_non_guru());
                                dt.setTgl(abNonGuru.getTgl());

                                newData.add(dt);
                            }
                            setInfoAbsence(newData, jamDatang, jamDatangKeterangan, jamPulang, jamPulangKeterangan);
                        }

                    }
                    @Override
                    public void onFailure(Call<List<AbsenHadirPulangNonGuru>> call, Throwable t) {
                        t.printStackTrace();
//                    String id_guru = param.get("id_guru");
//                    DatabaseHandler db = new DatabaseHandler(context);
//                    List<alriyat.absensielektronik.pojo.AbsenHadirPulang> data = db.getInfoAbsenOffline(id_guru);
//                    setInfoAbsence(data, jamDatang, jamDatangKeterangan, jamPulang, jamPulangKeterangan);
                    }
                });
            }

        } else {
            setInfoAbsence(dataLocal, jamDatang, jamDatangKeterangan, jamPulang, jamPulangKeterangan);
        }
    }

    private static void setInfoAbsence(List<alriyat.absensielektronik.pojo.AbsenHadirPulang> data, final TextView jamDatang, final TextView jamDatangKeterangan, final TextView jamPulang, final TextView jamPulangKeterangan) {
        if (data.size() > 0) {
            for (alriyat.absensielektronik.pojo.AbsenHadirPulang dataAbsen : data) {
                if (!dataAbsen.getKeterangan().contains("pulang")) {
                    if (dataAbsen.getKeterangan().equals("")) {
                        jamDatang.setText("DATANG : " + dataAbsen.getJam());
                        jamDatangKeterangan.setText("Anda masuk tepat waktu.");
                    } else {
                        jamDatang.setText("DATANG : " + dataAbsen.getJam());
                        jamDatangKeterangan.setText(dataAbsen.getKeterangan());
                    }

                } else {
                    if (dataAbsen.getKeterangan().equals("")) {
                        jamPulang.setText("PULANG : " + dataAbsen.getJam());
                        jamPulangKeterangan.setText("Anda pulang tepat waktu.");
                    } else {
                        jamPulang.setText("PULANG : " + dataAbsen.getJam());
                        jamPulangKeterangan.setText(dataAbsen.getKeterangan());
                    }
                }
            }
        }
    }
}
