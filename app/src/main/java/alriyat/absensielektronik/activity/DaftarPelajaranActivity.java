package alriyat.absensielektronik.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.AlarmReceiver;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.Mapel;
import alriyat.absensielektronik.util.DatabaseHandler;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import alriyat.absensielektronik.util.Vira;
import retrofit2.Call;
import retrofit2.Callback;

public class DaftarPelajaranActivity extends AppCompatActivity {
    private static final String TAG = DaftarPelajaranActivity.class.getName();
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private ArrayList<Mapel> arrayList;
    private MapelAdapter mapelAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private SessionConfig sessionConfig;
    private SessionManager sessionManager;
    private TextView noJadwal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_pelajaran);

        toolbar = (Toolbar)findViewById(R.id.toolbar_daftar);
        toolbar.setTitle("Daftar Pelajaran");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sessionConfig = new SessionConfig(this);
        sessionManager = new SessionManager(this);

        noJadwal = (TextView)findViewById(R.id.text_nojadwal);

        arrayList = new ArrayList<>();
        recyclerView = (RecyclerView)findViewById(R.id.recycler_pelajaran);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        mapelAdapter = new MapelAdapter(this,arrayList);
        recyclerView.setAdapter(mapelAdapter);
        arrayList = getMapel();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                goToBack();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void goToBack(){
        super.onBackPressed();
    }

    private ArrayList<Mapel> getMapel() {
        if (Vira.isConnectedToServer(DaftarPelajaranActivity.this)) {
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            final ProgressDialog loading = ProgressDialog.show(this, "", "Loading Data...", false, false);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("idguru", sessionManager.preferences.getString("username", null));

            Call<List<Mapel>> call = service.daftarMapelAll(param);
            call.enqueue(new Callback<List<Mapel>>() {
                @Override
                public void onResponse(Call<List<Mapel>>call, retrofit2.Response<List<Mapel>> response) {
                    loading.dismiss();

                    arrayList.clear();
                    Log.d(TAG, "onResponse: "+response.body());
                    List<Mapel> listMapel = response.body();

                    for (Mapel mapel : listMapel) {
                        arrayList.add(mapel);
                    }

                    mapelAdapter.notifyDataSetChanged();
                    if (mapelAdapter.getItemCount() > 0) {
                        noJadwal.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<List<Mapel>>call, Throwable t) {
                    // Log error here since request failed
                    loading.dismiss();
                    DatabaseHandler dh = new DatabaseHandler(DaftarPelajaranActivity.this);
                    SessionManager sessionManager = new SessionManager(DaftarPelajaranActivity.this);
                    String guru = sessionManager.preferences.getString("username", null);
                    ArrayList<Mapel> temp = dh.daftarMapelAll(DaftarPelajaranActivity.this, guru);
                    for (Mapel i : temp) {
                        arrayList.add(i);
                    }
                    mapelAdapter.notifyDataSetChanged();
                    if (mapelAdapter.getItemCount() > 0) {
                        noJadwal.setVisibility(View.GONE);
                    }
                }
            });

            return arrayList;
        }
        else {
            DatabaseHandler dh = new DatabaseHandler(DaftarPelajaranActivity.this);
            SessionManager sessionManager = new SessionManager(DaftarPelajaranActivity.this);
            String guru = sessionManager.preferences.getString("username", null);
            ArrayList<Mapel> temp = dh.daftarMapelAll(DaftarPelajaranActivity.this, guru);
            Log.d(TAG, "getMapel: total daftar mapel : "+temp.size());
            for (Mapel i : temp) {
                arrayList.add(i);
            }
            mapelAdapter.notifyDataSetChanged();
            if (mapelAdapter.getItemCount() > 0) {
                noJadwal.setVisibility(View.GONE);
            }
            return arrayList;
        }
    }

    private int getTotalAbsenOffline(){
        DatabaseHandler dh = new DatabaseHandler(DaftarPelajaranActivity.this);
        return dh.getTotalAbsenMapelOffline();
    }

    class MapelAdapter extends RecyclerView.Adapter<DaftarPelajaranActivity.MapelAdapter.TcashViewHolder> {

        private Context mContext;
        private ArrayList<Mapel> arrayList;


        public MapelAdapter(Context mContext,ArrayList<Mapel> arrayList) {
            Log.d(TAG, "MapelAdapter: 99999999999999999999999999999999999 : "+arrayList.size());
            this.mContext = mContext;
            this.arrayList = arrayList;
        }

        public class  TcashViewHolder extends RecyclerView.ViewHolder{
            public TextView id,mapel, waktu,kelas,idjadwal,stat,nama;
            public ImageView img;
            private LinearLayout linearLayout;
            public TcashViewHolder(View itemView) {
                super(itemView);
                id = (TextView)itemView.findViewById(R.id.id_mapel);
                stat = (TextView)itemView.findViewById(R.id.textStat);
                idjadwal = (TextView)itemView.findViewById(R.id.id_jadwal);
                mapel = (TextView)itemView.findViewById(R.id.nma_mapel);
                nama = (TextView) itemView.findViewById(R.id.nma_guru);
                kelas = (TextView)itemView.findViewById(R.id.nma_kelas);
                waktu = (TextView)itemView.findViewById(R.id.jm_tgl);
                linearLayout = (LinearLayout)itemView.findViewById(R.id.itemview);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if(getTotalAbsenOffline() == 0){
                            if (stat.getText().equals("ada")){
                                Intent n = new Intent(DaftarPelajaranActivity.this,AbsensiPelajaranActivity.class);
                                n.putExtra("idkelas",id.getText().toString());
                                n.putExtra("idjadwal",idjadwal.getText().toString());
                                startActivity(n);
                            }else {
                                Toast.makeText(DaftarPelajaranActivity.this,"Belum waktu jam absen",Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            Toast.makeText(DaftarPelajaranActivity.this,"Anda memiliki data absensi yang tertunda,silahkan klik sync kirim!",Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }
        }

        @Override
        public DaftarPelajaranActivity.MapelAdapter.TcashViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_mapel, parent, false);
            return new DaftarPelajaranActivity.MapelAdapter.TcashViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(TcashViewHolder holder, int position) {

            Mapel news = arrayList.get(position);
            Log.d("adapter", "onBindViewHolder: is null : "+news == null ? "null" : "not null");
            holder.id.setText(news.getIdapel());
            holder.stat.setText(news.getStatus());
            holder.idjadwal.setText(news.getIdjadwal());
            holder.kelas.setText(news.getJenjang()+" Kelas "+news.getKelas());
            holder.nama.setText(news.getNama());
            holder.mapel.setText("Pelajaran "+news.getMapel());
            holder.waktu.setText(news.getWaktu()+" s/d "+news.getJam());
            if (holder.stat.getText().equals("ada")) {
                holder.linearLayout.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                holder.mapel.setTextColor(getResources().getColor(R.color.white));
                holder.kelas.setTextColor(getResources().getColor(R.color.white));
                holder.waktu.setTextColor(getResources().getColor(R.color.input_login));

            }
            else {
                holder.linearLayout.setBackgroundColor(getResources().getColor(R.color.white));
                holder.mapel.setTextColor(getResources().getColor(R.color.hitam));
                holder.kelas.setTextColor(getResources().getColor(R.color.hitam));
                holder.waktu.setTextColor(getResources().getColor(R.color.input_login));
            }
            if (news.getStatusGuruBK() != null && news.getStatusGuruBK().equals("1")) holder.nama.setVisibility(View.VISIBLE);
            String jam = "";
            jam += news.getJam().substring(0, 2);
            String menit = "";
            menit += news.getJam().substring(3, 5);

            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(jam));
            calendar.set(Calendar.MINUTE, Integer.valueOf(menit));
            calendar.add(Calendar.MINUTE, -5);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            Intent notificationIntent = new Intent(DaftarPelajaranActivity.this, AlarmReceiver.class);
            PendingIntent broadcast = PendingIntent.getBroadcast(DaftarPelajaranActivity.this, 1, notificationIntent, 0);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && calendar.getTimeInMillis() >= Calendar.getInstance().getTimeInMillis() && news.getStatus1().equals("pulang")) {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), broadcast);
            }
        }

        @Override
        public int getItemCount() {
            Log.d(TAG, "getItemCount: totaaaaaaaaaaaa : "+arrayList.size());
            return arrayList.size();
        }
    }
}
