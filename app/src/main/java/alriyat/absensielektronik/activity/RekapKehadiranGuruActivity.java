package alriyat.absensielektronik.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.app.MySingleton;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.Kehadiran;
import alriyat.absensielektronik.model.KehadiranGuru;
import alriyat.absensielektronik.model.Mapel;
import alriyat.absensielektronik.model.RekapKehadiranGuru;
import alriyat.absensielektronik.model.SettingOffline;
import alriyat.absensielektronik.util.DatabaseHandler;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import alriyat.absensielektronik.util.Vira;
import retrofit2.Call;
import retrofit2.Callback;

public class RekapKehadiranGuruActivity extends AppCompatActivity {

    private EditText etDari;
    private EditText etKe;
    private EditText etJenjang;
    private EditText etTahunajaran;
    private EditText etGuru;
    private Button bFilter;
    private RecyclerView recyclerView;
    private SessionConfig sessionConfig;
    private SessionManager sessionManager;
    private RekapKehadiranGuruAdapter adapter;
    ArrayList<KehadiranGuru> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rekap_kehadiran_guru);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Rekap kehadiran Guru");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etDari = findViewById(R.id.dari);
        etKe = findViewById(R.id.ke);
        etJenjang = findViewById(R.id.jenjang);
        etTahunajaran = findViewById(R.id.tahunajaran);
        etGuru = findViewById(R.id.guru);
        bFilter = findViewById(R.id.filter);
        recyclerView = findViewById(R.id.recycler_view);

        sessionConfig = new SessionConfig(this);
        sessionManager = new SessionManager(this);

        final Calendar calendarDari = Calendar.getInstance();
        final Calendar calendarKe = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener dateDari = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendarDari.set(Calendar.YEAR, year);
                calendarDari.set(Calendar.MONTH, monthOfYear);
                calendarDari.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                etDari.setText(String.format("%02d", calendarDari.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendarDari.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", calendarDari.get(Calendar.YEAR)));
            }

        };

        etDari.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(RekapKehadiranGuruActivity.this, dateDari, calendarDari
                        .get(Calendar.YEAR), calendarDari.get(Calendar.MONTH),
                        calendarDari.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final DatePickerDialog.OnDateSetListener dateKe = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendarKe.set(Calendar.YEAR, year);
                calendarKe.set(Calendar.MONTH, monthOfYear);
                calendarKe.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                etKe.setText(String.format("%02d", calendarKe.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendarKe.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", calendarKe.get(Calendar.YEAR)));
            }

        };

        etKe.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(RekapKehadiranGuruActivity.this, dateKe, calendarKe
                        .get(Calendar.YEAR), calendarKe.get(Calendar.MONTH),
                        calendarKe.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        etJenjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RekapKehadiranGuruActivity.this, PilihJenjangActivity.class);
                startActivity(intent);
            }
        });

        etTahunajaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RekapKehadiranGuruActivity.this, PilihTahunajaranActivity.class);
                startActivity(intent);
            }
        });

        etGuru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RekapKehadiranGuruActivity.this, PilihGuruActivity.class);
                startActivity(intent);
            }
        });

        bFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dari = etDari.getText().toString();
                String ke = etKe.getText().toString();
                String jenjang = etJenjang.getText().toString();
                String tahunajaran = etTahunajaran.getText().toString();

                if (dari.isEmpty()) {
                    Toast.makeText(RekapKehadiranGuruActivity.this, "Tanggal awal kosong!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (ke.isEmpty()) {
                    Toast.makeText(RekapKehadiranGuruActivity.this, "Tanggal akhir kosong!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (jenjang.isEmpty()) {
                    Toast.makeText(RekapKehadiranGuruActivity.this, "Jenjang kosong!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (tahunajaran.isEmpty()) {
                    Toast.makeText(RekapKehadiranGuruActivity.this, "Tahun ajaran kosong!", Toast.LENGTH_SHORT).show();
                    return;
                }

                getKehadiran();
            }
        });

        adapter = new RekapKehadiranGuruAdapter(arrayList);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

        SharedPreferences sharedPreferences = getSharedPreferences("rekap_kehadiran_guru", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("id_jenjang", "");
        editor.putString("jenjang", "");
        editor.putString("id_ta", "");
        editor.putString("tahunajaran", "");
        editor.putString("id_guru", "");
        editor.putString("nama", "");
        editor.apply();
    }

    @Override
    protected void onResume() {
        SharedPreferences sharedPreferences = getSharedPreferences("rekap_kehadiran_guru", Context.MODE_PRIVATE);
        etGuru.setText(sharedPreferences.getString("nama", ""));
        etJenjang.setText(sharedPreferences.getString("jenjang", ""));
        etTahunajaran.setText(sharedPreferences.getString("tahunajaran", ""));
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void getKehadiran() {
        if (Vira.isConnectedToServer(getApplicationContext())) {
            final ProgressDialog loading = ProgressDialog.show(this, "", "Loading Data...", false, false);
            arrayList.clear();
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(com.android.volley.Request.Method.POST, "http://" + ip + "/web_absence/assets/file/rekap_kehadiran_guru.php?dari=" + etDari.getText() + "&ke=" + etKe.getText(), null,
//                    new Response.Listener<JSONArray>() {
//
//                        @Override
//                        public void onResponse(JSONArray response) {
//                            loading.dismiss();
//                            int count = 0;
//                            while (count < response.length()) {
//                                try {
//                                    JSONObject object = response.getJSONObject(count);
//                                    KehadiranGuru kehadiranGuru = new KehadiranGuru();
//                                    kehadiranGuru.setJamAbsen(object.getString("jam_absen"));
//                                    kehadiranGuru.setJenjang(object.getString("jenjang"));
//                                    kehadiranGuru.setKelas(object.getString("kelas"));
//                                    kehadiranGuru.setKeterangan(object.getString("keterangan"));
//                                    kehadiranGuru.setMapel(object.getString("mapel"));
//                                    kehadiranGuru.setNama(object.getString("nama"));
//                                    kehadiranGuru.setStatus(object.getString("status"));
//                                    arrayList.add(kehadiranGuru);
//                                    Log.e("", "Kehadiran Guru" + arrayList);
//                                    count++;
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                            Toast.makeText(RekapKehadiranGuruActivity.this, "Sukses", Toast.LENGTH_SHORT).show();
//                            adapter.notifyDataSetChanged();
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
////                        AlertDialog.Builder builder = new AlertDialog.Builder(DaftarPelajaranActivity.this);
////                        builder.setMessage("Tidak ada jaringan internet...");
////                        builder.setCancelable(false);
////                        builder.setPositiveButton("Muat Ulang", new DialogInterface.OnClickListener() {
////                            @Override
////                            public void onClick(DialogInterface dialog, int which) {
////                                Intent a = new Intent(getIntent());
////                                startActivity(a);
////                            }
////                        });
////                        builder.show();
//                            Toast.makeText(RekapKehadiranGuruActivity.this, String.valueOf(error.networkResponse.statusCode), Toast.LENGTH_SHORT).show();
//                            loading.dismiss();
//                        }
//                    });
//            MySingleton.getmInstance(this).addToRequestque(jsonArrayRequest);

            SharedPreferences sharedPreferences = getSharedPreferences("rekap_kehadiran_guru", Context.MODE_PRIVATE);
            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put("dari", etDari.getText().toString());
            params.put("ke", etKe.getText().toString());
            params.put("id_jenjang", sharedPreferences.getString("id_jenjang", ""));
            params.put("id_ta", sharedPreferences.getString("id_ta", ""));
            params.put("id_guru", sharedPreferences.getString("id_guru", ""));
            Call<List<KehadiranGuru>> call = service.rekapKehadiranGuru(params);
            call.enqueue(new Callback<List<KehadiranGuru>>() {

                @Override
                public void onResponse(Call<List<KehadiranGuru>> call, retrofit2.Response<List<KehadiranGuru>> response) {
                    loading.dismiss();
                    List<KehadiranGuru> listKehadiranGuru = response.body();
                    for (int i = 0; i < listKehadiranGuru.size(); i++) {
                        arrayList.add(listKehadiranGuru.get(i));
                    }
                    Toast.makeText(RekapKehadiranGuruActivity.this, "Sukses", Toast.LENGTH_SHORT).show();
                    adapter.notifyDataSetChanged();

                    buatPdf(listKehadiranGuru);

                }

                @Override
                public void onFailure(Call<List<KehadiranGuru>> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    Toast.makeText(RekapKehadiranGuruActivity.this, t.toString(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
        }
    }

    private void buatPdf(final List<KehadiranGuru> listKehadiranGuru) {
        SessionConfig sessionConfig = new SessionConfig(RekapKehadiranGuruActivity.this);
        final String ip = sessionConfig.preferences.getString("ipconfig",null);

        GetDataService service =
                RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

        HashMap<String, String> param = new HashMap<>();
        param.put("id_guru", "");

        Call<List<SettingOffline>> call = service.syncUpdateTabelSetting(param);
        call.enqueue(new Callback<List<SettingOffline>>() {
            @Override
            public void onResponse(Call<List<SettingOffline>>call, retrofit2.Response<List<SettingOffline>> response) {
//                    loading.dismiss();
                List<SettingOffline> listSettingOffline = response.body();
                SettingOffline setting = new SettingOffline();

                for (SettingOffline settingOffline : listSettingOffline) {
                    setting.setIdSetting(settingOffline.getIdSetting());
                    setting.setLogoMini(settingOffline.getLogoMini());
                    setting.setLogoGambar(settingOffline.getLogoGambar());
                    setting.setLogoFavicon(settingOffline.getLogoFavicon());
                    setting.setNik(settingOffline.getNik());
                    setting.setKartuGsm(settingOffline.getKartuGsm());
                    setting.setCekPulsa(settingOffline.getCekPulsa());
                    setting.setMetode(settingOffline.getMetode());
                    setting.setKodeAktivasi(settingOffline.getKodeAktivasi());
                    setting.setLinkTujuan(settingOffline.getLinkTujuan());
                    setting.setLogoBesar(settingOffline.getLogoBesar());
                    setting.setLogoSekolah(settingOffline.getLogoSekolah());
                    setting.setPimpinan(settingOffline.getPimpinan());
                    setting.setAkunGmail(settingOffline.getAkunGmail());
                    setting.setAlamat(settingOffline.getAlamat());
                    setting.setWebsite(settingOffline.getWebsite());
                    setting.setKopSurat(settingOffline.getKopSurat());
                    setting.setTtd(settingOffline.getTtd());
                    setting.setTelat(settingOffline.getTelat());
                    setting.setToleransiJamAkhir(settingOffline.getToleransiJamAkhir());
                    setting.setLat(settingOffline.getLat());
                    setting.setLng(settingOffline.getLng());
                    setting.setRadius(settingOffline.getRadius());
                    setting.setJamPulang(settingOffline.getJamPulang());
                    setting.setKota(settingOffline.getKota());
                }

                Document document = new Document();

                // Creating an ImageData object
                String imFile = "http://" + ip + "/web_absence/assets/panel/images/" + setting.getKopSurat();

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

                StrictMode.setThreadPolicy(policy);

                Image image = null;
                try {
                    image = Image.getInstance(imFile);
                } catch (BadElementException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                image.scaleAbsolute(document.getPageSize().getWidth() - document.leftMargin(), 150f);

                PdfPTable table = new PdfPTable(new float[] { 3, 3, 1, 1, 1, 1, 1 });
                table.setTotalWidth(document.getPageSize().getWidth() - document.leftMargin() * 2);
                table.setLockedWidth(true);
                table.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell("NAMA");
                table.addCell("MATA PELAJARAN");
                table.addCell("HADIR");
                table.addCell("SAKIT");
                table.addCell("IZIN");
                table.addCell("NIHIL");
                table.addCell("TANPA KET.");
                table.setHeaderRows(1);
                PdfPCell[] cells = table.getRow(0).getCells();
                for (int j=0;j<cells.length;j++){
                    cells[j].setBackgroundColor(BaseColor.GRAY);
                }
                for (KehadiranGuru kehadiranGuru : listKehadiranGuru) {
                    PdfPCell cell = new PdfPCell();
                    Phrase p = new Phrase(kehadiranGuru.getNama());
                    cell.addElement(p);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);

                     cell = new PdfPCell();
                     p = new Phrase(kehadiranGuru.getMapel());
                    cell.addElement(p);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);
                    table.addCell(kehadiranGuru.getHadir());
                    table.addCell(kehadiranGuru.getSakit());
                    table.addCell(kehadiranGuru.getIzin());
                    table.addCell(kehadiranGuru.getTidakAbsen());
                    table.addCell(kehadiranGuru.getTanpaKeterangan());
                }

                SharedPreferences sharedPreferences = getSharedPreferences("rekap_kehadiran_guru", Context.MODE_PRIVATE);
                Calendar calendar = Calendar.getInstance();

                String para = setting.getKota() + ", " + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));
                Paragraph paragraph = new Paragraph (para);
                paragraph.setAlignment(Element.ALIGN_RIGHT);

                para = "                                                                                 Kepala " + setting.getLogoBesar();
                Paragraph paragraph1 = new Paragraph(para);
                paragraph1.setAlignment(Element.ALIGN_RIGHT);

                para = "                                                                                 " + setting.getPimpinan();
                Paragraph paragraph2 = new Paragraph(para);
                paragraph2.setAlignment(Element.ALIGN_RIGHT);

                para = "                                                                                 " + setting.getNik();
                Paragraph paragraph3 = new Paragraph(para);
                paragraph3.setAlignment(Element.ALIGN_RIGHT);

                para = "REKAP KEHADIRAN GURU";
                Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
                Paragraph paragraph4 = new Paragraph(para, boldFont);
                paragraph4.setAlignment(Element.ALIGN_CENTER);

                para = "PERIODE : " + etDari.getText().toString() + " s/d " + etKe.getText().toString();
                Paragraph paragraph5 = new Paragraph(para);

                para = "TAHUN AJARAN : " + sharedPreferences.getString("tahunajaran", "");
                Paragraph paragraph6 = new Paragraph(para);

                para = "JENJANG : " + sharedPreferences.getString("jenjang", "");
                Paragraph paragraph7 = new Paragraph(para);

                para = "GURU : " + sharedPreferences.getString("nama", "");
                Paragraph paragraph8 = new Paragraph(para);

                // write the document content
                String directory_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/eAbsence/";
//                    directory_path = RekapKehadiranGuruActivity.this.getFilesDir() + "/mypdf/";
                File file = new File(directory_path);
                if (!file.exists()) {
                    file.mkdirs();
                }
                String targetPdf = directory_path
                        +String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))
                        + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1)
                        + "-" + String.format("%04d", calendar.get(Calendar.YEAR))
                        + "_Rekap_kehadiran_guru_"
                        + String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY))
                        + String.format("%02d", calendar.get(Calendar.MINUTE))
                        + ".pdf";

                File filePath = new File(targetPdf);
                try {
                    PdfWriter.getInstance(document, new FileOutputStream(filePath));
                } catch (DocumentException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                document.open();
                try {
                    document.add(image);
                    document.add(paragraph4);
                    document.add( Chunk.NEWLINE );
                    document.add(paragraph5);
                    document.add(paragraph6);
                    document.add(paragraph7);
                    document.add(paragraph8);
                    document.add( Chunk.NEWLINE );
                    document.add(table);
                    document.add( Chunk.NEWLINE );
                    document.add( Chunk.NEWLINE );
                    document.add(paragraph);
                    document.add(paragraph1);
                    document.add( Chunk.NEWLINE );
                    document.add( Chunk.NEWLINE );
                    document.add( Chunk.NEWLINE );
                    document.add( Chunk.NEWLINE );
                    document.add(paragraph2);
                    document.add(paragraph3);
                } catch (DocumentException e) {
                    e.printStackTrace();
                }
                document.close();

            }

            @Override
            public void onFailure(Call<List<SettingOffline>>call, Throwable t) {
                // Log error here since request failed
//                    loading.dismiss();
                t.printStackTrace();
            }
        });

    }
}

class RekapKehadiranGuruAdapter extends RecyclerView.Adapter<RekapKehadiranGuruAdapter.MyViewHolder> {

    private List<KehadiranGuru> kehadiranGuruList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nama, jenjang, mapel, hadir, tidakAbsen, sakit, izin, tanpaKeterangan;

        public MyViewHolder(View view) {
            super(view);
            nama = (TextView) view.findViewById(R.id.nama);
            jenjang = (TextView) view.findViewById(R.id.jenjang);
            mapel = view.findViewById(R.id.mapel);
            hadir = view.findViewById(R.id.hadir);
            tidakAbsen = view.findViewById(R.id.tidak_absen);
            sakit = view.findViewById(R.id.sakit);
            izin = view.findViewById(R.id.izin);
            tanpaKeterangan = view.findViewById(R.id.tanpa_keterangan);
        }
    }


    public RekapKehadiranGuruAdapter(List<KehadiranGuru> kehadiranGuruList) {
        this.kehadiranGuruList = kehadiranGuruList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_rekap_kehadiran_guru, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        KehadiranGuru kehadiranGuru = kehadiranGuruList.get(position);
        holder.nama.setText(kehadiranGuru.getNama());
        holder.jenjang.setText(kehadiranGuru.getJenjang());
        holder.mapel.setText(kehadiranGuru.getMapel());
        holder.hadir.setText(kehadiranGuru.getHadir());
        holder.tidakAbsen.setText(kehadiranGuru.getTidakAbsen());
        holder.sakit.setText(kehadiranGuru.getSakit());
        holder.izin.setText(kehadiranGuru.getIzin());
        holder.tanpaKeterangan.setText(kehadiranGuru.getTanpaKeterangan());
    }
    @Override
    public int getItemCount() {
        return kehadiranGuruList.size();
    }


}