package alriyat.absensielektronik.activity;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.KehadiranGuru;
import alriyat.absensielektronik.model.PassSync;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import alriyat.absensielektronik.util.Vira;
import retrofit2.Call;
import retrofit2.Callback;

public class PassSyncActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private SessionConfig sessionConfig;
    private SessionManager sessionManager;
    private PassSyncAdapter adapter;
    ArrayList<PassSync> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pass_sync);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Pass. Sync");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView = findViewById(R.id.recycler_view);

        sessionConfig = new SessionConfig(this);
        sessionManager = new SessionManager(this);

        adapter = new PassSyncAdapter(arrayList);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        getPassSync();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void getPassSync() {
        if (Vira.isConnectedToServer(getApplicationContext())) {
//            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            final ProgressDialog loading = ProgressDialog.show(this, "", "Loading Data...", false, false);
            arrayList.clear();
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(com.android.volley.Request.Method.POST, "http://" + ip + "/web_absence/assets/file/rekap_kehadiran_guru.php?dari=" + etDari.getText() + "&ke=" + etKe.getText(), null,
//                    new Response.Listener<JSONArray>() {
//
//                        @Override
//                        public void onResponse(JSONArray response) {
//                            loading.dismiss();
//                            int count = 0;
//                            while (count < response.length()) {
//                                try {
//                                    JSONObject object = response.getJSONObject(count);
//                                    KehadiranGuru kehadiranGuru = new KehadiranGuru();
//                                    kehadiranGuru.setJamAbsen(object.getString("jam_absen"));
//                                    kehadiranGuru.setJenjang(object.getString("jenjang"));
//                                    kehadiranGuru.setKelas(object.getString("kelas"));
//                                    kehadiranGuru.setKeterangan(object.getString("keterangan"));
//                                    kehadiranGuru.setMapel(object.getString("mapel"));
//                                    kehadiranGuru.setNama(object.getString("nama"));
//                                    kehadiranGuru.setStatus(object.getString("status"));
//                                    arrayList.add(kehadiranGuru);
//                                    Log.e("", "Kehadiran Guru" + arrayList);
//                                    count++;
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                            Toast.makeText(RekapKehadiranGuruActivity.this, "Sukses", Toast.LENGTH_SHORT).show();
//                            adapter.notifyDataSetChanged();
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
////                        AlertDialog.Builder builder = new AlertDialog.Builder(DaftarPelajaranActivity.this);
////                        builder.setMessage("Tidak ada jaringan internet...");
////                        builder.setCancelable(false);
////                        builder.setPositiveButton("Muat Ulang", new DialogInterface.OnClickListener() {
////                            @Override
////                            public void onClick(DialogInterface dialog, int which) {
////                                Intent a = new Intent(getIntent());
////                                startActivity(a);
////                            }
////                        });
////                        builder.show();
//                            Toast.makeText(RekapKehadiranGuruActivity.this, String.valueOf(error.networkResponse.statusCode), Toast.LENGTH_SHORT).show();
//                            loading.dismiss();
//                        }
//                    });
//            MySingleton.getmInstance(this).addToRequestque(jsonArrayRequest);
//
            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Call<List<PassSync>> call = service.passSync();
            call.enqueue(new Callback<List<PassSync>>() {
//                @Override
//                public void onResponse(Call<JSONArray> call, Response<JSONArray> response) {
//
//                }

//                @Override
//                public void onResponse(Call<Query> call, Response<Query> response) {
//                    syncAmbilTabelSiswakelas(context, idGuru);
//                }

                @Override
                public void onResponse(Call<List<PassSync>> call, retrofit2.Response<List<PassSync>> response) {
                    loading.dismiss();
                    List<PassSync> listPassSync = response.body();
                    for (int i = 0; i < listPassSync.size(); i++) {
                        arrayList.add(listPassSync.get(i));
                    }
                    Toast.makeText(PassSyncActivity.this, "Sukses", Toast.LENGTH_SHORT).show();
                    adapter.notifyDataSetChanged();
                }

                @Override
                public void onFailure(Call<List<PassSync>> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    Toast.makeText(PassSyncActivity.this, t.toString(), Toast.LENGTH_LONG).show();
                }
            });
        } else {
        }
    }
}
class PassSyncAdapter extends RecyclerView.Adapter<PassSyncAdapter.MyViewHolder> {

    private List<PassSync> passSyncList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nik, nama, passSync;

        public MyViewHolder(View view) {
            super(view);
            nik = view.findViewById(R.id.nik);
            nama = (TextView) view.findViewById(R.id.nama);
            passSync = view.findViewById(R.id.pass_sync);
        }
    }


    public PassSyncAdapter(List<PassSync> passSyncList) {
        this.passSyncList = passSyncList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_pass_sync, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        PassSync passSync = passSyncList.get(position);
        holder.nik.setText(passSync.getNik());
        holder.nama.setText(passSync.getNama());
        holder.passSync.setText(passSync.getPassSync());
    }
    @Override
    public int getItemCount() {
        return passSyncList.size();
    }
}
