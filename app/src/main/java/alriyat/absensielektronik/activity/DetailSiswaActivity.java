package alriyat.absensielektronik.activity;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import alriyat.absensielektronik.app.MySingleton;
import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.AmbilSiswa;
import alriyat.absensielektronik.model.HapusSiswa;
import alriyat.absensielektronik.util.DatabaseHandler;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import alriyat.absensielektronik.util.Vira;
import retrofit2.Call;
import retrofit2.Callback;

public class DetailSiswaActivity extends AppCompatActivity {

    private TextView tvNisn,tvNama,tvTgl,tvAgama,tvAlamat,tvJk,tvTempat,tvAyah,tvIbu,tvKelas,tvWaliKelas;
    private ImageView gambar;
    private Button bRekapKehadiran;
    private String idnews, idkelas;
    private SessionConfig sessionConfig;
    private Boolean statusWaliKelas = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_siswa);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Detail Siswa");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        idnews = getIntent().getStringExtra("idnews");
        idkelas = getIntent().getStringExtra("idkelas");
        sessionConfig = new SessionConfig(this);
        tvNisn = (TextView) findViewById(R.id.nisn);
        tvNama = (TextView) findViewById(R.id.nama_lengkap);
        tvTgl = (TextView) findViewById(R.id.tgl_lahir);
        tvAgama = (TextView) findViewById(R.id.agama);
        tvAlamat = (TextView) findViewById(R.id.alamat);
        tvAyah = (TextView) findViewById(R.id.ayah);
        tvIbu = (TextView) findViewById(R.id.ibu);
        tvTempat = (TextView)findViewById(R.id.tempat_lahir);
        tvJk = (TextView)findViewById(R.id.jko) ;
        tvKelas = (TextView)findViewById(R.id.kelas) ;
        tvWaliKelas = (TextView)findViewById(R.id.wali_kelas) ;
        gambar = (ImageView) findViewById(R.id.gambar);
        bRekapKehadiran = findViewById(R.id.rekap_kehadiran);

        cekStatusWaliKelas();
        ambilDetail(idnews);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        getMenuInflater().inflate(R.menu.menu_delete, menu);
        return true;
    }

    @Override
    protected void onResume() {
        cekStatusWaliKelas();
        ambilDetail(idnews);
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                goToBack();
                break;
            case R.id.action_edit:
                SessionManager sessionManager = new SessionManager(DetailSiswaActivity.this);
                String statusGuruBK = sessionManager.preferences.getString("statusGuruBK", null);
                if (statusGuruBK.equals("1") || statusWaliKelas) {
                    Intent n = new Intent(DetailSiswaActivity.this, EditSiswaActivity.class);
                    n.putExtra("idnews", idnews);
                    n.putExtra("idkelas", idkelas);
                    startActivity(n);
                }
                break;
            case R.id.action_delete:
                sessionManager = new SessionManager(DetailSiswaActivity.this);
                statusGuruBK = sessionManager.preferences.getString("statusGuruBK", null);
                if (statusGuruBK.equals("1") || statusWaliKelas) {
                    GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

                    Map<String, String> param = new HashMap<>();
                    param.put("id_siswa", idnews);

                    Call<HapusSiswa> call = service.hapusSiswa(param);
                    call.enqueue(new Callback<HapusSiswa>() {
                        @Override
                        public void onResponse(Call<HapusSiswa> call, retrofit2.Response<HapusSiswa> response) {
                            HapusSiswa hapusSiswa = response.body();

                            Toast.makeText(DetailSiswaActivity.this, hapusSiswa.getResponse(), Toast.LENGTH_SHORT).show();
                            goToBack();
                        }

                        @Override
                        public void onFailure(Call<HapusSiswa> call, Throwable t) {

                        }
                    });
                }
                break;

        }
        return super.onOptionsItemSelected(item);
    }
    private void goToBack(){
        super.onBackPressed();
    }

    private void cekStatusWaliKelas(){
        final SessionManager sessionManager = new SessionManager(DetailSiswaActivity.this);
        if (Vira.isConnectedToServer(DetailSiswaActivity.this)) {
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            final ProgressDialog loading = ProgressDialog.show(this, "", "Loading Data...", false, false);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("idguru", sessionManager.preferences.getString("username", null));
            param.put("idkelas", idkelas);

            Call<String> call = service.cekStatusWaliKelas(param);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String>call, retrofit2.Response<String> response) {
                    loading.dismiss();
                    if (response.body().equals("ya")) {
                        statusWaliKelas = true;
                    }
                    else {
                        statusWaliKelas = false;
                    }
                }

                @Override
                public void onFailure(Call<String>call, Throwable t) {
                    // Log error here since request failed
                    loading.dismiss();
                    DatabaseHandler dh = new DatabaseHandler(DetailSiswaActivity.this);
                    statusWaliKelas = dh.cekStatusWaliKelas(DetailSiswaActivity.this, sessionManager.preferences.getString("username", null), idkelas);
                }
            });

//            StringRequest request = new StringRequest(Request.Method.POST, "http://" + ip + "/web_absence/assets/file/cek_status_wali_kelas.php?idguru=" + sessionManager.preferences.getString("username", null) + "&idkelas=" + idkelas,
//                    new Response.Listener<String>() {
//                        @Override
//                        public void onResponse(String response) {
//                            loading.dismiss();
//                            if (response.equals("ya")) {
//                                statusWaliKelas = true;
//                            }
//                            else {
//                                statusWaliKelas = false;
//                            }
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
//                            loading.dismiss();
//                            DatabaseHandler dh = new DatabaseHandler(DetailSiswaActivity.this);
//                            statusWaliKelas = dh.cekStatusWaliKelas(DetailSiswaActivity.this, sessionManager.preferences.getString("username", null), idkelas);
//                        }
//                    }) {
//                @Override
//                protected Map<String, String> getParams() throws AuthFailureError {
//                    //final String idpenm = idpenempatan.getText().toString().trim();
//                    //final String ketabsen = spinner.getSelectedItem().toString().trim();
//                    Map<String, String> params = new HashMap<>();
//                    params.put("idpenem", "");
//                    params.put("absen", "");
//                    return params;
//                }
//            };
//            MySingleton.getmInstance(DetailSiswaActivity.this).addToRequestque(request);
        }
        else {
            DatabaseHandler dh = new DatabaseHandler(DetailSiswaActivity.this);
            statusWaliKelas = dh.cekStatusWaliKelas(DetailSiswaActivity.this, sessionManager.preferences.getString("username", null), idkelas);
        }
    }

    private void ambilDetail(String idsiswa){
        final String ip = sessionConfig.preferences.getString("ipconfig",null);
        final ProgressDialog loading = ProgressDialog.show(this,"","Loading Data...",false,false);

        GetDataService service =
                RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

        HashMap<String, String> param = new HashMap<>();
        param.put("id", idsiswa);

        Call<AmbilSiswa> call = service.ambilSiswa(param);
        call.enqueue(new Callback<AmbilSiswa>() {
            @Override
            public void onResponse(Call<AmbilSiswa>call, retrofit2.Response<AmbilSiswa> response) {
                loading.dismiss();
                Log.e("Error",""+response);

                final AmbilSiswa ambilSiswa = response.body();

                    tvNisn.setText(ambilSiswa.getNisn());
                    tvNama.setText(ambilSiswa.getNama());
                    tvTgl.setText(ambilSiswa.getTgl());
                    tvAlamat.setText(ambilSiswa.getAlamat());
                    tvTgl.setText(ambilSiswa.getTgl());
                    tvAyah.setText(ambilSiswa.getAyah());
                    tvIbu.setText(ambilSiswa.getIbu());
                    tvAgama.setText(ambilSiswa.getAgama());
                    tvTempat.setText(ambilSiswa.getTempat());
                    tvJk.setText(ambilSiswa.getJk());
                    tvKelas.setText(ambilSiswa.getKelas());
                    tvWaliKelas.setText(ambilSiswa.getWaliKelas());

                    Picasso.with(DetailSiswaActivity.this)
                            .load("http://" + ip + "/web_absence/assets/panel/images/" + ambilSiswa.getGambar())
                            .placeholder(R.drawable.siswa).memoryPolicy(MemoryPolicy.NO_CACHE)
                            .into(gambar);

                    SessionManager sessionManager = new SessionManager(DetailSiswaActivity.this);
                    String statusGuruBK = sessionManager.preferences.getString("statusGuruBK", "0");

                    if (statusGuruBK.equals("1") || statusWaliKelas) {
                        bRekapKehadiran.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent n = new Intent(DetailSiswaActivity.this, RekapKehadiranSiswaActivity.class);
                                n.putExtra("nisn", ambilSiswa.getNisn());
                                n.putExtra("kelas", ambilSiswa.getKelas());
                                n.putExtra("nama", ambilSiswa.getNama());
                                startActivity(n);
                            }
                        });
                    }


            }

            @Override
            public void onFailure(Call<AmbilSiswa>call, Throwable t) {
                // Log error here since request failed
                loading.dismiss();
                t.printStackTrace();
                Toast.makeText(DetailSiswaActivity.this, "Failed, check internet connection...", Toast.LENGTH_SHORT).show();
            }
        });

//        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.GET, "http://"+ip+"/web_absence/assets/file/ambil_siswa.php?id="+idsiswa, null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        loading.dismiss();
//                        Log.e("Error",""+response);
//                        try {
//
//                            final String nisn = response.getString("nisn");
//                            String nama = response.getString("nama");
//                            String jk = response.getString("jk");
//                            String tempat = response.getString("tempat");
//                            String tgl = response.getString("tgl");
//                            String alamt = response.getString("alamat");
//                            String hp = response.getString("hp");
//                            String email = response.getString("email");
//                            String ayah = response.getString("ayah");
//                            String ibu = response.getString("ibu");
//                            String agama = response.getString("agama");
//                            final String kelas = response.getString("kelas");
//                            String waliKelas = response.getString("wali_kelas");
//
//                            tvNisn.setText(nisn);
//                            tvNama.setText(nama);
//                            tvTgl.setText(tgl);
//                            tvAlamat.setText(alamt);
//                            tvTgl.setText(tgl);
//                            tvAyah.setText(ayah);
//                            tvIbu.setText(ibu);
//                            tvAgama.setText(agama);
//                            tvTempat.setText(tempat);
//                            tvJk.setText(jk);
//                            tvKelas.setText(kelas);
//                            tvWaliKelas.setText(waliKelas);
//
//                            Picasso.with(DetailSiswaActivity.this)
//                                    .load("http://" + ip + "/web_absence/assets/panel/images/" + response.getString("gambar"))
//                                    .placeholder(R.drawable.siswa).memoryPolicy(MemoryPolicy.NO_CACHE)
//                                    .into(gambar);
//
//                            SessionManager sessionManager = new SessionManager(DetailSiswaActivity.this);
//                            String statusGuruBK = sessionManager.preferences.getString("statusGuruBK", "0");
//
//                            if (statusGuruBK.equals("1") || statusWaliKelas) {
//                                bRekapKehadiran.setOnClickListener(new View.OnClickListener() {
//                                    @Override
//                                    public void onClick(View v) {
//                                        Intent n = new Intent(DetailSiswaActivity.this, RekapKehadiranSiswaActivity.class);
//                                        n.putExtra("nisn", nisn);
//                                        n.putExtra("kelas", kelas);
//                                        startActivity(n);
//                                    }
//                                });
//                            }
//
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//                        }
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        loading.dismiss();
//                        Log.e("Error",""+error);
//                        Toast.makeText(DetailSiswaActivity.this, "Failed, check internet connection...", Toast.LENGTH_SHORT).show();
//                    }
//                });
//
//        MySingleton.getmInstance(DetailSiswaActivity.this).addToRequestque(objectRequest);

    }
}
