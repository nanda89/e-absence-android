package alriyat.absensielektronik.activity;


import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.SettingOffline;
import alriyat.absensielektronik.pojo.LapAbsensiHarian;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import alriyat.absensielektronik.util.Vira;
import retrofit2.Call;
import retrofit2.Callback;

public class RekapKelasHariActivity extends AppCompatActivity {

    private static final String TAG = RekapKelasHariActivity.class.getName();
    private EditText tglRekap;
    private EditText etJenjang;
    private EditText etTahunajaran;
    private EditText etKelas;
    private Button bFilter,btnRekapKelasHari;
    private RecyclerView recyclerView;
    private SessionConfig sessionConfig;
    private SessionManager sessionManager;
    private String id_guru;

    private RekapKelasHariAdapter adapter;
    ArrayList<LapAbsensiHarian> arrayList= new ArrayList<>();
    SharedPreferences sharedPreferences ,generalPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rekap_kelas_hari);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_rekap_mengajar);
        toolbar.setTitle("Rekap Kelas Harian");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        sharedPreferences = this.getSharedPreferences("rekap_kehadiran_guru", Context.MODE_PRIVATE);
        generalPreferences = this.getSharedPreferences("absensiElek", Context.MODE_PRIVATE);

        Log.d(TAG, "onCreate: username : "+generalPreferences.getString("username",""));

        init();
        setDatePicker();
        getData();

        bFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tgl = tglRekap.getText().toString();
                String jenjang = etJenjang.getText().toString();
                String tahunajaran = etTahunajaran.getText().toString();
                String kelas = etKelas.getText().toString();

                if (tgl.isEmpty()) {
                    Toast.makeText(RekapKelasHariActivity.this, "Tanggal tidak boleh kosong", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (jenjang.isEmpty()) {
                    Toast.makeText(RekapKelasHariActivity.this, "Jenjang tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (tahunajaran.isEmpty()) {
                    Toast.makeText(RekapKelasHariActivity.this, "Tahun ajaran tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (kelas.isEmpty()) {
                    Toast.makeText(RekapKelasHariActivity.this, "Kelas tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                    return;
                }

                getRekap();
            }
        });

        adapter = new RekapKelasHariAdapter(arrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onResume() {

        if (Vira.isConnectedToServer(getApplicationContext())) {
            bFilter.setClickable(true);
        }else{
            bFilter.setClickable(false);
            Toast.makeText(this, "Anda tidak terhubung ke server", Toast.LENGTH_LONG).show();
        }

        etJenjang.setText(sharedPreferences.getString("jenjang", ""));
        etTahunajaran.setText(sharedPreferences.getString("tahunajaran", ""));
        etKelas.setText(sharedPreferences.getString("kelas", ""));
        super.onResume();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void init(){
        tglRekap = findViewById(R.id.tgl_rekap_kelas);
        etJenjang = findViewById(R.id.jenjang_kelas_harian);
        etTahunajaran = findViewById(R.id.tahun_ajaran_kelas_harian);
        etKelas = findViewById(R.id.kelas_rekap_hari);
        bFilter = findViewById(R.id.bt_filter_rekap_kelas_hari);
        btnRekapKelasHari = findViewById(R.id.btn_view_rekap_kelas_hari);
        recyclerView = findViewById(R.id.recycler_view_rekap_kelas_hari);

        sessionConfig = new SessionConfig(this);
        sessionManager = new SessionManager(this);
        id_guru = sessionManager.preferences.getString("username", null);
    }

    private void setDatePicker() {
        final Calendar tgl = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener dateDari = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                tgl.set(Calendar.YEAR, year);
                tgl.set(Calendar.MONTH, monthOfYear);
                tgl.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                tglRekap.setText(String.format("%02d", tgl.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", tgl.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", tgl.get(Calendar.YEAR)));
            }
        };
        tglRekap.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(RekapKelasHariActivity.this, dateDari, tgl
                        .get(Calendar.YEAR), tgl.get(Calendar.MONTH),
                        tgl.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

    }

    private void getData() {
        etJenjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RekapKelasHariActivity.this, PilihJenjangActivity.class);
                startActivity(intent);
            }
        });

        etTahunajaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RekapKelasHariActivity.this, PilihTahunajaranActivity.class);
                startActivity(intent);
            }
        });

        etKelas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RekapKelasHariActivity.this, PilihKelasByWaliKelasActivity.class);
                startActivity(intent);
            }
        });
    }

    private void getRekap(){
        if (Vira.isConnectedToServer(getApplicationContext())) {
            final ProgressDialog loading = ProgressDialog.show(this, "", "Loading Data...", false, false);
            arrayList.clear();

            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put("tgl", tglRekap.getText().toString());
            params.put("id_jenjang", sharedPreferences.getString("id_jenjang",""));
            params.put("id_ta", sharedPreferences.getString("id_ta",""));
            params.put("id_kelas", sharedPreferences.getString("id_kelas",""));
            params.put("id_guru", id_guru);


            Call<List<LapAbsensiHarian>> call = service.getLapAbsensiHarian(params);
            call.enqueue(new Callback<List<LapAbsensiHarian>>() {

                @Override
                public void onResponse(Call<List<LapAbsensiHarian>> call, retrofit2.Response<List<LapAbsensiHarian>> response) {
                    loading.dismiss();
                    Log.d(TAG, "onResponse: "+response.body());
                    final List<LapAbsensiHarian> rekap = response.body();
                    Gson gson = new Gson();
                    Log.d(TAG, "onResponse: response : "+gson.toJson(rekap));
                    if(rekap.size() >0){
                        for(LapAbsensiHarian harian : rekap){
                            arrayList.add(harian);
                        }
                        btnRekapKelasHari.setVisibility(View.VISIBLE);
                        btnRekapKelasHari.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String ta = etTahunajaran.getText().toString();
                                String jenjang = etJenjang.getText().toString();
                                Toast.makeText(RekapKelasHariActivity.this, "generate pdf file", Toast.LENGTH_SHORT).show();
                                buatPdf(rekap,ta,jenjang);
                            }
                        });
                        adapter.notifyDataSetChanged();

                    }else{
                        btnRekapKelasHari.setVisibility(View.GONE);
                        Toast.makeText(RekapKelasHariActivity.this, "Data tidak ditemukan", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<List<LapAbsensiHarian>> call, Throwable t) {
                    loading.dismiss();
                }
            });

        }else{
            Toast.makeText(this, "Pencarian gagal, anda tidak terhubung dengan server", Toast.LENGTH_SHORT).show();
        }
    }
    private void buatPdf(final List<LapAbsensiHarian> rekapData, final String ta, final String jenjang) {
        SessionConfig sessionConfig = new SessionConfig(RekapKelasHariActivity.this);
        final String ip = sessionConfig.preferences.getString("ipconfig",null);

        GetDataService service =
                RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

        HashMap<String, String> param = new HashMap<>();
        param.put("id_guru", "");

        Call<List<SettingOffline>> call = service.syncUpdateTabelSetting(param);
        call.enqueue(new Callback<List<SettingOffline>>() {
            @Override
            public void onResponse(Call<List<SettingOffline>>call, retrofit2.Response<List<SettingOffline>> response) {
//                    loading.dismiss();
                List<SettingOffline> listSettingOffline = response.body();
                SettingOffline setting = new SettingOffline();

                for (SettingOffline settingOffline : listSettingOffline) {
                    setting.setIdSetting(settingOffline.getIdSetting());
                    setting.setLogoMini(settingOffline.getLogoMini());
                    setting.setLogoGambar(settingOffline.getLogoGambar());
                    setting.setLogoFavicon(settingOffline.getLogoFavicon());
                    setting.setNik(settingOffline.getNik());
                    setting.setKartuGsm(settingOffline.getKartuGsm());
                    setting.setCekPulsa(settingOffline.getCekPulsa());
                    setting.setMetode(settingOffline.getMetode());
                    setting.setKodeAktivasi(settingOffline.getKodeAktivasi());
                    setting.setLinkTujuan(settingOffline.getLinkTujuan());
                    setting.setLogoBesar(settingOffline.getLogoBesar());
                    setting.setLogoSekolah(settingOffline.getLogoSekolah());
                    setting.setPimpinan(settingOffline.getPimpinan());
                    setting.setAkunGmail(settingOffline.getAkunGmail());
                    setting.setAlamat(settingOffline.getAlamat());
                    setting.setWebsite(settingOffline.getWebsite());
                    setting.setKopSurat(settingOffline.getKopSurat());
                    setting.setTtd(settingOffline.getTtd());
                    setting.setTelat(settingOffline.getTelat());
                    setting.setToleransiJamAkhir(settingOffline.getToleransiJamAkhir());
                    setting.setLat(settingOffline.getLat());
                    setting.setLng(settingOffline.getLng());
                    setting.setRadius(settingOffline.getRadius());
                    setting.setJamPulang(settingOffline.getJamPulang());
                    setting.setKota(settingOffline.getKota());
                }

                Document document = new Document();

                // Creating an ImageData object
                String imFile = "http://" + ip + "/web_absence/assets/panel/images/" + setting.getKopSurat();

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

                StrictMode.setThreadPolicy(policy);

                Image image = null;
                try {
                    image = Image.getInstance(imFile);
                } catch (BadElementException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                image.scaleAbsolute(document.getPageSize().getWidth() - document.leftMargin(), 150f);

                PdfPTable table = new PdfPTable(new float[] { 3,3, 1, 1, 1, 1, 1 });
                table.setTotalWidth(document.getPageSize().getWidth() - document.leftMargin() * 2);
                table.setLockedWidth(true);
                table.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell("TANGGAL");
                table.addCell("MATA PELAJARAN");
                table.addCell("HADIR");
                table.addCell("SAKIT");
                table.addCell("IZIN");
                table.addCell("NIHIL");
                table.addCell("TANPA KET.");
                table.setHeaderRows(1);
                PdfPCell[] cells = table.getRow(0).getCells();
                for (int j=0;j<cells.length;j++){
                    cells[j].setBackgroundColor(BaseColor.GRAY);
                }
                for (LapAbsensiHarian rekap : rekapData) {
                    PdfPCell cell = new PdfPCell();
                    Phrase p = new Phrase(tglRekap.getText().toString());
                    cell.addElement(p);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);

                    cell = new PdfPCell();
                    p = new Phrase(String.valueOf(rekap.getMapel()));
                    cell.addElement(p);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);
                    table.addCell(String.valueOf(rekap.getHadir()));
                    table.addCell(String.valueOf(rekap.getSakit()));
                    table.addCell(String.valueOf(rekap.getIzin()));
                    table.addCell(String.valueOf(rekap.getNihil()));
                    table.addCell("-");
                }

                Calendar calendar = Calendar.getInstance();

                String para = setting.getKota() + ", " + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));
                Paragraph paragraph = new Paragraph (para);
                paragraph.setAlignment(Element.ALIGN_RIGHT);

                para = "                                                                                 Kepala " + setting.getLogoBesar();
                Paragraph paragraph1 = new Paragraph(para);
                paragraph1.setAlignment(Element.ALIGN_RIGHT);

                para = "                                                                                 " + setting.getPimpinan();
                Paragraph paragraph2 = new Paragraph(para);
                paragraph2.setAlignment(Element.ALIGN_RIGHT);

                para = "                                                                                 " + setting.getNik();
                Paragraph paragraph3 = new Paragraph(para);
                paragraph3.setAlignment(Element.ALIGN_RIGHT);

                para = "REKAPITULASI ABSESI HARIAN";
                Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
                Paragraph paragraph4 = new Paragraph(para, boldFont);
                paragraph4.setAlignment(Element.ALIGN_CENTER);

                para = "Hari/Tanggal    : "+getDay()+", " + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));
                Paragraph paragraph5 = new Paragraph(para);

                para = "Tahun Ajaran    : "+ta;
                Paragraph paragraph6 = new Paragraph(para);

                para = "Jenjang         : "+jenjang;
                Paragraph paragraph7 = new Paragraph(para);

                SharedPreferences sharedPreferences = getSharedPreferences("absensiElek", Context.MODE_PRIVATE);
//
                para = "Guru            : "+sharedPreferences.getString("name","");
                Paragraph paragraph8 = new Paragraph(para);
//
                para = "Rekap           : "+tglRekap.getText();
                Paragraph paragraph9 = new Paragraph(para);


                // write the document content
                String directory_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/eAbsence/";
//                    directory_path = RekapKehadiranGuruActivity.this.getFilesDir() + "/mypdf/";
                File file = new File(directory_path);
                if (!file.exists()) {
                    file.mkdirs();
                }
                String targetPdf = directory_path
                        +String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))
                        + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1)
                        + "-" + String.format("%04d", calendar.get(Calendar.YEAR))
                        + "_Rekap_kehadiran_guru_"
                        + String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY))
                        + String.format("%02d", calendar.get(Calendar.MINUTE))
                        + ".pdf";
                File filePath = new File(targetPdf);
                try {
                    PdfWriter.getInstance(document, new FileOutputStream(filePath));
                } catch (DocumentException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                document.open();
                try {
                    document.add(image);
                    document.add(paragraph4);
                    document.add( Chunk.NEWLINE );
                    document.add(paragraph5);
                    document.add(paragraph6);
                    document.add(paragraph7);
//                    document.add(paragraph8);
//                    document.add(paragraph9);
                    document.add( Chunk.NEWLINE );
                    document.add(table);
                    document.add( Chunk.NEWLINE );
                    document.add( Chunk.NEWLINE );
                    document.add(paragraph);
                    document.add(paragraph1);
                    document.add( Chunk.NEWLINE );
                    document.add( Chunk.NEWLINE );
                    document.add( Chunk.NEWLINE );
                    document.add( Chunk.NEWLINE );
                    document.add(paragraph2);
                    document.add(paragraph3);
                } catch (DocumentException e) {
                    e.printStackTrace();
                }
                document.close();
                showData(targetPdf);
            }

            @Override
            public void onFailure(Call<List<SettingOffline>>call, Throwable t) {
                // Log error here since request failed
//                    loading.dismiss();
                t.printStackTrace();
            }
        });

    }
    private void showData(String path) {
        File file = new File(path);
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(file),"application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent intent = Intent.createChooser(target , "Open File");
        try {
            startActivity(intent);
        }catch (ActivityNotFoundException e){
            e.printStackTrace();
        }
    }
    private String getDay(){
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);

        String result = null;

        switch (day) {
            case Calendar.SUNDAY:
                result = "Minggu";
                break;
            case Calendar.MONDAY:
                result = "Senin";
                break;
            case Calendar.TUESDAY:
                result = "Selasa";
                break;
            case Calendar.WEDNESDAY:
                result = "Rabu";
                break;
            case Calendar.THURSDAY:
                result = "Kamis";
                break;
            case Calendar.FRIDAY:
                result = "Jumat";
                break;
            case Calendar.SATURDAY:
                result = "Sabtu";
                break;
        }
        return result;
    }
}
class RekapKelasHariAdapter extends RecyclerView.Adapter<RekapKelasHariAdapter.MyViewHolder>{

    private List<LapAbsensiHarian>lapHarian;

    public RekapKelasHariAdapter(List<LapAbsensiHarian>lapHarian){
        this.lapHarian = lapHarian;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView lblMapel , lblHadir,lblSakit,lblTK,lblNihil,lblIzin;
        public MyViewHolder(View view) {
            super(view);
            lblMapel = view.findViewById(R.id.val_mapel_rekap_mengajar);
            lblHadir = view.findViewById(R.id.hadir_rekap_mengajar);
            lblSakit = view.findViewById(R.id.sakit_rekap_mengajar);
            lblIzin = view.findViewById(R.id.izin_rekap_mengajar);
            lblNihil = view.findViewById(R.id.nihil_rekap_mengajar);
            lblTK = view.findViewById(R.id.tk_rekap_mengajar);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_rekap_mengajar_guru, parent, false);

        return new RekapKelasHariAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RekapKelasHariAdapter.MyViewHolder holder, int position) {
        LapAbsensiHarian rekapGuru = lapHarian.get(position);
        holder.lblMapel.setText(rekapGuru.getMapel());
        holder.lblHadir.setText(String.valueOf(rekapGuru.getHadir()));
        holder.lblSakit.setText(String.valueOf(rekapGuru.getSakit()));
        holder.lblIzin.setText(String.valueOf(rekapGuru.getIzin()));
        holder.lblNihil.setText(String.valueOf(rekapGuru.getNihil()));
//        holder.lblTK.setText(String.valueOf(rekapGuru.getTk()));
    }

    @Override
    public int getItemCount() {
        return lapHarian.size();
    }
}