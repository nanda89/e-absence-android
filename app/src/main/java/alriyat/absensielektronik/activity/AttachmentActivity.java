package alriyat.absensielektronik.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Objects;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.pojo.GeneralResponse;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import alriyat.absensielektronik.util.Vira;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AttachmentActivity extends AppCompatActivity {

    private static final String TAG = AttachmentActivity.class.getName();
    private SessionConfig sessionConfig;
    private SessionManager sessionManager;
    private EditText etMateri, etKeterangan;
    private ImageButton btnRecMateri, btnRecKeterangan;

    private int idJadwal;
    private String jenis;

    public static final int REQUEST_PICK_VIDEO = 3;
    // Current playback position (in milliseconds).
    private int mCurrentPosition = 0;

    // Tag for the instance state bundle.
    private static final String PLAYBACK_TIME = "play_time";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attachment);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_attachment);
        toolbar.setTitle("Unggah Materi");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etMateri = findViewById(R.id.et_materi);
        etKeterangan = findViewById(R.id.et_keterangan);

        btnRecMateri = findViewById(R.id.btnMateri);
        btnRecKeterangan = findViewById(R.id.btnKeterangan);

        Intent intent = getIntent();
        idJadwal = intent.getIntExtra("jadwalId", 0);
        jenis = intent.getStringExtra("jenis");

        Log.d(TAG, "onCreate: data from intent jadwal id : " + idJadwal + " ,jenis : " + jenis);

        sessionConfig = new SessionConfig(this);
        sessionManager = new SessionManager(this);

        btnRecMateri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, 10);
                } else {
                    Toast.makeText(AttachmentActivity.this, "Handphone anda tidak kompatibel dengan fitur ini", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btnRecKeterangan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, 11);
                } else {
                    Toast.makeText(AttachmentActivity.this, "Handphone anda tidak kompatibel dengan fitur ini", Toast.LENGTH_SHORT).show();
                }
            }
        });


//        initDialog();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.attachment, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_upload) {
            if (checkForm()) {
                if (Vira.isConnectedToServer(AttachmentActivity.this)) {
                    uploadFile();
                } else {
                    Toast.makeText(this, "Anda tidak terhubung ke server, harap cek koneksi !", Toast.LENGTH_SHORT).show();
                }
            } else {
                Toast.makeText(this, "Judul dan Deskripsi tidak boleh kosong !", Toast.LENGTH_SHORT).show();
            }
        }
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 10:
                if(resultCode == RESULT_OK && data != null){
                    ArrayList<String>result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    etMateri.setText(result.get(0));
                }
                break;
            case 11:
                if(resultCode == RESULT_OK && data != null){
                    ArrayList<String>result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    etKeterangan.setText(result.get(0));
                }
                break;
        }
    }
//    private void uploadFile() {
//        showpDialog();
//
//        final String ip = sessionConfig.preferences.getString("ipconfig", null);
//        GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
//
//        Map<String, RequestBody> map = new HashMap<>();
//        Log.d(TAG, "uploadFile: videoPath : " + videoPath);
//        File file = new File(videoPath);
//        RequestBody requestBody = RequestBody
//                .create(MediaType.parse("*/*"), file);
//        map.put("file\"; filename=\"" + file.getName() + "\"", requestBody);
//
//        requestBody = RequestBody.create(MediaType.parse("text/plain"), etJudulMateri.getText().toString());
//        map.put("judul", requestBody);
//
//        requestBody = RequestBody.create(MediaType.parse("text/plain"), etDeskripsiMateri.getText().toString());
//        map.put("deskripsi", requestBody);
//
//        requestBody = RequestBody.create(MediaType.parse("text/plain"), Objects.requireNonNull(sessionManager.preferences.getString("username", null)));
//        map.put("id_guru", requestBody);
//
//        requestBody = RequestBody.create(MediaType.parse("text/plain"), jenis);
//        map.put("jenis", requestBody);
//
//        requestBody = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(idJadwal));
//        map.put("id_jadwal", requestBody);
//
//        SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
//        String tanggal = fmt.format(new Date());
//
//        requestBody = RequestBody.create(MediaType.parse("text/plain"), tanggal);
//        map.put("tanggal", requestBody);
//
//        Call<ServerResponse> call = service.attachment("token", map);
//
//        call.enqueue(new Callback<ServerResponse>() {
//            @Override
//            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {
//                if (response.isSuccessful()) {
//                    if (response.body() != null) {
//                        hidepDialog();
//                        ServerResponse serverResponse = response.body();
//                        Toast.makeText(getApplicationContext(), serverResponse.getMessage(), Toast.LENGTH_SHORT).show();
//
//                    }
//                } else {
//                    hidepDialog();
//                    Toast.makeText(getApplicationContext(), "problem uploading image", Toast.LENGTH_SHORT).show();
//                }
//            }
//
//            @Override
//            public void onFailure(Call<ServerResponse> call, Throwable t) {
//                hidepDialog();
//                Log.v("Response gotten is", t.getMessage());
//                Toast.makeText(getApplicationContext(), "problem uploading image " + t.getMessage(), Toast.LENGTH_SHORT).show();
//
//            }
//        });
//    }
    private boolean checkForm() {

        String materi = etMateri.getText().toString();
        String keterangan = etKeterangan.getText().toString();
        if (materi.isEmpty() || materi.equals("") || keterangan.isEmpty() || keterangan.equals("")) {
            return false;
        } else {
            return true;
        }
    }

    private void uploadFile(){

        Log.d(TAG, "uploadFile: process upload data ");
        SessionConfig sessionConfig = new SessionConfig(AttachmentActivity.this);
        final String ip = sessionConfig.preferences.getString("ipconfig",null);

        final ProgressDialog dialogo = new ProgressDialog(AttachmentActivity.this);
        dialogo.setMessage("Pengiriman data...");
        dialogo.setCancelable(false);
        dialogo.show();

        GetDataService service =
                RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

        SimpleDateFormat fmt = new SimpleDateFormat("dd-MM-yyyy");
        String tanggal = fmt.format(new Date());

        HashMap<String, String> param = new HashMap<>();
        param.put("jenis", jenis);
        param.put("id_jadwal", String.valueOf(idJadwal));
        param.put("id_guru", Objects.requireNonNull(sessionManager.preferences.getString("username", null)));
        param.put("materi", etMateri.getText().toString());
        param.put("keterangan", etKeterangan.getText().toString());
        param.put("tanggal", tanggal);


        Log.d(TAG, "uploadFile: \n" +
                "jenis : "+jenis+"\n" +
                "id_jadwal : "+idJadwal+"\n" +
                "id_guru : "+Objects.requireNonNull(sessionManager.preferences.getString("username", null))+"\n" +
                "materi : "+etMateri.getText().toString()+"\n" +
                "keterangan : "+etKeterangan.getText().toString()+"\n" +
                "tanggal : "+tanggal+"\n" +
                "");



        Call<GeneralResponse> call = service.attachment(param);
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                dialogo.dismiss();

                Log.d(TAG, "onResponse: "+response.body().toString());
                Log.d(TAG, "onResponse: "+response.body().getMessage());
                if(response.body().isSuccess()){
                    Toast.makeText(AttachmentActivity.this, response.body().getMessage(), Toast.LENGTH_LONG).show();
                    finish();
                }

            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                dialogo.dismiss();
            }
        });

    }
}
