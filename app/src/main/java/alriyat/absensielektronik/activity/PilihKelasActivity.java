package alriyat.absensielektronik.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.KelasOffline;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;

public class PilihKelasActivity extends AppCompatActivity {

    private KelasAdapter kelasAdapter;
    private RecyclerView rvRecyclerView;
    private ArrayList<KelasOffline> listKelas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_kelas);

        rvRecyclerView = findViewById(R.id.recycler_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Pilih Kelas");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listKelas = new ArrayList<>();

        kelasAdapter = new KelasAdapter(listKelas);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvRecyclerView.setLayoutManager(mLayoutManager);
        rvRecyclerView.setItemAnimator(new DefaultItemAnimator());
        rvRecyclerView.setAdapter(kelasAdapter);

        getKelas();
    }

    private void getKelas() {
        SessionConfig sessionConfig = new SessionConfig(PilihKelasActivity.this);
        SessionManager sessionManager = new SessionManager(PilihKelasActivity.this);
        GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

        Map<String, String> param = new HashMap<>();
        param.put("id_jenjang", getIntent().getStringExtra("id_jenjang"));
        param.put("id_guru", sessionManager.preferences.getString("idguru", ""));

        Call<List<KelasOffline>> call;
        if (!getIntent().getStringExtra("status").equals("wali_kelas")) call = service.pilihKelas(param);
        else call = service.pilihKelasWaliKelas(param);

        call.enqueue(new Callback<List<KelasOffline>>() {
            @Override
            public void onResponse(Call<List<KelasOffline>>call, retrofit2.Response<List<KelasOffline>> response) {
//                    loading.dismiss();
                List<KelasOffline> listKelasOffline = response.body();

                listKelas.clear();

                for (KelasOffline kelasOffline : listKelasOffline) {
                    listKelas.add(kelasOffline);
                }

                kelasAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<List<KelasOffline>>call, Throwable t) {
                // Log error here since request failed
//                    loading.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
class KelasAdapter extends RecyclerView.Adapter<KelasAdapter.MyViewHolder> {

    private List<KelasOffline> listKelas;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView idKelas, kelas, namaGuru;

        public MyViewHolder(final View view) {
            super(view);
            idKelas = view.findViewById(R.id.id_kelas);
            kelas = view.findViewById(R.id.kelas);
            namaGuru = view.findViewById(R.id.nama_guru);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences sharedPreferences = view.getContext().getSharedPreferences("rekap_kehadiran_siswa_per_kelas", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("id_kelas", idKelas.getText().toString());
                    editor.putString("kelas", kelas.getText().toString());
                    editor.apply();

                    sharedPreferences = view.getContext().getSharedPreferences("tambah_siswa", Context.MODE_PRIVATE);
                    editor = sharedPreferences.edit();
                    editor.putString("id_kelas", idKelas.getText().toString());
                    editor.putString("kelas", kelas.getText().toString());
                    editor.apply();

                    ((PilihKelasActivity) view.getContext()).onBackPressed();
                }
            });
        }
    }


    public KelasAdapter(List<KelasOffline> listKelas) {
        this.listKelas = listKelas;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_kelas, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        KelasOffline kelasOffline = listKelas.get(position);
        holder.idKelas.setText(kelasOffline.getIdKelas());
        holder.kelas.setText(kelasOffline.getKelas());
        holder.namaGuru.setText(kelasOffline.getNamaGuru());
    }

    @Override
    public int getItemCount() {
        return listKelas.size();
    }
}
