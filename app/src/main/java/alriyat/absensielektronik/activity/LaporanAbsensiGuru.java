package alriyat.absensielektronik.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.pojo.LapAbsensiGuru;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import alriyat.absensielektronik.util.Vira;
import retrofit2.Call;
import retrofit2.Callback;

public class LaporanAbsensiGuru extends AppCompatActivity {

    private static final String TAG = LaporanAbsensiGuru.class.getName();
    private EditText etTanggal;
    private EditText etJenjang;
    private EditText etTahunajaran;
    private Button bFilter,btnLapAbsensiGuru;
    private RecyclerView recyclerView;
    private SessionConfig sessionConfig;
    private SessionManager sessionManager;
    private String id_guru;

    private final String ERR_DATE_EMPTY = "Tanggal tidak boleh kosong";
    private final String ERR_JENJANG_EMPTY = "Jenjang tidak boleh kosong!";
    private final String ERR_YEAR_EMPTY = "Tahun ajaran tidak boleh kosong!";
    private final String ERR_CONN_SERVER = "Pencarian gagal, anda tidak terhubung dengan server";

    private LaporanAbsensiGuruAdapter adapter;
    ArrayList<LapAbsensiGuru>arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan_absensi_guru);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_lap_absensi_guru);
        toolbar.setTitle("Laporan Absensi Guru");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        setUI();

        sessionConfig = new SessionConfig(this);
        sessionManager = new SessionManager(this);
        id_guru = sessionManager.preferences.getString("username", null);

        setDatePicker();
        setPeriode();

        bFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String tanggal = etTanggal.getText().toString();
                String jenjang = etJenjang.getText().toString();
                String tahunajaran = etTahunajaran.getText().toString();

                if (tanggal.isEmpty()) {
                    Toast.makeText(LaporanAbsensiGuru.this, ERR_DATE_EMPTY, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (jenjang.isEmpty()) {
                    Toast.makeText(LaporanAbsensiGuru.this, ERR_JENJANG_EMPTY, Toast.LENGTH_SHORT).show();
                    return;
                }
                if (tahunajaran.isEmpty()) {
                    Toast.makeText(LaporanAbsensiGuru.this, ERR_YEAR_EMPTY, Toast.LENGTH_SHORT).show();
                    return;
                }

              getData();

            }
        });

        adapter = new LaporanAbsensiGuruAdapter(arrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Vira.isConnectedToServer(getApplicationContext())) {
            bFilter.setClickable(true);
        }else{
            bFilter.setClickable(false);
            Toast.makeText(this, "Anda tidak terhubung ke server", Toast.LENGTH_LONG).show();
        }
        SharedPreferences sharedPreferences = getSharedPreferences("rekap_kehadiran_guru", Context.MODE_PRIVATE);
        etJenjang.setText(sharedPreferences.getString("jenjang", ""));
        etTahunajaran.setText(sharedPreferences.getString("tahunajaran", ""));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void getData(){
        if (Vira.isConnectedToServer(getApplicationContext())) {
            final ProgressDialog loading = ProgressDialog.show(this, "", "Loading Data...", false, false);
            arrayList.clear();
            SharedPreferences sharedPreferences = getSharedPreferences("rekap_kehadiran_guru", Context.MODE_PRIVATE);
            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put("tgl", etTanggal.getText().toString());
            params.put("id_jenjang", sharedPreferences.getString("id_jenjang", ""));
            params.put("id_ta", sharedPreferences.getString("id_ta", ""));

            Call<List<LapAbsensiGuru>> call = service.lapAbsensiGuru(params);
            call.enqueue(new Callback<List<LapAbsensiGuru>>() {

                @Override
                public void onResponse(Call<List<LapAbsensiGuru>> call, retrofit2.Response<List<LapAbsensiGuru>> response) {
                    loading.dismiss();
                    Log.d(TAG, "onResponse: "+response.body());
                    final List<LapAbsensiGuru> rekap = response.body();
                    if(rekap.size() >0){
                        for (int i = 0; i < rekap.size(); i++) {
                            arrayList.add(rekap.get(i));
                        }

                        btnLapAbsensiGuru.setVisibility(View.VISIBLE);
                        btnLapAbsensiGuru.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
//                                String ta = etTahunajaran.getText().toString();
//                                String jenjang = etJenjang.getText().toString();
//                                buatPdf(rekap,ta,jenjang);
                            }
                        });
                        adapter.notifyDataSetChanged();
                    }else{
                        btnLapAbsensiGuru.setVisibility(View.GONE);
                        Toast.makeText(LaporanAbsensiGuru.this, "Data tidak ditemukan", Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<List<LapAbsensiGuru>> call, Throwable t) {
                    loading.dismiss();
                }
            });
        }else{
            Toast.makeText(this, ERR_CONN_SERVER, Toast.LENGTH_SHORT).show();
        }
    }

    private void setUI(){
        etTanggal = findViewById(R.id.date_absensi_guru);
        etJenjang = findViewById(R.id.periode_absensi_guru);
        etTahunajaran = findViewById(R.id.tahun_ajaran_absensi_guru);
        bFilter = findViewById(R.id.bt_filter_absensi_guru);
        btnLapAbsensiGuru = findViewById(R.id.btnDetail_lap_absensi_guru);
        recyclerView = findViewById(R.id.recycler_view_lap_absensi_guru);
    }

    private void setDatePicker() {
        final Calendar calendarDari = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener dateDari = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendarDari.set(Calendar.YEAR, year);
                calendarDari.set(Calendar.MONTH, monthOfYear);
                calendarDari.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                etTanggal.setText(String.format("%02d", calendarDari.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendarDari.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", calendarDari.get(Calendar.YEAR)));
            }
        };
        etTanggal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(LaporanAbsensiGuru.this, dateDari, calendarDari
                        .get(Calendar.YEAR), calendarDari.get(Calendar.MONTH),
                        calendarDari.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }
    private void setPeriode() {
        etJenjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LaporanAbsensiGuru.this, PilihJenjangActivity.class);
                startActivity(intent);
            }
        });

        etTahunajaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LaporanAbsensiGuru.this, PilihTahunajaranActivity.class);
                startActivity(intent);
            }
        });
    }
}
class LaporanAbsensiGuruAdapter extends RecyclerView.Adapter<LaporanAbsensiGuruAdapter.MyViewHolder>{

    private List<LapAbsensiGuru> rekapAbsensiGuru;

    public LaporanAbsensiGuruAdapter(List<LapAbsensiGuru>lapAbsensiGurus){
        this.rekapAbsensiGuru = lapAbsensiGurus;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView lblNama,lbltglMasuk,lblJamMasuk,lblKeteranganMasuk,lbltglPulang,lblJamPulang,lblKeteranganPulang;
        public MyViewHolder(View view) {
            super(view);
            lblNama = view.findViewById(R.id.nama_guru);
            lbltglMasuk = view.findViewById(R.id.row_tanggal_masuk);
            lblJamMasuk = view.findViewById(R.id.row_jam_masuk);
            lblKeteranganMasuk = view.findViewById(R.id.row_keterangan_masuk);
            lbltglPulang = view.findViewById(R.id.row_tanggal_pulang);
            lblJamPulang = view.findViewById(R.id.row_jam_pulang);
            lblKeteranganPulang = view.findViewById(R.id.row_keterangan_pulang);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_absensi_guru, parent, false);
        return new LaporanAbsensiGuruAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(LaporanAbsensiGuruAdapter.MyViewHolder holder, int position) {
        LapAbsensiGuru data = rekapAbsensiGuru.get(position);

        holder.lblNama.setText(data.getNama());
        holder.lbltglMasuk.setText(String.valueOf(data.getTglMasuk()));
        holder.lblJamMasuk.setText(String.valueOf(data.getJamMasuk()));
        holder.lblKeteranganMasuk.setText(String.valueOf(data.getKetMasuk()));

        holder.lbltglPulang.setText(String.valueOf(data.getTglPulang()));
        holder.lblJamPulang.setText(String.valueOf(data.getJamPulang()));
        holder.lblKeteranganPulang.setText(String.valueOf(data.getKetPulang()));

    }

    @Override
    public int getItemCount() {
        return rekapAbsensiGuru.size();
    }
}