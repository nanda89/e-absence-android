package alriyat.absensielektronik.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.model.GuruOffline;
import alriyat.absensielektronik.model.JenjangOffline;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;

public class PilihJenjangActivity extends AppCompatActivity {

    private JenjangAdapter jenjangAdapter;
    private RecyclerView rvRecyclerView;
    private ArrayList<JenjangOffline> listJenjang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_jenjang);

        rvRecyclerView = findViewById(R.id.recycler_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Pilih Jenjang");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listJenjang = new ArrayList<>();

        jenjangAdapter = new JenjangAdapter(listJenjang);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvRecyclerView.setLayoutManager(mLayoutManager);
        rvRecyclerView.setItemAnimator(new DefaultItemAnimator());
        rvRecyclerView.setAdapter(jenjangAdapter);

        getJenjang();
    }

    private void getJenjang() {
        SessionConfig sessionConfig = new SessionConfig(PilihJenjangActivity.this);
        GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

        Map<String, String> param = new HashMap<>();
        param.put("id_guru", "");

        Call<List<JenjangOffline>> call = service.syncUpdateTabelJenjang(param);
        call.enqueue(new Callback<List<JenjangOffline>>() {
            @Override
            public void onResponse(Call<List<JenjangOffline>>call, retrofit2.Response<List<JenjangOffline>> response) {
//                    loading.dismiss();
                List<JenjangOffline> listJenjangOffline = response.body();

                listJenjang.clear();

                for (JenjangOffline jenjangOffline : listJenjangOffline) {
                    listJenjang.add(jenjangOffline);
                }

                jenjangAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<List<JenjangOffline>>call, Throwable t) {
                // Log error here since request failed
//                    loading.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

class JenjangAdapter extends RecyclerView.Adapter<JenjangAdapter.MyViewHolder> {

    private List<JenjangOffline> listJenjang;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView idJenjang, jenjang, jamMasuk, jamPulang;

        public MyViewHolder(final View view) {
            super(view);
            idJenjang = view.findViewById(R.id.id_jenjang);
            jenjang = view.findViewById(R.id.jenjang);
            jamMasuk = view.findViewById(R.id.jam_masuk);
            jamPulang = view.findViewById(R.id.jam_pulang);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences sharedPreferences = view.getContext().getSharedPreferences("rekap_kehadiran_guru", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("id_jenjang", idJenjang.getText().toString());
                    editor.putString("jenjang", jenjang.getText().toString());
                    editor.apply();

                     sharedPreferences = view.getContext().getSharedPreferences("rekap_kehadiran_siswa", Context.MODE_PRIVATE);
                     editor = sharedPreferences.edit();
                    editor.putString("id_jenjang", idJenjang.getText().toString());
                    editor.putString("jenjang", jenjang.getText().toString());
                    editor.apply();

                    sharedPreferences = view.getContext().getSharedPreferences("rekap_kehadiran_siswa_per_kelas", Context.MODE_PRIVATE);
                    editor = sharedPreferences.edit();
                    editor.putString("id_jenjang", idJenjang.getText().toString());
                    editor.putString("jenjang", jenjang.getText().toString());
                    editor.apply();

                    sharedPreferences = view.getContext().getSharedPreferences("tambah_siswa", Context.MODE_PRIVATE);
                    editor = sharedPreferences.edit();
                    editor.putString("id_jenjang", idJenjang.getText().toString());
                    editor.putString("jenjang", jenjang.getText().toString());
                    editor.apply();

                    ((PilihJenjangActivity) view.getContext()).onBackPressed();
                }
            });
        }
    }


    public JenjangAdapter(List<JenjangOffline> listJenjang) {
        this.listJenjang = listJenjang;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_jenjang, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        JenjangOffline jenjang = listJenjang.get(position);
        holder.idJenjang.setText(jenjang.getIdJenjang());
        holder.jenjang.setText(jenjang.getJenjang());
        holder.jamMasuk.setText("Jam masuk : " + jenjang.getJamMasuk());
        holder.jamPulang.setText("Jam pulang : "  + jenjang.getJamPulang());
    }

    @Override
    public int getItemCount() {
        return listJenjang.size();
    }
}
