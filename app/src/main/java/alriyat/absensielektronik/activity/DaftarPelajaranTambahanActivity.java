package alriyat.absensielektronik.activity;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.AlarmReceiver;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.MapelTambahan;
import alriyat.absensielektronik.util.DatabaseHandler;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import alriyat.absensielektronik.util.Vira;
import retrofit2.Call;
import retrofit2.Callback;

public class DaftarPelajaranTambahanActivity extends AppCompatActivity {

    private static final String TAG = DaftarPelajaranTambahanActivity.class.getName();
    private Toolbar toolbar;
    private RecyclerView recyclerView;
    private ArrayList<MapelTambahan> arrayList;
    private DaftarPelajaranTambahanActivity.MapelAdapter mapelAdapter;
    private RecyclerView.LayoutManager layoutManager;
    private SessionConfig sessionConfig;
    private SessionManager sessionManager;
    private TextView noJadwal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_pelajaran_tambahan);

        toolbar = findViewById(R.id.toolbar_daftar_tambahan);
        toolbar.setTitle("Daftar Pelajaran tambahan");
        setSupportActionBar(toolbar);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        sessionConfig = new SessionConfig(this);
        sessionManager = new SessionManager(this);

        noJadwal = findViewById(R.id.text_nojadwal_tambahan);

        arrayList = new ArrayList<>();
        recyclerView = findViewById(R.id.recycler_pelajaran_tambahan);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        mapelAdapter = new MapelAdapter(this,arrayList);
        recyclerView.setAdapter(mapelAdapter);
        arrayList = getMapel();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                goToBack();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void goToBack(){
        super.onBackPressed();
    }

    private ArrayList<MapelTambahan> getMapel() {
        if (Vira.isConnectedToServer(DaftarPelajaranTambahanActivity.this)) {
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            final ProgressDialog loading = ProgressDialog.show(this, "", "Loading Data...", false, false);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("idguru", sessionManager.preferences.getString("username", null));

            Call<List<MapelTambahan>> call = service.daftarMapelTambahanAll(param);
            call.enqueue(new Callback<List<MapelTambahan>>() {
                @Override
                public void onResponse(Call<List<MapelTambahan>>call, retrofit2.Response<List<MapelTambahan>> response) {
                    loading.dismiss();

                    arrayList.clear();
                    Log.d(TAG, "onResponse: "+response.body());
                    List<MapelTambahan> listMapel = response.body();

                    if(!listMapel.isEmpty()){
                        for (MapelTambahan mapel : listMapel) {
//                        Log.d(TAG, "onResponse: data mapel tambahan : \n" +
//                                "id kelas : "+mapel.getIdkelas()+"\n" +
//                                "id jenjang : "+mapel.getJenjang()+"\n" +
//                                "kelas : "+mapel.getKelas()+"\n" +
//                                "mapel : "+mapel.getMapel()+"\n" +
//                                "jam awal : "+mapel.getJamAwal()+"\n" +
//                                "jam akhir : "+mapel.getJamAkhir()+"\n" +
//                                "id jadwal : "+mapel.getIdjadwal()+"\n" +
//                                "waktu : "+mapel.getWaktu()+"\n" +
//                                "jam : "+mapel.getJam()+"\n" +
//                                "nama : "+mapel.getNama()+"\n" +
//                                "apakah guru BK : "+mapel.getStatusGuruBK());
                            arrayList.add(mapel);
                        }
                    }

                    mapelAdapter.notifyDataSetChanged();
                    if (mapelAdapter.getItemCount() > 0) {
                        noJadwal.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onFailure(Call<List<MapelTambahan>>call, Throwable t) {
                    // Log error here since request failed
                    loading.dismiss();
                    DatabaseHandler dh = new DatabaseHandler(DaftarPelajaranTambahanActivity.this);
                    SessionManager sessionManager = new SessionManager(DaftarPelajaranTambahanActivity.this);
                    String guru = sessionManager.preferences.getString("username", null);
                    ArrayList<MapelTambahan> temp = dh.daftarMapelTambahanAll(DaftarPelajaranTambahanActivity.this, guru);
                    for (MapelTambahan i : temp) {
                        arrayList.add(i);
                    }
                    mapelAdapter.notifyDataSetChanged();
                    if (mapelAdapter.getItemCount() > 0) {
                        noJadwal.setVisibility(View.GONE);
                    }
                }
            });
            return arrayList;
        }else {
            DatabaseHandler dh = new DatabaseHandler(DaftarPelajaranTambahanActivity.this);
            SessionManager sessionManager = new SessionManager(DaftarPelajaranTambahanActivity.this);
            String guru = sessionManager.preferences.getString("username", null);
            ArrayList<MapelTambahan> temp = dh.daftarMapelTambahanAll(DaftarPelajaranTambahanActivity.this, guru);
            for (MapelTambahan i : temp) {
                arrayList.add(i);
            }
            mapelAdapter.notifyDataSetChanged();
            if (mapelAdapter.getItemCount() > 0) {
                noJadwal.setVisibility(View.GONE);
            }
            return arrayList;
        }
    }

    class MapelAdapter extends RecyclerView.Adapter<MapelAdapter.TcashViewHolder> {
        private Context mContext;
        private ArrayList<MapelTambahan> arrayList;


        public MapelAdapter(Context mContext,ArrayList<MapelTambahan> arrayList) {
            this.mContext = mContext;
            this.arrayList = arrayList;
        }


        public class  TcashViewHolder extends RecyclerView.ViewHolder{

            public TextView id,mapel, waktu,kelas,idjadwal,stat,nama;
            public ImageView img;
            private LinearLayout linearLayout;

            public TcashViewHolder(View itemView) {
                super(itemView);
                id = itemView.findViewById(R.id.id_mapel);
                stat = itemView.findViewById(R.id.textStat);
                idjadwal = itemView.findViewById(R.id.id_jadwal);
                mapel = itemView.findViewById(R.id.nma_mapel);
                nama =  itemView.findViewById(R.id.nma_guru);
                kelas = itemView.findViewById(R.id.nma_kelas);
                waktu = itemView.findViewById(R.id.jm_tgl);
                linearLayout = itemView.findViewById(R.id.itemview);

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (stat.getText().equals("ada")){
                            Log.d(TAG, "onClick: id : "+id.getText().toString()+"\n" +
                                    "stat : "+stat.getText().toString()+"\n" +
                                    "idjadwal : "+idjadwal.getText().toString()+"\n" +
                                    "mapel : "+mapel.getText().toString()+"\n" +
                                    "nama : "+nama.getText().toString()+"\n" +
                                    "kelas : "+kelas.getText().toString()+"\n" +
                                    "waktu : "+waktu.getText().toString()+"\n" +
                                    "");

                            Intent n = new Intent(DaftarPelajaranTambahanActivity.this, AbsensiPelajaranActivity.class);
                            n.putExtra("idkelastambahan",idjadwal.getText().toString());
                            n.putExtra("code","extra");
                            startActivity(n);
                        }else {
                            Toast.makeText(DaftarPelajaranTambahanActivity.this,"Belum waktu jam absen",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            }
        }


        @NonNull
        @Override
        public DaftarPelajaranTambahanActivity.MapelAdapter.TcashViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_mapel, parent, false);
            return new DaftarPelajaranTambahanActivity.MapelAdapter.TcashViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(@NonNull TcashViewHolder holder, int position) {
            MapelTambahan news = arrayList.get(position);
            holder.id.setText(news.getMapel());
            holder.stat.setText(news.getWaktu());
            holder.idjadwal.setText(news.getIdjadwal());
            holder.kelas.setText(news.getJenjang()+" Kelas "+news.getKelas());
            holder.nama.setText(news.getNama());
            holder.mapel.setText("Pelajaran "+news.getMapel());
            holder.waktu.setText(news.getJamAwal()+" s/d "+news.getJamAkhir());
            if (news.getWaktu().equals("ada")) {
                holder.linearLayout.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                holder.mapel.setTextColor(getResources().getColor(R.color.white));
                holder.kelas.setTextColor(getResources().getColor(R.color.white));
                holder.waktu.setTextColor(getResources().getColor(R.color.input_login));

            }
            else {
                holder.linearLayout.setBackgroundColor(getResources().getColor(R.color.white));
                holder.mapel.setTextColor(getResources().getColor(R.color.hitam));
                holder.kelas.setTextColor(getResources().getColor(R.color.hitam));
                holder.waktu.setTextColor(getResources().getColor(R.color.input_login));
            }
            if (news.getStatusGuruBK() != null && news.getStatusGuruBK().equals("1")) holder.nama.setVisibility(View.VISIBLE);
            String jam = "";
            jam += news.getJam().substring(0, 2);
            String menit = "";
            menit += news.getJam().substring(3, 5);

            AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(jam));
            calendar.set(Calendar.MINUTE, Integer.valueOf(menit));
            calendar.add(Calendar.MINUTE, -5);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MILLISECOND, 0);

            Intent notificationIntent = new Intent(DaftarPelajaranTambahanActivity.this, AlarmReceiver.class);
            PendingIntent broadcast = PendingIntent.getBroadcast(DaftarPelajaranTambahanActivity.this, 1, notificationIntent, 0);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && calendar.getTimeInMillis() >= Calendar.getInstance().getTimeInMillis()) {
                alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), broadcast);
            }
        }

        @Override
        public int getItemCount() {
            return arrayList.size();
        }


    }
}
