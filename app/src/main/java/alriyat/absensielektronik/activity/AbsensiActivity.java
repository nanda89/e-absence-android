package alriyat.absensielektronik.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.AlarmReceiver;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.Depan;
import alriyat.absensielektronik.model.Jadwal;
import alriyat.absensielektronik.model.KehadiranResponse;
import alriyat.absensielektronik.model.Mapel;
import alriyat.absensielektronik.model.SettingOffline;
import alriyat.absensielektronik.model.User;
import alriyat.absensielektronik.service.AbsenHadirPulang;
import alriyat.absensielektronik.util.DatabaseHandler;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import alriyat.absensielektronik.util.Vira;
import retrofit2.Call;
import retrofit2.Callback;

import static alriyat.absensielektronik.R.id.fragMap;
import static alriyat.absensielektronik.service.AbsenHadirPulang.getInfoAbsen;

public class AbsensiActivity extends AppCompatActivity
        implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener,
        NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = AbsensiActivity.class.getName();
    private SessionManager sessionManager;
    private SessionConfig sessionConfig;
    private ArrayList<Mapel> arrayList;
    private final String HADIR = "hadir";
    private final String PULANG = "pulang";

    private NavigationView navigationView;
    private TextView nmHead, emHead, nmWali;
    private boolean isGuru = true;
    private ImageView profil_image, logo;
    int year_x, month_x, day_x;

    private LinearLayout lyInfoOnline, lyInfoOffline;
    private ImageView ivPicture;
    private TextView tvInformation;
    private LinearLayout vMap;
    private ImageView ivBtnComing, ivBtnGoHome;
    private TextView tvDefaultComing, tvDefaultGohome;
    private TextView tvCurrentDate;
    private TextView tvTimeComing, tvTimeComingDesc, tvTimeGoHome, tvTimeGoHomeDesc;
    private TextView tvSchooleInformation;
    private TextView tvSchooleAddress;
    private boolean isConnected;
    String ip;

    static GoogleMap mGoogleMap;
    GoogleApiClient mGoogleApiClient;
    static LatLng ll;
    private static final int ZOOM_MAP = 16;
    GetDataService service;
    SwipeRefreshLayout pullToRefresh;
    private final String ERR_NOT_CONNECT = "Error : pastikan anda terhubung dengan koneksi sekolah.";

    User userLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* all about map */
        if (googleServicesAvailable()) {
            setContentView(R.layout.activity_absensi);
            initMap();
        }
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_absensi);
        toolbar.setTitle("Dashboard");

        pullToRefresh = (SwipeRefreshLayout) findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                // TODO Auto-generated method stub
                refreshContent();
            }
        });

        final Calendar cal = Calendar.getInstance();
        year_x = cal.get(Calendar.YEAR);
        month_x = cal.get(Calendar.MONTH) + 1;
        day_x = cal.get(Calendar.DAY_OF_MONTH);
        //etTgl.setText(day_x+"-"+month_x+"-"+year_x);
        if (month_x <= 9) {
            if (day_x <= 9) {
                toolbar.setSubtitle("0" + day_x + "-" + "0" + month_x + "-" + year_x);
            } else {
                toolbar.setSubtitle(day_x + "-" + "0" + month_x + "-" + year_x);
            }
        } else {
            if (day_x <= 9) {
                toolbar.setSubtitle("0" + day_x + "-" + month_x + "-" + year_x);
            } else {
                toolbar.setSubtitle(day_x + "-" + month_x + "-" + year_x);
            }
        }
        //toolbar.setSubtitle("05/04/2017");
        setSupportActionBar(toolbar);

        sessionManager = new SessionManager(this);
        sessionConfig = new SessionConfig(this);
        setGeneral();



        service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
        ip = sessionConfig.preferences.getString("ipconfig", null);
        isConnected = sessionManager.preferences.getBoolean("isConnect", false);
        if(!isConnected){
            MediaPlayer mPlayer = MediaPlayer.create(AbsensiActivity.this, R.raw.alarm);
            mPlayer.start();
        }
//        checkConnection(service);
        final Animation animation = AnimationUtils.loadAnimation(this, R.anim.fade);
        ivBtnComing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivBtnComing.startAnimation(animation);
                boolean isConnect = Vira.pingToApp(ip,service,sessionManager);
                if (isConnect) {
                    final SessionManager sessionManager = new SessionManager(AbsensiActivity.this);
                    Map<String, String> param = new HashMap<>();
                    if(isGuru){
                        param.put("id_guru", sessionManager.preferences.getString("idguru", null));
                    }else{
                        param.put("id_non_guru", sessionManager.preferences.getString("idnonguru", null));
                        param.put("jenis", HADIR);
                    }
                    AbsenHadirPulang.absenHadir(service, param, AbsensiActivity.this, sessionManager, ll, isGuru);
                    refreshContent();
                } else {
                    AbsenHadirPulang.absenHadirOffline(ll, AbsensiActivity.this, sessionManager,isGuru);
                    refreshContent();
                }
            }
        });
        ivBtnGoHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ivBtnGoHome.startAnimation(animation);
                boolean isConnect = Vira.pingToApp(ip,service,sessionManager);
                if (isConnect) {
                    final SessionManager sessionManager = new SessionManager(AbsensiActivity.this);
                    Map<String, String> param = new HashMap<>();
                    if(isGuru){
                        param.put("id_guru", sessionManager.preferences.getString("idguru", null));
                    }else {
                        param.put("id_non_guru", sessionManager.preferences.getString("idnonguru", null));
                        param.put("jenis", PULANG);
                    }
                    AbsenHadirPulang.absenPulang(service, param, AbsensiActivity.this, sessionManager, ll,isGuru);
                    refreshContent();
                } else {
                    AbsenHadirPulang.absenPulangOffline(ll, AbsensiActivity.this, sessionManager,isGuru);
                    refreshContent();
                }
            }
        });


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);
        nmHead = (TextView) header.findViewById(R.id.namaGuru);
        emHead = (TextView) header.findViewById(R.id.namaKelas);
        profil_image = (ImageView) header.findViewById(R.id.profile_image_head);
        navigationView.setNavigationItemSelectedListener(this);

        DatabaseHandler dh = new DatabaseHandler(AbsensiActivity.this);
        ArrayList<Jadwal> haha = dh.debugTabelJadwal();
        haha.isEmpty();

        arrayList = new ArrayList<>();

    }

    private void refreshContent() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                pullToRefresh.setRefreshing(false);
                onResume();
            }
        }, 5000);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.refresh, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            ambilUser();
            ambilDepan();
            cekStatusKepsek();
            getMapel();
        }
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    public boolean googleServicesAvailable() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int isAvailable = apiAvailability.isGooglePlayServicesAvailable(this);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (apiAvailability.isUserResolvableError(isAvailable)) {
            Dialog dialog = apiAvailability.getErrorDialog(this, isAvailable, 0);
            dialog.show();
        } else {
            Toast.makeText(this, "Can not connect to play services", Toast.LENGTH_LONG).show();
        }
        return false;
    }

    private void initMap() {
        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(fragMap);
        mapFragment.getMapAsync(this);
    }

    private ArrayList<Mapel> getMapel() {
        if (Vira.isConnectedToServer(AbsensiActivity.this) && isConnected) {

            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            HashMap<String, String> param = new HashMap<>();
            param.put("idguru", sessionManager.preferences.getString("username", null));

            Call<List<Mapel>> call = service.daftarMapelAll(param);
            call.enqueue(new Callback<List<Mapel>>() {
                @Override
                public void onResponse(Call<List<Mapel>> call, retrofit2.Response<List<Mapel>> response) {
                    arrayList.clear();
                    List<Mapel> listMapel = response.body();

                    for (Mapel mapel : listMapel) {
                        arrayList.add(mapel);

                        Mapel news = mapel;

                        String jam = "";
                        jam += news.getJam().substring(0, 2);
                        String menit = "";
                        menit += news.getJam().substring(3, 5);

                        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
                        AlarmManager alarmManager1 = (AlarmManager) getSystemService(ALARM_SERVICE);
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.HOUR_OF_DAY, Integer.valueOf(jam));
                        calendar.set(Calendar.MINUTE, Integer.valueOf(menit));
                        calendar.add(Calendar.MINUTE, -5);
                        calendar.set(Calendar.SECOND, 0);
                        calendar.set(Calendar.MILLISECOND, 0);

                        Intent notificationIntent = new Intent(AbsensiActivity.this, AlarmReceiver.class);
                        PendingIntent broadcast = PendingIntent.getBroadcast(AbsensiActivity.this, 1, notificationIntent, 0);

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && calendar.getTimeInMillis() >= Calendar.getInstance().getTimeInMillis() && news.getStatus1().equals("pulang")) {
                            alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), broadcast);
                        }
                    }
                }

                @Override
                public void onFailure(Call<List<Mapel>> call, Throwable t) {
                    // Log error here since request failed
                    DatabaseHandler dh = new DatabaseHandler(AbsensiActivity.this);
                    SessionManager sessionManager = new SessionManager(AbsensiActivity.this);
                    String guru = sessionManager.preferences.getString("username", null);
                    ArrayList<Mapel> temp = dh.daftarMapelAll(AbsensiActivity.this, guru);
                    for (Mapel i : temp) {
                        arrayList.add(i);
                    }
                }
            });

            return arrayList;
        } else {
            DatabaseHandler dh = new DatabaseHandler(AbsensiActivity.this);
            SessionManager sessionManager = new SessionManager(AbsensiActivity.this);
            String guru = sessionManager.preferences.getString("username", null);
            ArrayList<Mapel> temp = dh.daftarMapelAll(AbsensiActivity.this, guru);
            for (Mapel i : temp) {
                arrayList.add(i);
            }
            return arrayList;
        }
    }

    private void dialog5Menit(Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("5 menit lagi jam akan berakhir")
                .setTitle("Warning");

        AlertDialog alert = builder.create();
        alert.show();
    }

    /**
     * dipanggil di : onCreate dan onResume
     * nilai isConnected pada onCreate diambil dari activity splashscreen yaitu dari preferences
     * nilai isConnected pada onResume diambil dari function pingToServer
     * dan untuk memperbaharui nilai dari variabel isConnected
     */
    private void ambilUser() {
        if (isConnected) {

            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            HashMap<String, String> map = new HashMap<>();
            map.put("username", sessionManager.preferences.getString("username", null));
            Call<User> call;
            if (isGuru) {
                call = service.ambilUser(map);
            } else {
                call = service.ambilUserNonGuru(map);
            }

            call.enqueue(new Callback<User>() {
                @Override
                public void onResponse(Call<User> call, retrofit2.Response<User> response) {
                    User user = response.body();
                    Log.d(TAG, "onResponse: ambil user, "+user.getGambar());
                    Picasso.with(AbsensiActivity.this)
                            .load("http://" + ip + "/web_absence/assets/panel/images/" + user.getGambar())
                            .placeholder(R.drawable.siswa)
                            .into(ivPicture);
                    nmHead.setText(user.getNama());
                    tvInformation.setText(user.getNama());

                    userLogin = user;

                    Log.d(TAG, "onResponse: set data");
                    SharedPreferences sharedPreferences = getSharedPreferences("absensiElek", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("isWalikelas", user.isWalikelas() ? "y" :"n");
                    editor.putString("name", user.getNama());
                    editor.apply();

                    Log.d(TAG, "onResponse: get data : "+sharedPreferences.getString("isWalikelas",""));
                    Log.d(TAG, "onResponse: get data : "+sharedPreferences.getString("name",""));

                }

                @Override
                public void onFailure(Call<User> call, Throwable t) {
                    t.printStackTrace();
                    DatabaseHandler dh = new DatabaseHandler(AbsensiActivity.this);
                    dh.ambilUser(AbsensiActivity.this, sessionManager.preferences.getString("username", null), nmHead, tvInformation);

                }
            });

        } else {
            DatabaseHandler dh = new DatabaseHandler(AbsensiActivity.this);
            dh.ambilUser(AbsensiActivity.this, sessionManager.preferences.getString("username", null), nmHead, tvInformation);
        }
    }

    /**
     * dipanggil di : onCreate dan onResume
     * nilai isConnected pada onCreate diambil dari activity splashscreen yaitu dari preferences
     * nilai isConnected pada onResume diambil dari function pingToServer
     * dan untuk memperbaharui nilai dari variabel isConnected
     */
    private void ambilDepan() {
        Log.d(TAG, "ambilDepan: masuk sini");
        if (isConnected) {
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            HashMap<String, String> param = new HashMap<>();
            Call<Depan> call;
            if (isGuru) {
                param.put("idguru", sessionManager.preferences.getString("username", null));
                call = service.ambilDepan(param);
            } else {
                param.put("idnonguru", sessionManager.preferences.getString("username", null));
                call = service.depanNonGuru(param);
            }

            call.enqueue(new Callback<Depan>() {
                @Override
                public void onResponse(Call<Depan> call, retrofit2.Response<Depan> response) {
                    Depan depan = response.body();
                    Log.d(TAG, "onResponse: " + depan.getLogo());
//                    Picasso.with(AbsensiActivity.this)
//                            .load("http://" + ip + "/web_absence/assets/panel/images/" + depan.getLogo())
//                            .placeholder(R.drawable.siswa)
//                            .into(ivPicture);
                    tvSchooleInformation.setText(depan.getSekolah());
                    tvSchooleAddress.setText(depan.getAlamat());
                }

                @Override
                public void onFailure(Call<Depan> call, Throwable t) {
                    Log.d(TAG, "ambilDepan: masuk sini 1");
                    DatabaseHandler dh = new DatabaseHandler(AbsensiActivity.this);
                    dh.ambilDepan(AbsensiActivity.this, sessionManager.preferences.getString("username", null), tvSchooleInformation, tvSchooleAddress, isGuru);
                    ivPicture.setImageResource(R.drawable.tidakterhubungkeserver);
                    isConnected = false;
                    doOffline();
                    if(ll != null){
                        setJarak(dh);
                    }

                }
            });

        } else {
            Log.d(TAG, "ambilDepan: masuk sini 2");
            DatabaseHandler dh = new DatabaseHandler(AbsensiActivity.this);
            dh.ambilDepan(AbsensiActivity.this, sessionManager.preferences.getString("username", null), tvSchooleInformation, tvSchooleAddress, isGuru);
            ivPicture.setImageResource(R.drawable.tidakterhubungkeserver);
            isConnected = false;
            doOffline();
            if(ll != null){
                setJarak(dh);
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_sync_absen_guru) {
            //nilai isConnected dari onResume
            if (isConnected) {
                DatabaseHandler dh = new DatabaseHandler(this);
                if(isGuru){
                    final String idGuru = sessionManager.preferences.getString("idguru", null);
                    dh.syncAbsenGuru(AbsensiActivity.this, idGuru);
                }else{
                    final String idNonGuru = sessionManager.preferences.getString("idnonguru", null);
                    dh.syncAbsenNonGuru(AbsensiActivity.this, idNonGuru);
                }
                refreshContent();
            } else {
                Toast.makeText(this, "Sync Absen Kehadiran gagal, pastikan anda terhubung dengan koneksi sekolah.", Toast.LENGTH_LONG).show();
            }
        } else if (id == R.id.nav_daftarSiswa) {
            Intent i = new Intent(AbsensiActivity.this, AbsensiKelasActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_konfigIp) {
            Intent i = new Intent(AbsensiActivity.this, SettingConfigActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_profilUser) {
            Intent i = new Intent(AbsensiActivity.this, DetailUserActivity.class);
            startActivity(i);

        } else if (id == R.id.nav_daftarMapel) {
            Intent i = new Intent(AbsensiActivity.this, DaftarPelajaranActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_daftarMapelTambahan) {
            Intent i = new Intent(AbsensiActivity.this, DaftarPelajaranTambahanActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_syncUpdate) {
            if (Vira.isConnectedToServer(AbsensiActivity.this) && isConnected) {

                final String ip = sessionConfig.preferences.getString("ipconfig", null);
                final SessionManager sessionManager = new SessionManager(AbsensiActivity.this);

                final String idGuru = sessionManager.preferences.getString("idguru", null);
                DatabaseHandler dh = new DatabaseHandler(AbsensiActivity.this);
                dh.syncUpdateTabelGuru(AbsensiActivity.this, idGuru);

            } else {
                Toast.makeText(this, "Sync Update gagal, pastikan anda terhubung dengan koneksi sekolah.", Toast.LENGTH_LONG).show();
            }
        } else if (id == R.id.nav_syncAmbil) {
            if (isConnected) {
                final String ip = sessionConfig.preferences.getString("ipconfig", null);
                final SessionManager sessionManager = new SessionManager(AbsensiActivity.this);

                HashMap<String, String> param = new HashMap<>();
                param.put("id_guru", sessionManager.preferences.getString("idguru", null));
                param.put("pass", "");

                Call<String> call = service.syncOffline(param);
                call.enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                        if (response.body().equals("ok")) {
//                                        for (int i = 0; i < 5; i++) {
                            DatabaseHandler dh = new DatabaseHandler(AbsensiActivity.this);
                            dh.syncAmbilTabelGuru(AbsensiActivity.this, sessionManager.preferences.getString("idguru", null));
                        } else {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AbsensiActivity.this);
                            alertDialogBuilder.setMessage("password salah");
                            alertDialogBuilder.setPositiveButton("ok",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface arg0, int arg1) {
                                            arg0.cancel();
                                        }
                                    });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        // Log error here since request failed
                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AbsensiActivity.this);
                        alertDialogBuilder.setMessage("error");
                        alertDialogBuilder.setPositiveButton("ok",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface arg0, int arg1) {
                                        arg0.cancel();
                                    }
                                });

                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }
                });

            } else {
                Toast.makeText(this, "Sync Kirim gagal, pastikan anda terhubung dengan koneksi sekolah.", Toast.LENGTH_LONG).show();
            }
        } else if (id == R.id.nav_dataSiswa) {
            Intent i = new Intent(AbsensiActivity.this, DataSiswaActivity.class);
            startActivity(i);

        } else if (id == R.id.nav_lap_harian) {
            Intent i = new Intent(AbsensiActivity.this, LaporanHarianActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_rekap_mengajar_guru) {
            Intent i = new Intent(AbsensiActivity.this, RekapMengajarGuruActivity.class);
            startActivity(i);
        }else if(id == R.id.lap_kelas_hari){
            // hanya untuk walikelas saja
            if (Vira.isConnectedToServer(AbsensiActivity.this) && isConnected) {
                Intent i = new Intent(AbsensiActivity.this, RekapKelasHariActivity.class);
                startActivity(i);
            }else{
                Toast.makeText(this, ERR_NOT_CONNECT, Toast.LENGTH_SHORT).show();
            }

        }else if(id == R.id.lap_absensi_guru){
            if(isConnected){
                Intent i = new Intent(AbsensiActivity.this, LaporanAbsensiGuru.class);
                startActivity(i);
            }else {
                Toast.makeText(this, ERR_NOT_CONNECT, Toast.LENGTH_LONG).show();
            }

        }else if (id == R.id.nav_lap_guru) {
            Intent i = new Intent(AbsensiActivity.this, LaporanGuruActivity.class);
            startActivity(i);

        } else if (id == R.id.nav_lap_rekap_hadir_guru) {
            Intent i = new Intent(AbsensiActivity.this, LaporanKuActivity.class);
            startActivity(i);

        } else if (id == R.id.nav_rekapKehadiranGuru) {
            Intent i = new Intent(AbsensiActivity.this, RekapKehadiranGuruActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_daftar_hadir) {
            Intent i = new Intent(AbsensiActivity.this, DaftarHadirActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_rekapKehadiranNonGuru) {
            Intent i = new Intent(AbsensiActivity.this, RekapKehadiranNonGuruActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_rekapKehadiranSiswaPerKelas) {
            Intent i = new Intent(AbsensiActivity.this, RekapKehadiranSiswaPerKelasActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_passSync) {
            Intent i = new Intent(AbsensiActivity.this, PassSyncActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_Logout) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Apakah yakin ingin logout?");
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    sessionConfig.setLogin(false, "");
                    sessionManager.setLogin(false, "", "", "", "", "", "","" , false);
                    dialog.cancel();
                    Intent n = new Intent(AbsensiActivity.this, LoginActivity.class);
                    n.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(n);
                    finish();
                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.show();
        }else if (id == R.id.nav_kegiatan) {
            if (Vira.isConnectedToServer(AbsensiActivity.this)) {
                Intent intent = new Intent(AbsensiActivity.this, KegiatanActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(this, ERR_NOT_CONNECT, Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_daftar_hadir_non_guru) {
            if (Vira.isConnectedToServer(AbsensiActivity.this)) {
                Intent i = new Intent(AbsensiActivity.this, DaftarHadirNonGuruActivity.class);
                startActivity(i);
            } else {
                Toast.makeText(this, ERR_NOT_CONNECT, Toast.LENGTH_SHORT).show();
            }
        } else if (id == R.id.nav_syncUpdate_non_guru) {
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            final SessionManager sessionManager = new SessionManager(AbsensiActivity.this);
//            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            DatabaseHandler dh = new DatabaseHandler(AbsensiActivity.this);
            dh.syncUpdateTabelNonGuru(AbsensiActivity.this, sessionManager.preferences.getString("idnonguru", null));

        } else if (id == R.id.nav_syncAmbil_non_guru) {
            AlertDialog.Builder builder = new AlertDialog.Builder(AbsensiActivity.this);
            builder.setTitle("Sync");

// Set up the input
            final EditText input = new EditText(AbsensiActivity.this);
// Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
            input.setInputType(InputType.TYPE_CLASS_TEXT);
            builder.setView(input);

// Set up the buttons
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialog, int which) {
                    final String ip = sessionConfig.preferences.getString("ipconfig", null);
                    final SessionManager sessionManager = new SessionManager(AbsensiActivity.this);
//                    GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

                    Map<String, String> param = new HashMap<>();
                    param.put("id_non_guru", sessionManager.preferences.getString("idnonguru", null));
                    param.put("pass", input.getText().toString());

                    Call<String> call = service.syncNonGuruOffline(param);
                    call.enqueue(new Callback<String>() {
                        @Override
                        public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                            if (response.body().equals("ok")) {
                                dialog.cancel();
//                                        for (int i = 0; i < 5; i++) {
                                DatabaseHandler dh = new DatabaseHandler(AbsensiActivity.this);
                                dh.syncAmbilTabelNonGuru(AbsensiActivity.this, sessionManager.preferences.getString("idnonguru", null));
                            } else {
                                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AbsensiActivity.this);
                                alertDialogBuilder.setMessage("password salah");
                                alertDialogBuilder.setPositiveButton("ok",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface arg0, int arg1) {
                                                arg0.cancel();
                                            }
                                        });

                                AlertDialog alertDialog = alertDialogBuilder.create();
                                alertDialog.show();
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AbsensiActivity.this);
                            alertDialogBuilder.setMessage("error");
                            alertDialogBuilder.setPositiveButton("ok",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface arg0, int arg1) {
                                            arg0.cancel();
                                        }
                                    });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }
                    });
                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });

            builder.show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isConnected = Vira.pingToApp(ip,service,sessionManager);
        Log.d(TAG, "onResume: isConnected : "+isConnected);

        ambilUser();
        ambilDepan();
        if (isGuru) {
            cekStatusKepsek();
            getMapel();
            hideMenu(isGuru);
        } else {
            hideMenu(isGuru);
        }

        DatabaseHandler dh = new DatabaseHandler(AbsensiActivity.this);
        SettingOffline setting = dh.getSetting();
        if (setting != null) {
            tvDefaultComing.setText("DATANG\n" + setting.getJamMasuk() + " WITA");
            tvDefaultGohome.setText("PULANG\n" + setting.getJamPulang() + " WITA");
            tvSchooleInformation.setText(setting.getLogoBesar());
            tvSchooleAddress.setText(setting.getAlamat());
        }

        if(isConnected){
            doOnline();
        }else {
            doOffline();
        }

        if (!isConnected && ll != null) {
            setJarak(dh);
        }

        final SessionManager sessionManager = new SessionManager(AbsensiActivity.this);
        Map<String, String> param = new HashMap<>();
        if(isGuru){
            param.put("id_guru", sessionManager.preferences.getString("idguru", null));
        }else{
            param.put("id_non_guru", sessionManager.preferences.getString("idnonguru", null));
        }
        getInfoAbsen(tvTimeComing, tvTimeComingDesc, tvTimeGoHome, tvTimeGoHomeDesc, service, param, AbsensiActivity.this, sessionManager, ll,isGuru,isConnected);
    }

    private void setJarak(DatabaseHandler dh) {
        double jarak = dh.distincTwoPosition(ll);
        Log.d(TAG, "onResume: jarak : " + jarak);
        double rJarak = jarak > 1000 ? jarak / 1000 : jarak;
        String satuan = jarak > 1000 ? "km" : "m";
        String pesan = "jarak anda dengan sekolah adalah " + new DecimalFormat("##.##").format(rJarak)+" "+satuan;
        tvInformation.setText(pesan);
    }

    public void checkConnection(GetDataService service){
        Call<List<alriyat.absensielektronik.pojo.Mapel>> call = service.getAllMapel();
        call.enqueue(new Callback<List<alriyat.absensielektronik.pojo.Mapel>>() {
            @Override
            public void onResponse(Call<List<alriyat.absensielektronik.pojo.Mapel>> call, retrofit2.Response<List<alriyat.absensielektronik.pojo.Mapel>> response) {
                isConnected = true;
                sessionManager.isConnected(true);
            }
            @Override
            public void onFailure(Call<List<alriyat.absensielektronik.pojo.Mapel>> call, Throwable t) {
                t.printStackTrace();
                isConnected = false;
                sessionManager.isConnected(false);
            }
        });
    }

    private void setGeneral() {

        lyInfoOnline = findViewById(R.id.statusOnline);
        lyInfoOffline = findViewById(R.id.statusOffline);
        vMap = findViewById(R.id.mapView);
        ivPicture = findViewById(R.id.teacher_pic);
        tvInformation = findViewById(R.id.infomasi);
        ivBtnComing = findViewById(R.id.btn_coming);
        ivBtnGoHome = findViewById(R.id.btn_goHome);
        tvDefaultComing = findViewById(R.id.jam_datang);
        tvDefaultGohome = findViewById(R.id.jam_pulang);
        tvCurrentDate = findViewById(R.id.tgl);
        tvTimeComing = findViewById(R.id.time_coming);
        tvTimeComingDesc = findViewById(R.id.time_coming_description);
        tvTimeGoHome = findViewById(R.id.time_gohome);
        tvTimeGoHomeDesc = findViewById(R.id.time_gohome_description);
        tvSchooleInformation = findViewById(R.id.school_information);
        tvSchooleAddress = findViewById(R.id.school_address);

        @SuppressLint("SimpleDateFormat") SimpleDateFormat format = new SimpleDateFormat("dd MMMM yyyy");
        tvCurrentDate.setText("Tanggal : " + format.format(new Date()));

        SharedPreferences sharedPreferences = getSharedPreferences("absensiElek", Context.MODE_PRIVATE);
        isGuru = sharedPreferences.getString("isGuru","").equals("Y");
    }

    private void goToLocation(double lat, double lng) {
        Log.d(TAG, "goToLocation: " + lat + " , " + lng);
        LatLng ll = new LatLng(lat, lng);
        CameraUpdate camUpdate = CameraUpdateFactory.newLatLngZoom(ll, ZOOM_MAP);
        mGoogleMap.moveCamera(camUpdate);
    }

    private void doOnline() {
        lyInfoOnline.setVisibility(View.VISIBLE);
        lyInfoOffline.setVisibility(View.GONE);
        ivPicture.setVisibility(View.VISIBLE);
        vMap.setVisibility(View.GONE);
    }

    private void doOffline() {
        lyInfoOnline.setVisibility(View.GONE);
        lyInfoOffline.setVisibility(View.VISIBLE);
        ivPicture.setVisibility(View.GONE);
        vMap.setVisibility(View.VISIBLE);
    }

    private void hideMenu(boolean isGuru) {
        Menu nav_Menu = navigationView.getMenu();
        if (isGuru) {
            // hide menu non guru
//            nav_Menu.findItem(R.id.nav_absen).setVisible(false);
//            nav_Menu.findItem(R.id.nav_absen_pulang).setVisible(false);
            nav_Menu.findItem(R.id.nav_kegiatan).setVisible(false);
            nav_Menu.findItem(R.id.nav_daftar_hadir_non_guru).setVisible(false);
            nav_Menu.findItem(R.id.nav_syncUpdate_non_guru).setVisible(false);
            nav_Menu.findItem(R.id.nav_syncAmbil_non_guru).setVisible(false);

        } else {
            // hide menu guru
//            nav_Menu.findItem(R.id.nav_absenKehadiranGuru).setVisible(false);
            nav_Menu.findItem(R.id.nav_daftarSiswa).setVisible(false);
            nav_Menu.findItem(R.id.nav_daftarMapel).setVisible(false);
            nav_Menu.findItem(R.id.nav_daftarMapelTambahan).setVisible(false);
            nav_Menu.findItem(R.id.nav_lap_harian).setVisible(false);
            nav_Menu.findItem(R.id.nav_lap_guru).setVisible(false);
            nav_Menu.findItem(R.id.nav_rekap_mengajar_guru).setVisible(false);
            nav_Menu.findItem(R.id.nav_lap_rekap_hadir_guru).setVisible(false);
            nav_Menu.findItem(R.id.nav_rekapKehadiranGuru).setVisible(false);
            nav_Menu.findItem(R.id.nav_daftar_hadir).setVisible(false);
            nav_Menu.findItem(R.id.nav_rekapKehadiranNonGuru).setVisible(false);
            nav_Menu.findItem(R.id.nav_rekapKehadiranSiswaPerKelas).setVisible(false);
            nav_Menu.findItem(R.id.nav_syncUpdate).setVisible(false);
            nav_Menu.findItem(R.id.nav_syncAmbil).setVisible(false);
            nav_Menu.findItem(R.id.nav_dataSiswa).setVisible(false);
            nav_Menu.findItem(R.id.nav_passSync).setVisible(false);
            nav_Menu.findItem(R.id.nav_syncAmbil).setVisible(false);
        }
        SharedPreferences sharedPreferences = getSharedPreferences("absensiElek", Context.MODE_PRIVATE);
        String iswalikelas = sharedPreferences.getString("isWalikelas","");
        Log.d(TAG, "hideMenu: iswalikelas : "+iswalikelas);
        if(iswalikelas.equals("y")){
            nav_Menu.findItem(R.id.lap_kelas_hari).setVisible(true);
        }else{
            nav_Menu.findItem(R.id.lap_kelas_hari).setVisible(false);
        }

        boolean isKepsek = sharedPreferences.getBoolean("isKepsek",false);
        if(isKepsek || !isGuru){
            nav_Menu.findItem(R.id.lap_absensi_guru).setVisible(true);
        }else{
            nav_Menu.findItem(R.id.lap_absensi_guru).setVisible(false);
        }
    }

    private void cekStatusKepsek() {
        if (Vira.isConnectedToServer(AbsensiActivity.this) && isConnected) {

            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            HashMap<String, String> param = new HashMap<>();
            param.put("idguru", sessionManager.preferences.getString("username", null));

            Call<String> call = service.cekStatusKepsek(param);
            call.enqueue(new Callback<String>() {
                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    if (!response.body().equals("1")) {
                        hideRekap();
                        hidePassSync();
                    } else {
                        showRekap();
                        showPassSync();
                        menuOnlyTeacher();
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    DatabaseHandler dh = new DatabaseHandler(AbsensiActivity.this);
                    String cek = dh.cekStatusKepsek(AbsensiActivity.this, sessionManager.preferences.getString("username", null));
                    if (!cek.equals("1")) {
                        hideRekap();
                        hidePassSync();
                    } else {
                        showRekap();
                        showPassSync();
                        menuOnlyTeacher();
                    }
                }
            });
        } else {
            DatabaseHandler dh = new DatabaseHandler(AbsensiActivity.this);
            String cek = dh.cekStatusKepsek(AbsensiActivity.this, sessionManager.preferences.getString("username", null));
            if (!cek.equals("1")) {
                hideRekap();
                hidePassSync();
            } else {
                showRekap();
                showPassSync();
                menuOnlyTeacher();
            }
        }
    }

    public void hideRekap() {
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_rekapKehadiranGuru).setVisible(false);
        nav_Menu.findItem(R.id.nav_rekapKehadiranNonGuru).setVisible(false);
    }

    public void menuOnlyTeacher() {
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_lap_rekap_hadir_guru).setVisible(false);
    }

    public void hidePassSync() {
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_passSync).setVisible(false);
    }

    public void showRekap() {
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_rekapKehadiranGuru).setVisible(true);
        nav_Menu.findItem(R.id.nav_rekapKehadiranNonGuru).setVisible(true);
    }

    public void showPassSync() {
        Menu nav_Menu = navigationView.getMenu();
        nav_Menu.findItem(R.id.nav_passSync).setVisible(true);
    }

    private void doAbsen(final String jenis) {
        final String ip = sessionConfig.preferences.getString("ipconfig", null);
        final SessionManager sessionManager = new SessionManager(AbsensiActivity.this);

        String idNonGuru = sessionManager.preferences.getString("idnonguru", null);
        Map<String, String> param = new HashMap<>();
        param.put("id_non_guru", idNonGuru);
        param.put("jenis", jenis);

        Call<KehadiranResponse> call = service.absenNonGuru(param);
        call.enqueue(new Callback<KehadiranResponse>() {
            @Override
            public void onResponse(Call<KehadiranResponse> call, retrofit2.Response<KehadiranResponse> response) {
                KehadiranResponse kehadiranResponse = response.body();
                if (kehadiranResponse.getStatus().equals("ok")) {
                    DatabaseHandler dh = new DatabaseHandler(AbsensiActivity.this);
                    dh.absenNonGuruOnline(AbsensiActivity.this, sessionManager.preferences.getString("idnonguru", null), jenis);
                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(AbsensiActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                    } else {
                        builder = new AlertDialog.Builder(AbsensiActivity.this);
                    }
                    builder = new AlertDialog.Builder(AbsensiActivity.this);
                    builder.setTitle("Status")
                            .setMessage("Absen online " + kehadiranResponse.getKeterangan())
                            .show();
                } else {
                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(AbsensiActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                    } else {
                        builder = new AlertDialog.Builder(AbsensiActivity.this);
                    }
                    builder = new AlertDialog.Builder(AbsensiActivity.this);
                    builder.setTitle("Status")
                            .setMessage(kehadiranResponse.getKeterangan())
                            .show();
                }
            }

            @Override
            public void onFailure(Call<KehadiranResponse> call, Throwable t) {
                t.printStackTrace();
                DatabaseHandler dh = new DatabaseHandler(AbsensiActivity.this);
                KehadiranResponse response = dh.absenNonGuru(AbsensiActivity.this, sessionManager.preferences.getString("idnonguru", null), jenis);
                if (response.getStatus().equals("ok")) {
                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(AbsensiActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                    } else {
                        builder = new AlertDialog.Builder(AbsensiActivity.this);
                    }
                    builder = new AlertDialog.Builder(AbsensiActivity.this);
                    builder.setTitle("Status")
                            .setMessage("Absen offline " + response.getKeterangan())
                            .show();
                } else {
                    AlertDialog.Builder builder;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        builder = new AlertDialog.Builder(AbsensiActivity.this, android.R.style.Theme_Material_Dialog_Alert);
                    } else {
                        builder = new AlertDialog.Builder(AbsensiActivity.this);
                    }
                    builder = new AlertDialog.Builder(AbsensiActivity.this);
                    builder.setTitle("Status")
                            .setMessage(response.getKeterangan())
                            .show();
                }
            }
        });
    }

    private void doAbsenOffline(String hadir) {
        DatabaseHandler dh = new DatabaseHandler(AbsensiActivity.this);
        KehadiranResponse response = dh.absenNonGuru(AbsensiActivity.this, sessionManager.preferences.getString("idnonguru", null), HADIR);
        if (response.getStatus().equals("ok")) {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(AbsensiActivity.this, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(AbsensiActivity.this);
            }
            builder = new AlertDialog.Builder(AbsensiActivity.this);
            builder.setTitle("Status")
                    .setMessage("Absen offline " + response.getKeterangan())
                    .show();
        } else {
            AlertDialog.Builder builder;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(AbsensiActivity.this, android.R.style.Theme_Material_Dialog_Alert);
            } else {
                builder = new AlertDialog.Builder(AbsensiActivity.this);
            }
            builder = new AlertDialog.Builder(AbsensiActivity.this);
            builder.setTitle("Status")
                    .setMessage(response.getKeterangan())
                    .show();
        }
    }

    LocationRequest mLocationRequest;

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = LocationRequest.create();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(1000);

        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this, // Activity
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    111);
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMapReady: ready");
        mGoogleMap = googleMap;
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        double lat = -6.2602227;
        double lng = 106.8271493;
        goToLocation(lat, lng);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();

    }

    @Override
    public void onLocationChanged(Location location) {
        if (location == null) {
            Toast.makeText(this, "cannot get current location", Toast.LENGTH_SHORT).show();
        } else {
            Log.d(TAG, "onLocationChanged: ready");
            mGoogleMap.clear();
            ll = new LatLng(location.getLatitude(), location.getLongitude());
            CameraUpdate camUpdate = CameraUpdateFactory.newLatLngZoom(ll, ZOOM_MAP);
            mGoogleMap.animateCamera(camUpdate);
            mGoogleMap.addMarker(new MarkerOptions().position(ll).title("Posisi saya"));
            goToLocation(location.getLatitude(), location.getLongitude());
        }
    }

}
