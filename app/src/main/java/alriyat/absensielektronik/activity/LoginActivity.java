package alriyat.absensielektronik.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.Login;
import alriyat.absensielektronik.util.DatabaseHandler;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import alriyat.absensielektronik.util.Vira;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = LoginActivity.class.getName();
    private static final int MY_PERMISSIONS_REQUEST = 1;
    @BindView(R.id.etUsername)
    EditText etUsername;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.tvInfo)
    TextView tvInfo;

    private SessionManager sessionManager;
    private SessionConfig sessionConfig;
    TelephonyManager tm;
    String imei;
    private static final int PERMISSION_READ_STATE = 1;
    private static final String GURU = "guru";

    private boolean isConnected;
    String ip;
    GetDataService service;
    SwipeRefreshLayout pullToRefresh;
    Gson gson;

    @SuppressLint("HardwareIds")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        givePermission();
        ButterKnife.bind(this);
        init();
        gson = new Gson();
    }

    @Override
    protected void onResume() {
        super.onResume();
        int version = Build.VERSION.SDK_INT;
        Log.d(TAG, "onResume: android version "+version);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "onCreate: masuk sini");
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        if(version < 10){
            imei = tm.getDeviceId();
        }else{
            imei = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        }
        Log.d(TAG, "onCreate: imei : " + imei);
        isConnected = Vira.pingToApp(ip,service,sessionManager);
        Log.d(TAG, "onResume: ping into "+ip+ " : "+isConnected);
        sessionManager.isConnected(isConnected);
        if(isConnected){
            doOnline();
        }else {
            doOffline();
        }
    }

    public void givePermission() {
        ArrayList<String> arrPerm = new ArrayList<>();
        arrPerm.add(Manifest.permission.ACCESS_FINE_LOCATION);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            arrPerm.add(Manifest.permission.READ_PHONE_STATE);
        }
        if(!arrPerm.isEmpty()) {
            String[] permissions = new String[arrPerm.size()];
            permissions = arrPerm.toArray(permissions);
            ActivityCompat.requestPermissions(this, permissions, MY_PERMISSIONS_REQUEST);
        }
    }

    private void init() {
        pullToRefresh = (SwipeRefreshLayout) findViewById(R.id.pullToRefresh);
        pullToRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {
                // TODO Auto-generated method stub
                refreshContent();
            }
        });

        sessionManager = new SessionManager(this);
        sessionConfig = new SessionConfig(this);
        if (sessionManager.setLogout()) {
            startActivity(new Intent(LoginActivity.this, AbsensiActivity.class));
            finish();
        }
        ip = sessionConfig.preferences.getString("ipconfig", null);
        service =
                RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
        Log.d(TAG, "init: isConnected "+isConnected);
    }

    private void refreshContent() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                pullToRefresh.setRefreshing(false);
                onResume();
            }
        }, 3000);

    }
    @OnClick(R.id.btnLogin)
    protected void loginClikced() {
        cekLogin();
    }

    @OnClick(R.id.btnConfig)
    protected void configClicked() {
        sessionConfig.setLogin(false, "");
        Intent e = new Intent(LoginActivity.this, SettingConfigFirstActivity.class);
        startActivity(e);
    }

    private void cekLogin() {
        Log.d(TAG, "cekLogin: isConnected : "+isConnected);
        if (Vira.isConnectedToServer(LoginActivity.this) && isConnected) {
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            final ProgressDialog dialog = new ProgressDialog(LoginActivity.this);
            dialog.setMessage("Mohon tunggu...");
            dialog.setCancelable(false);
            dialog.show();

            Log.d(TAG, "cekLogin: imei : " + imei);
            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);


            HashMap<String, String> param = new HashMap<>();
            param.put("username", etUsername.getText().toString().trim());
            param.put("password", etPassword.getText().toString().trim());
            param.put("imei", imei);

            Call<Login> call = service.login(param);
            call.enqueue(new Callback<Login>() {
                @Override
                public void onResponse(Call<Login> call, retrofit2.Response<Login> response) {
                    Login login = response.body();
                    if (!login.getUsername().isEmpty()) {
                        dialog.dismiss();
                        //String idguru = object.getString("id_guru");

                        String username = login.getUsername();
                        String password = login.getPassword();
                        String idUser = login.getId_user();

                        String idGuruNonGuru;

                        if(login.getLevel().equals(GURU)){

                            idGuruNonGuru = login.getIdGuru();
                            String statusGuruBK = login.getStatusGuruBK();
                            String isGuru = "Y";
                            boolean isKepsek = login.getStatusKepsek().equals("1");
                            sessionManager.setLogin(true, "", idGuruNonGuru, username, password, statusGuruBK,isGuru,idUser , isKepsek);

                        }else{
                            idGuruNonGuru = login.getIdNonGuru();
                            String isGuru = "N";
                            sessionManager.setLogin(true, "", idGuruNonGuru, username, password, null,isGuru,idUser,false);
                        }


                        /* update tabel_setting  and tabel_jam_masuk*/
                        DatabaseHandler dh = new DatabaseHandler(LoginActivity.this);
                        dh.synUpdateSetting(LoginActivity.this,idGuruNonGuru);

                    } else {
                        dialog.dismiss();
                        final AlertDialog.Builder adialog;
                        adialog = new AlertDialog.Builder(LoginActivity.this);
                        adialog.setCancelable(true);
                        adialog.setMessage(login.getMessage());
                        Log.e("URL", "LoginActivity" + response);
                        adialog.show();
                    }
                }

                @Override
                public void onFailure(Call<Login> call, Throwable t) {
                    // Log error here since request failed
                    t.printStackTrace();
                    Log.d(TAG, "onFailure: fail : "+t.getMessage());
                    dialog.dismiss();
                    Log.d(TAG, "onFailure: "+t.getMessage());
                    DatabaseHandler dh = new DatabaseHandler(LoginActivity.this);
                    String res = dh.cekLogin(LoginActivity.this, etUsername.getText().toString().trim(), etPassword.getText().toString().trim());
                    if (res == "ok") {
                        Intent n = new Intent(LoginActivity.this, AbsensiActivity.class);
                        startActivity(n);
                    } else {
                        final AlertDialog.Builder adialog;
                        adialog = new AlertDialog.Builder(LoginActivity.this);
                        adialog.setCancelable(true);
                        adialog.setMessage("Error : "+t.getMessage());
                        adialog.show();
                    }
                }
            });

        } else {
            DatabaseHandler dh = new DatabaseHandler(LoginActivity.this);
            String res = dh.cekLogin(LoginActivity.this, etUsername.getText().toString().trim(), etPassword.getText().toString().trim());
            if (res == "ok") {
                Intent n = new Intent(LoginActivity.this, AbsensiActivity.class);
                startActivity(n);
            } else {
                final AlertDialog.Builder adialog;
                adialog = new AlertDialog.Builder(LoginActivity.this);
                adialog.setCancelable(true);
                adialog.setMessage("Error : terdapat permasalahan komunikasi dengan server.");
                adialog.show();
                onResume();
            }
        }
    }

    private void doOnline(){
        etUsername.setEnabled(true);
        etPassword.setEnabled(true);
        tvInfo.setVisibility(View.GONE);
    }
    private void doOffline(){
        etUsername.setEnabled(false);
        etPassword.setEnabled(false);
        tvInfo.setVisibility(View.VISIBLE);
    }
}
