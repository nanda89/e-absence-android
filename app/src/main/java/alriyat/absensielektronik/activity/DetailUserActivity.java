package alriyat.absensielektronik.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.User;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;

public class DetailUserActivity extends AppCompatActivity {
    private static final String TAG = DetailUserActivity.class.getName();
    private SessionManager sessionManager;
    private SessionConfig sessionConfig;
    private EditText namaLengkap,Username;
    private Button kePass,btnPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_user);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_detail_user);
        toolbar.setTitle("Pengaturan Akun");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sessionManager = new SessionManager(this);
        sessionConfig = new SessionConfig(this);

        namaLengkap = (EditText)findViewById(R.id.namalengkap_edit);
        Username = (EditText)findViewById(R.id.username_edit);

        namaLengkap.setEnabled(false);
        Username.setEnabled(false);

        kePass = (Button)findViewById(R.id.kepass);
        kePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final AlertDialog dialog;
                LayoutInflater layoutInflater = LayoutInflater.from(DetailUserActivity.this);
                View view = layoutInflater.inflate(R.layout.set_password, null);
                final AlertDialog.Builder builder = new AlertDialog.Builder(DetailUserActivity.this);
                builder.setView(view);
                builder.setCancelable(true);
                final EditText etPassword = (EditText) view.findViewById(R.id.pass_edit);
                final Button button = (Button) view.findViewById(R.id.set_pass_btn);
                dialog = builder.create();
                dialog.show();
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final ProgressDialog loading = ProgressDialog.show(DetailUserActivity.this, "", "Loading Data...", false, false);
                        final String ip = sessionConfig.preferences.getString("ipconfig", null);

                        GetDataService service =
                                RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

                        String user = Username.getText().toString().trim();
                        String passss = etPassword.getText().toString().trim();
                        String nama = namaLengkap.getText().toString().trim();
                        String idUser = sessionManager.preferences.getString("id_user",null);
                        Map<String, String> param = new HashMap<>();
                        param.put("nama", nama);
                        param.put("username", user);
                        param.put("pass", passss);
                        param.put("id_user", idUser);

                        Log.d(TAG, "onClick: nama : "+nama+"\nusername : "+user+"\npass : "+passss+"\nid_user : "+idUser);
                        Call<String> call = service.updatePassword(param);
                        call.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String>call, retrofit2.Response<String> response) {
                                String res = response.body();
                                if(res.equals("ok")){
                                    loading.dismiss();
                                    button.setText("Berhasil, Tutup.");
                                    button.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            dialog.dismiss();
                                        }
                                    });
                                }else{
                                    loading.dismiss();
                                    dialog.dismiss();
                                    Toast.makeText(DetailUserActivity.this, "Error : password gagal diubah", Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<String>call, Throwable t) {
                                // Log error here since request failed
                                loading.dismiss();
                            }
                        });
                    }
                });

            }
        });
        ambilUser();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                goToBack();
                break;
            case R.id.action_edit:
                Intent intent = new Intent(this, EditGuruActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    private void goToBack(){
        super.onBackPressed();
    }

    private void ambilUser(){
        final ProgressDialog loading = ProgressDialog.show(DetailUserActivity.this, "", "Loading Data...", false, false);
        final String ip = sessionConfig.preferences.getString("ipconfig",null);

        GetDataService service =
                RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

        HashMap<String, String> param = new HashMap<>();
        param.put("username", sessionManager.preferences.getString("username",null));

        Call<User> call = service.ambilUser(param);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User>call, retrofit2.Response<User> response) {
                Log.e("Hasilnya", "Hasilnya" + response);

                    loading.dismiss();
                    User user = response.body();
                    namaLengkap.setText(user.getNama());
                    Username.setText(user.getUsername());

            }

            @Override
            public void onFailure(Call<User>call, Throwable t) {
                // Log error here since request failed
                loading.dismiss();
                t.printStackTrace();
            }
        });

//        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,"http://"+ip+"/web_absence/assets/file/ambil_user.php?username="+sessionManager.preferences.getString("username",null), null,
//                new Response.Listener<JSONObject>() {
//                    @Override
//                    public void onResponse(JSONObject response) {
//                        Log.e("Hasilnya", "Hasilnya" + response);
//                        try {
//                            loading.dismiss();
//                            namaLengkap.setText(response.getString("nama"));
//                            Username.setText(response.getString("username"));
//                        } catch (JSONException e) {
//                            e.printStackTrace();
//
//                        }
//                    }
//                }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                loading.dismiss();
//                Log.e("error", "errornya" + error);
//            }
//        });
//        MySingleton.getmInstance(this).addToRequestque(request);
    }

}
