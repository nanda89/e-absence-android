package alriyat.absensielektronik.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;

public class SettingConfigActivity extends AppCompatActivity {
    private EditText configManual;
    private Button btnComfig;
    private SessionConfig sessionConfig;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hal_konfigurasi_ip);
        sessionConfig = new SessionConfig(this);
        final String ip = sessionConfig.preferences.getString("ipconfig",null);
        configManual = (EditText)findViewById(R.id.config_ip_manual);
        btnComfig = (Button)findViewById(R.id.btnConfig);
        configManual.setText(ip);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_konfigurasi);
        toolbar.setTitle("Konfigurasi IP Address");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        btnComfig.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sessionConfig.setLogin(true, configManual.getText().toString());
                final AlertDialog.Builder bdialog;
                bdialog = new AlertDialog.Builder(SettingConfigActivity.this);
                bdialog.setCancelable(true);
                bdialog.setMessage("IP berhasil di konfigurasi ulang, IP baru : "+configManual.getText().toString());
                bdialog.setPositiveButton("OKE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent n = new Intent(SettingConfigActivity.this,AbsensiActivity.class);
                        startActivity(n);

                    }
                });
                bdialog.show();
            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                goToBack();
                break;

        }
        return super.onOptionsItemSelected(item);
    }
    private void goToBack(){
        super.onBackPressed();
    }
}
