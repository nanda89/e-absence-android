package alriyat.absensielektronik.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import alriyat.absensielektronik.app.AppController;
import alriyat.absensielektronik.app.MySingleton;
import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.app.Config;
import alriyat.absensielektronik.model.GuruModel;
import alriyat.absensielektronik.model.NonGuruModel;
import alriyat.absensielektronik.util.Vira;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditGuruActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.etNik) EditText etNik;
    @BindView(R.id.etNama) EditText etNama;
    @BindView(R.id.etNoKtp) EditText etNoKtp;
    @BindView(R.id.etNoHp) EditText etNoHp;
    @BindView(R.id.etEmail) EditText etEmail;
    @BindView(R.id.etAlamat) EditText etAlamat;
    @BindView(R.id.etGambar)
    ImageView etGambar;
    @BindView(R.id.etTake)
    Button etTake;

    private SessionManager sessionManager;
    private SessionConfig sessionConfig;

    private String nik, nama, no_ktp, no_hp, email, alamat, id_guru;
    private Uri mCropImageUri;
    private boolean isGuru;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_guru);
        ButterKnife.bind(this);

        toolbar.setTitle("Edit GuruOffline");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sessionManager = new SessionManager(this);
        sessionConfig = new SessionConfig(this);
        isGuru = sessionManager.preferences.getString("isGuru",null).equals("Y") ? true : false;
        if(isGuru){
            id_guru = sessionManager.preferences.getString("idguru",null);
        }else{
            id_guru = sessionManager.preferences.getString("idnonguru",null);
        }

        sessionConfig = new SessionConfig(this);
        doGetDetailGuru();

        etTake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (CropImage.isExplicitCameraPermissionRequired(EditGuruActivity.this)) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
                } else {
                    CropImage.startPickImageActivity(EditGuruActivity.this);
                }
            }
        });
    }

    @OnClick(R.id.txtUpdate)
    protected void updateClicked() {
        nik = etNik.getText().toString();
        nama = etNama.getText().toString();
        no_ktp = etNoKtp.getText().toString();
        no_hp = etNoHp.getText().toString();
        email = etEmail.getText().toString();
        alamat = etAlamat.getText().toString();
        fetchDataFromServer();
    }


    private void doGetDetailGuru() {
        final String ip = sessionConfig.preferences.getString("ipconfig",null);
        final ProgressDialog loading = ProgressDialog.show(this,"","Loading Data...",false,false);
        if(isGuru){
            setGuru(ip,loading);
        }else {
            setNonGuru(ip,loading);
        }
        
    }

    private void setNonGuru(String ip, final ProgressDialog loading) {
        StringRequest sr = new StringRequest(Request.Method.POST,
                "http://"+ip+ "/web_absence/api/non_guru_api/profil", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loading.dismiss();
                Vira.showLogContent(response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    NonGuruModel model = new NonGuruModel(jsonObject.getJSONObject("resources"));
                    etNoHp.setText(model.getNo_hp());
                    etNama.setText(model.getNama());
                    etAlamat.setText(model.getAlamat());
                    etEmail.setText(model.getEmail());
                    etNik.setText(model.getNip());
                    etNoKtp.setText(model.getNo_ktp());
                    Picasso.with(EditGuruActivity.this)
                            .load(model.getUrl_photo())
                            .placeholder(R.drawable.siswa).memoryPolicy(MemoryPolicy.NO_CACHE)
                            .into(etGambar);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_non_guru", id_guru);
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(30 * 10000, 1, 1.0f));
        AppController.getInstance().addToRequestQueue(sr);
    }

    private void setGuru(String ip,final ProgressDialog loading) {
        StringRequest sr = new StringRequest(Request.Method.POST,
                "http://"+ip+ Config.PROFILE_GURU, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loading.dismiss();
                Vira.showLogContent(response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    GuruModel model = new GuruModel(jsonObject.getJSONObject("resources"));
                    etNoHp.setText(model.getNo_hp());
                    etNama.setText(model.getNama());
                    etAlamat.setText(model.getAlamat());
                    etEmail.setText(model.getEmail());
                    etNik.setText(model.getNik());
                    etNoKtp.setText(model.getNo_ktp());
                    Picasso.with(EditGuruActivity.this)
                            .load(model.getUrl_photo())
                            .placeholder(R.drawable.siswa).memoryPolicy(MemoryPolicy.NO_CACHE)
                            .into(etGambar);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_guru", id_guru);
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(30 * 10000, 1, 1.0f));
        AppController.getInstance().addToRequestQueue(sr);
    }

    private void fetchDataFromServer() {
        final String ip = sessionConfig.preferences.getString("ipconfig",null);
        final ProgressDialog loading = ProgressDialog.show(this,"","Loading Data...",false,false);

        if(isGuru){
            saveToGuru(ip , loading);
        }else {
            saveToNonGuru(ip , loading);
        }

    }

    private void saveToNonGuru(String ip, final ProgressDialog loading) {
        StringRequest sr = new StringRequest(Request.Method.POST,
                "http://"+ip+ "/web_absence/api/non_guru_api/save", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loading.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    if(status.equalsIgnoreCase("success")) {
                        Toast.makeText(EditGuruActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(EditGuruActivity.this, DetailUserActivity.class);
                        startActivity(intent);
                        finish();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("editguru", error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_non_guru", id_guru);
                params.put("nip", nik);
                params.put("nama", nama);
                params.put("no_ktp", no_ktp);
                params.put("no_hp", no_hp);
                params.put("email", email);
                params.put("alamat", alamat);

                String encodedImage = null;

                if (etGambar.getDrawable() != null) {
                    Bitmap bm = ((BitmapDrawable) etGambar.getDrawable()).getBitmap();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                    byte[] b = baos.toByteArray();

                    encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

                }

                params.put("gambar", encodedImage);

                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(30 * 10000, 1, 1.0f));
        MySingleton.getmInstance(this).addToRequestque(sr);
    }

    private void saveToGuru(String ip,final ProgressDialog loading) {
        StringRequest sr = new StringRequest(Request.Method.POST,
                "http://"+ip+ Config.EDIT_GURU, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loading.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    if(status.equalsIgnoreCase("success")) {
                        Toast.makeText(EditGuruActivity.this, "Success", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(EditGuruActivity.this, DetailUserActivity.class);
                        startActivity(intent);
                        finish();

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
                Log.d("editguru", error.getMessage());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_guru", id_guru);
                params.put("nik", nik);
                params.put("nama", nama);
                params.put("no_ktp", no_ktp);
                params.put("no_hp", no_hp);
                params.put("email", email);
                params.put("alamat", alamat);

                String encodedImage = null;

                if (etGambar.getDrawable() != null) {
                    Bitmap bm = ((BitmapDrawable) etGambar.getDrawable()).getBitmap();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                    byte[] b = baos.toByteArray();

                    encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

                }

                params.put("gambar", encodedImage);

                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(30 * 10000, 1, 1.0f));
        MySingleton.getmInstance(this).addToRequestque(sr);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                goToBack();
                break;

        }
        return super.onOptionsItemSelected(item);
    }
    private void goToBack(){
        super.onBackPressed();
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .start(this);
    }

    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            mCropImageUri = imageUri;
            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},   CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            } else {
                // no permissions required or already granted, can start crop image activity
                startCropImageActivity(mCropImageUri);
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                etGambar.setImageURI(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.startPickImageActivity(this);
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // required permissions granted, start crop image activity
                startCropImageActivity(mCropImageUri);
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
    }
}
