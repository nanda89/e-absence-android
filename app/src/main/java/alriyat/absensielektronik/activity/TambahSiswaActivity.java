package alriyat.absensielektronik.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.TambahSiswa;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.GET;

public class TambahSiswaActivity extends AppCompatActivity {

    private EditText etTahunAjaran;
    private EditText etJenjang;
    private EditText etKelas;
    private EditText etTglDaftar;
    private EditText etNoInduk;
    private EditText etNisn;
    private EditText etNamaLengkap;
    private Spinner sJenisKelamin;
    private EditText etTempatLahir;
    private EditText etTglLahir;
    private Spinner sAgama;
    private EditText etNoHp;
    private EditText etAlamat;
    private EditText etEmail;
    private EditText etAsalSekolah;
    private EditText etKetAsal;
    private EditText etNamaAyah;
    private EditText etNamaIbu;
    private ImageView ivPasFoto;
    private Button bAmbilFoto;
    private Button bTambah;

    private SessionConfig sessionConfig;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_siswa);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Tambah Siswa");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etTahunAjaran = findViewById(R.id.tahun_ajaran);
        etJenjang = findViewById(R.id.jenjang);
        etKelas = findViewById(R.id.kelas);
        etTglDaftar = findViewById(R.id.tgl_daftar);
        etNoInduk = findViewById(R.id.no_induk);
        etNisn = findViewById(R.id.nisn);
        etNamaLengkap = findViewById(R.id.nama_lengkap);
        sJenisKelamin = findViewById(R.id.jenis_kelamin);
        etTempatLahir = findViewById(R.id.tempat_lahir);
        etTglLahir = findViewById(R.id.tgl_lahir);
        sAgama = findViewById(R.id.agama);
        etNoHp = findViewById(R.id.no_hp);
        etAlamat = findViewById(R.id.alamat);
        etEmail = findViewById(R.id.email);
        etAsalSekolah = findViewById(R.id.asal_sekolah);
        etKetAsal = findViewById(R.id.ket_asal);
        etNamaAyah = findViewById(R.id.nama_ayah);
        etNamaIbu = findViewById(R.id.nama_ibu);
        ivPasFoto = findViewById(R.id.pas_foto);
        bAmbilFoto = findViewById(R.id.ambil_foto);
        bTambah = findViewById(R.id.tambah);

        sessionConfig = new SessionConfig(this);
        sessionManager = new SessionManager(this);

//create a list of items for the spinner.
        String[] listJenisKelamin = new String[]{"Laki-Laki", "Perempuan"};
//create an adapter to describe how the items are displayed, adapters are used in several places in android.
//There are multiple variations of this, but this is the basic variant.
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, listJenisKelamin);
//set the spinners adapter to the previously created one.
        sJenisKelamin.setAdapter(adapter);

        //create a list of items for the spinner.
        String[] listAgama = new String[]{"Islam", "Kristen", "Katholik", "Hindu", "Buddha", "Konghucu", "Tidak Diisi", "Lainnya"};
//create an adapter to describe how the items are displayed, adapters are used in several places in android.
//There are multiple variations of this, but this is the basic variant.
        adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, listAgama);
//set the spinners adapter to the previously created one.
        sAgama.setAdapter(adapter);

        etTahunAjaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TambahSiswaActivity.this, PilihTahunajaranActivity.class);
                startActivity(intent);
            }
        });

        etJenjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TambahSiswaActivity.this, PilihJenjangActivity.class);
                startActivity(intent);
            }
        });

        etKelas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getSharedPreferences("tambah_siswa", Context.MODE_PRIVATE);
                String idJenjang = sharedPreferences.getString("id_jenjang", "");

                if (idJenjang.isEmpty()) {
                    Toast.makeText(TambahSiswaActivity.this, "Jenjang kosong", Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent intent = new Intent(TambahSiswaActivity.this, PilihKelasActivity.class);
                intent.putExtra("id_jenjang", idJenjang);
                intent.putExtra("status", "wali_kelas");
                startActivity(intent);
            }
        });

        final Calendar calendarTglDaftar = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener dateTglDaftar = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendarTglDaftar.set(Calendar.YEAR, year);
                calendarTglDaftar.set(Calendar.MONTH, monthOfYear);
                calendarTglDaftar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "dd-MM-yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                etTglDaftar.setText(sdf.format(calendarTglDaftar.getTime()));
            }

        };

        etTglDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(TambahSiswaActivity.this, dateTglDaftar, calendarTglDaftar
                        .get(Calendar.YEAR), calendarTglDaftar.get(Calendar.MONTH),
                        calendarTglDaftar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final Calendar calendarTglLahir = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener dateTglLahir = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendarTglLahir.set(Calendar.YEAR, year);
                calendarTglLahir.set(Calendar.MONTH, monthOfYear);
                calendarTglLahir.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                String myFormat = "dd-MM-yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

                etTglLahir.setText(sdf.format(calendarTglLahir.getTime()));
            }

        };

        etTglLahir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(TambahSiswaActivity.this, dateTglLahir, calendarTglLahir
                        .get(Calendar.YEAR), calendarTglLahir.get(Calendar.MONTH),
                        calendarTglLahir.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        bAmbilFoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CropImage.activity()
                        .setGuidelines(CropImageView.Guidelines.ON)
                        .start(TambahSiswaActivity.this);
            }
        });

        bTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences sharedPreferences = getSharedPreferences("tambah_siswa", Context.MODE_PRIVATE);
                String idTa = sharedPreferences.getString("id_ta", "");
                String idJenjang = sharedPreferences.getString("id_jenjang", "");
                String idKelas = sharedPreferences.getString("id_kelas", "");
                String tglDaftar = etTglDaftar.getText().toString();
                String noInduk = etNoInduk.getText().toString();
                String nisn = etNisn.getText().toString();
                String namaLengkap = etNamaLengkap.getText().toString();
                String jenisKelamin = sJenisKelamin.getSelectedItem().toString();
                String tempatLahir = etTempatLahir.getText().toString();
                String tglLahir = etTglLahir.getText().toString();
                String agama = sAgama.getSelectedItem().toString();
                String noHp = etNoHp.getText().toString();
                String alamat = etAlamat.getText().toString();
                String email = etEmail.getText().toString();
                String asalSekolah = etAsalSekolah.getText().toString();
                String ketAsal = etKetAsal.getText().toString();
                String namaAyah = etNamaAyah.getText().toString();
                String namaIbu = etNamaIbu.getText().toString();

                Bitmap bitmap = ((BitmapDrawable) ivPasFoto.getDrawable()).getBitmap();
                ByteArrayOutputStream stream=new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 10, stream);
                byte[] image=stream.toByteArray();

                String pasFoto = Base64.encodeToString(image, Base64.DEFAULT);

                if (idTa.isEmpty()) {
                    Toast.makeText(TambahSiswaActivity.this, "Tahun ajaran kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (idJenjang.isEmpty()) {
                    Toast.makeText(TambahSiswaActivity.this, "Jenjang kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (idKelas.isEmpty()) {
                    Toast.makeText(TambahSiswaActivity.this, "Kelas kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (tglDaftar.isEmpty()) {
                    Toast.makeText(TambahSiswaActivity.this, "Tgl. daftar kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (noInduk.isEmpty()) {
                    Toast.makeText(TambahSiswaActivity.this, "No. induk kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (nisn.isEmpty()) {
                    Toast.makeText(TambahSiswaActivity.this, "Nisn kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (namaLengkap.isEmpty()) {
                    Toast.makeText(TambahSiswaActivity.this, "Nama lengkap kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (jenisKelamin.isEmpty()) {
                    Toast.makeText(TambahSiswaActivity.this, "Jenis kelamin kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (tempatLahir.isEmpty()) {
                    Toast.makeText(TambahSiswaActivity.this, "Tempat lahir kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (tglLahir.isEmpty()) {
                    Toast.makeText(TambahSiswaActivity.this, "Tgl. lahir kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (agama.isEmpty()) {
                    Toast.makeText(TambahSiswaActivity.this, "Agama kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (noHp.isEmpty()) {
                    Toast.makeText(TambahSiswaActivity.this, "No. hp kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (alamat.isEmpty()) {
                    Toast.makeText(TambahSiswaActivity.this, "Alamat kosong", Toast.LENGTH_SHORT).show();
                    return;
                }

                GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

                Map<String, String> param = new HashMap<>();
                param.put("id_ta", idTa);
                param.put("id_jenjang", idJenjang);
                param.put("id_kelas", idKelas);
                param.put("tgl_daftar", tglDaftar);
                param.put("no_induk", noInduk);
                param.put("nisn", nisn);
                param.put("nama_lengkap", namaLengkap);
                param.put("jenis_kelamin", jenisKelamin);
                param.put("tempat_lahir", tempatLahir);
                param.put("tgl_lahir", tglLahir);
                param.put("agama", agama);
                param.put("no_hp", noHp);
                param.put("alamat", alamat);
                param.put("email", email);
                param.put("asal_sekolah", asalSekolah);
                param.put("ket_asal", ketAsal);
                param.put("nama_ayah", namaAyah);
                param.put("nama_ibu", namaIbu);
                param.put("pas_foto", pasFoto);

                Call<TambahSiswa> call = service.tambahSiswa(param);

                call.enqueue(new Callback<TambahSiswa>() {
                    @Override
                    public void onResponse(Call<TambahSiswa> call, Response<TambahSiswa> response) {
                        TambahSiswa tambahSiswa = response.body();

                        if (!tambahSiswa.getStatus().equals("ok")) {
                            Toast.makeText(TambahSiswaActivity.this, tambahSiswa.getStatus(), Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(TambahSiswaActivity.this, "Berhasil", Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<TambahSiswa> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
            }
        });

        ActivityCompat.requestPermissions(TambahSiswaActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, 1);


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                ivPasFoto.setImageURI(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    @Override
    protected void onResume() {
        SharedPreferences sharedPreferences = getSharedPreferences("tambah_siswa", Context.MODE_PRIVATE);
        etJenjang.setText(sharedPreferences.getString("jenjang", ""));
        etKelas.setText(sharedPreferences.getString("kelas", ""));
        etTahunAjaran.setText(sharedPreferences.getString("tahunajaran", ""));
        super.onResume();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
