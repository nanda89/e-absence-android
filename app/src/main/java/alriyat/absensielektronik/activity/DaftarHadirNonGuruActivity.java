package alriyat.absensielektronik.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.SettingOffline;
import alriyat.absensielektronik.pojo.Kegiatan;
import alriyat.absensielektronik.pojo.LaporanNonGuruHarian;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import alriyat.absensielektronik.util.Vira;
import retrofit2.Call;
import retrofit2.Callback;

public class DaftarHadirNonGuruActivity extends AppCompatActivity {

    private static final String TAG = DaftarHadirActivity.class.getName();
    private EditText etDari;
    private EditText etKe;
    private Button bFilter, btnViewPdf;
    private RadioGroup radioGroup;
    private RadioButton radioButtonShow, radioButtonHide;
    private RecyclerView recyclerView;
    private SessionConfig sessionConfig;
    private SessionManager sessionManager;
    private String id;
    private LaporanAdapterNonGuru adapter;
    ArrayList<LaporanNonGuruHarian> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_hadir_non_guru);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_laporan_daftar_hadir);
        toolbar.setTitle("Laporan Daftar Hadir");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        etDari = findViewById(R.id.start_date_daftar_hadir);
        etKe = findViewById(R.id.end_date_daftar_hadir);
        bFilter = findViewById(R.id.bt_filter_daftar_hadir);
        btnViewPdf = findViewById(R.id.btnDetail_lap_harian);
        recyclerView = findViewById(R.id.recycler_view_lap_harian);
        radioGroup = findViewById(R.id.showPhotoGroup);
        radioButtonShow = findViewById(R.id.showPhoto);
        radioButtonHide = findViewById(R.id.hidePhoto);

        sessionConfig = new SessionConfig(this);
        sessionManager = new SessionManager(this);
        id = sessionManager.preferences.getString("idnonguru", null);

        setDatePicker();

        bFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dari = etDari.getText().toString();
                String ke = etKe.getText().toString();

                if (dari.isEmpty()) {
                    Toast.makeText(DaftarHadirNonGuruActivity.this, "Tanggal awal tidak boleh kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (ke.isEmpty()) {
                    Toast.makeText(DaftarHadirNonGuruActivity.this, "Tanggal akhir tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                    return;
                }
                lapHarian();
            }
        });

        adapter = new LaporanAdapterNonGuru(arrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Vira.isConnectedToServer(getApplicationContext())) {
            bFilter.setClickable(true);
        } else {
            bFilter.setClickable(false);
            Toast.makeText(this, "Anda tidak terhubung ke server", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void lapHarian() {
        if (Vira.isConnectedToServer(getApplicationContext())) {
            final ProgressDialog loading = ProgressDialog.show(this, "", "Loading Data...", false, false);
            arrayList.clear();

            final GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            final Map<String, String> params = new HashMap<String, String>();
            params.put("tgl", etDari.getText().toString());
            params.put("tgl_akhir", etKe.getText().toString());
            params.put("id_non_guru", id);

            Log.d(TAG, "lapHarian: parameter : \n" +
                    "id_non_guru : " + id + "\n" +
                    "tgl : " + etDari.getText().toString() + "\n" +
                    "tgl_akhir : " + etKe.getText().toString() + "\n" +
                    "");

            Call<List<LaporanNonGuruHarian>> call = service.laporan_harian(params);
            call.enqueue(new Callback<List<LaporanNonGuruHarian>>() {

                @Override
                public void onResponse(Call<List<LaporanNonGuruHarian>> call, retrofit2.Response<List<LaporanNonGuruHarian>> response) {
                    loading.dismiss();
                    final List<LaporanNonGuruHarian> laporanHarianGuru = response.body();

                    for (int i = 0; i < laporanHarianGuru.size(); i++) {
                        arrayList.add(laporanHarianGuru.get(i));
                    }

                    adapter.notifyDataSetChanged();
                    if (laporanHarianGuru.size() > 0) {
                        radioGroup.setVisibility(View.VISIBLE);
                        btnViewPdf.setVisibility(View.VISIBLE);
                        btnViewPdf.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                boolean isShow = true;
                                if (radioButtonHide.isChecked()) {
                                    isShow = false;
                                }
                                Log.d(TAG, "onClick: isShow : " + isShow);
                                List<Kegiatan> kegiatans = getKegiatan(service, params);
                                buatPdf(laporanHarianGuru, kegiatans, etDari.getText().toString(), etKe.getText().toString(), isShow);
                            }
                        });

                        Toast.makeText(DaftarHadirNonGuruActivity.this, "Sukses", Toast.LENGTH_SHORT).show();
                    } else {
                        radioGroup.setVisibility(View.GONE);
                        btnViewPdf.setVisibility(View.GONE);
                        Toast.makeText(DaftarHadirNonGuruActivity.this, "Data tidak ditemukan", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<List<LaporanNonGuruHarian>> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    Toast.makeText(DaftarHadirNonGuruActivity.this, t.toString(), Toast.LENGTH_LONG).show();
                }


            });
        } else {
            Toast.makeText(this, "Pencarian gagal, anda tidak terhubung dengan server", Toast.LENGTH_SHORT).show();
        }
    }

    private List<Kegiatan> getKegiatan(GetDataService service, Map<String, String> params) {
        final ProgressDialog loading = ProgressDialog.show(this, "", "Loading Data...", false, false);

        final List<Kegiatan> listKegiatan = new ArrayList<>();

        Call<List<Kegiatan>> call = service.daftarKegiatan(params);
        call.enqueue(new Callback<List<Kegiatan>>() {

            @Override
            public void onResponse(Call<List<Kegiatan>> call, retrofit2.Response<List<Kegiatan>> response) {
                loading.dismiss();
                List<Kegiatan> data = response.body();
                if (data.size() > 0) {
                    for (Kegiatan k : data) {
                        listKegiatan.add(k);
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Kegiatan>> call, Throwable t) {
                loading.dismiss();
                return;
            }
        });
        return listKegiatan;
    }

    private void buatPdf(final List<LaporanNonGuruHarian> laporanKegiatanNonGuru, final List<Kegiatan> kegiatans, final String startDate, final String endDate, final boolean isShow) {
        SessionConfig sessionConfig = new SessionConfig(DaftarHadirNonGuruActivity.this);
        final String ip = sessionConfig.preferences.getString("ipconfig", null);

        GetDataService service =
                RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

        HashMap<String, String> param = new HashMap<>();
        param.put("id_guru", "");

        Call<List<SettingOffline>> call = service.syncUpdateTabelSetting(param);
        call.enqueue(new Callback<List<SettingOffline>>() {
            @Override
            public void onResponse(Call<List<SettingOffline>> call, retrofit2.Response<List<SettingOffline>> response) {
//                    loading.dismiss();
                List<SettingOffline> listSettingOffline = response.body();
                SettingOffline setting = new SettingOffline();

                for (SettingOffline settingOffline : listSettingOffline) {
                    setting.setIdSetting(settingOffline.getIdSetting());
                    setting.setLogoMini(settingOffline.getLogoMini());
                    setting.setLogoGambar(settingOffline.getLogoGambar());
                    setting.setLogoFavicon(settingOffline.getLogoFavicon());
                    setting.setNik(settingOffline.getNik());
                    setting.setKartuGsm(settingOffline.getKartuGsm());
                    setting.setCekPulsa(settingOffline.getCekPulsa());
                    setting.setMetode(settingOffline.getMetode());
                    setting.setKodeAktivasi(settingOffline.getKodeAktivasi());
                    setting.setLinkTujuan(settingOffline.getLinkTujuan());
                    setting.setLogoBesar(settingOffline.getLogoBesar());
                    setting.setLogoSekolah(settingOffline.getLogoSekolah());
                    setting.setPimpinan(settingOffline.getPimpinan());
                    setting.setAkunGmail(settingOffline.getAkunGmail());
                    setting.setAlamat(settingOffline.getAlamat());
                    setting.setWebsite(settingOffline.getWebsite());
                    setting.setKopSurat(settingOffline.getKopSurat());
                    setting.setTtd(settingOffline.getTtd());
                    setting.setTelat(settingOffline.getTelat());
                    setting.setToleransiJamAkhir(settingOffline.getToleransiJamAkhir());
                    setting.setLat(settingOffline.getLat());
                    setting.setLng(settingOffline.getLng());
                    setting.setRadius(settingOffline.getRadius());
                    setting.setJamPulang(settingOffline.getJamPulang());
                    setting.setKota(settingOffline.getKota());
                }

                Document document = new Document();
                document.setPageSize(PageSize.LEGAL.rotate());
                List<String> data = checkDate(laporanKegiatanNonGuru);

                // Creating an ImageData object
                String imFile = "http://" + ip + "/web_absence/assets/panel/images/" + setting.getKopSurat();

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

                StrictMode.setThreadPolicy(policy);

                Image image = null;
                try {
                    image = Image.getInstance(imFile);
                } catch (BadElementException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                image.scaleAbsolute(document.getPageSize().getWidth() - document.leftMargin(), 150f);

                PdfPTable table = new PdfPTable(new float[]{1, 2, 2, 1, 1, 1, 1, 3, 3});
                table.setTotalWidth(document.getPageSize().getWidth() - document.leftMargin() * 2);
                table.setLockedWidth(true);
                table.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell("NO");
                table.addCell("NAMA NON GURU");
                table.addCell("TANGGAL");
                table.addCell("JAM MASUK");
                table.addCell("JAM PULANG");
                table.addCell("VAL.JAM MASUK");
                table.addCell("VAL.JAM PULANG");
                table.addCell("KET.JAM MASUK");
                table.addCell("KET.JAM PULANG");
                table.setHeaderRows(1);

                PdfPCell[] cells = table.getRow(0).getCells();
                for (int j = 0; j < cells.length; j++) {
                    cells[j].setBackgroundColor(BaseColor.GRAY);
                }
                Log.d(TAG, "onResponse: isShow : " + isShow);
                float kolom[] = new float[0];
                if(isShow){
                    kolom = new float[7];
                }else{
                    kolom = new float[6];
                }

                kolom[0] = 1;
                kolom[1] = 2;
                kolom[2] = 2;
                kolom[3] = 1;
                kolom[4] = 1;
                kolom[5] = 4;

                if (isShow) {
                    kolom[6] = 4;
                }
                PdfPTable table1 = new PdfPTable(kolom);

                table1.setTotalWidth(document.getPageSize().getWidth() - document.leftMargin() * 2);
                table1.setLockedWidth(true);
                table1.setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell("NO");
                table1.addCell("NAMA");
                table1.addCell("JABATAN");
                table1.addCell("TANGGAL");
                table1.addCell("JAM");
                table1.addCell("KEGIATAN");
                if (isShow) {
                    table1.addCell("FILE");
                }

                table1.setHeaderRows(1);
                PdfPCell[] cells1 = table1.getRow(0).getCells();
                for (int j = 0; j < cells1.length; j++) {
                    cells1[j].setBackgroundColor(BaseColor.GRAY);
                }

                int noKegiatan = 1;
                for (Kegiatan k : kegiatans) {
                    table1.addCell(String.valueOf(noKegiatan));
                    table1.addCell(k.getNama());
                    table1.addCell(k.getJabatan());
                    table1.addCell(k.getTanggal());
                    table1.addCell(k.getJam());
                    table1.addCell(k.getUraian());

                    if (isShow) {
                        String fileKegiatan = "http://" + ip + "/web_absence/assets/file/upload/" + k.getImg();

                        Log.d(TAG, "onResponse: type file : " + k.getType());
                        Log.d(TAG, "onResponse: url : " + fileKegiatan);
                        if (k.getType().equals("p")) {

                            Image imageKegiatan = null;
                            int indentation = 0;
                            float scaler = ((document.getPageSize().getWidth() - document.leftMargin()
                                    - document.rightMargin() - indentation) / image.getWidth()) * 100;
                            try {
                                imageKegiatan = Image.getInstance(fileKegiatan);
                            } catch (BadElementException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            imageKegiatan.scalePercent(scaler);
                            table1.addCell(imageKegiatan);
                        } else {
                            table1.addCell("video");
                        }

                    }
                    noKegiatan += 1;
                }

                SharedPreferences sharedPreferences = getSharedPreferences("rekap_kehadiran_guru", Context.MODE_PRIVATE);
                Calendar calendar = Calendar.getInstance();

                String para = setting.getKota() + ", " + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));
                Paragraph paragraph = new Paragraph(para);
                paragraph.setAlignment(Element.ALIGN_RIGHT);

                para = "                                                                                 Kepala " + setting.getLogoBesar();
                Paragraph paragraph1 = new Paragraph(para);
                paragraph1.setAlignment(Element.ALIGN_RIGHT);

                para = "                                                                                 " + setting.getPimpinan();
                Paragraph paragraph2 = new Paragraph(para);
                paragraph2.setAlignment(Element.ALIGN_RIGHT);

                para = "                                                                                 " + setting.getNik();
                Paragraph paragraph3 = new Paragraph(para);
                paragraph3.setAlignment(Element.ALIGN_RIGHT);

                para = "LAPORAN HARIAN VALIDASI\nKEHADIRAN NON GURU ";
                Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
                Paragraph paragraph4 = new Paragraph(para, boldFont);
                paragraph4.setAlignment(Element.ALIGN_CENTER);

                para = "Dari Tanggal : " + startDate + " s.d " + endDate;
                Paragraph paragraph5 = new Paragraph(para);

                // write the document content
                String directory_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/eAbsence/";
//                    directory_path = RekapKehadiranGuruActivity.this.getFilesDir() + "/mypdf/";
                File file = new File(directory_path);
                if (!file.exists()) {
                    file.mkdirs();
                }
                String targetPdf = directory_path
                        + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))
                        + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1)
                        + "-" + String.format("%04d", calendar.get(Calendar.YEAR))
                        + "_kegiatan_non_guru_"
                        + String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY))
                        + String.format("%02d", calendar.get(Calendar.MINUTE))
                        + ".pdf";
                Log.d(TAG, "onResponse: target --> " + targetPdf);
                File filePath = new File(targetPdf);
                try {
                    PdfWriter.getInstance(document, new FileOutputStream(filePath));
                } catch (DocumentException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                document.open();
                try {


                    for (String tgl : data) {
                        int no = 1;
                        Log.d(TAG, "onResponse: tanggal " + tgl);
                        for (LaporanNonGuruHarian harian : laporanKegiatanNonGuru) {
                            Log.d(TAG, "onResponse: harian : " + harian.getTanggal());
                            if (!harian.getTanggal().equals(tgl)) {
                                continue;
                            }

                            table.addCell(String.valueOf(no));

                            PdfPCell cell = new PdfPCell();
                            Phrase p = new Phrase(harian.getNama());
                            cell.addElement(p);
                            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                            table.addCell(cell);
                            table.addCell(harian.getTanggal());
                            table.addCell(harian.getJam_masuk());
                            table.addCell(harian.getJam_pulang());
                            table.addCell(harian.getValidasi_jam_masuk());
                            table.addCell(harian.getValidasi_jam_pulang());
                            table.addCell(harian.getKet_jam_masuk());
                            table.addCell(harian.getKet_jam_pulang());
                            no += 1;

                        }
                        document.add(image);
                        document.add(paragraph4);
                        document.add(Chunk.NEWLINE);
                        document.add(paragraph5);
                        document.add(Chunk.NEWLINE);
                        document.add(table);
                        document.add(Chunk.NEWLINE);
                        document.add(Chunk.NEWLINE);
                        document.add(paragraph);
                        document.add(paragraph1);
                        document.add(Chunk.NEWLINE);
                        document.add(Chunk.NEWLINE);
                        document.add(Chunk.NEWLINE);
                        document.add(Chunk.NEWLINE);
                        document.add(paragraph2);
                        document.add(paragraph3);
                        table.deleteBodyRows();
                    }

                    document.newPage();
                    document.add(table1);


                } catch (DocumentException e) {
                    e.printStackTrace();
                }
                document.close();
                showData(targetPdf);
            }

            @Override
            public void onFailure(Call<List<SettingOffline>> call, Throwable t) {
                // Log error here since request failed
//                    loading.dismiss();
                t.printStackTrace();
            }
        });
    }

    private void showData(String path) {
        File file = new File(path);
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(file), "application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent intent = Intent.createChooser(target, "Open File");
        try {
            startActivity(intent);
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    private List<String> checkDate(List<LaporanNonGuruHarian> laporanHarians) {

        List<String> data = new ArrayList<>();

        for (LaporanNonGuruHarian h : laporanHarians) {

            if (data.contains(h.getTanggal())) {
                continue;
            }
            data.add(h.getTanggal());
        }
        return data;
    }

    private void setDatePicker() {
        final Calendar calendarDari = Calendar.getInstance();
        final Calendar calendarKe = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener dateDari = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendarDari.set(Calendar.YEAR, year);
                calendarDari.set(Calendar.MONTH, monthOfYear);
                calendarDari.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                etDari.setText(String.format("%02d", calendarDari.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendarDari.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", calendarDari.get(Calendar.YEAR)));
            }
        };
        etDari.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(DaftarHadirNonGuruActivity.this, dateDari, calendarDari
                        .get(Calendar.YEAR), calendarDari.get(Calendar.MONTH),
                        calendarDari.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final DatePickerDialog.OnDateSetListener dateKe = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendarKe.set(Calendar.YEAR, year);
                calendarKe.set(Calendar.MONTH, monthOfYear);
                calendarKe.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                etKe.setText(String.format("%02d", calendarKe.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendarKe.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", calendarKe.get(Calendar.YEAR)));
            }

        };

        etKe.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(DaftarHadirNonGuruActivity.this, dateKe, calendarKe
                        .get(Calendar.YEAR), calendarKe.get(Calendar.MONTH),
                        calendarKe.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }
}

class LaporanAdapterNonGuru extends RecyclerView.Adapter<LaporanAdapterNonGuru.MyViewHolder> {

    private List<LaporanNonGuruHarian> laporanNonGuru;

    public LaporanAdapterNonGuru(List<LaporanNonGuruHarian> lapHarian) {
        this.laporanNonGuru = lapHarian;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView ltanggal, lJamMasuk, lJamPulang, lValJamMasuk, lValJamPulang, lKetJamMasuk, lKetJamPulang;

        public MyViewHolder(View view) {
            super(view);
            ltanggal = (TextView) view.findViewById(R.id.lap_harian_date);
            lJamMasuk = view.findViewById(R.id.lap_harian_jam_masuk);
            lJamPulang = view.findViewById(R.id.lap_harian_jam_pulang);
            lValJamMasuk = view.findViewById(R.id.lap_harian_val_jam_masuk);
            lValJamPulang = view.findViewById(R.id.lap_harian_val_jam_pulang);
            lKetJamMasuk = view.findViewById(R.id.lap_harian_ket_jam_masuk);
            lKetJamPulang = view.findViewById(R.id.lap_harian_ket_jam_pulang);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_laporan_harian_non_guru, parent, false);

        return new LaporanAdapterNonGuru.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        LaporanNonGuruHarian lapHarian = laporanNonGuru.get(position);
        holder.ltanggal.setText(String.valueOf(lapHarian.getTanggal()));
        holder.ltanggal.setText(String.valueOf(lapHarian.getTanggal()));
        holder.lJamMasuk.setText(String.valueOf(lapHarian.getJam_masuk()));
        holder.lJamPulang.setText(String.valueOf(lapHarian.getJam_pulang()));
        holder.lValJamMasuk.setText(String.valueOf(lapHarian.getValidasi_jam_masuk()));
        holder.lValJamPulang.setText(String.valueOf(lapHarian.getValidasi_jam_pulang()));
        holder.lKetJamMasuk.setText(String.valueOf(lapHarian.getKet_jam_masuk()));
        holder.lKetJamPulang.setText(String.valueOf(lapHarian.getKet_jam_pulang()));
    }

    @Override
    public int getItemCount() {
        return laporanNonGuru.size();
    }
}
