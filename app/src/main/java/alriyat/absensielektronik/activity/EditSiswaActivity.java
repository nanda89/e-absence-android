package alriyat.absensielektronik.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.squareup.picasso.MemoryPolicy;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import alriyat.absensielektronik.app.AppController;
import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.app.Config;
import alriyat.absensielektronik.model.SiswaModel;
import alriyat.absensielektronik.util.Vira;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditSiswaActivity extends AppCompatActivity {
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.etNisn) EditText etNisn;
    @BindView(R.id.etNama) EditText etNama;
    @BindView(R.id.etTempatLahir) EditText etTempatLahir;
    @BindView(R.id.etTanggalLahir) EditText etTanggalLahir;
    @BindView(R.id.etAlamat) EditText etAlamat;
    @BindView(R.id.etNamaAyah) EditText etNamaAyah;
    @BindView(R.id.etNamaIbu) EditText etNamaIbu;
    @BindView(R.id.etNoHp) EditText etNoHp;
    @BindView(R.id.etAgama) EditText etAgama;
    @BindView(R.id.etJenisKelamin) EditText etJenisKelamin;
    @BindView(R.id.etGambar)
    ImageView etGambar;
    @BindView(R.id.etTake)
    Button etTake;

    private String idnews, nama_siswa, nisn, nama_ayah, nama_ibu, alamat, tanggal_lahir,
            tempat_lahir, no_hp;
    private SessionConfig sessionConfig;
    private Uri mCropImageUri;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_siswa);
        ButterKnife.bind(this);
        init();
    }

    private void init() {
        toolbar.setTitle("Edit Siswa");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        idnews = getIntent().getStringExtra("idnews");
        sessionConfig = new SessionConfig(this);
        doGetDetailSiswa();

        etTake.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("NewApi")
            @Override
            public void onClick(View v) {
                if (CropImage.isExplicitCameraPermissionRequired(EditSiswaActivity.this)) {
                    requestPermissions(new String[]{Manifest.permission.CAMERA}, CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE);
                } else {
                    CropImage.startPickImageActivity(EditSiswaActivity.this);
                }
            }
        });
    }

    @OnClick(R.id.txtUpdate)
    protected void updateClikced() {
        nama_siswa = etNama.getText().toString();
        nisn = etNisn.getText().toString();
        nama_ayah = etNamaAyah.getText().toString();
        nama_ibu = etNamaIbu.getText().toString();
        alamat = etAlamat.getText().toString();
        tanggal_lahir = etTanggalLahir.getText().toString();
        tempat_lahir = etTempatLahir.getText().toString();
        no_hp = etNoHp.getText().toString();
        fetchDataFromServer();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                goToBack();
                break;

        }
        return super.onOptionsItemSelected(item);
    }
    private void goToBack(){
        super.onBackPressed();
    }

    private void fetchDataFromServer() {
        final String ip = sessionConfig.preferences.getString("ipconfig",null);
        final ProgressDialog loading = ProgressDialog.show(this,"","Loading Data...",false,false);
        StringRequest sr = new StringRequest(Request.Method.POST,
                "http://"+ip+ Config.EDIT_SISWA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loading.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    if(status.equalsIgnoreCase("success")) {
                        Toast.makeText(EditSiswaActivity.this, "Success", Toast.LENGTH_SHORT).show();
//                        Intent intent = new Intent(EditSiswaActivity.this, DetailSiswaActivity.class);
//                        intent.putExtra("idnews", idnews);
//                        intent.putExtra("idkelas", getIntent().getStringExtra("idkelas"));
//                        startActivity(intent);
                        onBackPressed();
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(EditSiswaActivity.this, "Error! " + e.getMessage() +" " + response, Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                loading.dismiss();
                String json = null;
                NetworkResponse response = error.networkResponse;
                if(response != null && response.data != null){
                    json = new String(response.data);
                    Vira.showLogError(json);
                    Toast.makeText(EditSiswaActivity.this, "STATUS CODE " + response.statusCode, Toast.LENGTH_SHORT).show();
                    checkValidation(json);
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_siswa", idnews);
                params.put("nama_siswa", nama_siswa);
                params.put("nisn", nisn);
                params.put("nama_ayah", nama_ayah);
                params.put("nama_ibu", nama_ibu);
                params.put("alamat", alamat);
                params.put("tempat_lahir", tempat_lahir);
                params.put("tanggal_lahir", tanggal_lahir);
                params.put("no_hp", no_hp);

                String encodedImage = null;

                if (etGambar.getDrawable() != null) {
                    Bitmap bm = ((BitmapDrawable) etGambar.getDrawable()).getBitmap();
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, baos); //bm is the bitmap object
                    byte[] b = baos.toByteArray();

                    encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

                }

                params.put("gambar", encodedImage);
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(30 * 10000, 1, 1.0f));
        AppController.getInstance().addToRequestQueue(sr);
    }

    private void checkValidation(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            Toast.makeText(this, "" + jsonObject.getString("status"), Toast.LENGTH_SHORT).show();
            if(Vira.isWebServiceValid(jsonObject, "error")) {
                JSONObject jsonObject1 = jsonObject.getJSONObject("error");
                Toast.makeText(this, "TAMPILKAN PESAN ERROR " + jsonObject1.getString("message"), Toast.LENGTH_SHORT).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void doGetDetailSiswa() {
        final String ip = sessionConfig.preferences.getString("ipconfig",null);
        final ProgressDialog loading = ProgressDialog.show(this,"","Loading Data...",false,false);
        StringRequest sr = new StringRequest(Request.Method.POST,
                "http://"+ip+ Config.PROFILE_SISWA, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Vira.showLogContent(response);
                loading.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    SiswaModel model = new SiswaModel(jsonObject.getJSONObject("resources"));
                    etAgama.setText(model.getAgama());
                    etAlamat.setText(model.getAlamat());
                    etJenisKelamin.setText(model.getJenis_kelamin());
                    etNama.setText(model.getNama_lengkap());
                    etNamaAyah.setText(model.getNama_ayah());
                    etNamaIbu.setText(model.getNama_ibu());
                    etNisn.setText(model.getNisn());
                    etNoHp.setText(model.getNo_hp());
                    etTanggalLahir.setText(model.getTgl_lahir());
                    etTempatLahir.setText(model.getTempat_lahir());
                    Picasso.with(EditSiswaActivity.this)
                            .load(model.getUrl_photo())
                            .placeholder(R.drawable.siswa).memoryPolicy(MemoryPolicy.NO_CACHE)
                            .into(etGambar);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loading.dismiss();
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("id_siswa", idnews);
                return params;
            }
        };
        sr.setRetryPolicy(new DefaultRetryPolicy(30 * 10000, 1, 1.0f));
        AppController.getInstance().addToRequestQueue(sr);
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .start(this);
    }

    @Override
    @SuppressLint("NewApi")
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // handle result of pick image chooser
        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(this, data);
            mCropImageUri = imageUri;
            // For API >= 23 we need to check specifically that we have permissions to read external storage.
            if (CropImage.isReadExternalStoragePermissionsRequired(this, imageUri)) {
                // request permissions and handle the result in onRequestPermissionsResult()
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},   CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
            } else {
                // no permissions required or already granted, can start crop image activity
                startCropImageActivity(mCropImageUri);
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                etGambar.setImageURI(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.startPickImageActivity(this);
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // required permissions granted, start crop image activity
                startCropImageActivity(mCropImageUri);
            } else {
                Toast.makeText(this, "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
    }
}
