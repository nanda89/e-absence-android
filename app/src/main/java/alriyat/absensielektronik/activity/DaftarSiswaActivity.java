package alriyat.absensielektronik.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import alriyat.absensielektronik.app.MySingleton;
import alriyat.absensielektronik.R;
import alriyat.absensielektronik.model.Siswa;

public class DaftarSiswaActivity extends AppCompatActivity {

    private ArrayList<Siswa> siswaList;
    private RecyclerView recyclerView;
    private AdapterSiswa adapterSiswa;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_siswa);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_daftar);
        toolbar.setTitle("Daftar Siswa");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        siswaList = new ArrayList<>();
        recyclerView = (RecyclerView)findViewById(R.id.recycler_siswa);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        siswaList = getAllSiswa();
        adapterSiswa = new AdapterSiswa(this,siswaList);
        recyclerView.setAdapter(adapterSiswa);

    }

    private ArrayList<Siswa> getAllSiswa(){
        final ProgressDialog loading = ProgressDialog.show(this,"","Loading Data...",false,false);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://10.0.3.2/Android-Absensi/daftar_siswa.php",null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        loading.dismiss();
                        Log.e("Haasil", "array" + siswaList);
                        int count=0;
                        while (count < response.length()){
                            try {
                                JSONObject object = response.getJSONObject(count);
                                Siswa siswa = new Siswa();
                                siswa.setNis(object.getString("nis"));
                                siswa.setNama(object.getString("nama"));
                                siswa.setGmbr(object.getString("img"));
                                siswaList.add(siswa);
                                Log.e("Haasil", "array" + siswaList);
                                count++;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        adapterSiswa.notifyDataSetChanged();

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loading.dismiss();
                        Log.e("Haasil", "array" + error);
                        Toast.makeText(DaftarSiswaActivity.this, "Failed, check internet connection...", Toast.LENGTH_SHORT).show();
                    }
                });

        //Creating a request queue
        RequestQueue requestQueue = Volley.newRequestQueue(this);

        //Adding request to the queue
        requestQueue.add(jsonArrayRequest);
        return siswaList;
    }

    class AdapterSiswa extends RecyclerView.Adapter<AdapterSiswa.MyViewHolder>{
        private ImageLoader imageLoader;
        private Context mContext;
        private ArrayList<Siswa> siswalist;
        final String[] ketHadir={"Izin","Hadir","Sakit","tanpa keterangan"};


        public AdapterSiswa(Context mContext, ArrayList<Siswa> siswalist) {
            this.mContext = mContext;
            this.siswalist = siswalist;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder{
            public TextView nis,nama;
            public NetworkImageView gmbr;
            public Spinner spinner;

            public MyViewHolder(View itemView) {
                super(itemView);
                nis = (TextView)itemView.findViewById(R.id.nis);
                nama = (TextView)itemView.findViewById(R.id.nm_lengkap);
              //  gmbr = (NetworkImageView)itemView.findViewById(R.id.img_siswa);
                spinner = (Spinner)itemView.findViewById(R.id.kehadiran);
                spinner.setAdapter(new ArrayAdapter<String>(DaftarSiswaActivity.this, R.layout.spinner_item, ketHadir));
            }
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_siswa,parent,false);
            return new MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            //Buku buku = bukuList.get(position);
            holder.nis.setText(siswalist.get(position).getNis());
            holder.nama.setText(siswalist.get(position).getNama());
            imageLoader = MySingleton.getmInstance(mContext).getImageLoader();
            Picasso.with(mContext).load("http://10.0.3.2/Android-Absensi/img/"+siswalist.get(position).getGmbr()).into(holder.gmbr);
            holder.gmbr.setImageUrl(siswalist.get(position).getGmbr(),imageLoader);

        }

        @Override
        public int getItemCount() {
            return siswalist.size();
        }


    }
}
