package alriyat.absensielektronik.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.TextView;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import alriyat.absensielektronik.util.Vira;
import butterknife.BindView;
import butterknife.ButterKnife;


public class SplashActivity extends Activity {
    private static final String TAG = SplashActivity.class.getName();
    @BindView(R.id.circle_progress_bar)
    ProgressBar mProgressBar;
    @BindView(R.id.txtValuePercentage)
    TextView txtValuePercentage;

    private final int SPLASH_DISPLAY_LENGTH = 3600;
    private final int SPLASH_DISPLAY_SHORT = 3600;
    private Handler handler = new Handler();
    private String strValue;
    private SessionManager sessionManager;
    private SessionConfig sessionConfig;
    String ip;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        sessionManager = new SessionManager(this);
        sessionConfig = new SessionConfig(this);
        ip = sessionConfig.preferences.getString("ipconfig", null);
        Log.d(TAG, "run: IP Address "+ip);
        final GetDataService service =
                RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
        boolean isConnected = Vira.pingToApp(ip,service,sessionManager);
        Log.d(TAG, "run: isConnected : "+isConnected);
        sessionManager.isConnected(isConnected);
        init();
    }

    private void init() {
        ConnectivityManager conMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (conMgr.getActiveNetworkInfo() == null) {
            new Thread(new Runnable() {
                int i = 0;
                int progressStatus = 0;

                public void run() {
                    while (progressStatus < 100) {
                        progressStatus += 2;
                        //progressStatus = fileDownloadStatus();
                        strValue = String.valueOf(progressStatus);
                        txtValuePercentage.post(new Runnable() {
                            @Override
                            public void run() {
                                txtValuePercentage.setText(strValue);
                            }
                        });
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        handler.post(new Runnable() {
                            public void run() {
                                mProgressBar
                                        .setProgress(progressStatus);
                                i++;
                            }
                        });
                    }
                }
            }).start();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashActivity.this, SettingConfigFirstActivity.class);
                    SplashActivity.this.startActivity(intent);
                    SplashActivity.this.finish();
                }
            }, SPLASH_DISPLAY_SHORT);

        } else {
            new Thread(new Runnable() {
                int i = 0;
                int progressStatus = 0;

                public void run() {
                    while (progressStatus < 100) {
                        progressStatus += 2;
                        //progressStatus = fileDownloadStatus();
                        strValue = String.valueOf(progressStatus);
                        txtValuePercentage.post(new Runnable() {
                            @Override
                            public void run() {
                                txtValuePercentage.setText(strValue);
                            }
                        });
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }

                        // Update the progress bar
                        handler.post(new Runnable() {
                            public void run() {
                                mProgressBar
                                        .setProgress(progressStatus);
                                i++;
                            }
                        });
                    }
                }
            }).start();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(SplashActivity.this, SettingConfigFirstActivity.class);
                    SplashActivity.this.startActivity(intent);
                    SplashActivity.this.finish();
                }
            }, SPLASH_DISPLAY_LENGTH);
        }
    }
}
