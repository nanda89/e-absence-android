package alriyat.absensielektronik.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.SettingOffline;
import alriyat.absensielektronik.pojo.LaporanHarian;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import alriyat.absensielektronik.util.Vira;
import retrofit2.Call;
import retrofit2.Callback;

public class LaporanHarianActivity extends AppCompatActivity {

    private static final String TAG = LaporanHarianActivity.class.getName();
    private EditText etDari;
    private EditText etKe;
    private EditText etMapel;
    private Button bFilter,btnLapHarian;
    private RecyclerView recyclerView;
    private SessionConfig sessionConfig;
    private SessionManager sessionManager;
    private String id_guru;
    private LaporanAdapter adapter;
    ArrayList<LaporanHarian> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan_harian);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_laporan_harian);
        toolbar.setTitle("Laporan Harian");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        etDari = findViewById(R.id.start_date_lap_harian);
        etKe = findViewById(R.id.end_date_lap_harian);
        etMapel = findViewById(R.id.mapel_lap_harian);
        bFilter = findViewById(R.id.bt_filter_lap_harian);
        btnLapHarian = findViewById(R.id.btnDetail_lap_harian);
        recyclerView = findViewById(R.id.recycler_view_lap_harian);

        sessionConfig = new SessionConfig(this);
        sessionManager = new SessionManager(this);
        id_guru = sessionManager.preferences.getString("username", null);

        setDatePicker();
        getAllMapel();

        bFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dari = etDari.getText().toString();
                String ke = etKe.getText().toString();

                if (dari.isEmpty()) {
                    Toast.makeText(LaporanHarianActivity.this, "Tanggal awal tidak boleh kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (ke.isEmpty()) {
                    Toast.makeText(LaporanHarianActivity.this, "Tanggal akhir tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                    return;
                }
                lapHarian();
            }
        });

        adapter = new LaporanAdapter(arrayList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
//        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

        SharedPreferences sharedPreferences = getSharedPreferences("matapelajaran", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("id_mapel", "");
        editor.putString("mapel", "");
        editor.apply();
    }

    @Override
    protected void onResume() {

        if (Vira.isConnectedToServer(getApplicationContext())) {
            bFilter.setClickable(true);
        } else {
            bFilter.setClickable(false);
            Toast.makeText(this, "Anda tidak terhubung ke server", Toast.LENGTH_LONG).show();
        }
        SharedPreferences sharedPreferences = getSharedPreferences("matapelajaran", Context.MODE_PRIVATE);
        etMapel.setText(sharedPreferences.getString("mapel", ""));
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void getAllMapel() {
        etMapel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LaporanHarianActivity.this, PilihMapelActivity.class);
                intent.putExtra("code", 1);
                intent.putExtra("jenis", "Reguler");
                startActivity(intent);
            }
        });
    }

    private void lapHarian() {
        if (Vira.isConnectedToServer(getApplicationContext())) {
            final ProgressDialog loading = ProgressDialog.show(this, "", "Loading Data...", false, false);
            arrayList.clear();

            SharedPreferences sharedPreferences = getSharedPreferences("matapelajaran", Context.MODE_PRIVATE);
            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Map<String, String> params = new HashMap<String, String>();
            params.put("tgl", etDari.getText().toString());
            params.put("tgl_akhir", etKe.getText().toString());
            params.put("jenis", "Semua");
            params.put("mapel", sharedPreferences.getString("mapel", ""));
            params.put("id_guru", id_guru);

            Log.d(TAG, "getLaporanHarian: request : \n" +
                    "dari : " + etDari.getText().toString() + "\n" +
                    "ke : " + etKe.getText().toString() + "\n" +
                    "mapel : " + sharedPreferences.getString("mapel", "") + "\n" +
                    "id_guru : " + id_guru + "\n" +
                    "");

            Call<List<LaporanHarian>> call = service.laporanHarian(params);
            call.enqueue(new Callback<List<LaporanHarian>>() {

                @Override
                public void onResponse(Call<List<LaporanHarian>> call, retrofit2.Response<List<LaporanHarian>> response) {
                    loading.dismiss();
                    final List<LaporanHarian> laporanHarianGuru = response.body();
                    Log.d(TAG, "onResponse: " + response.body() + " , total : " + laporanHarianGuru.size());
                    for (int i = 0; i < laporanHarianGuru.size(); i++) {
                        arrayList.add(laporanHarianGuru.get(i));
                    }
//                    for (LaporanHarian k : laporanHarianGuru) {
//                        Log.d(TAG, "onResponse: nama : " + k.getNama() + " , id : " + k.getId_guru());
//                    }
                    adapter.notifyDataSetChanged();
                    if (laporanHarianGuru.size() > 0) {
                        btnLapHarian.setVisibility(View.VISIBLE);

                        btnLapHarian.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                buatPdf(laporanHarianGuru);
                            }
                        });

                        Toast.makeText(LaporanHarianActivity.this, "Sukses", Toast.LENGTH_SHORT).show();
                    }else{
                        btnLapHarian.setVisibility(View.GONE);
                        Toast.makeText(LaporanHarianActivity.this, "Data tidak ditemukan", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<List<LaporanHarian>> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    Toast.makeText(LaporanHarianActivity.this, t.toString(), Toast.LENGTH_LONG).show();
                }


            });
        } else {
            Toast.makeText(this, "Pencarian gagal, anda tidak terhubung dengan server", Toast.LENGTH_SHORT).show();
        }
    }

    private void setDatePicker() {
        final Calendar calendarDari = Calendar.getInstance();
        final Calendar calendarKe = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener dateDari = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendarDari.set(Calendar.YEAR, year);
                calendarDari.set(Calendar.MONTH, monthOfYear);
                calendarDari.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                etDari.setText(String.format("%02d", calendarDari.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendarDari.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", calendarDari.get(Calendar.YEAR)));
            }
        };
        etDari.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(LaporanHarianActivity.this, dateDari, calendarDari
                        .get(Calendar.YEAR), calendarDari.get(Calendar.MONTH),
                        calendarDari.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final DatePickerDialog.OnDateSetListener dateKe = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendarKe.set(Calendar.YEAR, year);
                calendarKe.set(Calendar.MONTH, monthOfYear);
                calendarKe.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                etKe.setText(String.format("%02d", calendarKe.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendarKe.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", calendarKe.get(Calendar.YEAR)));
            }

        };

        etKe.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(LaporanHarianActivity.this, dateKe, calendarKe
                        .get(Calendar.YEAR), calendarKe.get(Calendar.MONTH),
                        calendarKe.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void buatPdf(final List<LaporanHarian> lapHarian) {
        SessionConfig sessionConfig = new SessionConfig(LaporanHarianActivity.this);
        final String ip = sessionConfig.preferences.getString("ipconfig", null);

        GetDataService service =
                RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
        HashMap<String, String> param = new HashMap<>();
        param.put("id_guru", "");

        Call<List<SettingOffline>> call = service.syncUpdateTabelSetting(param);
        call.enqueue(new Callback<List<SettingOffline>>() {
            @Override
            public void onResponse(Call<List<SettingOffline>> call, retrofit2.Response<List<SettingOffline>> response) {

                List<SettingOffline> listSettingOffline = response.body();
                SettingOffline setting = new SettingOffline();

                for (SettingOffline settingOffline : listSettingOffline) {
                    setting.setIdSetting(settingOffline.getIdSetting());
                    setting.setLogoMini(settingOffline.getLogoMini());
                    setting.setLogoGambar(settingOffline.getLogoGambar());
                    setting.setLogoFavicon(settingOffline.getLogoFavicon());
                    setting.setNik(settingOffline.getNik());
                    setting.setKartuGsm(settingOffline.getKartuGsm());
                    setting.setCekPulsa(settingOffline.getCekPulsa());
                    setting.setMetode(settingOffline.getMetode());
                    setting.setKodeAktivasi(settingOffline.getKodeAktivasi());
                    setting.setLinkTujuan(settingOffline.getLinkTujuan());
                    setting.setLogoBesar(settingOffline.getLogoBesar());
                    setting.setLogoSekolah(settingOffline.getLogoSekolah());
                    setting.setPimpinan(settingOffline.getPimpinan());
                    setting.setAkunGmail(settingOffline.getAkunGmail());
                    setting.setAlamat(settingOffline.getAlamat());
                    setting.setWebsite(settingOffline.getWebsite());
                    setting.setKopSurat(settingOffline.getKopSurat());
                    setting.setTtd(settingOffline.getTtd());
                    setting.setTelat(settingOffline.getTelat());
                    setting.setToleransiJamAkhir(settingOffline.getToleransiJamAkhir());
                    setting.setLat(settingOffline.getLat());
                    setting.setLng(settingOffline.getLng());
                    setting.setRadius(settingOffline.getRadius());
                    setting.setJamPulang(settingOffline.getJamPulang());
                    setting.setKota(settingOffline.getKota());
                }
                Document document = new Document();
                List<String> data = checkDate(lapHarian);

                // Creating an ImageData object
                String imFile = "http://" + ip + "/web_absence/assets/panel/images/" + setting.getKopSurat();

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

                StrictMode.setThreadPolicy(policy);

                Image image = null;
                try {
                    image = Image.getInstance(imFile);
                } catch (BadElementException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                image.scaleAbsolute(document.getPageSize().getWidth() - document.leftMargin(), 150f);

                PdfPTable table = new PdfPTable(new float[]{3, 2, 1, 2, 1, 1, 3});
                table.setTotalWidth(document.getPageSize().getWidth() - document.leftMargin() * 2);
                table.setLockedWidth(true);
                table.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell("Nama Guru");
                table.addCell("Mengajar di Kelas");
                table.addCell("Mata Pelajaran");
                table.addCell("Tanggal");
                table.addCell("Jadwal Absen");
                table.addCell("Validasi Absen");
                table.addCell("Ket");
                table.setHeaderRows(1);

                PdfPCell[] cells = table.getRow(0).getCells();
                for (int j = 0; j < cells.length; j++) {
                    cells[j].setBackgroundColor(BaseColor.GRAY);
                }
                SharedPreferences sharedPreferences = getSharedPreferences("rekap_kehadiran_guru", Context.MODE_PRIVATE);
                Calendar calendar = Calendar.getInstance();

                String para = setting.getKota() + ", " + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));
                Paragraph paragraph = new Paragraph(para);
                paragraph.setAlignment(Element.ALIGN_RIGHT);

                para = "                                                                                 Kepala " + setting.getLogoBesar();
                Paragraph paragraph1 = new Paragraph(para);
                paragraph1.setAlignment(Element.ALIGN_RIGHT);

                para = "                                                                                 " + setting.getPimpinan();
                Paragraph paragraph2 = new Paragraph(para);
                paragraph2.setAlignment(Element.ALIGN_RIGHT);

                para = "                                                                                 " + setting.getNik();
                Paragraph paragraph3 = new Paragraph(para);
                paragraph3.setAlignment(Element.ALIGN_RIGHT);

                para = "LAPORAN HARIAN VALIDASI \nABSENSI GURU";
                Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
                Paragraph paragraph4 = new Paragraph(para, boldFont);
                paragraph4.setAlignment(Element.ALIGN_CENTER);

                para = "Tahun Ajaran : " + sharedPreferences.getString("tahunajaran", "");
                Paragraph paragraph5 = new Paragraph(para);

                para = "Dari Tanggal : " + etDari.getText().toString() + " s/d " + etKe.getText().toString();
                Paragraph paragraph6 = new Paragraph(para);

                para = "Jenis : Semua";
                Paragraph paragraph7 = new Paragraph(para);

                para = "GURU : " + lapHarian.get(0).getNama();
                Paragraph paragraph8 = new Paragraph(para);

                // write the document content
                String directory_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/eAbsence/";
//                    directory_path = RekapKehadiranGuruActivity.this.getFilesDir() + "/mypdf/";
                File file = new File(directory_path);
                if (!file.exists()) {
                    file.mkdirs();
                }
                String targetPdf = directory_path
                        + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))
                        + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1)
                        + "-" + String.format("%04d", calendar.get(Calendar.YEAR))
                        + "_Rekap_kehadiran_guru_"
                        + String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY))
                        + String.format("%02d", calendar.get(Calendar.MINUTE))
                        + ".pdf";

                File filePath = new File(targetPdf);
                try {
                    PdfWriter.getInstance(document, new FileOutputStream(filePath));
                } catch (DocumentException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                document.open();
                try {

                    for (String tgl : data) {
                        Log.d(TAG, "onResponse: tanggal : "+tgl);
                        for (LaporanHarian lap : lapHarian) {

                            if(!lap.getTgl().equals(tgl)){
                                continue;
                            }

                            PdfPCell cell = new PdfPCell();
                            Phrase p = new Phrase(lap.getNama());
                            cell.addElement(p);
                            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                            table.addCell(cell);

                            cell = new PdfPCell();
                            String mengajar = lap.getJenjang() + " " + lap.getKelas();
                            p = new Phrase(mengajar);
                            cell.addElement(p);
                            cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                            table.addCell(cell);
                            table.addCell(String.valueOf(lap.getMapel()));
                            table.addCell(String.valueOf(lap.getTgl()));
                            table.addCell(String.valueOf(lap.getJadwal_absen()));
                            table.addCell(String.valueOf(lap.getValidasi()));
                            table.addCell(String.valueOf(lap.getKet()));
                        }

                        document.add(image);
                        document.add(paragraph4);
                        document.add(Chunk.NEWLINE);
                        document.add(paragraph5);
                        document.add(paragraph6);
                        document.add(paragraph7);
                        document.add(paragraph8);
                        document.add(Chunk.NEWLINE);
                        document.add(table);
                        document.add(Chunk.NEWLINE);
                        document.add(Chunk.NEWLINE);
                        document.add(paragraph);
                        document.add(paragraph1);
                        document.add(Chunk.NEWLINE);
                        document.add(Chunk.NEWLINE);
                        document.add(Chunk.NEWLINE);
                        document.add(Chunk.NEWLINE);
                        document.add(paragraph2);
                        document.add(paragraph3);

                        document.newPage();
                        table.deleteBodyRows();
                    }
                } catch (DocumentException e) {
                    e.printStackTrace();
                }
                document.close();
                showData(targetPdf);
            }

            @Override
            public void onFailure(Call<List<SettingOffline>> call, Throwable t) {
                t.printStackTrace();
            }
        });

    }
    private void showData(String path) {
        File file = new File(path);
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(file),"application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent intent = Intent.createChooser(target , "Open File");
        try {
            startActivity(intent);
        }catch (ActivityNotFoundException e){
            e.printStackTrace();
        }
    }

    private List<String> checkDate(List<LaporanHarian> laporanHarians) {

        List<String> data = new ArrayList<>();

        for (LaporanHarian h : laporanHarians) {

            if (data.contains(h.getTgl())) {
                continue;
            }
            data.add(h.getTgl());
        }
        return data;
    }

}

class LaporanAdapter extends RecyclerView.Adapter<LaporanAdapter.MyViewHolder> {

    private List<LaporanHarian> laporanGuru;

    public LaporanAdapter(List<LaporanHarian> lapHarian) {
        this.laporanGuru = lapHarian;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView kelasLapharian, mapelLapHarian, tanggalLapharian, absenLapHarian, validasilapharian, ketLapHarian;

        public MyViewHolder(View view) {
            super(view);
            kelasLapharian = (TextView) view.findViewById(R.id.kelas_lap_harian);
            mapelLapHarian = view.findViewById(R.id.mapel_lap_harian);
            tanggalLapharian = view.findViewById(R.id.tanggal_lap_harian);
            absenLapHarian = view.findViewById(R.id.jadwal_absen_lap_harian);
            validasilapharian = view.findViewById(R.id.validasi_lap_harian);
            ketLapHarian = view.findViewById(R.id.keterangan_lap_harian);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_laporan_harian, parent, false);

        return new LaporanAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        LaporanHarian lapHarian = laporanGuru.get(position);
        holder.kelasLapharian.setText(String.valueOf(lapHarian.getKelas()));
        holder.mapelLapHarian.setText(String.valueOf(lapHarian.getMapel()));
        holder.tanggalLapharian.setText(String.valueOf(lapHarian.getTgl()));
        holder.absenLapHarian.setText(String.valueOf(lapHarian.getJadwal_absen()));
        holder.validasilapharian.setText(String.valueOf(lapHarian.getValidasi()));
        holder.ketLapHarian.setText(String.valueOf(lapHarian.getKet()));
    }

    @Override
    public int getItemCount() {
        return laporanGuru.size();
    }
}
