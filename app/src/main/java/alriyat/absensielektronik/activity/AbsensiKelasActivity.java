package alriyat.absensielektronik.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alriyat.absensielektronik.app.Config;
import alriyat.absensielektronik.app.MySingleton;
import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.Jadwal;
import alriyat.absensielektronik.model.Siswa;
import alriyat.absensielektronik.model.Absen;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;

public class AbsensiKelasActivity extends AppCompatActivity {

    private SessionManager sessionManager;
    private SessionConfig sessionConfig;
    private ArrayList<Siswa> siswaList;
    private ArrayList<Absen> absenArrayList;
    private RecyclerView recyclerView;
    private AbsensiKelasActivity.AdapterSiswa adapterSiswa;
    final String[] ket_absen={"-","Hadir","Alpha","Sakit","Izin"};
    private ArrayList<String> absen_label;
    private AlertDialog dialog;
    private TextView nmHead,emHead;
    private TextView noJadwal,btnValidMasuk,btnValidPulang;
    private TableLayout halValidasi;
    private ImageView profil_image;
    private RecyclerView.LayoutManager layoutManager;
    int year_x, month_x, day_x;
    String uname;
    private MenuItem fastMasuk,fastKeluar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absensi_kelas);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_absensi);
        toolbar.setTitle("Absensi Kelas");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Calendar cal = Calendar.getInstance();
        year_x = cal.get(Calendar.YEAR);
        month_x = cal.get(Calendar.MONTH)+1;
        day_x = cal.get(Calendar.DAY_OF_MONTH);
        //etTgl.setText(day_x+"-"+month_x+"-"+year_x);
        if (month_x <= 9){
            if (day_x <= 9){
                toolbar.setSubtitle("0" + day_x + "-" + "0" + month_x + "-" + year_x);
            }else if (day_x > 9){
                toolbar.setSubtitle(day_x + "-" + "0" + month_x + "-" + year_x);
            }
        }else if (month_x > 9){
            if (day_x <= 9){
                toolbar.setSubtitle("0" + day_x + "-" + month_x + "-" + year_x);
            }else if (day_x > 9){
                toolbar.setSubtitle(day_x + "-" + month_x + "-" + year_x);
            }
        }else {

        }
        //toolbar.setSubtitle("05/04/2017");
        setSupportActionBar(toolbar);

        sessionManager = new SessionManager(this);
        sessionConfig = new SessionConfig(this);
        noJadwal = (TextView)findViewById(R.id.text_nojadwal);
        btnValidMasuk = (TextView)findViewById(R.id.btnValidasiMasuk);
        btnValidPulang = (TextView)findViewById(R.id.btnValidasiKeluar);

        uname = sessionManager.preferences.getString("username",null);

        halValidasi = (TableLayout)findViewById(R.id.hal_validasi);
        siswaList = new ArrayList<>();
        recyclerView = (RecyclerView)findViewById(R.id.recycler_siswa);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);
        siswaList = getAllSiswa();
        adapterSiswa = new AbsensiKelasActivity.AdapterSiswa(this,siswaList);
        recyclerView.setAdapter(adapterSiswa);
    }


    private void goToBack(){
        super.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.


        getMenuInflater().inflate(R.menu.absen_kelas, menu);
        fastMasuk = menu.findItem(R.id.action_fast_masuk);
        fastKeluar = menu.findItem(R.id.action_fast_keluar);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_fast_masuk) {
            AlertDialog.Builder builder = new AlertDialog.Builder(AbsensiKelasActivity.this);
            builder.setMessage("Anda Akan melakukan absen masuk secara cepat...");
            builder.setCancelable(true);
            builder.setPositiveButton("Absen Masuk", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    fastAbsenMasuk(uname);


                }
            });
            builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            builder.show();
            return true;
        }else if(id == R.id.action_fast_keluar){
            AlertDialog.Builder builder = new AlertDialog.Builder(AbsensiKelasActivity.this);
            builder.setMessage("Anda Akan melakukan absen pulang secara cepat...");
            builder.setCancelable(true);
            builder.setPositiveButton("Absen Pulang", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    fastAbsenPulang(uname);


                }
            });
            builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            builder.show();
        }

        switch (item.getItemId()){
            case android.R.id.home:
                goToBack();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private ArrayList<Siswa> getAllSiswa(){
        final String ip = sessionConfig.preferences.getString("ipconfig",null);
        final ProgressDialog loading = ProgressDialog.show(this,"","Loading Data...",false,false);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
        HashMap<String, String> map = new HashMap<>();
        map.put("idguru", sessionManager.preferences.getString("username",null));
        Call<List<Siswa>> call = service.daftarSiswaAbsensi(map);

        call.enqueue(new Callback<List<Siswa>>() {
//                @Override
//                public void onResponse(Call<JSONArray> call, Response<JSONArray> response) {
//
//                }

//                @Override
//                public void onResponse(Call<Query> call, Response<Query> response) {
//                    syncAmbilTabelSiswakelas(context, idGuru);
//                }

            @Override
            public void onResponse(Call<List<Siswa>> call, retrofit2.Response<List<Siswa>> response) {
//                loading.dismiss();
                loading.dismiss();
                siswaList.clear();
                List<Siswa> listSiswa = response.body();

                for (Siswa siswa : listSiswa) {
                    siswaList.add(siswa);
                }
                adapterSiswa.notifyDataSetChanged();
                if (adapterSiswa.getItemCount()>0){
                    noJadwal.setVisibility(View.GONE);
                    halValidasi.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onFailure(Call<List<Siswa>> call, Throwable t) {
//                loading.dismiss();
                Toast.makeText(AbsensiKelasActivity.this, "Failed, check internet connection...", Toast.LENGTH_LONG).show();
                t.printStackTrace();
            }
        });

//        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.POST,"http://"+ip+ Config.DAFTAR_ABSENSI+"?idguru="+sessionManager.preferences.getString("username",null),null,
//                new Response.Listener<JSONArray>() {
//                    @Override
//                    public void onResponse(JSONArray response) {
//                        loading.dismiss();
//                        Log.e("URL", "kawan" + ip+Config.DAFTAR_ABSENSI);
//                        int count=0;
//                        while (count < response.length()){
//                            try {
//                                JSONObject object = response.getJSONObject(count);
//                                Siswa siswa = new Siswa();
//                                siswa.setNo(object.getString("no"));
//                                siswa.setNis(object.getString("id_siswa"));
//                                siswa.setNama(object.getString("nama_lengkap"));
//                                siswa.setGmbr(object.getString("gambar"));
//                                siswa.setIdpenmpatan(object.getString("id_penempatan"));
//                                siswa.setAbmasuk(object.getString("absen_masuk"));
//                                siswa.setAbkluar(object.getString("absen_pulang"));
//                                siswaList.add(siswa);
//                                Log.e("Haasil", "array" + siswaList);
//                                count++;
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
//                        }
//                        adapterSiswa.notifyDataSetChanged();
//                        if (adapterSiswa.getItemCount()>0){
//                            noJadwal.setVisibility(View.GONE);
//                            halValidasi.setVisibility(View.VISIBLE);
//                        }
//
//                    }
//                },
//                new Response.ErrorListener() {
//                    @Override
//                    public void onErrorResponse(VolleyError error) {
//                        loading.dismiss();
//                        Log.e("Haasil", "array" + error);
//                        Toast.makeText(AbsensiKelasActivity.this, "Failed, check internet connection...", Toast.LENGTH_LONG).show();
//                    }
//                });
//
//        //Creating a request queue
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//
//        //Adding request to the queue
//        requestQueue.add(jsonArrayRequest);
        return siswaList;
    }

    class AdapterSiswa extends RecyclerView.Adapter<AbsensiKelasActivity.AdapterSiswa.MyViewHolder>{
        private ImageLoader imageLoader;
        private Context mContext;
        private ArrayList<Siswa> siswalist;
        //final String[] ketHadir={"-","Hadir","Alpha","Sakit","Izin"};
        private TextView teAbm;


        public AdapterSiswa(Context mContext, ArrayList<Siswa> siswalist) {
            this.mContext = mContext;
            this.siswalist = siswalist;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder{
            public TextView no,nis,nama,idpenem,ketMasuk,ketPulang;
            public NetworkImageView gmbr;
            public Spinner spinner,spinner2;
            public ImageView abMasuk,abKeluar;

            public MyViewHolder(View itemView) {
                super(itemView);
                nis = (TextView)itemView.findViewById(R.id.nis);
                no = (TextView)itemView.findViewById(R.id.no);
                nama = (TextView)itemView.findViewById(R.id.nm_lengkap);
                idpenem = (TextView)itemView.findViewById(R.id.idpenempatan);
                gmbr = (NetworkImageView)itemView.findViewById(R.id.thumbnail_buku);
                abMasuk = (ImageView)itemView.findViewById(R.id.absenmasuk);
                abKeluar = (ImageView)itemView.findViewById(R.id.absenkeluar);
                ketMasuk = (TextView)itemView.findViewById(R.id.ketMasuk);
                ketPulang = (TextView)itemView.findViewById(R.id.ketKeluar);


                //teAbm = (TextView)findViewById(R.id.ketMasuk);
                abMasuk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pilihKategori(ketMasuk);
                        ketMasuk.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                String idpene = idpenem.getText().toString().trim();
                                saveAbsen(idpene, ketMasuk.getText().toString());
                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                refresh();
                            }
                        });


                    }
                });

                abKeluar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        pilihKategori(ketPulang);
                        ketPulang.addTextChangedListener(new TextWatcher() {
                            @Override
                            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                            }

                            @Override
                            public void onTextChanged(CharSequence s, int start, int before, int count) {
                                String idpene = idpenem.getText().toString().trim();
                                String kemasuk = ketPulang.getText().toString().trim();
                                saveAbsenPulang(idpene,kemasuk);
                            }

                            @Override
                            public void afterTextChanged(Editable s) {
                                refresh();
                            }
                        });
                    }
                });

                btnValidMasuk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(AbsensiActivity.this,"lagi dicklick",Toast.LENGTH_SHORT).show();
                        saveValidasiMasuk(abMasuk);
                    }
                });

                btnValidPulang.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Toast.makeText(AbsensiActivity.this,"lagi dicklick",Toast.LENGTH_SHORT).show();
                        saveValidasiPulang(abKeluar);
                    }
                });

                itemView.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {
                        Intent n = new Intent(AbsensiKelasActivity.this,DetailSiswaActivity.class);
                        n.putExtra("idnews",nis.getText().toString());
                        startActivity(n);
                    }
                });

                //gmbr = (NetworkImageView)itemView.findViewById(R.id.img_siswa);
                //spinner = (Spinner)itemView.findViewById(R.id.kehadiran);

                //spinner2 = (Spinner)itemView.findViewById(R.id.pulang);

                //spinner2.setAdapter(new ArrayAdapter<String>(AbsensiActivity.this, R.layout.spinner_item, ketPulang));

            }
        }

        @Override
        public AbsensiKelasActivity.AdapterSiswa.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_siswa,parent,false);
            return new AbsensiKelasActivity.AdapterSiswa.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final AbsensiKelasActivity.AdapterSiswa.MyViewHolder holder, int position) {
            //Buku buku = bukuList.get(position);
            final String ip = sessionConfig.preferences.getString("ipconfig",null);
            holder.no.setText(siswalist.get(position).getNo());
            holder.nis.setText(siswalist.get(position).getNis());
            holder.nama.setText(siswalist.get(position).getNama());
            holder.idpenem.setText(siswalist.get(position).getIdpenmpatan());
            holder.ketMasuk.setText(siswalist.get(position).getAbmasuk());
            holder.ketPulang.setText(siswalist.get(position).getAbkluar());


            if (holder.ketMasuk.getText().equals("hadir")){
                holder.nama.setTextColor(getResources().getColor(R.color.hitam));
            }else if (holder.ketMasuk.getText().equals("izin")){
                holder.nama.setTextColor(getResources().getColor(R.color.biru));
            }else if (holder.ketMasuk.getText().equals("sakit")){
                holder.nama.setTextColor(getResources().getColor(R.color.orange));
            }else if (holder.ketMasuk.getText().equals("alpha")){
                holder.nama.setTextColor(getResources().getColor(R.color.merah));
            }else {
                holder.nama.setTextColor(getResources().getColor(R.color.input_login));
            }
            imageLoader = MySingleton.getmInstance(mContext).getImageLoader();
            Picasso.with(mContext).load("http://"+ip+"/web_absence/assets/panel/images/" + siswalist.get(position)
                    .getGmbr())
                    .placeholder(R.drawable.siswa)
                    .into(holder.gmbr);
            holder.gmbr.setImageUrl(siswalist.get(position).getGmbr(), imageLoader);

        }

        @Override
        public int getItemCount() {
            return siswalist.size();
        }
    }

    private void refresh(){
        Intent n = new Intent(getIntent());
        startActivity(n);
    }

    private void saveValidasiMasuk(final ImageView a){
        final String ip = sessionConfig.preferences.getString("ipconfig",null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Apakah anda yakin validasi absen masuk?");
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final ProgressDialog dialogo = new ProgressDialog(AbsensiKelasActivity.this);
                dialogo.setMessage("Proses validasi masuk...");
                dialogo.setCancelable(false);
                dialogo.show();

                StringRequest request = new StringRequest(Request.Method.POST, "http://"+ip + Config.VALID_ABSEN_MASUK + "?name="+sessionManager.preferences.getString("username",null),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.e("hasil valid", "Masuk" + response);
                                dialogo.dismiss();
                                a.setEnabled(false);
                                if (response.equals("error")){
                                    AlertDialog.Builder builder = new AlertDialog.Builder(AbsensiKelasActivity.this);
                                    builder.setMessage("Gagal Validasi, periksa siswa yang belum absen...");
                                    builder.setCancelable(true);
                                    builder.setPositiveButton("Selesai", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent n = new Intent(getIntent());
                                            startActivity(n);


                                        }
                                    });

                                    builder.show();

                                }else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(AbsensiKelasActivity.this);
                                    builder.setMessage("Berhasil Validasi...");
                                    builder.setCancelable(true);
                                    builder.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent n = new Intent(getIntent());
                                            startActivity(n);
                                        }
                                    });
                                    builder.show();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                dialogo.dismiss();
                            }
                        });
                MySingleton.getmInstance(getApplicationContext()).addToRequestque(request);
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();

    }

    private void saveValidasiPulang(final ImageView t){
        final String ip = sessionConfig.preferences.getString("ipconfig",null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Apakah yakin validasi absen pulang?");
        builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                final ProgressDialog dialoge = new ProgressDialog(AbsensiKelasActivity.this);
                dialoge.setMessage("Proses validasi pulang...");
                dialoge.setCancelable(false);
                dialoge.show();
                StringRequest request = new StringRequest(Request.Method.POST, "http://"+ip + Config.VALID_ABSEN_PULANG + "?name="+sessionManager.preferences.getString("username",null),
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Log.e("hasil valid", "Pulang" + response);
                                dialoge.dismiss();
                                t.setEnabled(false);
                                if (response.equals("error")){
                                    AlertDialog.Builder builder = new AlertDialog.Builder(AbsensiKelasActivity.this);
                                    builder.setMessage("Gagal Validasi, periksa siswa yang belum absen...");
                                    builder.setCancelable(true);
                                    builder.setPositiveButton("Selesai", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent n = new Intent(getIntent());
                                            startActivity(n);


                                        }
                                    });

                                    builder.show();

                                }else {
                                    AlertDialog.Builder builder = new AlertDialog.Builder(AbsensiKelasActivity.this);
                                    builder.setMessage("Berhasil Validasi...");
                                    builder.setCancelable(true);
                                    builder.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent n = new Intent(getIntent());
                                            startActivity(n);
                                        }
                                    });
                                    builder.show();
                                }
                            }
                        },
                        new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                dialoge.dismiss();
                            }
                        });
                MySingleton.getmInstance(getApplicationContext()).addToRequestque(request);
            }
        });
        builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        builder.show();

    }

    private void pilihKategori(final TextView tv){
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View view = layoutInflater.inflate(R.layout.absensi, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        builder.setCancelable(true);
        RecyclerView rckategori = (RecyclerView)view.findViewById(R.id.recycler_absen);
        layoutManager = new LinearLayoutManager(this);
        rckategori.setLayoutManager(layoutManager);
        rckategori.setHasFixedSize(true);
        absen_label = new ArrayList<>();
        absen_label.add("hadir");
        absen_label.add("alpha");
        absen_label.add("izin");
        absen_label.add("sakit");
        RecyclerView.Adapter adapter = new adapterSelectAbsen(absen_label,tv);
        rckategori.setAdapter(adapter);
        //absenArrayList = getKategori();
        //adapterKat = new AdapterKategori(kategoriArrayList);
        //rckategori.setAdapter(new ArrayAdapter<String>(AbsensiActivity.this, R.layout.spinner_item, ket_absen));
//        dialog.setCancelable(true);
        dialog = builder.create();
        dialog.show();
    }

    class adapterSelectAbsen extends RecyclerView.Adapter<adapterSelectAbsen.ViewHolder> {
        //private List<String> labels;
        private ArrayList<String> absen_label;
        private TextView mtextView;

        public adapterSelectAbsen(ArrayList<String> absen_label,TextView tv){
            this.absen_label = absen_label;
            mtextView = tv;
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_absen, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            final String label = absen_label.get(position);
            holder.textView.setText(label);

            //handling item click event
            holder.textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(holder.textView.getContext(), label, Toast.LENGTH_SHORT).show();
                    mtextView.setText(label);
                    dialog.dismiss();
                }
            });
        }

        @Override
        public int getItemCount() {
            return absen_label.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView textView,kP;

            public ViewHolder(View itemView) {
                super(itemView);
                textView = (TextView) itemView.findViewById(R.id.itemnya);
                kP = (TextView)findViewById(R.id.ketMasuk);
            }
        }
    }
    private void saveAbsen(final String idpenempatan, final String ketAbsen){
        final String ip = sessionConfig.preferences.getString("ipconfig",null);
        final ProgressDialog dialog = new ProgressDialog(AbsensiKelasActivity.this);
        dialog.setMessage("Sedang Diproses...");
        dialog.setCancelable(false);
        dialog.show();
        StringRequest request = new StringRequest(Request.Method.POST, "http://"+ip+Config.SIMPAN_ABSEN,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        Log.e("Response",""+response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //final String idpenm = idpenempatan.getText().toString().trim();
                //final String ketabsen = spinner.getSelectedItem().toString().trim();
                Map<String, String> params = new HashMap<>();
                params.put("idpenem",idpenempatan);
                params.put("absen",ketAbsen);
                return params;
            }
        };
        MySingleton.getmInstance(getApplicationContext()).addToRequestque(request);
    }

    private void saveAbsenPulang(final String idpenempatan, final String ketAbsen){
        final String ip = sessionConfig.preferences.getString("ipconfig",null);
        final ProgressDialog dialog = new ProgressDialog(AbsensiKelasActivity.this);
        dialog.setMessage("Sedang Diproses...");
        dialog.setCancelable(false);
        dialog.show();
        StringRequest request = new StringRequest(Request.Method.POST, "http://"+ip+Config.SIMPAN_ABSEN_PULANG,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        Log.e("Response",""+response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //final String idpenm = idpenempatan.getText().toString().trim();
                //final String ketabsen = spinner.getSelectedItem().toString().trim();
                Map<String, String> params = new HashMap<>();
                params.put("idpenem",idpenempatan);
                params.put("absen",ketAbsen);
                return params;
            }
        };
        MySingleton.getmInstance(getApplicationContext()).addToRequestque(request);
    }

    private void fastAbsenMasuk(final String nik){
        final String ip = sessionConfig.preferences.getString("ipconfig",null);
        final ProgressDialog dialog = new ProgressDialog(AbsensiKelasActivity.this);
        dialog.setMessage("Sedang Diproses...");
        dialog.setCancelable(false);
        dialog.show();
        StringRequest request = new StringRequest(Request.Method.POST, "http://"+ip+"/web_absence/assets/file/hadir_masuk.php?name="+nik,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        Log.e("Response",""+response);
                        Intent n = new Intent(getIntent());
                        startActivity(n);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //final String idpenm = idpenempatan.getText().toString().trim();
                //final String ketabsen = spinner.getSelectedItem().toString().trim();
                Map<String, String> params = new HashMap<>();
                params.put("idpenem","");
                params.put("absen","");
                return params;
            }
        };
        MySingleton.getmInstance(getApplicationContext()).addToRequestque(request);
    }

    private void fastAbsenPulang(final String nik){
        final String ip = sessionConfig.preferences.getString("ipconfig",null);
        final ProgressDialog dialog = new ProgressDialog(AbsensiKelasActivity.this);
        dialog.setMessage("Sedang Diproses...");
        dialog.setCancelable(false);
        dialog.show();
        StringRequest request = new StringRequest(Request.Method.POST, "http://"+ip+"/web_absence/assets/file/hadir_pulang.php?name="+nik,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        dialog.dismiss();
                        Log.e("Response",""+response);
                        Intent n = new Intent(getIntent());
                        startActivity(n);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                dialog.dismiss();
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //final String idpenm = idpenempatan.getText().toString().trim();
                //final String ketabsen = spinner.getSelectedItem().toString().trim();
                Map<String, String> params = new HashMap<>();
                params.put("idpenem","");
                params.put("absen","");
                return params;
            }
        };
        MySingleton.getmInstance(getApplicationContext()).addToRequestque(request);
    }

}
