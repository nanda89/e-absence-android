package alriyat.absensielektronik.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.KelasOffline;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;

public class PilihKelasByWaliKelasActivity extends AppCompatActivity {

    private static final String TAG = PilihKelasByWaliKelasActivity.class.getName();
    private KelasByWaliKelasAdapter kelasAdapter;
    private RecyclerView rvRecyclerView;
    private ArrayList<KelasOffline> listKelas;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_kelas_by_wali_kelas);

        rvRecyclerView = findViewById(R.id.recycler_view_kelas);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_kelas);
        toolbar.setTitle("Pilih Kelas");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sharedPreferences = this.getSharedPreferences("rekap_kehadiran_guru", Context.MODE_PRIVATE);

        listKelas = new ArrayList<>();

        kelasAdapter = new KelasByWaliKelasAdapter(listKelas);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvRecyclerView.setLayoutManager(mLayoutManager);
        rvRecyclerView.setItemAnimator(new DefaultItemAnimator());
        rvRecyclerView.setAdapter(kelasAdapter);

        getKelas();
    }
    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    private void getKelas() {
        SessionConfig sessionConfig = new SessionConfig(PilihKelasByWaliKelasActivity.this);
        SessionManager sessionManager = new SessionManager(PilihKelasByWaliKelasActivity.this);
        GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

        String idJenjang = sharedPreferences.getString("id_jenjang","");
        String guru = sessionManager.preferences.getString("idguru", "");
        Map<String, String> param = new HashMap<>();
        param.put("id_guru", guru);
        param.put("id_jenjang", idJenjang);

        Log.d(TAG, "getKelas: \n" +
                "id_jenjang : "+idJenjang+"\n" +
                "guru : "+guru+"\n" +
                "");

        Call<List<KelasOffline>> call= service.pilihKelasWaliKelas(param);
        call.enqueue(new Callback<List<KelasOffline>>() {

            @Override
            public void onResponse(Call<List<KelasOffline>>call, retrofit2.Response<List<KelasOffline>> response) {
                Log.d(TAG, "onResponse: response msg : "+response.body().toString());
                List<KelasOffline> kelas = response.body();
                Log.d(TAG, "onResponse: total : "+kelas.size());
                listKelas.clear();
                for (KelasOffline kls : kelas) {
                    listKelas.add(kls);
                }
                kelasAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<KelasOffline>>call, Throwable t) {
                Log.d(TAG, "onFailure: error : "+t.getMessage());
                t.printStackTrace();
            }
        });
    }
}


class KelasByWaliKelasAdapter extends RecyclerView.Adapter<KelasByWaliKelasAdapter.MyViewHolder> {

    private List<KelasOffline> listKelas;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView idKelas, kelas, namaGuru;

        public MyViewHolder(final View view) {
            super(view);
            idKelas = view.findViewById(R.id.id_kelas);
            kelas = view.findViewById(R.id.kelas);
            namaGuru = view.findViewById(R.id.nama_guru);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences sharedPreferences = view.getContext().getSharedPreferences("rekap_kehadiran_guru", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("id_kelas", idKelas.getText().toString());
                    editor.putString("kelas", kelas.getText().toString());
                    editor.apply();


                    ((PilihKelasByWaliKelasActivity) view.getContext()).onBackPressed();
                }
            });
        }
    }


    public KelasByWaliKelasAdapter(List<KelasOffline> listKelas) {
        this.listKelas = listKelas;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_kelas, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        KelasOffline kls = listKelas.get(position);
        holder.idKelas.setText(String.valueOf(kls.getIdKelas()));
        holder.kelas.setText(kls.getKelas());
        holder.namaGuru.setText(kls.getNamaGuru());
    }

    @Override
    public int getItemCount() {
        return listKelas.size();
    }
}