package alriyat.absensielektronik.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.app.MySingleton;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.Depan;
import alriyat.absensielektronik.model.Kehadiran;
import alriyat.absensielektronik.model.KehadiranGuru;
import alriyat.absensielektronik.model.KehadiranNonGuru;
import alriyat.absensielektronik.model.Mapel;
import alriyat.absensielektronik.model.SettingOffline;
import alriyat.absensielektronik.util.DatabaseHandler;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import alriyat.absensielektronik.util.Vira;
import retrofit2.Call;
import retrofit2.Callback;

public class RekapKehadiranNonGuruActivity extends AppCompatActivity {

    private EditText etDari;
    private EditText etKe;
    private EditText etNonGuru;
    private Button bFilter;
    private RecyclerView recyclerView;
    private SessionConfig sessionConfig;
    private SessionManager sessionManager;
    private RekapKehadiranNonGuruAdapter adapter;
    ArrayList<KehadiranNonGuru> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rekap_kehadiran_non_guru);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Rekap kehadiran Non Guru");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etDari = findViewById(R.id.dari);
        etKe = findViewById(R.id.ke);
        etNonGuru = findViewById(R.id.pilih_non_guru);
        bFilter = findViewById(R.id.filter);
        recyclerView = findViewById(R.id.recycler_view);

        sessionConfig = new SessionConfig(this);
        sessionManager = new SessionManager(this);

        final Calendar calendarDari = Calendar.getInstance();
        final Calendar calendarKe = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener dateDari = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendarDari.set(Calendar.YEAR, year);
                calendarDari.set(Calendar.MONTH, monthOfYear);
                calendarDari.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                etDari.setText(String.format("%02d", calendarDari.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendarDari.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", calendarDari.get(Calendar.YEAR)));
            }

        };

        etDari.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(RekapKehadiranNonGuruActivity.this, dateDari, calendarDari
                        .get(Calendar.YEAR), calendarDari.get(Calendar.MONTH),
                        calendarDari.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final DatePickerDialog.OnDateSetListener dateKe = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendarKe.set(Calendar.YEAR, year);
                calendarKe.set(Calendar.MONTH, monthOfYear);
                calendarKe.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                etKe.setText(String.format("%02d", calendarKe.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendarKe.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", calendarKe.get(Calendar.YEAR)));
            }

        };

        etKe.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(RekapKehadiranNonGuruActivity.this, dateKe, calendarKe
                        .get(Calendar.YEAR), calendarKe.get(Calendar.MONTH),
                        calendarKe.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        etNonGuru.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RekapKehadiranNonGuruActivity.this, PilihNonGuruActivity.class);
                startActivity(intent);
            }
        });

        bFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dari = etDari.getText().toString();
                String ke = etKe.getText().toString();

                if (dari.isEmpty()) {
                    Toast.makeText(RekapKehadiranNonGuruActivity.this, "Tanggal awal kosong!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (ke.isEmpty()) {
                    Toast.makeText(RekapKehadiranNonGuruActivity.this, "Tanggal akhir kosong!", Toast.LENGTH_SHORT).show();
                    return;
                }
                getKehadiran();
            }
        });

        adapter = new RekapKehadiranNonGuruAdapter(arrayList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(RekapKehadiranNonGuruActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setAdapter(adapter);

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);

        SharedPreferences sharedPreferences = getSharedPreferences("rekap_kehadiran_non_guru", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("id_non_guru", "");
        editor.putString("nama", "");
        editor.apply();
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences sharedPreferences = getSharedPreferences("rekap_kehadiran_non_guru", Context.MODE_PRIVATE);
        etNonGuru.setText(sharedPreferences.getString("nama", ""));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void getKehadiran() {
        if (Vira.isConnectedToServer(getApplicationContext())) {
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            final ProgressDialog loading = ProgressDialog.show(this, "", "Loading Data...", false, false);
            arrayList.clear();
//            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(com.android.volley.Request.Method.POST, "http://" + ip + "/web_absence/assets/file/rekap_kehadiran_non_guru.php?dari=" + etDari.getText() + "&ke=" + etKe.getText(), null,
//                    new Response.Listener<JSONArray>() {
//
//                        @Override
//                        public void onResponse(JSONArray response) {
//                            loading.dismiss();
//                            int count = 0;
//                            while (count < response.length()) {
//                                try {
//                                    JSONObject object = response.getJSONObject(count);
//                                    KehadiranNonGuru kehadiranNonGuru = new KehadiranNonGuru();
//                                    kehadiranNonGuru.setNama(object.getString("nama"));
//                                    kehadiranNonGuru.setTanggal(object.getString("tgl"));
//                                    kehadiranNonGuru.setJamMasukKerja(object.getString("jam_masuk_kerja"));
//                                    kehadiranNonGuru.setJamPulangKerja(object.getString("jam_pulang_kerja"));
//                                    kehadiranNonGuru.setValidasiJamMasuk(object.getString("validasi_jam_masuk"));
//                                    kehadiranNonGuru.setValidasiJamPulang(object.getString("validasi_jam_pulang"));
//                                    kehadiranNonGuru.setKeteranganJamMasuk(object.getString("keterangan_jam_masuk"));
//                                    kehadiranNonGuru.setKeteranganJamPulang(object.getString("keterangan_jam_pulang"));
//                                    arrayList.add(kehadiranNonGuru);
//                                    Log.e("", "Kehadiran Non Guru" + arrayList);
//                                    count++;
//                                } catch (JSONException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                            adapter.notifyDataSetChanged();
//                        }
//                    },
//                    new Response.ErrorListener() {
//                        @Override
//                        public void onErrorResponse(VolleyError error) {
////                        AlertDialog.Builder builder = new AlertDialog.Builder(DaftarPelajaranActivity.this);
////                        builder.setMessage("Tidak ada jaringan internet...");
////                        builder.setCancelable(false);
////                        builder.setPositiveButton("Muat Ulang", new DialogInterface.OnClickListener() {
////                            @Override
////                            public void onClick(DialogInterface dialog, int which) {
////                                Intent a = new Intent(getIntent());
////                                startActivity(a);
////                            }
////                        });
////                        builder.show();
//                            loading.dismiss();
//                        }
//                    });
//            MySingleton.getmInstance(this).addToRequestque(jsonArrayRequest);

            SharedPreferences sharedPreferences = getSharedPreferences("rekap_kehadiran_non_guru", Context.MODE_PRIVATE);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("dari", etDari.getText().toString());
            param.put("ke", etKe.getText().toString());
            param.put("id_non_guru", sharedPreferences.getString("id_non_guru", ""));

            Call<List<KehadiranNonGuru>> call = service.rekapKehadiranNonGuru(param);
            call.enqueue(new Callback<List<KehadiranNonGuru>>() {
                @Override
                public void onResponse(Call<List<KehadiranNonGuru>>call, retrofit2.Response<List<KehadiranNonGuru>> response) {
//                    loading.dismiss();
                    loading.dismiss();

                    List<KehadiranNonGuru> listKehadiranNonGuru = response.body();

                    for (KehadiranNonGuru kehadiranNonGuru : listKehadiranNonGuru) {
                        arrayList.add(kehadiranNonGuru);
                    }
                    adapter.notifyDataSetChanged();

                    buatPdf(listKehadiranNonGuru);
                }

                @Override
                public void onFailure(Call<List<KehadiranNonGuru>>call, Throwable t) {
                    // Log error here since request failed
//                    loading.dismiss();
                    loading.dismiss();
                }
            });

        } else {
        }
    }

    private void buatPdf(final List<KehadiranNonGuru> listKehadiranNonGuru) {
        SessionConfig sessionConfig = new SessionConfig(RekapKehadiranNonGuruActivity.this);
        final String ip = sessionConfig.preferences.getString("ipconfig",null);

        GetDataService service =
                RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

        HashMap<String, String> param = new HashMap<>();
        param.put("id_guru", "");

        Call<List<SettingOffline>> call = service.syncUpdateTabelSetting(param);
        call.enqueue(new Callback<List<SettingOffline>>() {
            @Override
            public void onResponse(Call<List<SettingOffline>>call, retrofit2.Response<List<SettingOffline>> response) {
//                    loading.dismiss();
                List<SettingOffline> listSettingOffline = response.body();
                SettingOffline setting = new SettingOffline();

                for (SettingOffline settingOffline : listSettingOffline) {
                    setting.setIdSetting(settingOffline.getIdSetting());
                    setting.setLogoMini(settingOffline.getLogoMini());
                    setting.setLogoGambar(settingOffline.getLogoGambar());
                    setting.setLogoFavicon(settingOffline.getLogoFavicon());
                    setting.setNik(settingOffline.getNik());
                    setting.setKartuGsm(settingOffline.getKartuGsm());
                    setting.setCekPulsa(settingOffline.getCekPulsa());
                    setting.setMetode(settingOffline.getMetode());
                    setting.setKodeAktivasi(settingOffline.getKodeAktivasi());
                    setting.setLinkTujuan(settingOffline.getLinkTujuan());
                    setting.setLogoBesar(settingOffline.getLogoBesar());
                    setting.setLogoSekolah(settingOffline.getLogoSekolah());
                    setting.setPimpinan(settingOffline.getPimpinan());
                    setting.setAkunGmail(settingOffline.getAkunGmail());
                    setting.setAlamat(settingOffline.getAlamat());
                    setting.setWebsite(settingOffline.getWebsite());
                    setting.setKopSurat(settingOffline.getKopSurat());
                    setting.setTtd(settingOffline.getTtd());
                    setting.setTelat(settingOffline.getTelat());
                    setting.setToleransiJamAkhir(settingOffline.getToleransiJamAkhir());
                    setting.setLat(settingOffline.getLat());
                    setting.setLng(settingOffline.getLng());
                    setting.setRadius(settingOffline.getRadius());
                    setting.setJamPulang(settingOffline.getJamPulang());
                    setting.setKota(settingOffline.getKota());
                }

                Document document = new Document();

                // Creating an ImageData object
                String imFile = "http://" + ip + "/web_absence/assets/panel/images/" + setting.getKopSurat();

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

                StrictMode.setThreadPolicy(policy);

                Image image = null;
                try {
                    image = Image.getInstance(imFile);
                } catch (BadElementException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                image.scaleAbsolute(document.getPageSize().getWidth() - document.leftMargin(), 150f);

                PdfPTable table = new PdfPTable(new float[] { 3, 3, 1, 1, 1, 1, 1, 1 });
                table.setTotalWidth(document.getPageSize().getWidth() - document.leftMargin() * 2);
                table.setLockedWidth(true);
                table.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell("Nip");
                table.addCell("Nama");
                table.addCell("Masuk Absen Sesuai");
                table.addCell("Masuk Terlambat");
                table.addCell("Masuk Tidak Absen");
                table.addCell("Pulang Absen Sesuai");
                table.addCell("Pulang Terlambat");
                table.addCell("Pulang Tidak Absen");
                table.setHeaderRows(1);
                PdfPCell[] cells = table.getRow(0).getCells();
                for (int j=0;j<cells.length;j++){
                    cells[j].setBackgroundColor(BaseColor.GRAY);
                }
                for (KehadiranNonGuru kehadiranNonGuru : listKehadiranNonGuru) {
                    PdfPCell cell = new PdfPCell();
                    Phrase p = new Phrase(kehadiranNonGuru.getNip());
                    cell.addElement(p);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);
                     cell = new PdfPCell();
                     p = new Phrase(kehadiranNonGuru.getNama());
                    cell.addElement(p);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);
                    table.addCell(kehadiranNonGuru.getMasukAbsenSesuai());
                    table.addCell(kehadiranNonGuru.getMasukTerlambat());
                    table.addCell(kehadiranNonGuru.getMasukTidakAbsen());
                    table.addCell(kehadiranNonGuru.getPulangAbsenSesuai());
                    table.addCell(kehadiranNonGuru.getPulangTerlambat());
                    table.addCell(kehadiranNonGuru.getPulangTidakAbsen());
                }

                SharedPreferences sharedPreferences = getSharedPreferences("rekap_kehadiran_non_guru", Context.MODE_PRIVATE);
                Calendar calendar = Calendar.getInstance();

                String para = setting.getKota() + ", " + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));
                Paragraph paragraph = new Paragraph (para);
                paragraph.setAlignment(Element.ALIGN_RIGHT);

                para = "                                                                                 Kepala " + setting.getLogoBesar();
                Paragraph paragraph1 = new Paragraph(para);
                paragraph1.setAlignment(Element.ALIGN_RIGHT);

                para = "                                                                                 " + setting.getPimpinan();
                Paragraph paragraph2 = new Paragraph(para);
                paragraph2.setAlignment(Element.ALIGN_RIGHT);

                para = "                                                                                 " + setting.getNik();
                Paragraph paragraph3 = new Paragraph(para);
                paragraph3.setAlignment(Element.ALIGN_RIGHT);

                para = "REKAP KEHADIRAN NON GURU";
                Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
                Paragraph paragraph4 = new Paragraph(para, boldFont);
                paragraph4.setAlignment(Element.ALIGN_CENTER);

                para = "PERIODE : " + etDari.getText().toString() + " s/d " + etKe.getText().toString();
                Paragraph paragraph5 = new Paragraph(para);

                para = "NON GURU : " + sharedPreferences.getString("nama", "");
                Paragraph paragraph6 = new Paragraph(para);

                // write the document content
                String directory_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/eAbsence/";
//                    directory_path = RekapKehadiranGuruActivity.this.getFilesDir() + "/mypdf/";
                File file = new File(directory_path);
                if (!file.exists()) {
                    file.mkdirs();
                }
                String targetPdf = directory_path
                        +String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))
                        + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1)
                        + "-" + String.format("%04d", calendar.get(Calendar.YEAR))
                        + "_Rekap_kehadiran_non_guru_"
                        + String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY))
                        + String.format("%02d", calendar.get(Calendar.MINUTE))
                        + ".pdf";
                File filePath = new File(targetPdf);
                try {
                    PdfWriter.getInstance(document, new FileOutputStream(filePath));
                } catch (DocumentException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                document.open();
                try {
                    document.add(image);
                    document.add(paragraph4);
                    document.add( Chunk.NEWLINE );
                    document.add(paragraph5);
                    document.add(paragraph6);
                    document.add( Chunk.NEWLINE );
                    document.add(table);
                    document.add( Chunk.NEWLINE );
                    document.add( Chunk.NEWLINE );
                    document.add(paragraph);
                    document.add(paragraph1);
                    document.add( Chunk.NEWLINE );
                    document.add( Chunk.NEWLINE );
                    document.add( Chunk.NEWLINE );
                    document.add( Chunk.NEWLINE );
                    document.add(paragraph2);
                    document.add(paragraph3);
                } catch (DocumentException e) {
                    e.printStackTrace();
                }
                document.close();

            }

            @Override
            public void onFailure(Call<List<SettingOffline>>call, Throwable t) {
                // Log error here since request failed
//                    loading.dismiss();
                t.printStackTrace();
            }
        });

    }
}
class RekapKehadiranNonGuruAdapter extends RecyclerView.Adapter<RekapKehadiranNonGuruAdapter.MyViewHolder> {

    private List<KehadiranNonGuru> kehadiranNonGuruList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nip, nama, masukAbsenSesuai, masukTerlambat, masukTidakAbsen, pulangAbsenSesuai, pulangTerlambat,
        pulangTidakAbsen;

        public MyViewHolder(View view) {
            super(view);
            nip = view.findViewById(R.id.nip);
            nama = (TextView) view.findViewById(R.id.nama);
            masukAbsenSesuai = view.findViewById(R.id.masuk_absen_sesuai);
            masukTerlambat = view.findViewById(R.id.masuk_terlambat);
            masukTidakAbsen = view.findViewById(R.id.masuk_tidak_absen);
            pulangAbsenSesuai = view.findViewById(R.id.pulang_absen_sesuai);
            pulangTerlambat = view.findViewById(R.id.pulang_terlambat);
            pulangTidakAbsen = view.findViewById(R.id.pulang_tidak_absen);
        }
    }


    public RekapKehadiranNonGuruAdapter(List<KehadiranNonGuru> kehadiranNonGuruList) {
        this.kehadiranNonGuruList = kehadiranNonGuruList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_rekap_kehadiran_non_guru, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        KehadiranNonGuru kehadiranNonGuru = kehadiranNonGuruList.get(position);
        holder.nip.setText(kehadiranNonGuru.getNip());
        holder.nama.setText(kehadiranNonGuru.getNama());
        holder.masukAbsenSesuai.setText("Absen Sesuai : " + kehadiranNonGuru.getMasukAbsenSesuai());
        holder.masukTerlambat.setText("Terlambat : " + kehadiranNonGuru.getMasukTerlambat());
        holder.masukTidakAbsen.setText("Tidak Absen : " + kehadiranNonGuru.getMasukTidakAbsen());
        holder.pulangAbsenSesuai.setText("Absen Sesuai : " + kehadiranNonGuru.getPulangAbsenSesuai());
        holder.pulangTerlambat.setText("Terlambat : " + kehadiranNonGuru.getPulangTerlambat());
        holder.pulangTidakAbsen.setText("Tidak Absen : " + kehadiranNonGuru.getPulangTidakAbsen());
    }
    @Override
    public int getItemCount() {
        return kehadiranNonGuruList.size();
    }
}