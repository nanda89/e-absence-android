package alriyat.absensielektronik.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.pojo.Mapel;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;

public class PilihMapelActivity extends AppCompatActivity {

    private static final String TAG = PilihMapelActivity.class.getName();
    private MapelAdapter mapelAdapter;
    private RecyclerView rvRecyclerView;
    private ArrayList<Mapel> listMapel;
    private int isByGuru = 0;
    private String jenis = "Reguler";
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_mapel);

        rvRecyclerView = findViewById(R.id.recycler_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Pilih Mata Pelajaran");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        
        listMapel = new ArrayList<>();
        mapelAdapter = new MapelAdapter(listMapel);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvRecyclerView.setLayoutManager(mLayoutManager);
        rvRecyclerView.setItemAnimator(new DefaultItemAnimator());
        rvRecyclerView.setAdapter(mapelAdapter);

        Intent intent = getIntent();
        isByGuru = intent.getIntExtra("code",0);
        jenis = intent.getStringExtra("jenis");

        getAllMapel();
    }

    private void getAllMapel() {
        SessionConfig sessionConfig = new SessionConfig(PilihMapelActivity.this);
        SessionManager sessionManager = new SessionManager(PilihMapelActivity.this);
        GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

        Call<List<Mapel>> call;
        if(isByGuru == 0){
            call = service.getAllMapel();
        }else{
            if(jenis.equals("Reguler")){
                Log.d(TAG, "getAllMapel: getMapelByGuru");
                String guru = sessionManager.preferences.getString("idguru", "");
                Map<String, String> param = new HashMap<>();
                param.put("guru", guru);
                call = service.getMapelByGuru(param);
            }else{
                Log.d(TAG, "getAllMapel: getMapelByJenis");
                Map<String, String> param = new HashMap<>();
                param.put("jenis", "tambahan");
                call = service.getMapelByJenis(param);
            }
        }

        call.enqueue(new Callback<List<Mapel>>() {
            @Override
            public void onResponse(Call<List<Mapel>>call, retrofit2.Response<List<Mapel>> response) {

                List<Mapel> mapels = response.body();
                Log.d(TAG, "onResponse: response : "+response.body());
                listMapel.clear();

                for (Mapel mpl : mapels) {
                    listMapel.add(mpl);
                }

                mapelAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<Mapel>>call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}

class MapelAdapter extends RecyclerView.Adapter<MapelAdapter.MyViewHolder> {

    private List<Mapel>listMapel;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView idMapel, mataPelajaran, kkm, jenisMapel;

        public MyViewHolder(final View view) {
            super(view);
            idMapel = view.findViewById(R.id.id_mapel);
            mataPelajaran = view.findViewById(R.id.mapel);
            kkm = view.findViewById(R.id.kkm);
            jenisMapel = view.findViewById(R.id.jenis_mapel);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences sharedPreferences = view.getContext().getSharedPreferences("matapelajaran", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("id_mapel", idMapel.getText().toString());
                    editor.putString("mapel", mataPelajaran.getText().toString());
                    editor.apply();

                    ((PilihMapelActivity) view.getContext()).onBackPressed();
                }
            });
        }
    }

    public MapelAdapter(List<Mapel> listMapel) {
        this.listMapel = listMapel;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_mapel, parent, false);

        return new MapelAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Mapel m = listMapel.get(position);
        holder.idMapel.setText(String.valueOf(m.getIdMapel()));
        holder.mataPelajaran.setText(m.getMapel());
        holder.kkm.setText("KKM : " + m.getKkm());
        holder.jenisMapel.setText("Jenis : "  + m.getJenis());
    }

    @Override
    public int getItemCount() {
        return listMapel.size();
    }
}
