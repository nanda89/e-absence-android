package alriyat.absensielektronik.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingConfigFirstActivity extends AppCompatActivity {
    @BindView(R.id.etConfig) EditText etConfig;

    private SessionConfig sessionConfig;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        init();
    }


    private void init() {
        sessionConfig = new SessionConfig(this);
        if (sessionConfig.setLogout()){
            startActivity(new Intent(SettingConfigFirstActivity.this, LoginActivity.class));
            finish();
        }
    }

    @OnClick(R.id.btnConfig)
    protected void configClicked() {
        sessionConfig.setLogin(true, etConfig.getText().toString());
        Intent n = new Intent(SettingConfigFirstActivity.this, LoginActivity.class);
        startActivity(n);
    }
}
