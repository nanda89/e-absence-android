package alriyat.absensielektronik.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.model.GuruOffline;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;

public class PilihGuruActivity extends AppCompatActivity {

    private GuruAdapter guruAdapter;
    private RecyclerView rvRecyclerView;
    private ArrayList<GuruOffline> listGuru;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_guru);

        rvRecyclerView = findViewById(R.id.recycler_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Pilih Guru");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listGuru = new ArrayList<>();

        guruAdapter = new GuruAdapter(listGuru);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvRecyclerView.setLayoutManager(mLayoutManager);
        rvRecyclerView.setItemAnimator(new DefaultItemAnimator());
        rvRecyclerView.setAdapter(guruAdapter);

        getGuru();
    }

    private void getGuru() {
        SessionConfig sessionConfig = new SessionConfig(PilihGuruActivity.this);
        GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

        Map<String, String> param = new HashMap<>();
        param.put("id_guru", "");

        Call<List<GuruOffline>> call = service.syncUpdateTabelGuru(param);
        call.enqueue(new Callback<List<GuruOffline>>() {
            @Override
            public void onResponse(Call<List<GuruOffline>>call, retrofit2.Response<List<GuruOffline>> response) {
//                    loading.dismiss();
                List<GuruOffline> listGuruOffline = response.body();

                listGuru.clear();

                for (GuruOffline guruOffline : listGuruOffline) {
                    listGuru.add(guruOffline);
                }

                guruAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<List<GuruOffline>>call, Throwable t) {
                // Log error here since request failed
//                    loading.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

}
class GuruAdapter extends RecyclerView.Adapter<GuruAdapter.MyViewHolder> {

    private List<GuruOffline> listGuru;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView idGuru, nik, nama;

        public MyViewHolder(final View view) {
            super(view);
            idGuru = view.findViewById(R.id.id_guru);
            nik = view.findViewById(R.id.nik);
            nama = view.findViewById(R.id.nama);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences sharedPreferences = view.getContext().getSharedPreferences("rekap_kehadiran_guru", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("id_guru", idGuru.getText().toString());
                    editor.putString("nama", nama.getText().toString());
                    editor.apply();

                    ((PilihGuruActivity) view.getContext()).onBackPressed();
                }
            });
        }
    }


    public GuruAdapter(List<GuruOffline> listGuru) {
        this.listGuru = listGuru;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_guru, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        GuruOffline guru = listGuru.get(position);
        holder.idGuru.setText(guru.getIdGuru());
        holder.nik.setText(guru.getNik());
        holder.nama.setText(guru.getNama());
    }

    @Override
    public int getItemCount() {
        return listGuru.size();
    }
}
