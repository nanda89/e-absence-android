package alriyat.absensielektronik.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.model.TahunajaranOffline;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;

public class PilihTahunajaranActivity extends AppCompatActivity {

    private TahunajaranAdapter tahunajaranAdapter;
    private RecyclerView rvRecyclerView;
    private ArrayList<TahunajaranOffline> listTahunajaran;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_tahunajaran);

        rvRecyclerView = findViewById(R.id.recycler_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Pilih Tahun Ajaran");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listTahunajaran = new ArrayList<>();

        tahunajaranAdapter = new TahunajaranAdapter(listTahunajaran);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvRecyclerView.setLayoutManager(mLayoutManager);
        rvRecyclerView.setItemAnimator(new DefaultItemAnimator());
        rvRecyclerView.setAdapter(tahunajaranAdapter);

        getTahunajaran();
    }

    private void getTahunajaran() {
        SessionConfig sessionConfig = new SessionConfig(PilihTahunajaranActivity.this);
        GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

        Map<String, String> param = new HashMap<>();
        param.put("id_guru", "");

        Call<List<TahunajaranOffline>> call = service.syncUpdateTabelTahunajaran(param);
        call.enqueue(new Callback<List<TahunajaranOffline>>() {
            @Override
            public void onResponse(Call<List<TahunajaranOffline>>call, retrofit2.Response<List<TahunajaranOffline>> response) {
//                    loading.dismiss();
                List<TahunajaranOffline> listTahunajaranOffline = response.body();

                listTahunajaran.clear();

                for (TahunajaranOffline tahunajaranOffline : listTahunajaranOffline) {
                    listTahunajaran.add(tahunajaranOffline);
                }

                tahunajaranAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<List<TahunajaranOffline>>call, Throwable t) {
                // Log error here since request failed
//                    loading.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
class TahunajaranAdapter extends RecyclerView.Adapter<TahunajaranAdapter.MyViewHolder> {

    private List<TahunajaranOffline> listTahunajaran;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView idTa, tahunAjaran, tglAwal, tglAkhir;

        public MyViewHolder(final View view) {
            super(view);
            idTa = view.findViewById(R.id.id_ta);
            tahunAjaran = view.findViewById(R.id.tahun_ajaran);
            tglAwal = view.findViewById(R.id.tgl_awal);
            tglAkhir = view.findViewById(R.id.tgl_akhir);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences sharedPreferences = view.getContext().getSharedPreferences("rekap_kehadiran_guru", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("id_ta", idTa.getText().toString());
                    editor.putString("tahunajaran", tahunAjaran.getText().toString());
                    editor.apply();

                     sharedPreferences = view.getContext().getSharedPreferences("rekap_kehadiran_siswa", Context.MODE_PRIVATE);
                     editor = sharedPreferences.edit();
                    editor.putString("id_ta", idTa.getText().toString());
                    editor.putString("tahunajaran", tahunAjaran.getText().toString());
                    editor.apply();

                    sharedPreferences = view.getContext().getSharedPreferences("rekap_kehadiran_siswa_per_kelas", Context.MODE_PRIVATE);
                    editor = sharedPreferences.edit();
                    editor.putString("id_ta", idTa.getText().toString());
                    editor.putString("tahunajaran", tahunAjaran.getText().toString());
                    editor.apply();

                    sharedPreferences = view.getContext().getSharedPreferences("tambah_siswa", Context.MODE_PRIVATE);
                    editor = sharedPreferences.edit();
                    editor.putString("id_ta", idTa.getText().toString());
                    editor.putString("tahunajaran", tahunAjaran.getText().toString());
                    editor.apply();

                    ((PilihTahunajaranActivity) view.getContext()).onBackPressed();
                }
            });
        }
    }


    public TahunajaranAdapter(List<TahunajaranOffline> listTahunajaran) {
        this.listTahunajaran = listTahunajaran;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_tahunajaran, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        TahunajaranOffline tahunajaranOffline = listTahunajaran.get(position);
        holder.idTa.setText(tahunajaranOffline.getIdTa());
        holder.tahunAjaran.setText(tahunajaranOffline.getTahunAjaran());
        holder.tglAwal.setText("Tgl awal : " + tahunajaranOffline.getTglAwal());
        holder.tglAkhir.setText("Tgl akhir : " + tahunajaranOffline.getTglAkhir());
    }

    @Override
    public int getItemCount() {
        return listTahunajaran.size();
    }
}
