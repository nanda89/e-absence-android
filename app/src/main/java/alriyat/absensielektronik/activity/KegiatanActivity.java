package alriyat.absensielektronik.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.speech.RecognizerIntent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;
import android.widget.VideoView;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.pojo.GeneralResponse;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import alriyat.absensielektronik.util.SendData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KegiatanActivity extends AppCompatActivity {

    private static final String TAG = KegiatanActivity.class.getName();
    private static final int PERMISSION_CODE = 1000;
    private static final int IMAGE_CAPTURE_CODE = 1001;
    private static final int VIDEO_REQ_CODE = 100;
    private static final int URAIAN_CODE = 10;
    private static final String PHOTO = "p";
    private static final String VIDEO = "v";
    private static String BASE_URL = "http://192.168.42.42/web_absence/";

    EditText etDate ,etUraian;
    Button mCapture , btnSubmit;
    ImageButton btnUraian , btnCleanUraian;
    ImageView mImageView;
    VideoView vUraian;
    RadioGroup rGroup;
    RadioButton rbtnPhoto , rbtnVideo;
    LinearLayout lPhoto , lVideo;
    Uri image_uri , videoUri;
    private String nameFile;
    private String code = "p";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kegiatan);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_uraian);
        toolbar.setTitle("Uraian Kegiatan");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        etDate = findViewById(R.id.et_date);
        etUraian = findViewById(R.id.et_uraian_pekerjaan);
        btnUraian = findViewById(R.id.btnUraian);
        btnCleanUraian = findViewById(R.id.btnClear);
        mImageView = findViewById(R.id.image_view);
        vUraian = findViewById(R.id.video_uraian);
        mCapture = findViewById(R.id.btn_capture_image);
        rGroup = findViewById(R.id.radio_group);
        rbtnPhoto = findViewById(R.id.rbt_photo);
        rbtnVideo = findViewById(R.id.rbt_video);
        lPhoto = findViewById(R.id.lTake_photo);
        lVideo = findViewById(R.id.lTake_video);
        btnSubmit = findViewById(R.id.btn_submit);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        rbtnPhoto.setChecked(true);
        lVideo.setVisibility(View.GONE);

        setDatePicker();

        mCapture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    if(checkSelfPermission(Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED ||
                            checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){

                        String[]permissions = {Manifest.permission.CAMERA , Manifest.permission.WRITE_EXTERNAL_STORAGE};

                        requestPermissions(permissions , PERMISSION_CODE);
                    }else{
                        openCamera();
                    }
                }else{
                    openCamera();
                }
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isCan = checkForm();
                if(isCan){
                    try {
//                        doUpload();
                        doUploadNew();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else {
                    Toast.makeText(KegiatanActivity.this, "Data tidak boleh kosong", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnUraian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
                intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());

                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivityForResult(intent, URAIAN_CODE);
                } else {
                    Toast.makeText(KegiatanActivity.this, "Handphone anda tidak kompatibel dengan fitur ini", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnCleanUraian.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etUraian.setText("");
            }
        });
    }

    private void setDatePicker() {
        final Calendar tanggal = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener dateDari = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                tanggal.set(Calendar.YEAR, year);
                tanggal.set(Calendar.MONTH, monthOfYear);
                tanggal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                etDate.setText(String.format("%02d", tanggal.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", tanggal.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", tanggal.get(Calendar.YEAR)));
            }
        };
        etDate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(KegiatanActivity.this, dateDari, tanggal
                        .get(Calendar.YEAR), tanggal.get(Calendar.MONTH),
                        tanggal.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void doUploadNew() throws IOException {
        Log.d(TAG, "uploadFile: process upload data ");
        SessionConfig sessionConfig = new SessionConfig(KegiatanActivity.this);
        final SessionManager sessionManager = new SessionManager(KegiatanActivity.this);
        final String ip = sessionConfig.preferences.getString("ipconfig",null);
        BASE_URL = "http://"+ip+ "/web_absence/";

        String idNonGuru = sessionManager.preferences.getString("idnonguru", null);
        String tanggal;
        if(etDate.getText().toString().equals("")){
            SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
            tanggal = f.format(new Date());
        }else {
            tanggal = etDate.getText().toString();
        }
        String uraian = etUraian.getText().toString();
        String dir = Environment.getExternalStorageDirectory().getAbsolutePath()+"/eAbsence/image";
        String type = PHOTO;
        if(code.equals(VIDEO)){
            type = VIDEO;
            dir = Environment.getExternalStorageDirectory().getAbsolutePath()+"/eAbsence/video";
        }
        String filename = nameFile;
        File file = new File(dir , filename);

        new SendData.sendUraian(idNonGuru,tanggal,uraian,type,nameFile,file,BASE_URL,this).execute();


    }
    private void doUpload() throws IOException {
        Log.d(TAG, "uploadFile: process upload data ");
        SessionConfig sessionConfig = new SessionConfig(KegiatanActivity.this);
        final SessionManager sessionManager = new SessionManager(KegiatanActivity.this);
        final String ip = sessionConfig.preferences.getString("ipconfig",null);

        final ProgressDialog dialogo = new ProgressDialog(KegiatanActivity.this);
        dialogo.setMessage("Pengiriman data...");
        dialogo.setCancelable(false);
        dialogo.show();

        GetDataService service =
                RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

        String idNonGuru = sessionManager.preferences.getString("idnonguru", null);
        String tanggal;
        if(etDate.getText().toString().equals("")){
            SimpleDateFormat f = new SimpleDateFormat("dd-MM-yyyy");
            tanggal = f.format(new Date());
        }else {
            tanggal = etDate.getText().toString();
        }
        String uraian = etUraian.getText().toString();
        String dir = Environment.getExternalStorageDirectory().getAbsolutePath()+"/eAbsence/image";
        String type = PHOTO;
        if(code.equals(VIDEO)){
            type = VIDEO;
            dir = Environment.getExternalStorageDirectory().getAbsolutePath()+"/eAbsence/video";
        }
        String filename = nameFile;
        Log.d(TAG, "doUpload: idNonGuru : "+idNonGuru+"\n" +
                        "tanggal : "+tanggal+"\n"+
                        "uraian : "+uraian+"\n"+
                        "type : "+type+"\n"+
                        "filename : "+filename+"\n");

        File file = new File(dir , filename);
//        Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),image_uri);
//        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
//        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
//        String encodedImage = Base64.encodeToString(byteArrayOutputStream.toByteArray(), Base64.DEFAULT);
//        String imgname = String.valueOf(Calendar.getInstance().getTimeInMillis());

        Call<GeneralResponse> call = service.uploadUraian(idNonGuru,tanggal,uraian,type,file);
        call.enqueue(new Callback<GeneralResponse>() {
            @Override
            public void onResponse(Call<GeneralResponse> call, Response<GeneralResponse> response) {
                dialogo.dismiss();
                Log.d(TAG, "onResponse: "+response.body().getMessage());
            }

            @Override
            public void onFailure(Call<GeneralResponse> call, Throwable t) {
                dialogo.dismiss();
                Log.d(TAG, "onFailure: "+t.getMessage());
            }
        });


    }

    private boolean checkForm() {
        boolean result = true;

        String uraian = etUraian.getText().toString();
        if(uraian.isEmpty()){
            result = false;
        }
        Log.d(TAG, "checkForm: code existing : "+code);
        if(code.equals(VIDEO)){
            Log.d(TAG, "checkForm: durr "+vUraian.getDuration());
            if(vUraian.getDuration() < 0){
                Log.d(TAG, "checkForm: video null : ");
                result = false;
            }
        }
        return result;
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    public void captureVideo(View view){
        Intent cameraIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        File videoFile = getVideoPath();
        videoUri = Uri.fromFile(videoFile);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT , videoUri);
        cameraIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY,1);
        cameraIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT,10);
        startActivityForResult(cameraIntent , VIDEO_REQ_CODE);
    }
    private void openCamera() {
//        ContentValues values = new ContentValues();
//        values.put(MediaStore.Images.Media.TITLE , "New Picture");
//        values.put(MediaStore.Images.Media.DESCRIPTION , "From the camera");
//        image_uri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI , values);
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File photoFile = getImagePath();
        image_uri = Uri.fromFile(photoFile);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT , image_uri);
        startActivityForResult(cameraIntent , IMAGE_CAPTURE_CODE);
    }
    public File getVideoPath(){
        File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/eAbsence/video");
        if(!folder.exists()){
            folder.mkdirs();
        }
        SimpleDateFormat f = new SimpleDateFormat("ddMMyyyyhhss");
        String time = f.format(new Date());
        String vName = "uraian_"+time+".mp4";
        nameFile = vName;
        File vFile = new File(folder , vName);
        return vFile;
    }
    public File getImagePath(){
        File folder = new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/eAbsence/image");
        if(!folder.exists()){
            folder.mkdirs();
        }
        SimpleDateFormat f = new SimpleDateFormat("ddMMyyyyhhss");
        String time = f.format(new Date());
        String pName = "uraian_"+time+".jpg";
        nameFile = pName;
        File pFile = new File(folder , pName);
        return pFile;
    }

    public void setChecked(View view){
        boolean checked = ((RadioButton) view).isChecked();
        Log.d(TAG, "setChecked: id checked is : "+view.getId());
        switch (view.getId()){
            case R.id.rbt_photo:
                if(checked){
                    showPhotoOrVideo(PHOTO);
                }
                code = PHOTO;
                break;
            case R.id.rbt_video:
                if(checked){
                    showPhotoOrVideo(VIDEO);
                }
                code = VIDEO;
                break;
        }
    }

    private void showPhotoOrVideo(String code) {
        if(code.equals(PHOTO)){
            lPhoto.setVisibility(View.VISIBLE);
            if(lVideo != null){
                lVideo.setVisibility(View.GONE);
            }

        }else if(code.equals(VIDEO)){
            if(lPhoto != null){
                lPhoto.setVisibility(View.GONE);
            }
            lVideo.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        switch (requestCode){
            case PERMISSION_CODE:{
                if(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    openCamera();
                }else {
                    Toast.makeText(this, "Permission denied...", Toast.LENGTH_SHORT).show();
                }

            }
            default:
                throw new IllegalStateException("Unexpected value: " + requestCode);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK){
            if(requestCode == VIDEO_REQ_CODE){
//                Toast.makeText(this, "Success by name "+videoName, Toast.LENGTH_SHORT).show();
                vUraian.setVideoURI(videoUri);

                MediaController mediaController = new MediaController(this);
                vUraian.setMediaController(mediaController);
                mediaController.setAnchorView(vUraian);
            }else if(requestCode == IMAGE_CAPTURE_CODE){
                mImageView.setImageURI(image_uri);
            }else if(requestCode == URAIAN_CODE && data != null){
                ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                String dataUraian = etUraian.getText() == null ? "" : etUraian.getText().toString()+"\n";
                dataUraian += result.get(0);
                etUraian.setText(dataUraian);
            }

        }else {
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        }
    }

}
