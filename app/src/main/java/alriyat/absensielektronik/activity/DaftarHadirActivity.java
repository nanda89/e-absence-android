package alriyat.absensielektronik.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.github.barteksc.pdfviewer.PDFView;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.SettingOffline;
import alriyat.absensielektronik.pojo.DaftarHadir;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import alriyat.absensielektronik.util.Vira;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DaftarHadirActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private static final String TAG = DaftarHadirActivity.class.getName();
    private EditText etDari;
    private EditText etKe;
    private EditText etJenjang;
    private EditText etTahunajaran;
    private EditText etMapel;
    private EditText etKelas;
    private Button bFilter;
    private RelativeLayout rlContent;
    private TextView tvMapel, tvTatapMuka, tvhadir, tvSakit, tvIzin, tvTk, tvNihil;
    private Spinner spJenis;
    private Button btnDetail;
    private PDFView pdfView;
    private String jenis;
    Integer pageNumber = 0;
    private SessionConfig sessionConfig;
    private SessionManager sessionManager;
    private String id_guru;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daftar_hadir);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_laporan_daftar_hadir);
        toolbar.setTitle("Daftar Hadir");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        etDari = findViewById(R.id.start_date_daftar_hadir);
        etKe = findViewById(R.id.end_date_daftar_hadir);
        etJenjang = findViewById(R.id.periode_daftar_hadir);
        etTahunajaran = findViewById(R.id.tahun_ajaran_daftar_hadir);
        etMapel = findViewById(R.id.mapel_daftar_hadir);
        etKelas = findViewById(R.id.kelas_daftar_hadir);
        bFilter = findViewById(R.id.bt_filter_daftar_hadir);

        rlContent = findViewById(R.id.content_daftar_hadir);
        tvMapel = findViewById(R.id.val_mapel_daftar_hadir);
        tvTatapMuka = findViewById(R.id.val_tatap_muka_daftar_hadir);
        tvhadir = findViewById(R.id.val_hadir_daftar_hadir);
        tvSakit = findViewById(R.id.val_sakit_daftar_hadir);
        tvIzin = findViewById(R.id.val_izin_daftar_hadir);
        tvTk = findViewById(R.id.val_tk_daftar_hadir);
        tvNihil = findViewById(R.id.val_nihil_daftar_hadir);
        btnDetail = findViewById(R.id.btnDetail_daftar_hadir);
        spJenis = findViewById(R.id.spinnerJenis);
        pdfView = (PDFView) findViewById(R.id.pdfView);

        sessionConfig = new SessionConfig(this);
        sessionManager = new SessionManager(this);
        id_guru = sessionManager.preferences.getString("id_guru", null);
        ArrayAdapter<CharSequence> adpSpinner = ArrayAdapter.createFromResource(this, R.array.jenis_absensi, R.layout.spinner_layout);
        adpSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spJenis.setAdapter(adpSpinner);
        spJenis.setOnItemSelectedListener(this);

        setDatePicker();
        setPeriode();
        kelas();
        mapel();

        bFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String dari = etDari.getText().toString();
                String ke = etKe.getText().toString();
                String jenjang = etJenjang.getText().toString();
                String tahunajaran = etTahunajaran.getText().toString();
                String mapel = etMapel.getText().toString();
                String kelas = etKelas.getText().toString();

                if (dari.isEmpty()) {
                    Toast.makeText(DaftarHadirActivity.this, "Tanggal awal tidak boleh kosong", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (ke.isEmpty()) {
                    Toast.makeText(DaftarHadirActivity.this, "Tanggal akhir tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (jenjang.isEmpty()) {
                    Toast.makeText(DaftarHadirActivity.this, "Jenjang tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (tahunajaran.isEmpty()) {
                    Toast.makeText(DaftarHadirActivity.this, "Tahun ajaran tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (mapel.isEmpty()) {
                    Toast.makeText(DaftarHadirActivity.this, "Matapelajaran tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (etKelas.isEnabled()) {
                    if (kelas.isEmpty()) {
                        Toast.makeText(DaftarHadirActivity.this, "Kelas tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }
                if (jenis.isEmpty() || jenis.contains("Pilih")) {
                    Toast.makeText(DaftarHadirActivity.this, "Jenis tidak boleh kosong!", Toast.LENGTH_SHORT).show();
                    return;
                }

                getDaftarHadir();
            }
        });

        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
        emptyPref();


    }

    private void emptyPref() {
        SharedPreferences sharedPreferences = getSharedPreferences("rekap_kehadiran_guru", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("id_jenjang", "");
        editor.putString("jenjang", "");
        editor.putString("id_ta", "");
        editor.putString("tahunajaran", "");
        editor.putString("id_guru", "");
        editor.putString("nama", "");
        editor.putString("id_kelas", "");
        editor.apply();

        SharedPreferences sharedPreferencesKelas = getSharedPreferences("rekap_kehadiran_siswa_per_kelas", Context.MODE_PRIVATE);
        SharedPreferences.Editor editorkelas = sharedPreferencesKelas.edit();
        editorkelas.putString("id_kelas", "");
        editorkelas.putString("kelas", "");
        editorkelas.apply();

        SharedPreferences sharedPreferencesMapel = getSharedPreferences("matapelajaran", Context.MODE_PRIVATE);
        SharedPreferences.Editor editorMapel = sharedPreferencesMapel.edit();
        editorMapel.putString("id_mapel", "");
        editorMapel.putString("mapel", "");
        editorMapel.apply();
    }

    @Override
    protected void onResume() {

        if (Vira.isConnectedToServer(getApplicationContext())) {
            bFilter.setClickable(true);
        } else {
            bFilter.setClickable(false);
            Toast.makeText(this, "Anda tidak terhubung ke server", Toast.LENGTH_LONG).show();
        }
        SharedPreferences sharedPreferences = getSharedPreferences("rekap_kehadiran_guru", Context.MODE_PRIVATE);
        SharedPreferences sharedPreferencesMapel = getSharedPreferences("matapelajaran", Context.MODE_PRIVATE);
        SharedPreferences sharedPreferencesKelas = getSharedPreferences("rekap_kehadiran_siswa_per_kelas", Context.MODE_PRIVATE);
        etJenjang.setText(sharedPreferences.getString("jenjang", ""));
        etTahunajaran.setText(sharedPreferences.getString("tahunajaran", ""));
        etMapel.setText(sharedPreferencesMapel.getString("mapel", ""));
        etKelas.setText(sharedPreferencesKelas.getString("kelas", ""));
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    private void getDaftarHadir() {
        if (Vira.isConnectedToServer(getApplicationContext())) {
            SessionManager sessionManager = new SessionManager(DaftarHadirActivity.this);
            final ProgressDialog loading = ProgressDialog.show(this, "", "Loading Data...", false, false);
            SharedPreferences sharedPreferences = getSharedPreferences("rekap_kehadiran_guru", Context.MODE_PRIVATE);
            SharedPreferences sharedPreferencesKelas = getSharedPreferences("rekap_kehadiran_siswa_per_kelas", Context.MODE_PRIVATE);
            SharedPreferences sharedPreferencesMapel = getSharedPreferences("matapelajaran", Context.MODE_PRIVATE);

            String idTA = sharedPreferences.getString("id_ta", "");
            String idJenjang = sharedPreferences.getString("id_jenjang", "");
            String idKelas = sharedPreferencesKelas.getString("id_kelas", "");
            String idMapel = sharedPreferencesMapel.getString("id_mapel", "");
            String guru = sessionManager.preferences.getString("idguru", "");

            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            Map<String, String> params = new HashMap<>();
            params.put("dari", etDari.getText().toString());
            params.put("ke", etKe.getText().toString());
            params.put("id_jenjang", idJenjang);
            params.put("id_ta", idTA);
            params.put("id_kelas", idKelas);
            params.put("id_mapel", idMapel);
            params.put("guru", guru);
            params.put("jenis", jenis);

            Log.d(TAG, "getDaftarHadir: request : \n" +
                    "dari : " + etDari.getText().toString() + "\n" +
                    "ke : " + etKe.getText().toString() + "\n" +
                    "id_jenjang : " + idJenjang + "\n" +
                    "id_ta : " + idTA + "\n" +
                    "id_kelas : " + idKelas + "\n" +
                    "id_mapel : " + idMapel + "\n" +
                    "guru : " + guru + "\n" +
                    "jenis : " + jenis + "\n" +
                    "");

            Call<DaftarHadir> call = service.daftarHadir(params);
            call.enqueue(new Callback<DaftarHadir>() {
                @Override
                public void onResponse(Call<DaftarHadir> call, Response<DaftarHadir> response) {

                    Log.d(TAG, "onResponse: " + response.body().toString() + "\n" +
                            "total siswa : ");
                    DaftarHadir daftarHadir = response.body();

                    for (DaftarHadir.Siswa t : daftarHadir.getSiswas()) {
                        Log.d(TAG, "onResponse: --> " + t.getNama());
                        for (String absen : t.getAbsen()) {
                            Log.d(TAG, "onResponse: absen : " + absen);
                        }
                    }

                    if (response.body().getTanggal().size() > 0) {
                        showContent(daftarHadir);
                    } else {
                        Toast.makeText(DaftarHadirActivity.this, "Data tidak ditemukan, periksa filter data", Toast.LENGTH_SHORT).show();
                        rlContent.setVisibility(View.GONE);
                    }
                    loading.dismiss();
                }

                @Override
                public void onFailure(Call<DaftarHadir> call, Throwable t) {
                    rlContent.setVisibility(View.GONE);
                    loading.dismiss();
                }
            });


        } else {
            Toast.makeText(this, "Pencarian gagal, anda tidak terhubung dengan server", Toast.LENGTH_SHORT).show();
        }
    }

    private void showContent(final DaftarHadir daftarHadir) {
        rlContent.setVisibility(View.VISIBLE);
        tvTatapMuka.setText(String.valueOf(daftarHadir.getTatapMuka()));
        tvMapel.setText(etMapel.getText().toString());
        tvhadir.setText(String.valueOf(daftarHadir.getHadir()));
        tvSakit.setText(String.valueOf(daftarHadir.getSakit()));
        tvIzin.setText(String.valueOf(daftarHadir.getIzin()));
        tvNihil.setText(String.valueOf(daftarHadir.getNihil()));
        tvTk.setText(String.valueOf(daftarHadir.getAlpha()));

        btnDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mapel = etMapel.getText().toString();
                String kelas = etKelas.getText().toString();
                showPDF(daftarHadir, mapel, kelas);
            }
        });

    }

    private void mapel() {
        etMapel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!jenis.isEmpty() || !jenis.contains("pilih")){
                    Intent intent = new Intent(DaftarHadirActivity.this, PilihMapelActivity.class);
                    intent.putExtra("code", 1);
                    intent.putExtra("jenis", jenis);
                    startActivity(intent);
                }else {
                    Toast.makeText(DaftarHadirActivity.this, "Harap pilih jenis absensi terlebih dahulu!", Toast.LENGTH_LONG).show();
                }

            }
        });
    }

    private void kelas() {
        etKelas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DaftarHadirActivity.this, PilihKelasByGuruActivity.class);
                startActivity(intent);
            }
        });
    }

    private void setDatePicker() {
        final Calendar calendarDari = Calendar.getInstance();
        final Calendar calendarKe = Calendar.getInstance();

        final DatePickerDialog.OnDateSetListener dateDari = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendarDari.set(Calendar.YEAR, year);
                calendarDari.set(Calendar.MONTH, monthOfYear);
                calendarDari.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                etDari.setText(String.format("%02d", calendarDari.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendarDari.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", calendarDari.get(Calendar.YEAR)));
            }
        };
        etDari.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(DaftarHadirActivity.this, dateDari, calendarDari
                        .get(Calendar.YEAR), calendarDari.get(Calendar.MONTH),
                        calendarDari.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        final DatePickerDialog.OnDateSetListener dateKe = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                calendarKe.set(Calendar.YEAR, year);
                calendarKe.set(Calendar.MONTH, monthOfYear);
                calendarKe.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                etKe.setText(String.format("%02d", calendarKe.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendarKe.get(Calendar.MONTH) + 1) + "-" + String.format("%02d", calendarKe.get(Calendar.YEAR)));
            }

        };

        etKe.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(DaftarHadirActivity.this, dateKe, calendarKe
                        .get(Calendar.YEAR), calendarKe.get(Calendar.MONTH),
                        calendarKe.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
    }

    private void setPeriode() {
        etJenjang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DaftarHadirActivity.this, PilihJenjangActivity.class);
                startActivity(intent);
            }
        });

        etTahunajaran.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DaftarHadirActivity.this, PilihTahunajaranActivity.class);
                startActivity(intent);
            }
        });
    }
    @SuppressWarnings("ResultOfMethodCallIgnored")
    private void showPDF(final DaftarHadir daftarHadir, final String mapel, final String namaKelas) {
        SessionConfig sessionConfig = new SessionConfig(DaftarHadirActivity.this);
        final String ip = sessionConfig.preferences.getString("ipconfig", null);

        GetDataService service =
                RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

        HashMap<String, String> param = new HashMap<>();
        param.put("id_guru", "");

        Call<List<SettingOffline>> call = service.syncUpdateTabelSetting(param);
        call.enqueue(new Callback<List<SettingOffline>>() {
            @Override
            public void onResponse(Call<List<SettingOffline>> call, retrofit2.Response<List<SettingOffline>> response) {
//                    loading.dismiss();
                List<SettingOffline> listSettingOffline = response.body();
                SettingOffline setting = new SettingOffline();

                for (SettingOffline settingOffline : listSettingOffline) {
                    setting.setIdSetting(settingOffline.getIdSetting());
                    setting.setLogoMini(settingOffline.getLogoMini());
                    setting.setLogoGambar(settingOffline.getLogoGambar());
                    setting.setLogoFavicon(settingOffline.getLogoFavicon());
                    setting.setNik(settingOffline.getNik());
                    setting.setKartuGsm(settingOffline.getKartuGsm());
                    setting.setCekPulsa(settingOffline.getCekPulsa());
                    setting.setMetode(settingOffline.getMetode());
                    setting.setKodeAktivasi(settingOffline.getKodeAktivasi());
                    setting.setLinkTujuan(settingOffline.getLinkTujuan());
                    setting.setLogoBesar(settingOffline.getLogoBesar());
                    setting.setLogoSekolah(settingOffline.getLogoSekolah());
                    setting.setPimpinan(settingOffline.getPimpinan());
                    setting.setAkunGmail(settingOffline.getAkunGmail());
                    setting.setAlamat(settingOffline.getAlamat());
                    setting.setWebsite(settingOffline.getWebsite());
                    setting.setKopSurat(settingOffline.getKopSurat());
                    setting.setTtd(settingOffline.getTtd());
                    setting.setTelat(settingOffline.getTelat());
                    setting.setToleransiJamAkhir(settingOffline.getToleransiJamAkhir());
                    setting.setLat(settingOffline.getLat());
                    setting.setLng(settingOffline.getLng());
                    setting.setRadius(settingOffline.getRadius());
                    setting.setJamPulang(settingOffline.getJamPulang());
                    setting.setKota(settingOffline.getKota());
                }

                Document document = new Document();
                document.setPageSize(PageSize.LEGAL.rotate());


                // Creating an ImageData object
                String imFile = "http://" + ip + "/web_absence/assets/panel/images/" + setting.getKopSurat();

                StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);

                Image image = null;
                try {
                    image = Image.getInstance(imFile);
                } catch (BadElementException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                image.scaleAbsolute(document.getPageSize().getWidth() - document.leftMargin(), 150f);

                float kolom[] = new float[6 + daftarHadir.getTanggal().size()];
                kolom[0] = 1;
                kolom[1] = 2;
                kolom[2] = 3;
                kolom[3] = 1;

                for (int i = 0; i < daftarHadir.getTanggal().size(); i++) {
                    kolom[i + 4] = 1;
                }

                kolom[daftarHadir.getTanggal().size() + 4] = 1;
                kolom[daftarHadir.getTanggal().size() + 5] = 1;

                PdfPTable table = new PdfPTable(kolom);
                table.setTotalWidth(document.getPageSize().getWidth() - document.leftMargin() * 2);
                table.setLockedWidth(true);
                table.setHorizontalAlignment(Element.ALIGN_LEFT);
                table.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell("NO");
                table.addCell("NO STB");
                table.addCell("NAMA SISWA");
                table.addCell("JK");

                for (DaftarHadir.Tanggal t : daftarHadir.getTanggal()) {
                    table.addCell(t.getTgl());
                }
                table.addCell("HADIR");
                table.addCell("TDK.HDR");
                table.setHeaderRows(1);
                PdfPCell[] cells = table.getRow(0).getCells();
                for (int j = 0; j < cells.length; j++) {
                    cells[j].setBackgroundColor(BaseColor.GRAY);
                }
                for (DaftarHadir.Siswa siswa : daftarHadir.getSiswas()) {
                    table.addCell(String.valueOf(siswa.getNo()));
                    table.addCell(siswa.getStb());

                    PdfPCell cell = new PdfPCell();
                    Phrase p = new Phrase(siswa.getNama());
                    cell.addElement(p);
                    cell.setHorizontalAlignment(Element.ALIGN_LEFT);
                    table.addCell(cell);

                    table.addCell(siswa.getSex());
                    for (String absen : siswa.getAbsen()) {
                        table.addCell(absen);
                    }
                    table.addCell(String.valueOf(siswa.getHadir()));
                    table.addCell(String.valueOf(siswa.getAlpha()));
                }

                PdfPTable table1 = new PdfPTable(new float[]{1, 2, 4, 3});
                table1.setTotalWidth(document.getPageSize().getWidth() - document.leftMargin() * 2);
                table1.setLockedWidth(true);
                table1.setHorizontalAlignment(Element.ALIGN_LEFT);
                table1.getDefaultCell().setHorizontalAlignment(Element.ALIGN_CENTER);
                table1.addCell("NO");
                table1.addCell("TGL TATAP MUKA");
                table1.addCell("STANDAR KOMPETENSI / KOMPETENSI DASAR / INDIKATOR");
                table1.addCell("KETERANGAN");
                table1.setHeaderRows(1);
                PdfPCell[] cells1 = table1.getRow(0).getCells();
                for (int j = 0; j < cells1.length; j++) {
                    cells1[j].setBackgroundColor(BaseColor.GRAY);
                }

                for (DaftarHadir.Attachment at : daftarHadir.getAttachments()) {
                    table1.addCell(String.valueOf(at.getNo()));
                    table1.addCell(at.getTanggal());
                    table1.addCell(at.getKopetensi());
                    table1.addCell(at.getKeterangan());
                }

                SharedPreferences sharedPreferences = getSharedPreferences("rekap_kehadiran_guru", Context.MODE_PRIVATE);
                Calendar calendar = Calendar.getInstance();

                String para = setting.getKota() + ", " + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH)) + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1) + "-" + String.format("%04d", calendar.get(Calendar.YEAR));
                Paragraph paragraph = new Paragraph(para);
                paragraph.setAlignment(Element.ALIGN_RIGHT);

                para = "                                                                                 Kepala " + setting.getLogoBesar();
                Paragraph paragraph1 = new Paragraph(para);
                paragraph1.setAlignment(Element.ALIGN_RIGHT);

                para = "                                                                                 " + setting.getPimpinan();
                Paragraph paragraph2 = new Paragraph(para);
                paragraph2.setAlignment(Element.ALIGN_RIGHT);

                para = "                                                                                 " + setting.getNik();
                Paragraph paragraph3 = new Paragraph(para);
                paragraph3.setAlignment(Element.ALIGN_RIGHT);

                para = "DAFTAR HADIR KELAS " + namaKelas + "\nTAHUN PELASAJARAN " + etTahunajaran.getText().toString();
                Font boldFont = new Font(Font.FontFamily.TIMES_ROMAN, 18, Font.BOLD);
                Paragraph paragraph4 = new Paragraph(para, boldFont);
                paragraph4.setAlignment(Element.ALIGN_CENTER);

                para = "MATA DIKLAT : " + mapel;
                Paragraph paragraph5 = new Paragraph(para);

                para = "KELAS : " + namaKelas;
                Paragraph paragraph6 = new Paragraph(para);

                para = "JENJANG : " + sharedPreferences.getString("jenjang", "");
                Paragraph paragraph7 = new Paragraph(para);

//                para = "GURU : " + sharedPreferences.getString("nama", "");
//                para = "GURU : " + dataGuru.get(0).getNama();
//                Paragraph paragraph8 = new Paragraph(para);

                // write the document content
                String directory_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/eAbsence/";
                File file = new File(directory_path);
                if (!file.exists()) {
                    Log.d(TAG, "onResponse: directory not exist : "+directory_path);
                    file.mkdirs();
                    Log.d(TAG, "onResponse: directory created");
                }else{
                    Log.d(TAG, "onResponse: directory exist : "+directory_path);
                }
                String targetPdf = directory_path
                        + String.format("%02d", calendar.get(Calendar.DAY_OF_MONTH))
                        + "-" + String.format("%02d", calendar.get(Calendar.MONTH) + 1)
                        + "-" + String.format("%04d", calendar.get(Calendar.YEAR))
                        + "_daftar_hadir_"
                        + String.format("%02d", calendar.get(Calendar.HOUR_OF_DAY))
                        + String.format("%02d", calendar.get(Calendar.MINUTE))
                        + ".pdf";
                Log.d(TAG, "onResponse: target --> "+targetPdf);
                File filePath = new File(targetPdf);
                try {
                    PdfWriter.getInstance(document, new FileOutputStream(filePath));
                } catch (DocumentException e) {
                    e.printStackTrace();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
                document.open();
                try {
                    document.add(image);
                    document.add(paragraph4);
                    document.add(Chunk.NEWLINE);
                    document.add(paragraph5);
                    document.add(paragraph6);
                    document.add(paragraph7);
//                    document.add(paragraph8);
                    document.add(Chunk.NEWLINE);
                    document.add(table);
                    document.add(Chunk.NEWLINE);
                    document.add(Chunk.NEWLINE);
                    document.add(paragraph);
                    document.add(paragraph1);
                    document.add(Chunk.NEWLINE);
                    document.add(Chunk.NEWLINE);
                    document.add(Chunk.NEWLINE);
                    document.add(Chunk.NEWLINE);
                    document.add(paragraph2);
                    document.add(paragraph3);
                    if(daftarHadir.getAttachments().size() > 0){
                        document.add(Chunk.NEXTPAGE);
                    }
                    document.add(table1);

                } catch (DocumentException e) {
                    e.printStackTrace();
                }
                document.close();
//                Toast.makeText(DaftarHadirActivity.this, "File berhasil tersimpan pada path \n" + targetPdf, Toast.LENGTH_LONG).show();
                showData(targetPdf);
            }

            @Override
            public void onFailure(Call<List<SettingOffline>> call, Throwable t) {
                // Log error here since request failed
//                    loading.dismiss();
                t.printStackTrace();
            }
        });

    }

    private void showData(String path) {
        File file = new File(path);
        Intent target = new Intent(Intent.ACTION_VIEW);
        target.setDataAndType(Uri.fromFile(file),"application/pdf");
        target.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
        Intent intent = Intent.createChooser(target , "Open File");
        try {
            startActivity(intent);
        }catch (ActivityNotFoundException e){
            e.printStackTrace();
        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        jenis = parent.getItemAtPosition(position).toString();
        if (jenis.equals("Reguler")) {
            etKelas.setEnabled(true);
            etKelas.setFocusableInTouchMode(true);
        } else {
            etKelas.setEnabled(false);
            etKelas.setFocusableInTouchMode(false);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
