package alriyat.absensielektronik.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import alriyat.absensielektronik.app.MySingleton;
import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.Siswa;
import alriyat.absensielektronik.model.Absen;
import alriyat.absensielektronik.util.DatabaseHandler;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import alriyat.absensielektronik.util.Vira;
import retrofit2.Call;
import retrofit2.Callback;

public class AbsensiPelajaranActivity extends AppCompatActivity {
    private static final String TAG = AbsensiPelajaranActivity.class.getName();
    private static final String KELAS_EXTRA = "extra";
    private String idnews, idjadwal, idKelasTambahan;
    private Toolbar toolbar;
    private SessionManager sessionManager;
    private SessionConfig sessionConfig;
    private ArrayList<Siswa> siswaList;
    private ArrayList<Absen> absenArrayList;
    private RecyclerView recyclerView;
    private AbsensiPelajaranActivity.AdapterSiswa adapterSiswa;
    private ArrayList<String> absen_label;
    private RecyclerView.LayoutManager layoutManager;
    private AlertDialog dialog;
    int year_x, month_x, day_x;
    private MenuItem fastMenu, validmenu , attachment;
    private Boolean statusWaliKelas;
    private LocationManager locationManager;
    private double lat, lng;
    private boolean isKelasTambahan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_absensi_pelajaran);

        toolbar = (Toolbar) findViewById(R.id.toolbar_absen_kelas);
        toolbar.setTitle("Daftar Siswa");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        if (KELAS_EXTRA.equals(getIntent().getStringExtra("code"))) {
            idKelasTambahan = getIntent().getStringExtra("idkelastambahan");
            isKelasTambahan = true;
        } else {
            idnews = getIntent().getStringExtra("idkelas");
            idjadwal = getIntent().getStringExtra("idjadwal");
            isKelasTambahan = false;
        }
        sessionConfig = new SessionConfig(this);
        sessionManager = new SessionManager(this);

//        siswaList = new ArrayList<>();
//        recyclerView = (RecyclerView) findViewById(R.id.recycler_siswa_pelajaran);
//        layoutManager = new LinearLayoutManager(this);
//        recyclerView.setLayoutManager(layoutManager);
//        //recyclerView.setHasFixedSize(true);
//        adapterSiswa = new AbsensiPelajaranActivity.AdapterSiswa(this, siswaList);
//        recyclerView.setAdapter(adapterSiswa);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

//        siswaList = getAllSiswa();

        final Calendar cal = Calendar.getInstance();
        year_x = cal.get(Calendar.YEAR);
        month_x = cal.get(Calendar.MONTH) + 1;
        day_x = cal.get(Calendar.DAY_OF_MONTH);
        //etTgl.setText(day_x+"-"+month_x+"-"+year_x);
        if (month_x <= 9) {
            if (day_x <= 9) {
                toolbar.setSubtitle("0" + day_x + "-" + "0" + month_x + "-" + year_x);
            } else if (day_x > 9) {
                toolbar.setSubtitle(day_x + "-" + "0" + month_x + "-" + year_x);
            }
        } else if (month_x > 9) {
            if (day_x <= 9) {
                toolbar.setSubtitle("0" + day_x + "-" + month_x + "-" + year_x);
            } else if (day_x > 9) {
                toolbar.setSubtitle(day_x + "-" + month_x + "-" + year_x);
            }
        } else {

        }

        cekStatusWaliKelas();

        locationManager = (LocationManager) AbsensiPelajaranActivity.this.getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    0);
        } else {
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    0,
                    0, locationListenerGPS);
            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                    0,
                    0, locationListenerNetwork);
            isLocationEnabled();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 0: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                            0,
                            0, locationListenerGPS);
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,
                            0,
                            0, locationListenerNetwork);
                    isLocationEnabled();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    LocationListener locationListenerGPS = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            lat = latitude;
            lng = longitude;
//            Toast.makeText(AbsensiPelajaranActivity.this, "Lokasi sudah didapatkan", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    LocationListener locationListenerNetwork = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            lat = latitude;
            lng = longitude;
//            Toast.makeText(AbsensiPelajaranActivity.this, "Lokasi sudah didapatkan", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    protected void onResume() {
        super.onResume();

        siswaList = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.recycler_siswa_pelajaran);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        //recyclerView.setHasFixedSize(true);
        adapterSiswa = new AbsensiPelajaranActivity.AdapterSiswa(this, siswaList);
        recyclerView.setAdapter(adapterSiswa);

        isLocationEnabled();
        getAllSiswa();
    }

    private void isLocationEnabled() {

        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(AbsensiPelajaranActivity.this);
            alertDialog.setTitle("Enable Location");
            alertDialog.setMessage("Your locations setting is not enabled. Please enabled it in settings menu.");
            alertDialog.setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    dialog.cancel();
                }
            });
            AlertDialog alert = alertDialog.create();
            alert.show();
        }
    }

    private void goToBack() {
        super.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.


        getMenuInflater().inflate(R.menu.absensi, menu);
        Log.d(TAG, "onCreateOptionsMenu: 22222222222222222");
        fastMenu = menu.findItem(R.id.action_fast);
        validmenu = menu.findItem(R.id.action_valid);
        attachment = menu.findItem(R.id.action_attachment);
        siswaList = getAllSiswa();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_valid) {
            SessionManager sessionManager = new SessionManager(AbsensiPelajaranActivity.this);
            String statusGuruBK = sessionManager.preferences.getString("statusGuruBK", null);
            if (statusGuruBK.equals("1")) {
                AlertDialog.Builder builderSingle = new AlertDialog.Builder(AbsensiPelajaranActivity.this);
                builderSingle.setTitle("Status guru yang tidak hadir");

                final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(AbsensiPelajaranActivity.this, android.R.layout.simple_list_item_1);
                arrayAdapter.add("Sakit");
                arrayAdapter.add("Izin");
                arrayAdapter.add("Tanpa keterangan");

                builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String status = arrayAdapter.getItem(which);
                        saveValidasiMasuk(status);
                    }
                });
                builderSingle.show();
            } else saveValidasiMasuk("");
            return true;
        } else if (id == R.id.action_refresh) {
            siswaList.clear();
            getAllSiswa();
        } else if (id == R.id.action_fast) {
            AlertDialog.Builder builder = new AlertDialog.Builder(AbsensiPelajaranActivity.this);
            builder.setMessage("Anda Akan melakukan absen secara cepat...");
            builder.setCancelable(true);
            builder.setPositiveButton("Absen Cepat", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (isKelasTambahan) {
                        fastAbsen(null, idKelasTambahan, true);
                    } else {
                        fastAbsen(idnews, idjadwal, false);
                    }
                }
            });
            builder.setNegativeButton("Batal", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });

            builder.show();
        }else if(id == R.id.action_attachment){
            if (Vira.isConnectedToServer(AbsensiPelajaranActivity.this)) {
                String jadwalId = null;
                String jenis;
                if(isKelasTambahan){
                    jadwalId = idKelasTambahan;
                    jenis = "tambahan";
                }else{
                    jadwalId = idjadwal;
                    jenis = "reguler";
                }

                Intent intent = new Intent(AbsensiPelajaranActivity.this, AttachmentActivity.class);
                intent.putExtra("jadwalId" , Integer.valueOf(jadwalId));
                intent.putExtra("jenis" , jenis);
                startActivity(intent);

            }else{
                Toast.makeText(this, "Anda tidak terhubung ke server,harap cek koneksi anda !", Toast.LENGTH_SHORT).show();
            }

        }

        switch (item.getItemId()) {
            case android.R.id.home:
                goToBack();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void cekStatusWaliKelas() {
        final SessionManager sessionManager = new SessionManager(AbsensiPelajaranActivity.this);
        if (Vira.isConnectedToServer(AbsensiPelajaranActivity.this)) {
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            final ProgressDialog loading = ProgressDialog.show(this, "", "Loading Data...", false, false);

            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            HashMap<String, String> map = new HashMap<>();
            map.put("idguru", sessionManager.preferences.getString("username", null));
            map.put("idkelas", idnews);
            Call<String> call = service.cekStatusWaliKelas(map);

            call.enqueue(new Callback<String>() {

                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    loading.dismiss();
                    if (response.body().equals("ya")) {
                        statusWaliKelas = true;
                    } else {
                        statusWaliKelas = false;
                    }
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
                    t.printStackTrace();
                    loading.dismiss();
                    DatabaseHandler dh = new DatabaseHandler(AbsensiPelajaranActivity.this);
                    statusWaliKelas = dh.cekStatusWaliKelas(AbsensiPelajaranActivity.this, sessionManager.preferences.getString("username", null), idnews);
                }
            });

        } else {
            DatabaseHandler dh = new DatabaseHandler(AbsensiPelajaranActivity.this);
            if(isKelasTambahan){
                statusWaliKelas = dh.cekStatusWaliKelasTambahan(AbsensiPelajaranActivity.this, sessionManager.preferences.getString("username", null), idKelasTambahan);
            }else{
                statusWaliKelas = dh.cekStatusWaliKelas(AbsensiPelajaranActivity.this, sessionManager.preferences.getString("username", null), idnews);
            }
        }
    }

    private ArrayList<Siswa> getAllSiswa() {
        if (Vira.isConnectedToServer(AbsensiPelajaranActivity.this)) {
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            final ProgressDialog loading = ProgressDialog.show(this, "", "Loading Data...", false, false);

            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            HashMap<String, String> map = new HashMap<>();
            Call<List<Siswa>> call;
            if (isKelasTambahan) {
                map.put("jadwalTambahan", idKelasTambahan);
                call = service.daftarMapelAbsensiTambahan(map);
            } else {
                map.put("kelas", idnews);
                map.put("jadwal", idjadwal);
                call = service.daftarMapelAbsensi(map);
            }

            call.enqueue(new Callback<List<Siswa>>() {

                @Override
                public void onResponse(Call<List<Siswa>> call, retrofit2.Response<List<Siswa>> response) {
                    loading.dismiss();
                    siswaList.clear();
                    List<Siswa> listSiswa = response.body();
                    for (Siswa siswa : listSiswa) {
                        siswaList.add(siswa);
                    }
                    adapterSiswa.notifyDataSetChanged();
                }

                @Override
                public void onFailure(Call<List<Siswa>> call, Throwable t) {
//                loading.dismiss();
                    t.printStackTrace();
                    loading.dismiss();
                    DatabaseHandler dh = new DatabaseHandler(AbsensiPelajaranActivity.this);
                    ArrayList<Siswa> temp;
                    temp = dh.getAllSiswa(idnews, idjadwal);
                    for (Siswa i : temp) {
                        siswaList.add(i);
                    }
                    adapterSiswa.notifyDataSetChanged();
                }
            });
            return siswaList;
        } else {
            DatabaseHandler dh = new DatabaseHandler(AbsensiPelajaranActivity.this);
            ArrayList<Siswa> temp;

            if(isKelasTambahan){
                temp = dh.getAllSiswaTambahan(idKelasTambahan);
            }else{
                temp = dh.getAllSiswa(idnews, idjadwal);
            }

            Log.d(TAG, "getAllSiswa: total --> " + temp.size());
            siswaList.clear();
            for (Siswa i : temp) {
                siswaList.add(i);
            }
            adapterSiswa.notifyDataSetChanged();
            return siswaList;
        }
    }

    class AdapterSiswa extends RecyclerView.Adapter<AbsensiPelajaranActivity.AdapterSiswa.MyViewHolder> {
        private ImageLoader imageLoader;
        private Context mContext;
        private ArrayList<Siswa> siswalist;
        //final String[] ketHadir={"-","Hadir","Alpha","Sakit","Izin"};
        private TextView teAbm;


        public AdapterSiswa(Context mContext, ArrayList<Siswa> siswalist) {
            this.mContext = mContext;
            this.siswalist = siswalist;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public TextView no, nis, nama, idpenem, ketMasuk, ketPulang, status;
            public NetworkImageView gmbr;
            public Spinner spinner, spinner2;
            public ImageView abMasuk, abKeluar;


            public MyViewHolder(View itemView) {
                super(itemView);
                no = (TextView) itemView.findViewById(R.id.no);
                nis = (TextView) itemView.findViewById(R.id.nis);
                nama = (TextView) itemView.findViewById(R.id.nm_lengkap);
                status = (TextView) itemView.findViewById(R.id.status);
                idpenem = (TextView) itemView.findViewById(R.id.idpenempatan);
                gmbr = (NetworkImageView) itemView.findViewById(R.id.thumbnail_buku);
                abMasuk = (ImageView) itemView.findViewById(R.id.absenmasuk);
                abKeluar = (ImageView) itemView.findViewById(R.id.absenkeluar);
                ketMasuk = (TextView) itemView.findViewById(R.id.ketMasuk);
                ketPulang = (TextView) itemView.findViewById(R.id.ketKeluar);
                teAbm = (TextView) findViewById(R.id.ketMasuk);
                abKeluar.setVisibility(View.GONE);
                abMasuk.setVisibility(View.GONE);
                ketPulang.setVisibility(View.GONE);
                abMasuk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String idpen = idpenem.getText().toString().trim();
                        if (isKelasTambahan) {
                            Log.d(TAG, "onClick: idjad : " + idKelasTambahan);
                            Log.d(TAG, "onClick: ketMasuk : " + ketMasuk + " ,idpen : " + idpen + " ,idjad : " + idKelasTambahan + " , ketMasuk : " + ketMasuk.getText());
                            pilihKategori(ketMasuk, idpen, idKelasTambahan, ketMasuk.getText().toString(), status.getText().toString());
                        } else {
                            String idjad = idjadwal.toString().trim();
                            Log.d(TAG, "onClick: ketMasuk : " + ketMasuk + " ,idpen : " + idpen + " ,idjad : " + idjad + " , ketMasuk : " + ketMasuk.getText());
                            pilihKategori(ketMasuk, idpen, idjad, ketMasuk.getText().toString(), status.getText().toString());
                        }
                    }
                });

                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent n = new Intent(AbsensiPelajaranActivity.this, DetailSiswaActivity.class);
                        n.putExtra("idnews", nis.getText().toString());
                        n.putExtra("idkelas", idnews);
                        startActivity(n);
                    }
                });
            }
        }

        @Override
        public AbsensiPelajaranActivity.AdapterSiswa.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_siswa, parent, false);
            return new AbsensiPelajaranActivity.AdapterSiswa.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final AbsensiPelajaranActivity.AdapterSiswa.MyViewHolder holder, int position) {
            //Buku buku = bukuList.get(position);
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            holder.no.setText(siswalist.get(position).getNo());
            holder.nis.setText(siswalist.get(position).getNis());
            holder.nama.setText(siswalist.get(position).getNama());
            holder.status.setText(siswalist.get(position).getValid());
            holder.idpenem.setText(siswalist.get(position).getIdpenmpatan());
            holder.ketMasuk.setText(siswalist.get(position).getStatus());

            Log.d(TAG, "onBindViewHolder: is valid : "+holder.status.getText().toString());

            if (holder.status.getText().toString().equals("valid")) {
                holder.abMasuk.setVisibility(View.GONE);
                attachment.setVisible(true);
                if(fastMenu == null){
                    Log.d(TAG, "onBindViewHolder: 111111111111111111");

                }else{
                    Log.d(TAG, "onBindViewHolder: 22222222222222");
                    fastMenu.setVisible(false);
                    validmenu.setVisible(false);
                }

            } else {
                holder.abMasuk.setVisibility(View.VISIBLE);
            }

            if (holder.ketMasuk.getText().equals("hadir")) {
                holder.nama.setTextColor(getResources().getColor(R.color.hitam));
            } else if (holder.ketMasuk.getText().equals("izin")) {
                holder.nama.setTextColor(getResources().getColor(R.color.biru));
            } else if (holder.ketMasuk.getText().equals("sakit")) {
                holder.nama.setTextColor(getResources().getColor(R.color.orange));
            } else if (holder.ketMasuk.getText().equals("alpha")) {
                holder.nama.setTextColor(getResources().getColor(R.color.merah));
            } else {
                holder.nama.setTextColor(getResources().getColor(R.color.input_login));
            }
            imageLoader = MySingleton.getmInstance(mContext).getImageLoader();
            Picasso.with(mContext).load("http://" + ip + "/web_absence/assets/panel/images/" + siswalist.get(position)
                    .getGmbr())
                    .placeholder(R.drawable.siswa)
                    .into(holder.gmbr);
            holder.gmbr.setImageUrl(siswalist.get(position).getGmbr(), imageLoader);
            imageLoader = MySingleton.getmInstance(mContext).getImageLoader();


        }

        @Override
        public int getItemCount() {
            return siswalist.size();
        }
    }

    private void refresh() {
        siswaList.clear();
        getAllSiswa();
    }

    private void pilihKategori(final TextView tv, String idpen, String idjad, String ketmasuk, String status) {
        LayoutInflater layoutInflater = LayoutInflater.from(this);
        View view = layoutInflater.inflate(R.layout.absensi, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        builder.setCancelable(true);
        RecyclerView rckategori = (RecyclerView) view.findViewById(R.id.recycler_absen);
        layoutManager = new LinearLayoutManager(this);
        rckategori.setLayoutManager(layoutManager);
        rckategori.setHasFixedSize(true);
        absen_label = new ArrayList<>();
        absen_label.add("hadir");
        absen_label.add("alpha");
        absen_label.add("izin");
        absen_label.add("sakit");
        RecyclerView.Adapter adapter = new AbsensiPelajaranActivity.adapterSelectAbsen(absen_label, tv, idpen, idjad, ketmasuk, status);
        rckategori.setAdapter(adapter);
        //absenArrayList = getKategori();
        //adapterKat = new AdapterKategori(kategoriArrayList);
        //rckategori.setAdapter(new ArrayAdapter<String>(AbsensiActivity.this, R.layout.spinner_item, ket_absen));
//        dialog.setCancelable(true);
        dialog = builder.create();
        dialog.show();
    }

    class adapterSelectAbsen extends RecyclerView.Adapter<AbsensiPelajaranActivity.adapterSelectAbsen.ViewHolder> {
        //private List<String> labels;
        private ArrayList<String> absen_label;
        private TextView mtextView;
        private String idpen, idjad;
        private String ketmasuk, status;

        public adapterSelectAbsen(ArrayList<String> absen_label, TextView tv, String idpen, String idjad, String ketmasuk, String status) {
            this.absen_label = absen_label;
            mtextView = tv;
            this.idpen = idpen;
            this.idjad = idjad;
            this.ketmasuk = ketmasuk;
            this.status = status;
        }


        @Override
        public AbsensiPelajaranActivity.adapterSelectAbsen.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_absen, parent, false);
            return new AbsensiPelajaranActivity.adapterSelectAbsen.ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final AbsensiPelajaranActivity.adapterSelectAbsen.ViewHolder holder, final int position) {
            final String label = absen_label.get(position);
            holder.textView.setText(label);

            //handling item click event
            holder.textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(holder.textView.getContext(), label, Toast.LENGTH_SHORT).show();
                    mtextView.setText(label);
                    dialog.dismiss();
                    if (isKelasTambahan) {
                        saveAbsen(idpen, idjad, label, status, true);
                    } else {
                        saveAbsen(idpen, idjad, label, status, false);
                    }
                }
            });
        }

        @Override
        public int getItemCount() {
            return absen_label.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView textView, kP;

            public ViewHolder(View itemView) {
                super(itemView);
                textView = (TextView) itemView.findViewById(R.id.itemnya);
                kP = (TextView) findViewById(R.id.ketMasuk);
            }
        }
    }

    private void fastAbsen(final String Kelas, final String Jadwal, final boolean isExtrakulikuler) {
        if (Vira.isConnectedToServer(AbsensiPelajaranActivity.this)) {
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            final ProgressDialog dialog = new ProgressDialog(AbsensiPelajaranActivity.this);
            dialog.setMessage("Sedang Diproses...");
            dialog.setCancelable(false);
            dialog.show();

            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            HashMap<String, String> map = new HashMap<>();
            Call<String> call;
            if (isExtrakulikuler) {
                map.put("jadwalTambahan", Jadwal);
                call = service.hadirTambahan(map);
            } else {
                map.put("kelas", Kelas);
                map.put("jadwal", Jadwal);
                call = service.hadir(map);
            }
            call.enqueue(new Callback<String>() {

                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
                    dialog.dismiss();
                    Log.e("Response", "" + response);
                    siswaList.clear();
                    getAllSiswa();
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
//                loading.dismiss();
                    t.printStackTrace();
                    dialog.dismiss();
                    DatabaseHandler dh = new DatabaseHandler(AbsensiPelajaranActivity.this);
                    if(isExtrakulikuler){
                        dh.fastAbsenTambahan(idKelasTambahan);
                    }else{
                        dh.fastAbsen(getApplicationContext(),Kelas,Jadwal,new Intent());
                    }

                    siswaList.clear();
                    getAllSiswa();
                }
            });
        } else {
            DatabaseHandler dh = new DatabaseHandler(AbsensiPelajaranActivity.this);
            if(isExtrakulikuler){
                dh.fastAbsenTambahan(idKelasTambahan);
            }else{
                dh.fastAbsen(getApplicationContext(),Kelas,Jadwal,new Intent());
            }

            siswaList.clear();
            getAllSiswa();
        }
    }

    private void saveAbsen(final String idpenempatan, final String jadwal, final String ketAbsen, final String status, boolean isKelasTambahan) {
        if (Vira.isConnectedToServer(AbsensiPelajaranActivity.this)) {
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            final ProgressDialog dialog = new ProgressDialog(AbsensiPelajaranActivity.this);
            dialog.setMessage("Sedang Diproses...");
            dialog.setCancelable(false);
            dialog.show();

            GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
            HashMap<String, String> map = new HashMap<>();
            Call<String> call;
            if (isKelasTambahan) {
                map.put("idpenem", idpenempatan);
                map.put("absen", ketAbsen);
                map.put("jadwalTambahan", jadwal);
                call = service.simpanAbsenMapelTambahan(map);
            } else {
                map.put("idpenem", idpenempatan);
                map.put("absen", ketAbsen);
                map.put("jadwal", jadwal);
                map.put("status", status);
                call = service.simpanAbsenMapel(map);
            }

            call.enqueue(new Callback<String>() {

                @Override
                public void onResponse(Call<String> call, retrofit2.Response<String> response) {
//                loading.dismiss();
                    dialog.dismiss();
                    Log.d(TAG, "onResponse: response save absence : " + response);
                    siswaList.clear();
                    getAllSiswa();
                }

                @Override
                public void onFailure(Call<String> call, Throwable t) {
//                loading.dismiss();
                    Log.d(TAG, "onFailure: gagal save data absence");
                    t.printStackTrace();
                    dialog.dismiss();
                    DatabaseHandler dh = new DatabaseHandler(AbsensiPelajaranActivity.this);
                    dh.saveAbsen(idpenempatan, jadwal, ketAbsen, status);
                    siswaList.clear();
                    getAllSiswa();
                }
            });

        } else {
            DatabaseHandler dh = new DatabaseHandler(AbsensiPelajaranActivity.this);
            if(isKelasTambahan){
                Log.d(TAG, "saveAbsen: save kelas tambahan");
                dh.saveAbsenTambahan(idpenempatan,jadwal,ketAbsen);
            }else{
                Log.d(TAG, "saveAbsen: save kelas reguler");
                dh.saveAbsen(idpenempatan, jadwal, ketAbsen, status);
            }

            siswaList.clear();
            getAllSiswa();
        }
    }

    private void saveValidasiMasuk(final String status) {
        if (Vira.isConnectedToServer(AbsensiPelajaranActivity.this)) {
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Apakah anda yakin validasi absen masuk?");
            builder.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    final ProgressDialog dialogo = new ProgressDialog(AbsensiPelajaranActivity.this);
                    dialogo.setMessage("Proses validasi masuk...");
                    dialogo.setCancelable(false);
                    dialogo.show();

                    GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);
                    HashMap<String, String> map = new HashMap<>();
                    Call<String> call;
                    if (isKelasTambahan) {
                        map.put("idJadwalTambahan", idKelasTambahan);
                        call = service.validasiTambahan(map);
                    } else {
                        map.put("kelas", idnews);
                        map.put("jadwal", idjadwal);
                        map.put("status", status);
                        call = service.validasi(map);
                    }

                    call.enqueue(new Callback<String>() {

                        @Override
                        public void onResponse(Call<String> call, retrofit2.Response<String> response) {
//                loading.dismiss();
                            dialogo.dismiss();

                            if (response.body().equals("error")) {
                                AlertDialog.Builder builder = new AlertDialog.Builder(AbsensiPelajaranActivity.this);
                                builder.setMessage("Gagal Validasi, periksa siswa yang belum absen...");
                                builder.setCancelable(true);
                                builder.setPositiveButton("Selesai", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                                                Intent n = new Intent(getIntent());
//                                                startActivity(n);
                                        siswaList.clear();
                                        getAllSiswa();

                                    }
                                });

                                builder.show();

                            } else {
                                AlertDialog.Builder builder = new AlertDialog.Builder(AbsensiPelajaranActivity.this);
                                builder.setMessage("Berhasil Validasi...");
                                builder.setCancelable(true);
                                builder.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
//                                                Intent n = new Intent(getIntent());
//                                                startActivity(n);
                                        siswaList.clear();
                                        getAllSiswa();
                                    }
                                });
                                builder.show();
                            }
                        }

                        @Override
                        public void onFailure(Call<String> call, Throwable t) {
//                loading.dismiss();
                            t.printStackTrace();
                            dialogo.dismiss();
                            if (!isMockSettingsON(AbsensiPelajaranActivity.this)) {
                                DatabaseHandler dh = new DatabaseHandler(AbsensiPelajaranActivity.this);
                                Location lokasi = dh.getLocation(AbsensiPelajaranActivity.this);
                                double radius = dh.getRadius(AbsensiPelajaranActivity.this);

                                Location lokasiSekarang = new Location("");
                                lokasiSekarang.setLatitude(lat);
                                lokasiSekarang.setLongitude(lng);

                                double distance = Math.sqrt((lokasi.getLatitude() - lokasiSekarang.getLatitude()) * (lokasi.getLatitude() - lokasiSekarang.getLatitude()) + (lokasi.getLongitude() - lokasiSekarang.getLongitude()) * (lokasi.getLongitude() - lokasiSekarang.getLongitude()));

                                int ret = Double.compare(distance, radius);


                                Toast.makeText(AbsensiPelajaranActivity.this, String.valueOf("Jarak sebesar : " + ret + " dan radius : " + String.valueOf(radius) + " lokasi sekarang : " + lokasiSekarang.getLatitude() + " " + lokasiSekarang.getLongitude()), Toast.LENGTH_SHORT).show();

                                if (ret <= 0) {

                                    String response = dh.saveValidasiMasuk(idnews, idjadwal, status);
                                    if (response.equals("error")) {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(AbsensiPelajaranActivity.this);
                                        builder.setMessage("Gagal Validasi, periksa siswa yang belum absen...");
                                        builder.setCancelable(true);
                                        builder.setPositiveButton("Selesai", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
//                                                        Intent n = new Intent(getIntent());
//                                                        startActivity(n);
                                                siswaList.clear();
                                                getAllSiswa();

                                            }
                                        });

                                        builder.show();

                                    } else if (response.equals("waktu")) {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(AbsensiPelajaranActivity.this);
                                        builder.setMessage("Gagal Validasi, belum waktunya absen");
                                        builder.setCancelable(true);
                                        builder.setPositiveButton("Selesai", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
//                                                        Intent n = new Intent(getIntent());
//                                                        startActivity(n);
                                                siswaList.clear();
                                                getAllSiswa();

                                            }
                                        });

                                        builder.show();
                                    } else {
                                        AlertDialog.Builder builder = new AlertDialog.Builder(AbsensiPelajaranActivity.this);
                                        builder.setMessage("Berhasil Validasi...");
                                        builder.setCancelable(true);
                                        builder.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
//                                                        Intent n = new Intent(getIntent());
//                                                        startActivity(n);
                                                siswaList.clear();
                                                getAllSiswa();
                                            }
                                        });
                                        builder.show();
                                    }
                                } else {
                                    Toast.makeText(AbsensiPelajaranActivity.this, "Anda tidak berada di lokasi sekolah", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                    });

                }
            });
            builder.setNegativeButton("Tidak", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            builder.show();
        } else {
            if (!isMockSettingsON(AbsensiPelajaranActivity.this)) {
                DatabaseHandler dh = new DatabaseHandler(AbsensiPelajaranActivity.this);
                Location lokasi = dh.getLocation(AbsensiPelajaranActivity.this);
                double radius = dh.getRadius(AbsensiPelajaranActivity.this);

                Location lokasiSekarang = new Location("");
                lokasiSekarang.setLatitude(lat);
                lokasiSekarang.setLongitude(lng);

                double distance = Math.sqrt((lokasi.getLatitude() - lokasiSekarang.getLatitude()) * (lokasi.getLatitude() - lokasiSekarang.getLatitude()) + (lokasi.getLongitude() - lokasiSekarang.getLongitude()) * (lokasi.getLongitude() - lokasiSekarang.getLongitude()));

                int ret = Double.compare(distance, radius);
                Log.d(TAG, "calculation: ret : " + ret);
                Toast.makeText(AbsensiPelajaranActivity.this, String.valueOf("Jarak sebesar : " + distance + " dan radius : " + String.valueOf(radius) + " lokasi sekarang : " + lokasiSekarang.getLatitude() + " " + lokasiSekarang.getLongitude()), Toast.LENGTH_SHORT).show();

                if (ret <= 0) {
                    String response;
                    if (isKelasTambahan) {
                        response = dh.saveValidasiMasukTambahan(idKelasTambahan);
                    } else {
                        response = dh.saveValidasiMasuk(idnews, idjadwal, status);
                    }

                    Log.d(TAG, "response: ret : " + response);
                    if (response.equals("error")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(AbsensiPelajaranActivity.this);
                        builder.setMessage("Gagal Validasi, periksa siswa yang belum absen...");
                        builder.setCancelable(true);
                        builder.setPositiveButton("Selesai", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                Intent n = new Intent(getIntent());
//                                startActivity(n);
                                siswaList.clear();
                                getAllSiswa();

                            }
                        });

                        builder.show();

                    } else if (response.equals("waktu")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(AbsensiPelajaranActivity.this);
                        builder.setMessage("Gagal Validasi, belum waktunya absen");
                        builder.setCancelable(true);
                        builder.setPositiveButton("Selesai", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                Intent n = new Intent(getIntent());
//                                startActivity(n);
                                siswaList.clear();
                                getAllSiswa();

                            }
                        });

                        builder.show();
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(AbsensiPelajaranActivity.this);
                        builder.setMessage("Berhasil Validasi...");
                        builder.setCancelable(true);
                        builder.setPositiveButton("Oke", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
//                                Intent n = new Intent(getIntent());
//                                startActivity(n);
                                siswaList.clear();
                                getAllSiswa();
                            }
                        });
                        builder.show();
                    }
                } else {
                    Toast.makeText(AbsensiPelajaranActivity.this, "Anda tidak berada di lokasi sekolah", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(locationListenerGPS);
        locationManager.removeUpdates(locationListenerNetwork);
    }

    @Override
    protected void onStop() {
        super.onStop();
        locationManager.removeUpdates(locationListenerGPS);
        locationManager.removeUpdates(locationListenerNetwork);
    }

    public boolean isMockSettingsON(Context context) {
        // returns true if mock location enabled, false if not enabled.
        if (Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ALLOW_MOCK_LOCATION).equals("0"))
            return false;
        else {
            Toast.makeText(context, "Jangan menggunakan aplikasi seperti fake gps", Toast.LENGTH_LONG).show();
            return true;
        }
    }
}
