package alriyat.absensielektronik.activity;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.model.GuruOffline;
import alriyat.absensielektronik.model.NonGuruOffline;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import retrofit2.Call;
import retrofit2.Callback;

public class PilihNonGuruActivity extends AppCompatActivity {

    private NonGuruAdapter nonGuruAdapter;
    private RecyclerView rvRecyclerView;
    private ArrayList<NonGuruOffline> listNonGuru;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pilih_non_guru);

        rvRecyclerView = findViewById(R.id.recycler_view);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Pilih Non Guru");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        listNonGuru = new ArrayList<>();

        nonGuruAdapter = new NonGuruAdapter(listNonGuru);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvRecyclerView.setLayoutManager(mLayoutManager);
        rvRecyclerView.setItemAnimator(new DefaultItemAnimator());
        rvRecyclerView.setAdapter(nonGuruAdapter);

        getNonGuru();
    }

    private void getNonGuru() {
        SessionConfig sessionConfig = new SessionConfig(PilihNonGuruActivity.this);
        GetDataService service = RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

        Map<String, String> param = new HashMap<>();
        param.put("id_non_guru", "");

        Call<List<NonGuruOffline>> call = service.nonGuru(param);
        call.enqueue(new Callback<List<NonGuruOffline>>() {
            @Override
            public void onResponse(Call<List<NonGuruOffline>>call, retrofit2.Response<List<NonGuruOffline>> response) {
//                    loading.dismiss();
                List<NonGuruOffline> listNonGuruOffline = response.body();

                listNonGuru.clear();

                for (NonGuruOffline nonGuruOffline : listNonGuruOffline) {
                    listNonGuru.add(nonGuruOffline);
                }

                nonGuruAdapter.notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<List<NonGuruOffline>>call, Throwable t) {
                // Log error here since request failed
//                    loading.dismiss();
                t.printStackTrace();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
}
class NonGuruAdapter extends RecyclerView.Adapter<NonGuruAdapter.MyViewHolder> {

    private List<NonGuruOffline> listNonGuru;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView idNonGuru, nip, nama;

        public MyViewHolder(final View view) {
            super(view);
            idNonGuru = view.findViewById(R.id.id_non_guru);
            nip = view.findViewById(R.id.nip);
            nama = view.findViewById(R.id.nama);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences sharedPreferences = view.getContext().getSharedPreferences("rekap_kehadiran_non_guru", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("id_non_guru", idNonGuru.getText().toString());
                    editor.putString("nama", nama.getText().toString());
                    editor.apply();

                    ((PilihNonGuruActivity) view.getContext()).onBackPressed();
                }
            });
        }
    }


    public NonGuruAdapter(List<NonGuruOffline> listNonGuru) {
        this.listNonGuru = listNonGuru;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_non_guru, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NonGuruOffline nonGuru = listNonGuru.get(position);
        holder.idNonGuru.setText(nonGuru.getIdNonGuru());
        holder.nip.setText(nonGuru.getNip());
        holder.nama.setText(nonGuru.getNama());
    }

    @Override
    public int getItemCount() {
        return listNonGuru.size();
    }
}