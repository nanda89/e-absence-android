package alriyat.absensielektronik.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import alriyat.absensielektronik.R;
import alriyat.absensielektronik.app.Config;
import alriyat.absensielektronik.app.MySingleton;
import alriyat.absensielektronik.helper.SessionConfig;
import alriyat.absensielektronik.helper.SessionManager;
import alriyat.absensielektronik.model.Siswa;
import alriyat.absensielektronik.util.DatabaseHandler;
import alriyat.absensielektronik.util.GetDataService;
import alriyat.absensielektronik.util.RetrofitClientInstance;
import alriyat.absensielektronik.util.Vira;
import retrofit2.Call;
import retrofit2.Callback;

public class DataSiswaActivity extends AppCompatActivity {

    private ArrayList<Siswa> siswaList;
    private RecyclerView recyclerView;
    private DataSiswaActivity.AdapterSiswa adapterSiswa;
    private RecyclerView.LayoutManager layoutManager;
    private SessionConfig sessionConfig;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_siswa);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar_daftar);
        toolbar.setTitle("Daftar Siswa");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        siswaList = new ArrayList<>();
        recyclerView = (RecyclerView)findViewById(R.id.recycler_siswa);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

        sessionConfig = new SessionConfig(this);
        sessionManager = new SessionManager(this);


        adapterSiswa = new DataSiswaActivity.AdapterSiswa(this,siswaList);
        recyclerView.setAdapter(adapterSiswa);
        siswaList = getAllSiswa();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.refresh, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private ArrayList<Siswa> getAllSiswa(){
        if (Vira.isConnectedToServer(DataSiswaActivity.this)) {
            final String ip = sessionConfig.preferences.getString("ipconfig", null);
            final ProgressDialog loading = ProgressDialog.show(this, "", "Loading Data...", false, false);

            GetDataService service =
                    RetrofitClientInstance.getRetrofitInstance(sessionConfig).create(GetDataService.class);

            HashMap<String, String> param = new HashMap<>();
            param.put("guru", sessionManager.preferences.getString("username", null));

            Call<List<Siswa>> call = service.ambilDataSiswa(param);
            call.enqueue(new Callback<List<Siswa>>() {
                @Override
                public void onResponse(Call<List<Siswa>>call, retrofit2.Response<List<Siswa>> response) {
                    loading.dismiss();
                    Log.e("URL", "kawan" + ip + Config.DAFTAR_ABSENSI);

                    siswaList.clear();

                    List<Siswa> listSiswa = response.body();

                    for (Siswa siswa : listSiswa) {
                        siswaList.add(siswa);
                    }

                    adapterSiswa.notifyDataSetChanged();
                }

                @Override
                public void onFailure(Call<List<Siswa>>call, Throwable t) {
                    // Log error here since request failed
                    loading.dismiss();
                    DatabaseHandler dh = new DatabaseHandler(DataSiswaActivity.this);
                    ArrayList<Siswa> temp = new ArrayList<>();
                    temp = dh.getDataSiswa(DataSiswaActivity.this, sessionManager.preferences.getString("username", null));
                    for (Siswa i : temp) {
                        siswaList.add(i);
                    }
                    adapterSiswa.notifyDataSetChanged();
                }
            });

            return siswaList;
        }
        else {
            DatabaseHandler dh = new DatabaseHandler(DataSiswaActivity.this);
            ArrayList<Siswa> temp = new ArrayList<>();
            temp = dh.getDataSiswa(DataSiswaActivity.this, sessionManager.preferences.getString("username", null));
            for (Siswa i : temp) {
                siswaList.add(i);
            }
            adapterSiswa.notifyDataSetChanged();
            return siswaList;
        }
    }

    class AdapterSiswa extends RecyclerView.Adapter<DataSiswaActivity.AdapterSiswa.MyViewHolder>{
        private ImageLoader imageLoader;
        private Context mContext;
        private ArrayList<Siswa> siswalist;
        //final String[] ketHadir={"-","Hadir","Alpha","Sakit","Izin"};
        private TextView teAbm;


        public AdapterSiswa(Context mContext, ArrayList<Siswa> siswalist) {
            this.mContext = mContext;
            this.siswalist = siswalist;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder{
            public TextView no,nis,nama,idpenem,ketMasuk,ketPulang,status;
            public NetworkImageView gmbr;



            public MyViewHolder(View itemView) {
                super(itemView);
                no = (TextView)itemView.findViewById(R.id.no);
                nis = (TextView)itemView.findViewById(R.id.nis);
                nama = (TextView)itemView.findViewById(R.id.nm_lengkap);
                idpenem = (TextView)itemView.findViewById(R.id.idpenempatan);
                gmbr = (NetworkImageView)itemView.findViewById(R.id.thumbnail_buku);

                itemView.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View v) {

                        Intent n = new Intent(DataSiswaActivity.this, DetailSiswaActivity.class);
                        n.putExtra("idnews", nis.getText().toString());
                        n.putExtra("idkelas", siswalist.get(getPosition()).getIdkelas());
                        startActivity(n);

                    }
                });


            }
        }

        @Override
        public DataSiswaActivity.AdapterSiswa.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_data_siswa,parent,false);
            return new DataSiswaActivity.AdapterSiswa.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final DataSiswaActivity.AdapterSiswa.MyViewHolder holder, int position) {
            //Buku buku = bukuList.get(position);
            final String ip = sessionConfig.preferences.getString("ipconfig",null);
            holder.no.setText(siswalist.get(position).getNo());
            holder.nis.setText(siswalist.get(position).getNis());
            holder.nama.setText(siswalist.get(position).getNama());
            holder.idpenem.setText(siswalist.get(position).getIdpenmpatan());

            imageLoader = MySingleton.getmInstance(mContext).getImageLoader();
            Picasso.with(mContext).load("http://"+ip+"/web_absence/assets/panel/images/" + siswalist.get(position)
                    .getGmbr())
                    .placeholder(R.drawable.siswa)
                    .into(holder.gmbr);
            holder.gmbr.setImageUrl(siswalist.get(position).getGmbr(), imageLoader);
            imageLoader = MySingleton.getmInstance(mContext).getImageLoader();


        }

        @Override
        public int getItemCount() {
            return siswalist.size();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_refresh){
            Intent a = new Intent(getIntent());
            startActivity(a);
        }

        switch (item.getItemId()){
            case android.R.id.home:
                super.onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        siswaList = getAllSiswa();
    }
}
